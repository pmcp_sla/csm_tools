#!/bin/bash
# Generates SOAP csv, flats the swagger and then generates flat swagger csv
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Flat Swagger and CSV for SOAP and REST====================================="
echo ${restCommonServiceName}
${scriptLocation}/csvSOAPVBOGenerate.sh
read -p "SOAP csv complete! Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
${scriptLocation}/swaggerFlatten.sh
read -p "REST swagger flatten complete! Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
${scriptLocation}/csvFlatSwaggerGenerate.sh

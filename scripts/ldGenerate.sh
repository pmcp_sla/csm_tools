#!/bin/bash
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Excel====================================="
echo ${restCommonServiceName}
cd ${GENERATE_JSONLD_TOOL_LOCATION}
echo "Generate JSON LD"
# Create soft links
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
node ./index.js -l ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json
# Format the Json
jq . ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_JSON_LD.json > ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_JSON_LD2.json
# Move generated file
mv ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_JSON_LD2.json ${GENERATE_JSONLD_TOOL_LOCATION}/Generated/${VBOName}_JSON_LD.json
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json

#!/bin/bash
#Change these variables for each package
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================OPERATION CONSISTENCY CHECKS=====================================\e[0m"
cd ${OPERATION_VALIDATE_LOCATION}
/bin/echo -e  "Comparing \t${restVBSPath}/${restFileName}.swagger"
/bin/echo -e  "\t\t${restVBSPath}/${restFileName}.wadl "
/bin/echo -e  "\t\t${soapVBSPath}/${wsdlFileName}.wsdl"
node ./index.js -o 	${servicesLocation}/${soapServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger \
					${servicesLocation}/${soapServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl \
					${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl



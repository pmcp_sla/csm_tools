#!/bin/bash
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================GENERATE FROM FLAT SWAGGER=====================================\e[0m"
echo ${serviceVBOName} 
# Remove old generated output
rm -rf ${REST_TOOL_LOCATION}/Output
# Create soft links
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
#cp ${technicalLocation}/swagger-codegen-cli.jar ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/swagger-codegen-cli-2.2.3.jar
#cp /home/pmcparland/Documents/CSM_Interfaces/Technical/CurrentValidateRest/swagger-codegen-cli-2.4.8.jar ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/swagger-codegen-cli-2.2.3.jar

# Goto tools location and run checks 
#	NOTE THAT THIS IS NOT THE VERSION UNDER SLA_TOOLS; this is a copy of the wadlValidateRest_1.2.4 source
#	A copy is used so none of the generated files end up in git
cd ${REST_TOOL_LOCATION}
# Rename flat swagger file if it exists
if [ -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger ]; then
	/bin/echo -e "\e[1;34mGenerate from  ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger\e[0m"
	node ${gitScriptLocation}/CSMUtilities/index.js -g ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
	#node ${REST_TOOL_LOCATION}/index -g ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
	#node ${original}/index -g ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
else 
	/bin/echo -e "\e[1;34mFlat Swagger not found\e[0m"
fi
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json
#rm -rf ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/swagger-codegen-cli-2.2.3.jar
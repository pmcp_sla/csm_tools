#!/bin/bash
#Check SOAP, JSON and Swagger files for a package
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
cd ${REST_TOOL_LOCATION}
/bin/echo -e "\e[1;34mVBO Service ${serviceVBOName}\e[0m"
/bin/echo -e "\e[1;34mVBS Service ${serviceVBSName}\e[0m"
/bin/echo -e "\e[1;34mSOAP Version ${SOAP_Version}\e[0m"
/bin/echo -e "\e[1;34mREST Version ${REST_Version}\e[0m"
${scriptLocation}/setUpValidateSoap.sh
${scriptLocation}/wsdlCheck.sh
read -p "SOAP checks complete! Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
${scriptLocation}/wadlCheck.sh
read -p "WADL checks complete! Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
${scriptLocation}/swaggerCheck.sh
${scriptLocation}/flatSwagValidate.sh
read -p "Swagger checks complete! Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
${scriptLocation}/operationConsistencyCheck.sh
${scriptLocation}/csvCheck.sh
read -p "REST Operation checks complete! Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
#${scriptLocation}/setUpValidateRest.sh
echo "Run csmutilities -R from GitHub directory!!!!!!!"
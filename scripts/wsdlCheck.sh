#!/bin/bash
#Change these variables for each package
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
#remove current directories
cd ${technicalLocation}/ValidateSoap
rm -rf VBO
rm -rf VBS
echo  "Copy SOAP files"
# Confirm Service location for each new package - may not need to change
/bin/cp -rf ${servicesLocation}/${soapServicePath}/SOAP/VBO VBO
/bin/cp -rf ${servicesLocation}/${soapServicePath}/SOAP/VBS VBS
echo "Create Common soft links"
# Confirm Common location? May not need to change
echo ${SOAP_COMMON}
cp  -a ${SOAP_COMMON} ${technicalLocation}/ValidateSoap/VBO/Common
cp  -a ${SOAP_COMMON} ${technicalLocation}/ValidateSoap/VBO/${serviceGroup}/Common
echo
/bin/echo -e "\e[1;34m=======================SOAP SERVICE WSDL CHECKS=====================================\e[0m"
cd ${WSDL_VALIDATE_LOCATION}
echo "Validate WSDL ${soapVBSPath}/${wsdlFileName}.wsdl"
node ./index.js -v ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
if [ -e ${technicalLocation}/ValidateSoap/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl ]
then
	node ./index.js -v ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
fi
/bin/echo -e "\e[1;34m=======================SOAP SERVICE WSDL CHECK USING APACHE CXF=====================================\e[0m"
#Installed from http://cxf.apache.org/download.html 
${technicalLocation}/appache-cxf/apache-cxf-3.2.7/bin/wsdlvalidator -V ${technicalLocation}/ValidateSoap/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
if [ -e ${technicalLocation}/ValidateSoap/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl ]
then
    ${technicalLocation}/appache-cxf/apache-cxf-3.2.7/bin/wsdlvalidator -V ${technicalLocation}/ValidateSoap/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
fi


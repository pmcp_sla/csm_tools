#!/bin/bash
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================REST SERVICE VALIDATION=====================================\e[0m"
echo ${serviceVBOName} 
/bin/echo -e "\e[1;  RELEASE USING  - \e[0m"
/bin/echo -e "\e[1;34m   ${REST_TOOL_LOCATION}\e[0m"
# Remove old generated output
rm -rf ${REST_TOOL_LOCATION}/Output
# Create soft links
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/${serviceVBSName}/Common
# Rename flat swagger file if it exists
if [ -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger ]; then
    mv ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger.OLD
fi
# Goto tools location and run checks 
#	NOTE THAT THIS IS NOT THE VERSION UNDER SLA_TOOLS; this is a copy of the wadlValidateRest_1.2.4 source
#	A copy is used so none of the generated files end up in git
cd ${REST_TOOL_LOCATION}
/bin/echo -e "\e[1;34mVALIDATE ${VBOName}.json\e[0m"
node ./index -a ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json
/bin/echo -e "\e[1;34mVALIDATE ${restFileName}.swagger\e[0m"
node ./index -s ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
/bin/echo -e "\e[1;34mVALIDATE Examples\e[0m"
node ./index -v ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/Examples/ \
   ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json
read -p "Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	${scriptLocation}/tidyUp.sh
	if [ -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger.OLD ]; then
	    mv ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger.OLD ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
	fi
else 
	/bin/echo -e "\e[1;34mVALIDATE Package\e[0m"
	# Assumes that swagger-codegen-cli.jar is in the right location
	node ./index -p ${servicesLocation}/${restServicePath}
fi
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/${serviceVBSName}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json
if [ -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger.OLD ]; then
    mv ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger.OLD ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
fi
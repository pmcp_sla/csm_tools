#!/bin/bash
#Declarations for scripts
# Below is the path from the Services directory to the specific service being developed; for a few services serviceVBOName and serviceVBSName will differ
export soapServicePath=CustomerAccount #Used in VBO directory for the schema names
export serviceVBSName=CustomerAccount #Used in VBS directory for wsdl, wadl and swagger files
export serviceGroup=Customer
export SOAP_Version=1.21.0
export REST_Version=2.12.0
# These declaration will need changed occasionally 
export servicesLocation=${HOME}/Documents/GitHub 		# Base path
export technicalLocation=${HOME}/Documents/CSM_Interfaces/Technical 	# Base path
export gitScriptLocation=${HOME}/Documents/GitHub/Scripts/ 		# Base path for Vodafone CSM version of scripts
export scriptLocation=${technicalLocation}/SLA_Tools/Scripts    # Base path for my local copy of scripts
export original=${technicalLocation}/SLA_Tools/originalMaster/ValidateRest
export SOAP_COMMON=${servicesLocation}/CommonSchemas/SOAP/VBO/Common # This variable is referenced by js code for generators
export REST_COMMON=${servicesLocation}/CommonSchemas/REST/VBO/Common # This variable is referenced by js code for generators
export REST_TOOL_LOCATION=${technicalLocation}/SLA_Tools/wadlValidateRest_1.2.4
export VERSION_TOOL_LOCATION=${technicalLocation}/SLA_Tools/version
export GENERATE_CSV_TOOL_LOCATION=${technicalLocation}/SLA_Tools/generateCSV
export GENERATE_JSONLD_TOOL_LOCATION=${technicalLocation}/SLA_Tools/generateJSON_LD
export GENERATE_SWAGGER_TOOL_LOCATION=${technicalLocation}/SLA_Tools/generateSwaggerImpl
export GENERATE_EXAMPLE_TOOL_LOCATION=${technicalLocation}/SLA_Tools/generateExamples
export UPDATE_SWAGGER_TOOL_LOCATION=${technicalLocation}/SLA_Tools/updateSwagImpl
export WSDL_VALIDATE_LOCATION=${technicalLocation}/SLA_Tools/wsdlValidate
export OPERATION_VALIDATE_LOCATION=${technicalLocation}/SLA_Tools/operationConsistencyCheck
export FLATTEN_SWAGGER_LOCATION=${technicalLocation}/SLA_Tools/generateFlatSwagger
export VALIDATEREST_TOOL_LOCATION=${gitScriptLocation}/CSMUtilities
export soapVersion=${SOAP_Version:0:1}
export restVersion=${REST_Version:0:1}
export serviceVBOName=${soapServicePath} #Occassionality this will need hardcode e.g CheckServiceFeasibility, PortContact, CheckCustomerCreditRating
#No need to change the paths below ----------------------------
#SOAP DATA
export soapVBSPath=${serviceGroup}/${serviceVBSName}/V${soapVersion}
export soapVBOPath=${serviceGroup}/${serviceVBOName}/V${soapVersion}
export xsdFileName=${serviceVBOName}VBM # Occasionally these will need hardcoded in e.g. PaymentVBM, ServiceFeasibilityVBM
export xsdVBOFileName=${serviceVBOName}VBO # Occasionally these will need hardcoded in e.g. PaymentVBO, CheckServiceFeasibility, PortContact
export wsdlFileName=${serviceVBSName}VBS # Occasionally these will need hardcoded in e.g. ProcessPaymentVoucherVBS, CustomerPermissionsVBS
#REST DATA
export restServicePath=${soapServicePath}
export restVBSPath=${serviceGroup}/${serviceVBSName}/V${restVersion}
export restVBOPath=${serviceGroup}/${serviceVBOName}/V${restVersion}
export restFileName=${serviceVBSName}VBS # Occasionally these will need hardcode in e.g. ProcessPaymentVoucherVBS
export VBOName=${serviceVBOName}VBO # Occasionally these will need hardcode in e.g. PaymentVBO, 

#!/bin/bash
# Validates a single example against a report - takes the example file as a parameter
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
# Goto tools location and run checks
cd ${REST_TOOL_LOCATION}
/bin/echo -e "\e[1;34mVALIDATE  ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/Examples/${1}  against ${REST_COMMON}/ReportRequest.json \e[0m"
ajv  -s  ${REST_COMMON}/V1/ReportRequest.json -d ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/Examples/${1} \
	-r ${REST_COMMON}/V1/Links.json

#!/bin/bash
#Test script for the csv tools to generate lots of them from the GitHub directory 
# - will not work for Json which have different Schema and message names - for those version you need to use the -N version
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Excel====================================="
cd ${servicesLocation}
for service in ${servicesLocation}/*; do
	VBOName="${service##*/}"
	echo "${VBOName}"
	if [ ${VBOName} == Scripts ] || [ ${VBOName} == CommonSchemas ]
	then :
	else 
		#SOAP
		cd ${service}/SOAP/VBO/
		cd */
		cd */
		cd */
		# Should be at the SOAP VBO directory
		# Create soft links
		cp  -a  ${SOAP_COMMON} ../Common
		cp  -a  ${SOAP_COMMON} ../../Common
		cp  -a  ${SOAP_COMMON} ../../../Common
		node ${gitScriptLocation}/CSMUtilities/index.js -S ${PWD}/${VBOName}VBO.xsd
		# Remove soft links
		rm -rf   ../Common
		rm -rf   ../../Common
		rm -rf   ../../../Common
		cd ../../../../../..
		#REST
		echo ${service}/REST
		if [[ -d ${service}/REST ]]
		then
			cd ${service}/REST/VBO/
			cd */
			cd */
			cd */
			# Should be at the REST VBO directory
			# Create soft links
			cp  -a  ${REST_COMMON} ../Common
			cp  -a  ${REST_COMMON} ../../Common
			cp  -a  ${REST_COMMON} ../../../Common
			cd ../../../../../..
			cd ${service}/REST/VBS/
			cd */
			cd */
			cd */
			node ${gitScriptLocation}/CSMUtilities/index.js -J ${PWD}/${VBOName}VBS.flat.swagger
			# Remove soft links
			cd ../../../../../..
			cd ${service}/REST/VBO/
			cd */
			cd */
			cd */
			rm -rf   ../Common
			rm -rf   ../../Common
			rm -rf   ../../../Common
		fi
		read -p "Do you wish to continue (Y/N)? " -n 1 -r
		echo
		if [[ ! $REPLY =~ ^[Yy]$ ]]
		then
			exit 1
		fi
	fi
done


#!/bin/bash
#Change these variables for each package
#call declarations
echo "DEPRECATED!!!!!"
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================REST SERVICE SWAGGER CHECKS=====================================\e[0m"
echo ${restCommonServiceName}
cd ${gitScriptLocation}/CSMUtilities
echo "Create Common soft links"
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
echo "Validate Swagger ${restVBSPath}/${restFileName}.swagger"
node ./index.js -o ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
# Remove common links
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json
/bin/echo -e "\e[1;34m=======================REST SERVICE SYNTAX CHECKS=====================================\e[0m"
if grep -i -q ${REST_Version} ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl; then
  echo "WADL Version matches " ${REST_Version}
else
	/bin/echo -e "\e[1;31mWADL Versions do not match!!!!\e[0m" ${REST_Version} "Not found!"
	echo "Found" 
	grep -i 'version>' ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl
fi
if grep -i -q ${REST_Version} ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger; then
  echo "Swagger Version matches " ${REST_Version}
else
	/bin/echo -e "\e[1;31mSwagger Versions do not match!!!!\e[0m" ${REST_Version} "Not found!"
	echo "Found" 
	grep -i 'version\"' ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
fi
if [ ! -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger ]; then
    /bin/echo -e "\e[1;31mFile not found! ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger\e[0m"
else
	if grep -i -q ${REST_Version} ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger; then
	  echo "Swagger flat Version matches " ${REST_Version}
	else
		/bin/echo -e "\e[1;31mSwagger Versions do not match on flat file!!!!\e[0m" ${REST_Version} "Not found!"
		echo "Found" 
		grep -i 'version\"' ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
	fi
	if grep -i -q "BasicComponents" ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger; then
	  echo "Flat swagger contains Basic Components " 
	else
		/bin/echo -e "\e[1;31mBasic Components not found in flat swagger!\e[0m"
	fi
fi




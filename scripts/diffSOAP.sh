#!/bin/bash
#Diff command line tool - probably not needed anymore as can see this via Git
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================SOAP SERVICE DIFF====================================="
echo ${serviceVBOName} 
/bin/echo -e "\e[1;34mDIFF ${xsdVBOFileName}.xsd\e[0m"
#remove current directories
cd ${technicalLocation}/ValidateSoap
rm -rf VBO
rm -rf VBS
echo  "Copy SOAP files"
# Confirm Service location for each new package - may not need to change
/bin/cp -rf ${servicesLocation}/${soapServicePath}/SOAP/VBO VBO
/bin/cp -rf ${servicesLocation}/${soapServicePath}/SOAP/VBS VBS
/bin/echo -e "\e[1;34mCURRENT ${xsdVBOFileName}.xsd   \t\t\t\t\t OLD  ${xsdFileName}.xsd \e[0m"
sdiff -bBWs ${technicalLocation}/ValidateSoap/VBO/${soapVBOPath}/${xsdVBOFileName}.xsd ${technicalLocation}/ValidateSoap/oldSoapFiles/${xsdVBOFileName}.xsd



#!/bin/bash
#Generate examples for JSON and SOAP schemas - useful when adding new services or new components within a service.
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate JSON Example====================================="
echo ${restCommonServiceName}
cd ${GENERATE_EXAMPLE_TOOL_LOCATION}
echo "Generate JSON Example"
# Create soft links
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
node ./index.js -j ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json
# Format the Json
jq . ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_EXAMPLE.json > ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_EXAMPLE2.json
#Overwrite and then delete formatted file
mv -f ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_EXAMPLE2.json ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_EXAMPLE.json
rm -f ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_EXAMPLE2.json
echo "Generate SOAP Example"
node ./index.js -x ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}.xsd -e ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/Extension/Extended${xsdVBOFileName}.xsd
# Format the resulting XML
xmllint --format   ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}_EXAMPLE.xml  >  ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}_EXAMPLE2.xml
#Overwrite and then delete formatted file
mv -f ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}_EXAMPLE2.xml ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}_EXAMPLE.xml
rm -f ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}_EXAMPLE2.xml
# Move generated file
mv ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_EXAMPLE.json ${GENERATE_EXAMPLE_TOOL_LOCATION}/Generated
mv ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}_EXAMPLE.xml ${GENERATE_EXAMPLE_TOOL_LOCATION}/Generated
#mv ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}_EXAMPLE.xml ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/Examples
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json

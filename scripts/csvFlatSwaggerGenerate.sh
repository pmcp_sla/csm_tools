#!/bin/bash
# Generates a flat swagger file from the current Swagger file in a package - uses Vodafone CSM Utilies
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Excel====================================="
/bin/echo -e "\e[1;34mVBO Service ${serviceVBOName}\e[0m"
/bin/echo -e "\e[1;34mVBS Service ${serviceVBSName}\e[0m"
/bin/echo -e "\e[1;34mSOAP Version ${SOAP_Version}\e[0m"
/bin/echo -e "\e[1;34mREST Version ${REST_Version}\e[0m"
cd ${VALIDATEREST_TOOL_LOCATION}
echo "Generate CSV "
# Create soft links
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
echo "${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger"
node ./index.js -J ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger \
	-N ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json # Had to hard code CodelistVBO
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json

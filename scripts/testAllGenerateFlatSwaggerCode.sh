#!/bin/bash
#Test script for the Swagger checks 
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Test Swagger====================================="
cd ${servicesLocation}
for service in ${servicesLocation}/*; do
	VBOName="${service##*/}"
	echo "${VBOName}"
	if [ ${VBOName} == Scripts ] || [ ${VBOName} == CommonSchemas ]
	then :
	else 
		#REST
		if [[ -d ${service}/REST ]]
		then
			cd ${service}/REST/VBS/
			cd */
			cd */
			cd */
			echo ${PWD}
			if [ -f ${VBOName}VBS.flat.swagger ]; then	
				#mkdir Output
				cp  -a  ${REST_COMMON} ../../../../../REST/VBO/Common
				cp  -a  ${REST_COMMON} ../../../../../REST/VBO/Resource/Common
				cp  ${technicalLocation}/swagger-codegen-cli.jar swagger-codegen-cli-2.2.3.jar
				node ${gitScriptLocation}/CSMUtilities/index.js -g ${VBOName}VBS.flat.swagger
				cp -R  Output ${REST_TOOL_LOCATION}
				rm -rf swagger-codegen-cli-2.2.3.jar
				rm -rf Output
				rm -rf  ../../../../../REST/VBO/Common
				rm -rf  ../../../../../REST/VBO/*/Common
				read -p "Do you wish to continue (Y/N)? " -n 1 -r
				echo
				if [[ ! $REPLY =~ ^[Yy]$ ]]
				then
					exit 1
				fi
			fi
		fi
	fi
done


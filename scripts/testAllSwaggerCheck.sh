#!/bin/bash
#Test script for the Swagger checks 
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Test Swagger====================================="
cd ${servicesLocation}
for service in ${servicesLocation}/*; do
	VBOName="${service##*/}"
	echo "${VBOName}"
	if [ ${VBOName} == Scripts ] || [ ${VBOName} == CommonSchemas ]
	then :
	else 
		#REST
		if [[ -d ${service}/REST ]]
		then
			cd ${service}/REST/VBS/
			cd */
			cd */
			cd */
			node ${gitScriptLocation}/CSMUtilities/index.js -o ${PWD}/${VBOName}VBS.swagger
			read -p "Do you wish to continue (Y/N)? " -n 1 -r
			echo
			if [[ ! $REPLY =~ ^[Yy]$ ]]
			then
				exit 1
			fi
		fi
	fi
done


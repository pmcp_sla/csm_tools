#!/bin/bash
#Takes a swagger file and flattens it - flat file can then be used to generate a CSV file. - Uses Vodafone CSM Utilies
# Depencency on a specific node implementation of json-schema-ref-parser - best to get a copy from Vodafone
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================REST SERVICE FLATTEN SWAGGER=====================================\e[0m"
echo ${restCommonServiceName}
cd ${gitScriptLocation}/CSMUtilities
#cd ${FLATTEN_SWAGGER_LOCATION}
echo "Create Common soft links"
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/${serviceVBSName}/Common
echo "Swagger Flatten ${restVBSPath}/${restFileName}.swagger"
node ./index.js -b ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
# Remove common links
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/${serviceVBSName}/Common
# Check that the generated file has Basic Components
if grep -i -q "BasicComponents" ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger; then
  echo "Flat swagger contains Basic Components " 
else
	/bin/echo -e "\e[1;31mBasic Components not found in flat swagger!\e[0m"
fi



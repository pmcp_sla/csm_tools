#!/bin/bash
#Diff command line tool - probably not needed anymore as can see this via Git
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================REST SERVICE DIFF====================================="
echo ${serviceVBOName} 
echo "Diff with current service and the same file in oldJsonFiles - copy in the old file you wish to compare"
/bin/echo -e "\e[1;34mDIFF ${VBOName}.json\e[0m"
/bin/echo -e "\e[1;34mCURRENT ${VBOName}.json   \t\t\t\t\t OLD  ${VBOName}.json \e[0m"
sdiff -bBWs ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json oldJsonFiles/${VBOName}.json
read -p "Do you wish to continue for swagger(Y/N)? " -n 1 -r
echo 
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	exit 1
fi
/bin/echo -e "\e[1;34mDIFF ${restFileName}.swagger\e[0m"
sdiff -bBWs ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger oldJsonFiles/${restFileName}.swagger
read -p "Do you wish to continue for wadl (Y/N)? " -n 1 -r
echo	
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	exit 1
fi
/bin/echo -e "\e[1;34mDIFF ${restFileName}.wadl\e[0m"
sdiff -bBWs ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl oldJsonFiles/${restFileName}.wadl

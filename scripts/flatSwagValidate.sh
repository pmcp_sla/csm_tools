#!/bin/bash
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================Validate FLAT SWAGGER=====================================\e[0m"
echo ${serviceVBOName} 
if [ -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger ]; then
	node ${gitScriptLocation}/CSMUtilities/index.js -F ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
	#node ${REST_TOOL_LOCATION}/index -F ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
	#node ${original}/index -F ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.flat.swagger
else 
	/bin/echo -e "\e[1;34mFlat Swagger not found\e[0m"
fi

#!/bin/bash
#Change these variables for each package
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
#source declarations_ChangeAuthenticationCredential.sh
/bin/echo -e "\e[1;34m=======================SOAP SERVICE Validate=====================================\e[0m"
echo "VBO Service - " ${serviceVBOName} 
echo "VBS Service - " ${serviceVBSName} 
# Create soft links
cp  -a  ${SOAP_COMMON} ${servicesLocation}/${soapServicePath}/SOAP/VBO/Common
cp  -a  ${SOAP_COMMON} ${servicesLocation}/${soapServicePath}/SOAP/VBO/${serviceGroup}/Common
echo
echo "Execute xmllint"
# Confirm Service location for each new package? May not need to change
xmllint -noout -schema ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/Examples/*
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${soapServicePath}/SOAP/VBO/Common
rm -rf   ${servicesLocation}/${soapServicePath}/SOAP/VBO/${serviceGroup}/Common
echo
/bin/echo -e "\e[1;34m=======================SOAP SERVICE SYNTAX CHECKSe=====================================\e[0m"
if grep -i -q ${SOAP_Version} ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl; then
  echo "Version matches on wsdl file" ${SOAP_Version}
  if grep -i -q "version:${SOAP_Version}" ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl; then
  	echo "Version NAME matches on wsdl file" 
  else
  	/bin/echo -e "\e[1;31mVersion issue - expected version:\e[0m" 
  fi
else
	/bin/echo -e "\e[1;31mVersions do not match on wsdl!!!!\e[0m" ${SOAP_Version} "Not found!"
	echo "Found" 
	grep -i 'version:' ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
fi
# Check 12 wsdl file
if  [  -f ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl ]; then
  if grep -i -q ${SOAP_Version} ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl; then
    echo "Version matches on wsdl 12 file" ${SOAP_Version}
    if grep -i -q "version:${SOAP_Version}" ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl; then
      echo "Version NAME matches on wsdl 12 file" 
    else
      /bin/echo -e "\e[1;31mVersion issue 12 - expected version:\e[0m" 
    fi
  else
    /bin/echo -e "\e[1;31mVersions do not match on wsdl!!!!\e[0m" ${SOAP_Version} "Not found on 12 file!"
    echo "Found" 
    grep -i 'version:' ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
  fi
fi
#Check vbm file
if grep -i -q ${SOAP_Version} ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd; then
  echo "Version matches VBM" ${SOAP_Version}
else
	/bin/echo -e "\e[1;31mVersions do not match on VDM!!!!\e[0m" ${SOAP_Version} "Not found!"
	echo "Found" 
	grep -i 'version:'  ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd
fi

#!/bin/bash
#Change these variables for each package
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================REST SERVICE WADL CHECKS=====================================\e[0m"
echo ${restCommonServiceName}
cd ${REST_TOOL_LOCATION}
# Create soft links
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
echo "Validate Wadl ${restVBSPath}/${restFileName}.wadl"
node ./index.js -w ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl
/bin/echo -e "\e[1;34m=======================REST SERVICE SYNTAX CHECKS=====================================\e[0m"
if grep -i -q ${REST_Version} ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl; then
  echo "WADL Version matches " ${REST_Version}
else
	/bin/echo -e "\e[1;31mWADL Versions do not match!!!!\e[0m" ${REST_Version} "Not found!"
	echo "Found" 
	grep -i 'version>' ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl
fi
if grep -i -q ${REST_Version} ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger; then
  echo "Swagger Version matches " ${REST_Version}
else
	/bin/echo -e "\e[1;31mSwagger Versions do not match!!!!\e[0m" ${REST_Version} "Not found!"
	echo "Found" 
	grep -i 'version\"' ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
fi
read -p "Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	/bin/echo -e "\e[1;34mTIDY UP\e[0m"
	rm -rf   ${servicesLocation}/${restServicePath}/Common
	rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
	rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
	rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json
	exit 1
fi
/bin/echo -e "\e[1;34m======================= WADL CHECK USING APACHE CXF=====================================\e[0m"
	#Installed from http://cxf.apache.org/download.html  - call below does not work - not sure why
	${technicalLocation}/appache-cxf/apache-cxf-3.2.7/bin/wadl2java  -validate -verbose ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl

/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/BasicComponents.json


#!/bin/bash
#Change these variables for each package
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================CHECK CSV files exist=====================================\e[0m"
if [ ! -f ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}.csv ]; then
    /bin/echo -e "\e[1;31mSOAP csv file not found! ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}.csv\e[0m"
else
    echo "SOAP csv file found! ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}.csv"
fi
if [ ! -f ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.csv ]; then
    /bin/echo -e "\e[1;31mREST csv file not found! ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.csv\e[0m"
else
    echo "REST csv file found! ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.csv"
fi


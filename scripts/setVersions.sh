#!/bin/bash
#Sets the version to those in the declarations file
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
/bin/echo -e "\e[1;34m=======================Version Set=====================================\e[0m"
echo ${serviceVBOName}
# Update the VBM file
if [ -f ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd ]; then
	echo "Set SOAP VBM version to ${SOAP_Version}"
	sed -i "s/\"unqualified\" version=\"[0-9]*.[0-9]*\"/\"unqualified\" version=\"${SOAP_Version}\"/"  ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd
	sed -i "s/\"unqualified\" version=\"[0-9]*.[0-9]*.[0-9]*\"/\"unqualified\" version=\"${SOAP_Version}\"/"  ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd
fi
# Update the wsdl file
if [ -f ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl ]; then
	echo "Set SOAP wsdl version to ${SOAP_Version}"
	sed -i "s/documentation>version:[0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
	sed -i "s/documentation>Version:[0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
	sed -i "s/documentation>version: [0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
	sed -i "s/documentation>version [0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
	sed -i "s/documentation>Version [0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}.wsdl
fi
if  [  -f ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl ]; then
	# Update the wsdl 12 file
	echo "Set SOAP wsdl 12 version to ${SOAP_Version}"
		sed -i "s/documentation>version:[0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
		sed -i "s/documentation>Version:[0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
		sed -i "s/documentation>version: [0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
		sed -i "s/documentation>version [0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
		sed -i "s/documentation>Version [0-9]*.[0-9]*.[0-9]*/documentation>version:${SOAP_Version}/"  ${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/${wsdlFileName}12.wsdl
fi
# Update wadl file
if  [  -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl ]; then
	echo "Set REST wadl version to ${REST_Version}"
	sed -i "s/version>[0-9]*.[0-9]*.[0-9]*</version>${REST_Version}</"  ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl
	sed -i "s/version> [0-9]*.[0-9]*.[0-9]*</version>${REST_Version}</"  ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl
	sed -i "s/version> [0-9]*.[0-9]*.[0-9]* </version>${REST_Version}</"  ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl
fi
# Update the swagger file (but not the flat swagger)
if  [  -f ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger ]; then
	echo "Set REST swagger version to ${REST_Version}"
	sed -i "s/\"version\":\"[0-9]*.[0-9]*.[0-9]*\"/\"version\":\"${REST_Version}\"/" ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
	sed -i "s/\"version\": \"[0-9]*.[0-9]*.[0-9]*\"/\"version\":\"${REST_Version}\"/" ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
	sed -i "s/\"version\" : \"[0-9]*.[0-9]*.[0-9]*\"/\"version\":\"${REST_Version}\"/" ${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
fi
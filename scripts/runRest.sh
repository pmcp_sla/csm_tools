#!/bin/bash
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
cd ${REST_TOOL_LOCATION}/Output
echo "=======================REST SERVICE EXECUTION====================================="
if((${restVersion} == 1));
then
	/bin/echo -e "\e[1;34m http://localhost:8080/%7Baccess%7D/${serviceVBOName,}API/swagger-ui.html#/ \e[0m"  
	echo "OR"
	/bin/echo -e "\e[1;34m http://localhost:8080/%7Baccess%7D/${serviceVBOName,}API/v${restVersion}/swagger-ui.html#/ \e[0m"  
else
	/bin/echo -e "\e[1;34m http://localhost:8080/%7Baccess%7D/${serviceVBOName,}API/v${restVersion}/swagger-ui.html#/ \e[0m"  
fi
mvn -e clean package
mvn spring-boot:run

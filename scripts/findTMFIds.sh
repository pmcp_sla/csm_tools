#!/bin/bash
#Check SOAP, JSON and Swagger files to see if any types exist which should be changed to TMF types.
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
cd ${REST_TOOL_LOCATION}
#Declare patterns to search for
soapTMFIds="AttachmentType BaseComponentType ChannelReferenceType ContactMediumType DisabilityType \
		ExternalReferenceType IndividualType IndividualIdentificationType IndividualRelatedPartyType NoteType \
		OrganisationType OrganizationType  \
		OrganisationIdentificationType OrganizationIdentificationType\
		OrganisationRelatedPartyType OrganizationRelatedPartyType \
		OrganisationParentRelationshipType OrganizationParentRelationshipType \
		OrganisationChildRelationshipType OrganizationChildRelationshipType\
		OtherNameType RelatedEntityType RelatedPartyType TimePeriodType "
restTMFIds="attachmentType baseComponentType  channelRefType \
			contactMediumType disabilityType extendedBaseComponentType externalReferenceType \
			individualType individualIdentificationType individualRelatedPartyType noteType otherNameType \
			organisationType organizationType \
			organisationChildRelationshipType organizationChildRelationshipType \
			organisationIdentificationType organizationIdentificationType \
			organisationParentRelationshipType organizationParentRelationshipType \
			organisationRelatedPartyType organizationRelatedPartyType \
			relatedPartyType relatedEntityRefType timePeriodType"
/bin/echo -e "\e[1;34mVBO Service ${serviceVBOName}\e[0m"
#/bin/echo -e "\e[1;34mVBS Service ${serviceVBSName}\e[0m"
/bin/echo -e "\e[1;34mSOAP Version ${SOAP_Version}\e[0m"
/bin/echo -e "\e[1;34mREST Version ${REST_Version}\e[0m"
/bin/echo -e "\e[1;34m=======================SOAP SERVICE TMF IDENTIFIERS=====================================\e[0m"
file=${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}.xsd
echo "Checking SOAP VBO: " ${file}
for i in $soapTMFIds; do
    grep -iEn $i ${file} 
done
echo
echo
file=${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd
echo "Checking SOAP VBM: " ${file}
for i in $soapTMFIds; do
    grep -iEn $i ${file} 
done
echo
echo
file=${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/Extension/Extended${xsdVBOFileName}.xsd
echo "Checking SOAP Extension: " ${file}
for i in $soapTMFIds; do
    grep -iEn $i ${file} 
done
echo
echo
file=${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/Examples/
echo "Checking SOAP Examples: " ${file}
for i in $soapTMFIds; do
    grep -iEn $i ${file}*.xml 
done
echo
echo
file=${servicesLocation}/${soapServicePath}/SOAP/VBS/${soapVBSPath}/
echo "Checking SOAP WSDL: " ${file}
for i in $soapTMFIds; do
    grep -iEn $i ${file}*.wsdl 
done
echo
echo
echo
echo
read -p "Do you wish to continue (Y/N)? " -n 1 -r
echo   
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
/bin/echo -e "\e[1;34m=======================REST SERVICE TMF IDENTIFIERS=====================================\e[0m"file=${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdFileName}.xsd
file=${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/
echo "Checking REST VBO: " ${file}
for i in $restTMFIds; do
    grep -iEn $i ${file}*.json 
done
echo
echo
file=${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/Impl/Swagger/
echo "Checking REST Swagger Implemation files: " ${file}
for i in $restTMFIds; do
    grep -iEn $i ${file}*.json 
done
echo
echo
file=${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/Examples/
echo "Checking REST Swagger Examples files: " ${file}
for i in $restTMFIds; do
    grep -iEn $i ${file}*.json 
done
echo
echo
file=${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.swagger
echo "Checking REST Swagger: " ${file}
for i in $restTMFIds; do
    grep -iEn $i ${file} 
done
echo
echo
file=${servicesLocation}/${restServicePath}/REST/VBS/${restVBSPath}/${restFileName}.wadl
echo "Checking REST Swagger: " ${file}
for i in $restTMFIds; do
    grep -iEn $i ${file} 
done
echo
echo




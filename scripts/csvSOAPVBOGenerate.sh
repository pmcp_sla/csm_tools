#!/bin/bash
#Generate csv from a SOAP schema - uses Vodafone CSM Utilies
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Excel====================================="
echo ${restCommonServiceName}
cd ${VALIDATEREST_TOOL_LOCATION}
echo "Generate CSV "
# Create soft links
cp  -a  ${SOAP_COMMON} ${servicesLocation}/${soapServicePath}/SOAP/VBO/Common
cp  -a  ${SOAP_COMMON} ${servicesLocation}/${soapServicePath}/SOAP/VBO/${serviceGroup}/Common
echo "${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}.xsd"
node ./index.js -S ${servicesLocation}/${soapServicePath}/SOAP/VBO/${soapVBOPath}/${xsdVBOFileName}.xsd
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf  ${servicesLocation}/${soapServicePath}/SOAP/VBO/Common
rm -rf   ${servicesLocation}/${soapServicePath}/SOAP/VBO/${serviceGroup}/Common

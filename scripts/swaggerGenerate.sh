#!/bin/bash
#Generates the Swagger for a schema - useful for a new service creation
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Excel====================================="
echo ${restCommonServiceName}
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
cp  -a  ${REST_COMMON} ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/${serviceVBSName}/Common
#cd ${GENERATE_SWAGGER_TOOL_LOCATION}/Generated
cd ${gitScriptLocation}/CSMUtilities
echo "Generate Swagger Implementation"
#cp ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json .
#node ./../index.js -w ${VBOName}.json
node ./index -I ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}.json
echo "Files in ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/Impl/Swagger/"
rm -rf   ${servicesLocation}/${restServicePath}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/Common
rm -rf   ${servicesLocation}/${restServicePath}/REST/VBO/${serviceGroup}/${serviceVBSName}/Common
# Move generated file
#mv ${servicesLocation}/${restServicePath}/REST/VBO/${restVBOPath}/${VBOName}_JSON_LD.json ${GENERATE_JSONLD_TOOL_LOCATION}/Generated
# Note 1) Any files which fail due to an illegal reference will need the calling file to have baseComponent replaced by the actual attributes!"
# 	2) Required [] will need removed, i.e. required being empty
#   3) Soome swaggers require bespoke components tied to file under VBS - these need their swagger component file which may not be generated
#	4) Have seen cases of a base component failing such as "$ref": "../../../../../Common/V1/Impl/Swagger/CommonComponents.json#/properties/contactPointType"
#		In the case swap out with baseComponent


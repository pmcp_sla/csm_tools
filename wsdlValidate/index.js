#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var validate = require("./wsdlValidate.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('wsdlValidate' in cmdArgs)
{
    validate.wsdlValidate(cmdArgs['wsdlValidate']);
}
else
{
    cmdObj.printHelp();
}

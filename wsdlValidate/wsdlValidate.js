
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
const wsdl = require('./getWsdl.js');


var globalAddress = "http://cim.vodafone.com/";
var globalServiceName; 
var globalWriteStream; // Output file

const EXPECTED_INPUTS = ["Source", "Correlation", "Cache", "Destination", "RouteInfo"];
const OPTIONAL_INPUTS = ["ResultStatus", "ServiceDocumentation"];

const EXPECTED_OUTPUTS = ["Source", "Correlation", "Cache", "ResultStatus", "Destination"];
const EXPECTED_LIST_OUTPUTS = ["Source", "Correlation", "Cache", "ResultStatus", "Destination", "QueryResult"];
const OPTIONAL_OUTPUTS = ["ServiceDocumentation"];


function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}


function getInputs(operation) {
	var result;
	operation.elements.forEach(function (op) {
		if(op.name=="wsdl:input") {
			result = op.elements;
		}
	});
	return result;
}

function getOutputs(operation) {
	var result;
	operation.elements.forEach(function (op) {
		if(op.name=="wsdl:output") {
			result = op.elements;
		}
	});
	return result;
}

function getHeader (op){
	var result;
	if(op.name && ((op.name=="soap11:header") || (op.name=="soap12:header")) ) {
		if(op.attributes && op.attributes.part) {
			result = op.attributes.part;
		}
	}
	return result;
}

function duplicatesFound(values){

	var isDuplicate = values.some(function(item, idx){ 
	    return values.indexOf(item) != idx 
	});
	return isDuplicate;
}

function checkAgainstAllowedHeaders(headerList, expectedList, optionalList, inout) {
	// Check that the header list contains all of the expected headers and any others are in the optional list
	expectedList.forEach(function (header){
		if(headerList.includes(header)) {
			// Okay
		}
		else 
		{
			fns.logError(inout + " header " + header + " not found in operaton.");
		}
	});
	headerList.forEach(function (header){
		if(header) {
			if( (!expectedList.includes(header)) && (!optionalList.includes(header)) ) {
				fns.logError(inout + " header " + header + " not expected in operaton.");
			}
		}
	});
	if(duplicatesFound(headerList)){
		fns.logError(inout + " header duplicated in operaton.");
	}
}


function checkOperation(operation) {
	// Check an operation
	//console.log(operation);
	fns.logInfo(operation.attributes.name);
	var inputs = getInputs(operation);
	// Collect the message header names
	var inputHeaders = [];
	inputs.forEach(function (op) {
		inputHeaders.push(getHeader(op));
	});
	var outputs = getOutputs(operation);
	var outputHeaders = [];
	if(outputs) {
		outputs.forEach(function (op) {
			outputHeaders.push(getHeader(op));
		});
	}
	// Check if each input header in the expected or optional lists
	checkAgainstAllowedHeaders(inputHeaders, EXPECTED_INPUTS, OPTIONAL_INPUTS, "Input")

	// Check if each output header in the expected or optional lists
	if(operation.attributes.name.toLowerCase().startsWith("notify")) {
		// Ignore
	}
	else if(operation.attributes.name.toLowerCase().endsWith("list")) {
		checkAgainstAllowedHeaders(outputHeaders, EXPECTED_LIST_OUTPUTS, OPTIONAL_OUTPUTS, "Output")
	}
	else {
		checkAgainstAllowedHeaders(outputHeaders, EXPECTED_OUTPUTS, OPTIONAL_OUTPUTS, "Output")
	}

}

function checkOperationHeaders (wsdl) {
	// Check the list of inputs and outputs for each operation
	if(wsdl.elements && wsdl.elements[0].elements) {
		// Traverse elements looking for operation
		wsdl.elements[0].elements.forEach(function (el) {
			if(el.name == "wsdl:binding") {
				// Look within the binding for the operations
				if(el.elements) {
					//Travse for operations
					el.elements.forEach(function (op) {
						if(op.name == "wsdl:operation") {
							checkOperation(op);
						}
					});
				}
			}
		});
	} 
	else {
		fns.logWarn("No operations found");
	}

}



/**
 * Valiate WsdlFile  
 * @param {string} fileName 
 */
function valiateWsdlFile (fileName) {
	
	fns.logHeading("Valiate WSDL File");
	//set service name
	globalServiceName=getServiceName(fileName);

	fns.logHeading(globalServiceName);
	//Read  file
	var wsdlFile  = wsdl.openWsdlFile(fileName);

	checkOperationHeaders(wsdlFile);

}



function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


module.exports = {

  wsdlValidate: valiateWsdlFile
};


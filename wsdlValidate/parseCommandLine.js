const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  { name: 'wsdlValidate', alias: 'v', type: String, multiple: false},
  { name: 'timeout', alias: 't', type: Number }
];

module.exports = {
  parseCommandLine: function()
  {
    try {
        return commandLineArgs(optionDefinitions);
    } catch (e) {
        console.error(e.message);
    } finally {
    }
    return null;
  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'Validate CSM REST',
        content: 'Can perform different integrity [italic]{validations} (ajv, swagger, swagger-model, codegen)'
      },
      {
        header: 'Synopsis',
        content: '$ app <options>'
      },
      {
        header: 'Command List',
        content: [
          { name: 'help', summary: '-h Display help information ' },
          { name: 'wsdlValidate', summary: '-v Validates a wsdl file \n'+
                                                        '   Example: generateCSV -v <file'
                                                      }

        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

};

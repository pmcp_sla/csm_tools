
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const swagger = require('./getSwagger.js');


function createFile (filename, outputSchema) {
	// Write the file with the output schema
	let out = JSON.stringify(outputSchema, null, 4);
	fs.writeFile(filename.toString(), out, 
		function(err) { 
			if(err) {
					// Error
		        fns.logError("Error: " + filename);
		        console.log(err);
        	}
		});
}


function updateSchema (schema, filename) {
	// Add a default type
	if(schema && (Object.keys(schema).length == 1) && schema.allOf) {
		// Add a type
		schema['type'] = 'object';
		createFile(filename, schema);
		fns.logInfo("Swagger implementation file updated!");
	}
	else {
		fns.logInfo("Swagger implementation file update not required");
	}

}



/**
 * Add a type to a swagger implementation file
 * @param {string} fileName 
 */
function addType (filename) {
	
	fns.logHeading("Update swagger implementation file " + filename);

	var swagSchema = swagger.openJsonFile(filename.toString());

	if(swagSchema) {
		updateSchema(swagSchema, filename);
	}


}



module.exports = {

  addType: addType
};


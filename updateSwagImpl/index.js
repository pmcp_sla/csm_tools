#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var update = require("./addType.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('updateSwagger' in cmdArgs)
{
    update.addType(cmdArgs['updateSwagger']);
}
else
{
    cmdObj.printHelp();
}

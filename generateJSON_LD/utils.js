var chalk = require('chalk');
var Ajv = require('ajv');
const path = require('path');
const fs = require('fs');

/*
 * Load and parse a json file
 *
 * @param {type} filename   - the filename
 * @return {openFile.json|Array|Object}
 */
function openFile(filename) {
    var errorOnParse = false;
    var json = null;
    var file = path.resolve(process.cwd(), filename);

    try {
        try {
            json = JSON.parse(fs.readFileSync(file).toString());
        } catch (JSONerr) {
            console.error("%s %s", chalk.red('Error encountered while trying to parse the JSON file (most likely the BOM character), trying to use the require mechanism:  '), chalk.red(JSONerr));
            errorOnParse = true;
            json = require(file);
        }
    } catch (err) {
        console.error('error:  ' + err.message);
        process.exit(2);
    }

    if (errorOnParse)
    {
        console.log("%s %s", chalk.yellow('Managed to load the file using \'require\' mechanism, however the file should be manually checked'), filename);
    }
    return json;
}

/*
 * Perform a copy of the file between source and destination
 * @param {type} _sourceFile
 * @param {type} _destinationFile
 * @return
 */
function copyLocalFile(_sourceFile, _destinationFile)
{
    //normalize the file path
    var sourceFile = path.resolve(_sourceFile);
    var destinationFile = path.resolve(_destinationFile);

    console.log("copyFile->SourceFile: [%s]", sourceFile);
    console.log("copyFile->DestinationFile: [%s]", destinationFile);

    //check if the file exists on the disk
    if (!fs.existsSync(sourceFile))
    {
        console.error("File: [%s] doesn't exists", sourceFile);
        throw new Error("File specified does not exist on the disk");
    }

    //this will throw errors, is up for the upper layers to deal with the exceptions
    fs.createReadStream(sourceFile).pipe(fs.createWriteStream(destinationFile));
}

/**
 * Check if the file exists on the disk
 * @param {type} filePath
 * @return {Boolean} - true/false
 */
function checkPath(filePath)
{
    var pathNormalized = path.resolve(filePath);

    //for the moment the swagger codegen is hardcoded to: swagger-codegen-cli-2.2.3.jar
    if (!fs.existsSync(pathNormalized))
    {
        console.error("%s: [%s]", chalk.red("Unable to locate the path:"), pathNormalized);
        return false;
    } else
        return true;
}

/**
 *  Recursively find the keys in an object
 *
 */
function findObjects(obj, targetProp, targetValue, finalResults) {

  function getObject(theObject) {
    let result = null;
    if (theObject instanceof Array) {
      for (let i = 0; i < theObject.length; i++) {
        getObject(theObject[i]);
      }
    }
    else {
      for (let prop in theObject) {
        if(theObject.hasOwnProperty(prop)){
          //console.log(prop + ': ' + theObject[prop]);
          if (prop === targetProp) {
            //console.log('--found id');
            //if (theObject[prop] === targetValue) {
              //console.log('----found porop', prop, ', ', theObject[prop]);
              finalResults.push(theObject);
            //}
          }
          if (theObject[prop] instanceof Object || theObject[prop] instanceof Array){
            getObject(theObject[prop]);
          }
        }
      }
    }
  }

  getObject(obj);

}


module.exports = {
    loadFile: function (fileName)
    {
        return openFile(fileName);
    },

    copyFile: function (sourceFile, destinationFile)
    {
        return copyLocalFile(sourceFile, destinationFile);
    },

    checkExists: function (filePath)
    {
        return checkPath(filePath);
    },

    findObjects: function (obj, targetProp, targetValue, finalResults)
    {
        return findObjects(obj, targetProp, targetValue, finalResults);
    },

};

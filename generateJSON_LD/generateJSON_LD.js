
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
var $RefParser = require('json-schema-ref-parser'); // Node module which de-referecnes a json schema to one single schema

var globalAddress = "http://cim.vodafone.com/";
var globalServiceName; 
var globalWriteStream; // Output file



function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbo')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function outputSeparator () {
		globalWriteStream.write(', \r\n');
}


function outputHeaderInfo () {
	globalWriteStream.write('{ \r\n');
	globalWriteStream.write('"@graph": [ \r\n');
	let attribute = {};
	let context = {};
	context["rdf"] = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	context["rdfs"] = "http://www.w3.org/2000/01/rdf-schema#";
	context["xsd"] = "http://www.w3.org/2001/XMLSchema#";
	attribute["@context"] = context;
	let out = JSON.stringify(attribute, null, 4);
	globalWriteStream.write(out + '\r\n'); 
}

function outputClosingInfo () {
	globalWriteStream.write('] \r\n');
	globalWriteStream.write('} \r\n');
}


function generateAttribute (name, parent, description, type) {
	// Generate a single JSON LD attribute:
	//console.log(name);
	// Output a comma separator first
	outputSeparator();
	var outputType = capitalizeFirstLetter(type);
	var attribute = {};
	attribute["@id"] = "http://cim.vodafone.com/" + name;
	attribute["@type"] = "rdfs:Property";
	if(parent) {
		var domainIncludes = {};
		var outputParent = capitalizeFirstLetter(parent);
		domainIncludes["@id"]=  "http://cim.vodafone.com/" + outputParent;
		attribute['http\://schema.org/domainIncludes'] = domainIncludes;
	}
	if(type) {
		var rangeIncludes = {};
		rangeIncludes["@id"] = "http://cim.vodafone.com/" + type;
		attribute['http\://schema.org/rangeIncludes'] = rangeIncludes;
	}
	if((parent) && (type != "Boolean") && (type != "Number") && (type != "Text") ){
		var isPartOf = {};
		var outputParent = capitalizeFirstLetter(parent);
		isPartOf["@id"] =  "http://cim.vodafone.com/" + outputParent;
		attribute["http\://schema.org/isPartOf"] = isPartOf;
	}
	else {
		var isPartOf = {};
		isPartOf["@id"] =  "http://cim.vodafone.com/";
		attribute["http\://schema.org/isPartOf"] = isPartOf;
	}
	attribute["rdfs:comment"] = description;
	attribute["rdfs:label"] = outputType;

	let out = JSON.stringify(attribute, null, 4);
	globalWriteStream.write(out + '\r\n'); 
}


function traverseJson (schemaJson, parent){
	// Traverse the json schema and output an LD entry for each attribute
	//console.log('NAME=',currentName);
	//console.log(schemaJson.properties);
	var properties; // Grab the properties
	if (schemaJson.properties) 
		properties = Object.entries(schemaJson.properties);
	else if( (schemaJson.items) && (schemaJson.items.properties)) { // Sometimes extra layer has been added so ignore it
		properties = Object.entries(schemaJson.items.properties);
	}

	if(properties) {
		properties.forEach(function(property, value){ // Traverse each property looking for attributes

			// Process each property but ignore _links
			if(property[0] && (!(property[0].endsWith('_links')) ) ) { //Ignore _link
				switch (property[1].type) {
					case 'array':
						// Process array properties
						var arrayProperties;
						if((property[1].items) && (property[1].items.allOf)) { // List of items - allOf
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
						 	arrayProperties = property[1].items.allOf;						 	
							arrayProperties.forEach(function(arrayProperty){
									traverseJson(arrayProperty, property[0]);
								});
						} 
						else if ((property[1].items) && (property[1].items.properties)) { // Single object type
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
						 	arrayProperties = property[1].items;
							traverseJson(arrayProperties, property[0]);
						} 
						else if ((property[1].items) && (property[1].items.items) && (property[1].items.items.properties)) { // Array of arrays
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
						 	arrayProperties = property[1].items;
							traverseJson(arrayProperties, property[0]);
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
						}
						break;
					case 'object' : 
						// Process an object 
						var objProperties;
						if((property[1]) && (property[1].allOf)) { // List of items - allOf
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
						 	objProperties = property[1].allOf;
							objProperties.forEach(function(objProp){
									traverseJson(objProp, property[0]);
								});
						} 
						else if (property[1].properties) { // Single object type
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
						 	objProperties = property[1];
							traverseJson(objProperties, property[0] );
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
						}
						break;
					case 'string' :
						generateAttribute(property[0], parent, property[1].description, 'Text' ); 
						break;
					case 'boolean' : 
						generateAttribute(property[0], parent, property[1].description, 'Boolean' ); 
						break;
					case 'number' : 
						generateAttribute(property[0], parent, property[1].description, 'Number' ); 
						break;
					default:
						if(property[1] && property[1].properties) {
							// Should probably be an object
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
							objProperties = property[1];
							traverseJson(objProperties, property[0]);
						}
						else if(property[1] && property[1].allOf) {
							// Should probably be an Object
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
							objProperties = property[1].allOf;
							objProperties.forEach(function(key){
									traverseJson(key, property[0]);
								});
						}
						else {
						 	generateAttribute(property[0], parent, property[1].description, property[0] ); 
							fns.logWarn("Defaulted this entry:" + property[0]);
						}
						break;
				}
			}
		});
	}
	else {
			fns.logWarn("No entries for:" + schemaJson);
	}

}

/**
 * Generate JSON LD  from json schema
 * @param {string} schemaFileName 
 */
function generateJsonLDFromSchema (schemaFileName) {
	
	fns.logHeading("JSON LD Generation");
	//Read json schme file
	var jsonSchema = utils.loadFile(schemaFileName);

	//set service name
	globalServiceName=getServiceName(schemaFileName);


	fns.logHeading(globalServiceName);

	//Create an output file
	var outputFileName = schemaFileName.split('.').slice(0, -1).join('.') + '_JSON_LD.json'; //remove extension

	globalWriteStream = fs.createWriteStream(outputFileName);
	globalWriteStream.on('error', 
		function(err) { 
			// Error
        fns.logError("Error: ");
        console.log(err);
        fns.logError("If permission denied make sure the generated file is closed.") 
		});
	fns.logHeading("Generating: " + outputFileName);

    // Generate based on a dereferenced schema
	$RefParser.dereference(schemaFileName, "")
    .then(function(schemaResult) {
        // Success
        outputHeaderInfo();
        traverseJson(schemaResult, globalServiceName);
        outputClosingInfo();
        fns.logHeading("Success: file " +  outputFileName + " generated.");
    })
    .catch(function(err) {
        // Error
        fns.logError("Error: ");
        console.log(err);
    });

}



function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


module.exports = {

  generateJsonLD: generateJsonLDFromSchema
};


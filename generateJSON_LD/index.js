#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var generate = require("./generateJSON_LD.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('generateJSON_LD' in cmdArgs)
{
    generate.generateJsonLD(cmdArgs['generateJSON_LD']);
}
else
{
    cmdObj.printHelp();
}

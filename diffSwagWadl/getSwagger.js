/**
 * swagger module.
 * @module swaggerValidate
 */
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const fns = require('./helperFunctions');


/**
 * Reads a Json file and returns it as a Json object
 * @param {string} filename - The name of the Json file.
 * @return {Object} 
 */
function openJsonFile(filename){
    var json = null;
    var file = path.resolve(process.cwd(), filename);
    
        try {
            var content = fs.readFileSync(file);
            json = JSON.parse(content.toString().trim());
        } catch(JSONerr) {
                fns.logOperationFatal('error 101: file not found ' + filename + ' ' + JSONerr.message);
        }
    return json;
}
  




module.exports = {

  openJsonFile: openJsonFile
}

const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  {
    name: 'swag',
    type: String,
    multiple: false,
    description: 'The swagger input file to process',
    typeLabel: '<file>' 
  },
  {
    name: 'wadl',
    type: String,
    multiple: false,
    description: 'The wadl input file to process',
    typeLabel: '<file>' 
  },
  {
    name: 'print',
    type: String,
    multiple: false,
    description: 'Summary print for swagger and wadl file'
  }
];


module.exports = {
  parseCommandLine: function()
  {

    try {
        return commandLineArgs(optionDefinitions, { partial: true });
    } catch (e) {
        console.error(e.message);
    } finally {
    }

    return null;

  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'Diff swagger and wadl files',
        content: 'Does a diff on a swagger and a wadl file \n\t Usage: diffSwagWadl --swag <filename.swagger> --wadl <filename.wadl> \n\t Usage: diffSwagWadl --swag <filename.swagger> --wadl <filename.wadl> --print '
      },
      {
        header: 'Options',
        optionList: [
          {
            name: 'swag',
            typeLabel: '{underline file}',
            description: 'The swagger filename.'
          },
          {
            name: 'wadl',
            typeLabel: '{underline file}',
            description: 'The wadl filename.'
          },
          {
            name: 'print',
            description: 'Print a summary of the swagger and wadl files but does not do a diff.'
          },
          {
            name: 'help',
            description: 'Print this usage guide.'
          }
        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

}

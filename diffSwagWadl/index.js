#! /usr/bin/env node

const fs = require('fs');
const cmdObj = require("./parseCommandLine.js");
const cmdArgs = cmdObj.parseCommandLine();
const diff = require("./diffSwagWadl.js");


if ('help' in cmdArgs){
  cmdObj.printHelp();
}
else if (('swag' in cmdArgs) && ('wadl' in cmdArgs) && ('print' in cmdArgs))
{
  diff.print(cmdArgs['swag'], cmdArgs['wadl']);
}
else if (('swag' in cmdArgs) && ('wadl' in cmdArgs))
{
  diff.validate(cmdArgs['swag'], cmdArgs['wadl']);
}
else{
  cmdObj.printHelp();
}


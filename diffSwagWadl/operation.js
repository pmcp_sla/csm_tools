"use strict";
const chalk = require('chalk');
const fns = require('./helperFunctions');

/** Class representing an Operation. */
class Operation {
    

    /**
     * Create an operation.
     * @param {string} name - The operation name.
     * @param {json Object} json - The json object.
     * @param {string} type - The type of json obejct swagger or wadl.
     */
    constructor(name, json, type, wadlResponse) {
    	if (type == 'swagger') {
            this.name = name;
    		this.createOperationFromSwagger(json);
    	}
    	else if (type == 'wadl'){
            this.name = name;
    		this.createOperationFromWadl(json, wadlResponse);
    	}
    	else {
    		fns.logOperationFatal("Unable to create operation" + name);
    	}
    }

    /**
     * Get the Name value.
     * @return {string} The name value.
     */
    getName() {
        return this.name;
    }


    /**
     * Create an operation from Swagger.
     * @param {json Object} json - The json object.
     * @return {operation object} operation
     */
    createOperationFromSwagger (jsonObj){
        this.params = new Map();
        this.responses = [];
        // Move params to a list of names such as ID, string, type, string, ...
        var params = jsonObj.parameters;
        var key = 0;
        for (key in params){
            if (typeof params[key].name === 'string' || params[key].name instanceof String) {
                if (['default', 'undefined', 'query', 'X-Notify-EventCode', 'X-Notify-Business-EventCode', 'X-Total-Count', '$ref', 'hub'].indexOf(params[key].name) == -1 ) {
                    if (params[key].type)
                       this.params.set(params[key].name, params[key].type);
                }
            }
        }
        // Move response to a list of numbers such 200, 204, 404, ...
        var responses = jsonObj.responses;
        var key = 0;
        for (key in responses){
            if (['default', 'undefined', 'query'].indexOf(key) == -1 ) {
                this.responses.push(key);
            }
        }
    }

    /**
     * Create an operation from Wadl.
     * @param {json Object} json - The json object.
     * @param {array} responses - The responses for the operation.
     * @return {operation object} operation
     */
    createOperationFromWadl (jsonObj, responses){
        this.params = new Map();
        this.responses = responses;

        // Add the operation name - Get/POST/Patch to match Swagger name
        this.name = '/' + this.name + '_' + jsonObj.attributes.name.toLowerCase();

        //Get the params
        var key = 0;
        for (key in jsonObj.elements){
            var key2= 0;
            for (key2 in jsonObj.elements[key].elements){
                var name = jsonObj.elements[key].elements[key2].name;
                var attributes = jsonObj.elements[key].elements[key2].attributes;
                //Copy param
                if (name == 'wadl:param'){
                    if(['Authorization', 'X-Rate-Limit-Remaining', 'X-Rate-Limit-Reset', 'X-Notify-EventCode', 'X-Rate-Limit-Limit', 'X-Total-Count', 'Content-Range', 'offset', 'field', 'sort', 'count', 'hub'].indexOf(attributes.name) == -1 ) {
                        if (attributes.type == 'xsd:int') this.params.set(attributes.name,'integer');
                        else this.params.set(attributes.name, 'string');
                    }
                }
            }
        }
    }

    /**
     * Compare this operation with one passed in
     * @param {Operation} compareOp - The Operation object.
     * @return {boolean} 
     */
    compare(compareOp){
        console.log("Comparing operation:", chalk.bold(this.name));
        var opName = this.name;
        var opParams = this.params;
        //Compare params
        this.params.forEach(function(item, key, mapObj){ 
                if (compareOp.params.has(key)){
                    if (item != compareOp.params.get(key)) {
                        fns.logOperationError("Paramter " + key + " with type " + item + " does not match Wadl");
                    }
                }
                else {
                    fns.logOperationError("Parameter " + key + " not in Wadl but in Swagger");
                }
        });
        //Check if wadl has parameters not in Swagger
        compareOp.params.forEach(function(item, key, mapObj){ 
                 if (opParams.has(key)){
                    if (item != opParams.get(key)) {
                        fns.logOperationError("Parameter " + key + " with type " + item + " does not match Swagger");
                    }
                }
                else {
                    fns.logOperationError("Parameter " +  key + " not in Swagger but in Wadl");
                }
        });
        //Compare responses
        let difference = this.responses.filter(x => !compareOp.responses.includes(x));
        if (difference.length > 0) {
            fns.logOperationError("Response codes not in Wadl: " + difference + " but in Swagger");
        }
    }

    /**
     * Print a summary of the operation
     */
    print() {
        console.log('Operation Name:', chalk.bold(this.name));
        if (this.params.size==0)
        {
            console.log("Parameters: []");
        }
        else {
            console.log('Parameters');
            this.params.forEach(function(value, key){console.log('\t', key, ':', value);});
        }
        console.log('Responses:', this.responses);
    }

    /**
     * Validate an operation for common issues
     */
     validate(){
        //IGNORE
        /*
        console.log("Valiate operation:", chalk.bold(this.name));
        // Check the response codes for a get
        if (this.name.endsWith("_get")){
            if (this.responses.indexOf('200') == -1){
                fns.logOperationError("Response code 200 is missing");
            }
        }
        // Check the response codes for a patch
        if (this.name.endsWith("_patch")){
            if (this.responses.indexOf('200') == -1){
                fns.logOperationError("Response code 200 is missing");
            }
            if (this.responses.indexOf('202') == -1) {
                fns.logOperationWarn("Response code 202 is missing - required if operation is asynchronous");
            }
            if (this.responses.indexOf('204') == -1) {
                fns.logOperationError("Response code 204 is missing");
            }
        }
        // Check the response codes for a put
        if (this.name.endsWith("_put")){
            if (this.responses.indexOf('200') == -1){
                fns.logOperationError("Response code 200 is missing");
            }
            if (this.responses.indexOf('202') == -1) {
                fns.logOperationWarn("Response code 202 is missing - required if operation is asynchronous");
            }
            if (this.responses.indexOf('204') == -1) {
                fns.logOperationError("Response code 204 is missing");
            }
        }
        // Check the response codes for a delete
        if (this.name.endsWith("_delete")){
            if (this.responses.indexOf('200') == -1){
                fns.logOperationError("Response code 200 is missing");
            }
            if (this.responses.indexOf('202') == -1) {
                fns.logOperationError("Response code 202 is missing");
            }
            if (this.responses.indexOf('204') == -1) {
                fns.logOperationError("Response code 204 is missing");
            }
        }
        // Check the response codes for a post
        if (this.name.endsWith("notifications_post")){
            if (this.responses.indexOf('200') > -1)  {
                fns.logOperationError("Response code 200 is not required");
            }
            if (this.responses.indexOf('204') == -1) {
                fns.logOperationError("Response code 204 is missing");
            }
        }
        else if (this.name.endsWith("_post")){
            if ((this.responses.indexOf('200') == -1) && (this.responses.indexOf('201') == -1) ) {
                fns.logOperationError("Response code 200 or 201 is missing");
            }
            if (this.responses.indexOf('202') > -1) {
                fns.logOperationWarn("Response code 202 is missing - required if operation is asynchronous");
            }
            if (this.responses.indexOf('204') == -1) {
                fns.logOperationError("Response code 204 is missing");
            }
        }
        */
     }


}



module.exports = Operation;

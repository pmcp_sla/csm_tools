
const chalk = require('chalk');
//Local files
const swag = require('./getSwagger.js');
const wadl = require('./getWadl.js');
const Operation = require('./operation.js');
const fns = require('./helperFunctions');



function parseSwagger (swaggerJson) {
	var swaggerObjectMap = new Map();
	var swaggerOperations = swaggerJson.paths; //array of operations
	var keyPath, keyOperator =0;
	//Process each path
	for (keyPath in swaggerOperations){
		//Process each operation with a path
		for (keyOperator in swaggerOperations[keyPath]){
				if(keyOperator != 'parameters') {
				var nameIndex = keyPath + '_' + keyOperator.toLowerCase();
				var newOperation = new Operation(nameIndex, swaggerOperations[keyPath][keyOperator], 'swagger', []);
				swaggerObjectMap.set(newOperation.name,  newOperation);
			}	
		}
	}
	return swaggerObjectMap;
}

function parseWadl (wadlJson){
	var wadlObjectMap =new Map();
	var wadlOperations = wadlJson.elements[0].elements[2].elements; //array of operations
	var keyPath, keyOperator = 0;
	//Process each Operation
	for (keyPath in wadlOperations){
		var opName = wadlOperations[keyPath].attributes.path;
		var opWadl = wadlOperations[keyPath].elements;
		
		for (key in opWadl){
		    var name = opWadl[key].name
            var attributes = opWadl[key].attributes;
            var elements = opWadl[key].elements;
            //Grab the responses
            if (name == 'wadl:method'){
            	var keyEl = 0;
            	var wadlResponses = [];
                for (keyEl in elements){
                    var current = elements[keyEl]
                    if(current.name == 'wadl:response') {
                        Array.prototype.push.apply(wadlResponses, current.attributes.status.split(' '));
                    }
                }
            	var newOperation = new Operation(opName, opWadl[key], 'wadl', wadlResponses);
            	wadlObjectMap.set(newOperation.name, newOperation);
            }
        }
    }
    return wadlObjectMap;
}

/**
 * Identifiy differences between a swagger and wadl file.
 * @param {string} swaggerFileName 
 * @param {string} wadlFileName 
 */
function diffSwagWSDL (swaggerFileName, wadlFileName) {
	var swagger = swag.openJsonFile(swaggerFileName);
	var wadlXML  = wadl.openWadlFile(wadlFileName);
	var swaggerObjectMap = new Map();
	var wadlObjectMap =new Map();

	swaggerObjectMap = parseSwagger(swagger);
	//print the operators
	//swaggerObjectMap.forEach(function(op){ op.print();});

	wadlObjectMap = parseWadl(wadlXML);
	//print the operators
	//wadlObjectMap.forEach(function(op){ op.print();});


	//Run some checks on the swagger operations
	swaggerObjectMap.forEach(function(op){ op.validate();});
	//console.log('Swagger:', swaggerObjectMap.keys());
	//console.log('wqadl@', wadlObjectMap.keys());

	//compare Operations and print results
	swaggerObjectMap.forEach(function(op){ 
		if ((op.name.startsWith("/hub")) || (op.name.startsWith("/listener")) || (op.name.startsWith("/home"))){
			//Ignore
		}
		else if (wadlObjectMap.has(op.name)){
			op.compare(wadlObjectMap.get(op.name));
		}
		else {
			fns.logOperationError("Operation " + op.name + " found in Swagger but not in Wadl\n");
		}
	});


	//Check if any operations occur in wadl but not in Swagger
	wadlObjectMap.forEach(function(op){ 
		if ((op.name.startsWith("/hub")) || (op.name.startsWith("/listener")) || (op.name.startsWith("/home"))){
			//Ignore
		}
		else if (swaggerObjectMap.has(op.name)){
			//Ignore - already compared
		}
		else {
			fns.logOperationError("Operation " + op.name + " found in Wadl but not in Swagger\n");
		}
	});

	//compare base paths - http://api.vodafone.com/
	var wadlPath = wadlXML.elements[0].elements[2].attributes.base.replace('http://api.vodafone.com', '') ;
	var swaggerPath = swagger.basePath + '/';
	if(swaggerPath != wadlPath){
		fns.logOperationError("\nTarget paths are different, swagger: " +  swaggerPath + " wadl: " + wadlPath);
	}

}

function printSummary (swaggerFileName, wadlFileName) {
	var swagger = swag.openJsonFile(swaggerFileName);
	var wadlXML  = wadl.openWadlFile(wadlFileName);
	var swaggerObjectMap = new Map();
	var wadlObjectMap =new Map();

	swaggerObjectMap = parseSwagger(swagger);
	//print the Swagger operators
	console.log(chalk.yellow.bold('Swagger File operations'));
	swaggerObjectMap.forEach(function(op){ op.print();});

	wadlObjectMap = parseWadl(wadlXML);
	//print the operators
	console.log(chalk.yellow.bold('\nWadl File operations'));
	wadlObjectMap.forEach(function(op){ op.print();});
}


module.exports = {


  validate: function(swaggerFile, wadlFile)
  {
    diffSwagWSDL(swaggerFile, wadlFile);
  },

  print: function(swaggerFile, wadlFile)
  {
    printSummary(swaggerFile, wadlFile);
  }
}


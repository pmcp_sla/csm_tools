/**
 * Global helper function module.
 * @module helperFunction
 */

const chalk = require('chalk');

/**
 * Displays an error message
 * @param {string} display - The error message.
 */
function logOperationError(display){
        console.log("\tError: ", chalk.red.bold(display));
  }

/**
 * Displays an info message
 * @param {string} display - The error message.
 */
function logOperationInfo(display){
        console.log(display);
  }

/**
 * Displays a warning message
 * @param {string} display - The error message.
 */
function logOperationWarn(display){
        console.log("\tWarning: ", chalk.yellow.bold(display));
  }

/**
 * Displays a fatal error message
 * @param {string} display - The error message.
 */
function logOperationFatal(display){
        console.log("Fatal Error: ", chalk.red.bold(display));
        process.exit(2);
  }

module.exports = {

  logOperationError: logOperationError,
  logOperationInfo: logOperationInfo,
  logOperationWarn: logOperationWarn,
  logOperationFatal: logOperationFatal
};

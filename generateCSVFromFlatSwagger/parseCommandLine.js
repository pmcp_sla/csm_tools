const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  { name: 'generateJSONCSV', alias: 'j', type: String, multiple: false},
  { name: 'timeout', alias: 't', type: Number }
];

module.exports = {
  parseCommandLine: function()
  {
    try {
        return commandLineArgs(optionDefinitions);
    } catch (e) {
        console.error(e.message);
    } finally {
    }
    return null;
  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'Generate CSV',
        content: 'Generate CSV from flat swagger'
      },
      {
        header: 'Synopsis',
        content: '$ app <options>'
      },
      {
        header: 'Command List',
        content: [
          { name: 'help', summary: '-h Display help information ' },
          { name: 'generateJSONCSV', summary: '-j Generates csv file from flat swagger \n'+
                                                        '   Example: generateCSV -j CustomerPrivacyProfileVBS.flat.swagger'
                                                      }

        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

};

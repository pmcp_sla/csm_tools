
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
var xml2js = require('xml2js'); // Node module to parse XML schema into json
var $RefParser = require('json-schema-ref-parser'); // Node module which de-referecnes a json schema to one single schema
const convert = require('xml-js'); 
const optionsConvert = {object: false, ignoreComment: true, arrayNotation: true, trim: true};

var globalServiceName; 
var globalWriteStream; // Output file

var globalDefaultMap = new Map ( // Defaults for some common attributes; only used if data is not provided in the conversion process; used for json and SOAP
[ // Order is important in this list!!!
	// Columns line up with csv columns which are output
 	// endswith Name : 	{ type, 		coreDataType,			jsonType, 		cardinality,	validation rule,	sample data ,	description}
 	[ "code", 				['', 		'Code. Type', 			'Code',			'', 			'',				'token',		''					]],
 	[ "id", 				['', 		'Identifier. Type', 	'Identifier',					'', 			'',				'123',			''					]],
 	[ "name", 				['', 		'Text. Type', 			'String',		'', 			'',				'',				''					]],
 	[ "value", 				['', 		'Code. Type', 			'Code',			'', 			'',				'',				''					]],
  	[ "unitcode", 			['', 		'Text. Type', 			'String',		'', 			'',				'token',		'The unit of the quantity or measure'	]],
	//alphabetic below this
 	[ "@actioncode", 		['', 		'Text. type', 			'String',		'',				'',				'ADD',			''					]],
 	[ "amount", 			['',	 	'Amount. Type', 		'Number',		'', 			'',				'123.00',		'The Amount'		]],
 	[ "agencyname", 		['', 		'Text. type', 			'String',		'', 			'',				'Vodafone',		'The name of the agency'	]],
 	[ "aristocraticTitle", 	['', 		'Text. type', 			'String',		'', 			'',				'Sir',			'Aristocratic Title e.g. Duke, Knight'	]],
 	[ "binaryobject", 		['',	 	'BinaryObject. Type',	'base64Binary',	'', 			'',				'123.00',		'The Amount'		]],
 	[ "bloodtype", 			['', 		'Text. type', 			'String',		'', 			'',				'B Neg.',		''					]],
	[ "content", 			['', 		'Text. Type', 			'String',		'', 			'',				'Free text',	''					]],
	[ "code.value", 		['', 		'Code. Type', 			'String',		'', 			'',				'token',		''					]],
 	[ "currencyid", 		['', 		'Text. Type', 			'String',		'', 			'ISO4127',		'EUR',			'The currency'					]],
 	[ "date", 				['', 		'Date. Type', 			'Date',			'', 			'',				'2018-05-06',	''					]],
 	[ "datestring", 		['', 		'DateTime. Type', 		'DateTime',		'', 			'ISO8601',		'2018-05-06',	''					]],
 	[ "datestring.format", 	['', 		'Date. Format. Text',	'Date',			'', 			'',				'ISO8601',		''					]],
 	[ "desc", 				['', 		'Text. Type', 			'String',		'', 			'',				'Free text',	''					]],
 	[ "description", 		['', 		'Text. Type', 			'String',		'', 			'',				'Free text',	''					]],
 	[ "duration", 			['', 		'Quantity. Type', 		'Number',		'', 			'',				'321',			''					]],
 	[ "familyname", 		['', 		'Text. type', 			'String',		'', 			'',				'Smith',		'The family name of the person'		]],
 	[ "firstname", 			['', 		'Text. type', 			'String',		'', 			'',				'John',			'The first name of the person'			]],
 	[ "format", 			['', 		'Text. type', 			'String',		'', 			'',				'',				'The format of the content'			]],
 	[ "formattedname", 		['', 		'Text. type', 			'String',		'', 			'',				'Smith',		'The formatted name of the person'		]],
 	[ "gender", 			['', 		'Text. Type', 			'String',		'', 			'',				'male',			'The gender of the person'		]],
 	[ "href", 				['', 		'Text. Type', 			'String',		'', 			'',				''	,			''					]],
 	[ "id.value", 			['', 		'Identifier. Type', 	'Identifier',	'', 			'',				'123',			''					]],
 	[ "id[*].value", 		['', 		'Identifier. Type', 	'Identifier',	'', 			'',				'123',			''					]],
 	[ "indicator", 			['', 		'Indicator. Type', 		'Boolean',		'', 			'',				'TRUE',			'A flag indicator'		]],
 	[ "indicatorstring", 	['', 		'Indicator. Type', 		'Boolean',		'', 			'',				'TRUE',			''					]],
 	[ "language", 			['', 		'Text. Type', 			'String',		'', 			'',				'en',			''					]],
 	[ "legalname", 			['', 		'Text. type', 			'String',		'', 			'',				'Joesph Smith',	'Legal Name by which the person is addressed'		]],
 	[ "maritalstatus", 		['', 		'Text. Type', 			'String',		'', 			'',				'single',		'The martial status of the person'		]],
 	[ "measure.value", 		['', 		'Measure. Type',		'Number',		'', 			'',				'',				''					]],
 	[ "measure.unitcode", 	['', 		'Measure. Unit. Code',	'Code',			'', 			'',				'',				'The unit of the measure'		]],
 	[ "middlename", 		['', 		'Text. Type', 			'String',		'', 			'',				'Joe',			'The middle name of the person'		]],
 	[ "nationality", 		['', 		'Text. Type', 			'String',		'', 			'',				'British',		'The nationality of the subject'		]],
 	[ "number", 			['', 		'Numeric. Type',		'Number',		'', 			'',				'123',			''					]],
 	[ "preferredgivenname", ['', 		'Text. type', 			'String',		'', 			'',				'Joe',			'Name by which the person prefers to be addressed'	]],
 	[ "price.value", 		['', 		'Amount. Type', 		'Number',		'', 			'',				'123.00',		''					]],
 	[ "quantity.value", 	['', 		'Quantity. Type', 		'Number',		'', 			'',				'321'	,		''					]],
 	[ "rating", 			['', 		'Numeric. Type',		'Number',		'', 			'',				'456',			''					]],
 	[ "salutation", 		['', 		'Text. type', 			'String',		'', 			'',				'Mr',			'Way the person is addressed e.g. Honorable etc'			]],
 	[ "schemeagencyname", 	['', 		'Text. type', 			'String',		'', 			'',				'Vodafone',		'The name of the agency that maintains the identification scheme.'	]],
 	[ "schemeid", 			['', 		'Identifier. Type', 	'Identifier',	'', 			'',				'Vodafone01',	'The identification of the identification scheme'			]],
 	[ "schemename", 		['', 		'Text. type', 			'String',		'', 			'',				'Vodafone',		'The name of the identification scheme'			]],
 	[ "score", 				['', 		'Numeric. Type',		'Number',		'', 			'',				'400',			''					]],
 	[ "size.value", 		['', 		'Quantity. Type', 		'Number',		'', 			'',				'234',			''					]],
 	[ "status", 			['', 		'Code. Type', 			'CodeType',		'', 			'',				'token',		''					]],
 	[ "status.value", 		['', 		'Code. Type', 			'CodeType',		'', 			'',				'Active',		''					]],
 	[ "surname", 			['', 		'Text. type', 			'String',		'', 			'',				'Smith',		'The surname of the person'	]],
 	[ "text", 				['', 		'Text. Type', 			'String',		'', 			'',				'',				''					]],
 	[ "thresold", 			['', 		'Numeric. Type',		'Number',		'', 			'',				'123',			''					]],
 	[ "time", 				['', 		'DateTime. Type', 		'DateTime',		'', 			'ISO8601',		'2015-04-01T00:00:00',	''			]],
 	[ "type.value", 		['', 		'Code. Type', 			'CodeType',		'', 			'',				'token',		''					]],
 	[ "version", 			['', 		'Text. Type', 			'String',		'', 			'',				'1.1',			''					]],
 	[ "verified", 			['', 		'Boolean. Type', 		'Boolean',		'', 			'',				'TRUE',			''					]],
 ]);

var globalAttributeArray =  
	// Hard to identify attributes from fields so this is a list of known attributes which are set as Attributes in the output
	// Taken from common basic components - 'name' has been left out deliberately
	// Used for json and XML/SOAP
[
	'actionCode', 'characteristicName', 'characterSetCode', 'currencyID', 'currencyCodeListVersionID', 'dateStringformat',
	'encodingCode', 'exponent', 'filename', 'format', 
	'languageID', 'languageLocaleID', 'listAgencyID', 'listAgencyName', 
	'listID', 'listHierarchyID', 'listHierarchyId',  'listName', 'listSchemeURI', 'listURI', 'listVersionID', 
	'mimeCode',
	'relationshipLocationCode','relationshipTypeCode', 'relationshipOrganisationCode',   
	'schemeAgencyID', 'schemeAgencyName', 'schemeName', 'schemeDataURI', 'schemeID', 'schemeURI', 'schemeVersionID',  
	'unitCode', 'unitCodeListID', 'unitCodeListAgencyID', 'unitCodeListAgencyName', 'unitCodeListVersionID', 'uri',
	'value'
] ;



/**
 * Reads a XSD file and returns it as a Json object
 * @param {string} filename - The name of the wadl file.
 * @return {Object} 
 */
function openXSDFile(filename){
    var json = null;
    var file = path.resolve(process.cwd(), filename);

        try {
            json = JSON.parse(convert.xml2json(fs.readFileSync(file).toString().trim(), optionsConvert));
        } 
        catch(JSONerr) {
                fns.logFatal('error 102: file not found ' + filename + ' ' + JSONerr.message);
        }

    return json;
}


function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, ""); // removes extension .swagger
	serviceName = serviceName.replace( /\.[^/\\.]+$/, ""); // removes extension . flatten
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}

function getLastName(name, isJson){
	//get the last part of a name, for json get from the last '.'; for XML get from last '/'
	// For example "/BankAccountVBO/Categories/Category" will return "Category"
	var nameParts;
	if(isJson) {
		nameParts = name.split('.');
	}
	else { // XML
		nameParts = name.split('/');
	}
	var lastPart = nameParts.pop();
	return lastPart;
}

function setColumnType (name, type, isJson) {
	// Set a column to be attribute or field by doing a look up of known attributes in globalAttributeArray
	var isXML = !(isJson);
	var columnType= type;
	var lastNamePart = getLastName(name, isJson);
	if(globalAttributeArray.includes(lastNamePart)) {
		columnType = 'Attribute';
	}
	else if(type == 'Attribute') { // Default any other Attributes to Field as these are 'guesses'
		columnType = 'Field';
	}
	// if the name ends with /name then set type to attribute
	if(isXML && ( (name.endsWith("/name"))  )) {
		columnType = 'Attribute';
	}
	// if the name ends with Extension then set type to Tag
	if(isXML && ( (name.endsWith("/Extension"))  )) {
		columnType = 'Tag';
	}

	return columnType;
}

function outputRow(text) {
	// Write to the file
	globalWriteStream.write(text + '\r\n');
}

function generateAttribute (name, type, jsonType, cardinality, description, isJson) {
	// Generate a single csv line made up of:
	//		name, columnType, coreDataType, jsonType, cardinality, validationRule, sampleData, description
	// Same call used for Json and XML
	var isXML = !(isJson);
	//console.log(name);
	var desc = ''; //description column
	var columnType = type; //type column (i.e. Attribute or Field or Tag or Collection or Object)
	var coreDataType = ' '; // core data type column
	var validationRule = ' ';
	var sampleData = '';
	// Set Description column
	if(description) { // remove commas and new lines
		desc = description.replace(/,/g , " ").replace(/\r?\n|\r/g, ' ');
	}
	// Set Column type
	columnType = setColumnType(name, columnType, isJson);

	// Set name 
	// Add @ to names of SOAP/XML arribute names
	if(isXML && (columnType=='Attribute')) {
		var nameParts = name.split('/');
		var lastPart = nameParts.pop();
		name = nameParts.join('/') + '/@' + lastPart;
	}
	var nameLower = name.toLowerCase();

	// Set values Using the values from the GlobalMap unless the values have been passed in
	globalDefaultMap.forEach((value, key) => {;
		if(nameLower && (nameLower.endsWith(key)) ) {
			// Apply dafaults from global data
			if(value[0] != ''){
				// Set type
				columnType = value[0];
			}
			if(value[1] != ''){
				// Set core data type
				coreDataType = value[1];
			}
			if(value[2] != ''){
				// Set json type
				jsonType = value[2];
			}
			if(value[3] != ''){
				// Set cardinality
				cardinality = value[3];
			}
			if(value[4] != ''){
				// Set validation
				validationRule = value[4];
			}
			if(value[5] != ''){
				// Set sample data
				sampleData = value[5];
			}
			if((value[6] != '') && (desc =='')){
				// Set description
				desc = value[6];
			}
		}
	});
	// if the type is a Collection or Tag set the coreDataType and jsonType to be empty
	if((columnType=='Collection') || (columnType=='Tag')) {
		coreDataType = ' ';
		jsonType = ' ';
	}

	// From description grab part of the path as the sample data - defaults the description!
	if( isJson && ((nameLower.endsWith('description')) ||   (nameLower.endsWith('desc'))) ) {
		sampleData = name.split('.').slice(0, -1).join('.'); //remove .description
		var path = require('path');
		sampleData = path.extname(sampleData).replace('[*]', '').replace('.', ''); // Set it to the last name part
	}
	// Add double quotes to sample data
	if(sampleData != '') sampleData = "'" + sampleData + "'";
	// Start with an empty column
	//outputRow(' ,' + name + ',' + columnType + ',' + coreDataType + ',' + jsonType + ',' + cardinality + ',' + 
	//				validationRule + ',' + sampleData + ',' + desc );
	outputRow(' ,' + name + ',' + columnType + ', '  + jsonType + ',' + cardinality + ',' + desc );
}


function generateJsonCSV (schemaJson, currentName, requiredList){
	// Traverse the json schema and output a csv row for each attribute
	//console.log('NAME=',currentName);
	//console.log(schemaJson.properties);
	var properties; // Grab the properties
	if (schemaJson.properties) 
		properties = Object.entries(schemaJson.properties);
	else if( (schemaJson.items) && (schemaJson.items.properties)) { // Sometimes extra layer has been added so ignore it
		properties = Object.entries(schemaJson.items.properties);
	}

	if(properties) {
		properties.forEach(function(property, value){ // Traverse each property looking for attributes

			// Identify any required attributes - applied in next iteration so will be passed on in recursive calls
			var required = [];
			if(property[1] && property[1].required) {
				required = property[1].required;
			}
			// Identify the cardinality
			var card = '0'; //defaults to zero
			if(property[1].minItems && (property[1].minItems == 1)){
				card = '1';
			}
			if(requiredList && (requiredList.length > 0) && (requiredList.indexOf(property[0]) > -1) ) {
				// If current property is in the required list passed from previous iteration then set card to 1
				card = '1';
			}
			// Process each property but ignore _links
			if(property[0] && (!(property[0].endsWith('_links')) ) ) { //Ignore _link
				switch (property[1].type) {
					case 'array':
						// Output an array entry
						generateAttribute(currentName + '.' + property[0] +  '[*]', 'Collection', 'Array', card + '..*', property[1].description, true);
						// Process array properties
						var arrayProperties;
						if((property[0]=='_links') && (property[1].items) && (property[1].items.additionalProperties) &&  
							(property[1].items.additionalProperties.oneOf)) { // Not called at present due to outer if-stmt
							arrayProperties = property[1].items.additionalProperties.oneOf;
							if(arrayProperties.required) {// Add required
									required = arrayProperties.required;
								}
							arrayProperties.forEach(function(arrayProperty){
									generateJsonCSV(arrayProperty, currentName + '.' + property[0] +'[*]', required);
								});
						}
						else if((property[1].items) && (property[1].items.allOf)) { // List of items - allOf
						 	arrayProperties = property[1].items.allOf;
						 	
							arrayProperties.forEach(function(arrayProperty){
									if(arrayProperty.required) {// Add required
										required = arrayProperty.required;
									}
									generateJsonCSV(arrayProperty, currentName + '.' + property[0] +'[*]', required);
								});
						} 
						else if ((property[1].items) && (property[1].items.properties)) { // Single object type
						 	arrayProperties = property[1].items;
						 	if(arrayProperties.required) {// Add required
								required = arrayProperties.required;
							}
							generateJsonCSV(arrayProperties, currentName + '.' + property[0] +'[*]', required);
						}
						else if ((property[1].items) && (property[1].items.items) && (property[1].items.items.properties)) { // Array of arrays
						 	arrayProperties = property[1].items;
						 	if(arrayProperties.required) {// Add required
								required = arrayProperties.required;
							}
							generateJsonCSV(arrayProperties, currentName + '.' + property[0] +'[*]', required);
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
						}
						break;
					case 'object' : 
						// Process an object 
						var objProperties;
						if((property[1]) && (property[1].allOf)) { // List of items - allOf
							// Output an object entry
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', property[1].description, true );
						 	objProperties = property[1].allOf;
							objProperties.forEach(function(objProp){
									if(objProp.required) {// Add required
										required = objProp.required;
									}
									generateJsonCSV(objProp, currentName + '.' + property[0], required);
								});
						} 
						else if (property[1].properties) { // Single object type
							// Output an array entry
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', property[1].description, true );
						 	objProperties = property[1];
						 	if(objProperties.required) {// Added required
								required = property[1].required;
							}
							generateJsonCSV(objProperties, currentName + '.' + property[0], required );
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
						}
						break;
					case 'string' :
						generateAttribute(currentName + '.' + property[0], 'Field', 'String', card +  '..1', property[1].description, true ); 
						break;
					case 'boolean' : 
						generateAttribute(currentName + '.' + property[0], 'Field', 'Boolean', card +  '..1', property[1].description, true );
						break;
					case 'number' : 
						generateAttribute(currentName + '.' + property[0], 'Field', 'Number', card +  '..1', property[1].description, true );
						break;
					default:
						if(property[0] == 'href') {
							generateAttribute(currentName + '.' + property[0], 'Field', 'String', card + '..1', property[1].description, true );
						}
						else if(property[1] && property[1].properties) {
							// Should probably be an object
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', '', true );
							objProperties = property[1];
							if(property[1].required) {// Added required
								required = property[1].required;
							}
							generateJsonCSV(objProperties, currentName + '.' + property[0], required );
						}
						else if(property[1] && property[1].allOf) {
							// Should probably be an Object
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', '', true );
							//fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
							objProperties = property[1].allOf;
							objProperties.forEach(function(key){
									if(key.required) {// Add required
										required = key.required;
									}
									generateJsonCSV(key, currentName + '.' + property[0], required);
								});
						}
						else {
							generateAttribute(currentName + '.' + property[0], 'Field', property[1].type, card + '..1', property[1].description, true );
							fns.logWarn("Defaulted this entry:" + property[0]);
						}
						break;
				}
			}
		});
	}
	else {
			fns.logWarn("No entries for:" + currentName);
	}

}

function findVBO(schema, vboName) {
	// Traverse the schema searching for VBO
	let result;
	if(schema.definitions) {
		// Found definitions
		let defs = schema.definitions;
		// set the first character to be lower
		let name = vboName.charAt(0).toLowerCase() + vboName.substring(1);
		result = defs[name];
		// Check the vbo was found 
		if ( typeof result !== 'undefined' && result ) {
			// Success
		}
		else {
			// set the first character to be Upper
			name= vboName.charAt(0).toUpperCase() + vboName.substring(1);
			result = defs[name];
		}
	}
	else {
		fns.logFatal(vboName + " not found in definitions");
	}
	return result;
}

function getSchemaVersion(schema) {
	let result;
	if(schema.info && schema.info.version) {
		result = schema.info.version;
	}
	else {
		fns.logWarn("Version not defined under 'info'");
	}
	return result;
}

function outputSchemaVersion(schema){
	// Output the schema version to the csv
	let version = getSchemaVersion(schema);
	fns.logHeading(globalServiceName + "  version: " + version);
	outputRow(" , " + globalServiceName + "  version: " + version);
	outputRow(" , " );
}

function outputHeadings() {
    outputRow(' ,Path, Type, json Type, Cardinality, Description ');
    outputRow(' ,$,Object, 1..1');
}

/**

 * Generate csv. from flat swagger file
 * @param {string} fileName 
 */
function generateCSVFromFlatSwagger (fileName) {
	
	fns.logHeading("CSV Generation from Flat Swagger");
	//Read json schme file
	var jsonSchema = utils.loadFile(fileName);

	//set service name
	globalServiceName=getServiceName(fileName);

	//Create an output file
	var outputFileName = fileName + '.csv'; 

	globalWriteStream = fs.createWriteStream(outputFileName);
	globalWriteStream.on('error', 
		function(err) { 
			// Error
        fns.logError("Error: ");
        console.log(err);
        fns.logError("If permission denied make sure the generated file is closed.") 
		});

    // Generate based on a dereferenced schema
	$RefParser.dereference(fileName)
    .then(function(schemaResult) {
        // Success
        // Find the VBO definition and use it as the basis for the generation
        let vbo = findVBO(schemaResult, globalServiceName + "VBO");
		// Get the schema version
		outputSchemaVersion(schemaResult);
		outputHeadings();
        generateJsonCSV(vbo, '$');
        fns.logHeading("Success: file " +  outputFileName + " generated.");
    });


}



module.exports = {

  generateCSVFromJsonSchema: generateCSVFromFlatSwagger
};


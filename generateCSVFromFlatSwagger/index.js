#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var generateCSV = require("./generateFromSwagggerCSV.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('generateJSONCSV' in cmdArgs)
{
    generateCSV.generateCSVFromJsonSchema(cmdArgs['generateJSONCSV']);
}
else
{
    cmdObj.printHelp();
}

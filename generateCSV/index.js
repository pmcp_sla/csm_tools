#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var generateCSV = require("./generateCSV.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('generateJSONCSV' in cmdArgs)
{
    generateCSV.generateJson(cmdArgs['generateJSONCSV']);
}
else if ('generateXMLCSV' in cmdArgs)
{
    generateCSV.generateXML(cmdArgs['generateXMLCSV'], cmdArgs['extensionFile'], cmdArgs['messageFile']);
}
else
{
    cmdObj.printHelp();
}

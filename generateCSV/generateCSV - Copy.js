
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
var xml2js = require('xml2js'); // Node module to parse XML schema into json
var $RefParser = require('json-schema-ref-parser'); // Node module which de-referecnes a json schema to one single schema
const convert = require('xml-js'); 
const optionsConvert = {object: false, ignoreComment: true, arrayNotation: true, trim: true};

//var globalCommonDirName = process.env.SOAP_COMMON + "/V1/"; // Taken from external declarations file!
var globalCommonDirName ;
var globalCommonTypes;
// Schemas to be processed
var globalCommonSchema; // SOAP common schema
var globalCommonTypeSchema; // SOAP common types schema
var globalExtensionSchema;	// SOAP extension schema
var globalMessageSchema;	// SOAP VBM schema
var globalOriginalSchema; // SOAP original input schema

var globalServiceName; 
var globalWriteStream; // Output file

var globalDefaultMap = new Map ( // Defaults for some common attributes; only used if data is not provided in the conversion process; used for json and SOAP
[ // Order is important in this list!!!
	// Columns line up with csv columns which are output
 	// endswith Name : 	{ type, 		coreDataType,			jsonType, 		cardinality,	validation rule,	sample data ,	description}
 	[ "code", 				['', 		'Code. Type', 			'Code',			'', 			'',				'token',		''					]],
 	[ "id", 				['', 		'Identifier. Type', 	'Identifier',					'', 			'',				'123',			''					]],
 	[ "name", 				['', 		'Text. Type', 			'String',		'', 			'',				'',				''					]],
 	[ "value", 				['', 		'Code. Type', 			'Code',			'', 			'',				'',				''					]],
  	[ "unitcode", 			['', 		'Text. Type', 			'String',		'', 			'',				'token',		'The unit of the quantity or measure'	]],
	//alphabetic below this
 	[ "@actioncode", 		['', 		'Text. type', 			'String',		'',				'',				'ADD',			''					]],
 	[ "amount", 			['',	 	'Amount. Type', 		'Number',		'', 			'',				'123.00',		'The Amount'		]],
 	[ "agencyname", 		['', 		'Text. type', 			'String',		'', 			'',				'Vodafone',		'The name of the agency'	]],
 	[ "aristocraticTitle", 	['', 		'Text. type', 			'String',		'', 			'',				'Sir',			'Aristocratic Title e.g. Duke, Knight'	]],
 	[ "binaryobject", 		['',	 	'BinaryObject. Type',	'base64Binary',	'', 			'',				'123.00',		'The Amount'		]],
 	[ "bloodtype", 			['', 		'Text. type', 			'String',		'', 			'',				'B Neg.',		''					]],
	[ "content", 			['', 		'Text. Type', 			'String',		'', 			'',				'Free text',	''					]],
	[ "code.value", 		['', 		'Code. Type', 			'String',		'', 			'',				'token',		''					]],
 	[ "currencyid", 		['', 		'Text. Type', 			'String',		'', 			'ISO4127',		'EUR',			'The currency'					]],
 	[ "date", 				['', 		'Date. Type', 			'Date',			'', 			'',				'2018-05-06',	''					]],
 	[ "datestring", 		['', 		'DateTime. Type', 		'DateTime',		'', 			'ISO8601',		'2018-05-06',	''					]],
 	[ "datestring.format", 	['', 		'Date. Format. Text',	'Date',			'', 			'',				'ISO8601',		''					]],
 	[ "desc", 				['', 		'Text. Type', 			'String',		'', 			'',				'Free text',	''					]],
 	[ "description", 		['', 		'Text. Type', 			'String',		'', 			'',				'Free text',	''					]],
 	[ "duration", 			['', 		'Quantity. Type', 		'Number',		'', 			'',				'321',			''					]],
 	[ "familyname", 		['', 		'Text. type', 			'String',		'', 			'',				'Smith',		'The family name of the person'		]],
 	[ "firstname", 			['', 		'Text. type', 			'String',		'', 			'',				'John',			'The first name of the person'			]],
 	[ "format", 			['', 		'Text. type', 			'String',		'', 			'',				'',				'The format of the content'			]],
 	[ "formattedname", 		['', 		'Text. type', 			'String',		'', 			'',				'Smith',		'The formatted name of the person'		]],
 	[ "gender", 			['', 		'Text. Type', 			'String',		'', 			'',				'male',			'The gender of the person'		]],
 	[ "href", 				['', 		'Text. Type', 			'String',		'', 			'',				''	,			''					]],
 	[ "id.value", 			['', 		'Identifier. Type', 	'Identifier',	'', 			'',				'123',			''					]],
 	[ "id[*].value", 		['', 		'Identifier. Type', 	'Identifier',	'', 			'',				'123',			''					]],
 	[ "indicator", 			['', 		'Indicator. Type', 		'Boolean',		'', 			'',				'TRUE',			'A flag indicator'		]],
 	[ "indicatorstring", 	['', 		'Indicator. Type', 		'Boolean',		'', 			'',				'TRUE',			''					]],
 	[ "language", 			['', 		'Text. Type', 			'String',		'', 			'',				'en',			''					]],
 	[ "legalname", 			['', 		'Text. type', 			'String',		'', 			'',				'Joesph Smith',	'Legal Name by which the person is addressed'		]],
 	[ "maritalstatus", 		['', 		'Text. Type', 			'String',		'', 			'',				'single',		'The martial status of the person'		]],
 	[ "measure.value", 		['', 		'Measure. Type',		'Number',		'', 			'',				'',				''					]],
 	[ "measure.unitcode", 	['', 		'Measure. Unit. Code',	'Code',			'', 			'',				'',				'The unit of the measure'		]],
 	[ "middlename", 		['', 		'Text. Type', 			'String',		'', 			'',				'Joe',			'The middle name of the person'		]],
 	[ "nationality", 		['', 		'Text. Type', 			'String',		'', 			'',				'British',		'The nationality of the subject'		]],
 	[ "number", 			['', 		'Numeric. Type',		'Number',		'', 			'',				'123',			''					]],
 	[ "preferredgivenname", ['', 		'Text. type', 			'String',		'', 			'',				'Joe',			'Name by which the person prefers to be addressed'	]],
 	[ "price.value", 		['', 		'Amount. Type', 		'Number',		'', 			'',				'123.00',		''					]],
 	[ "quantity.value", 	['', 		'Quantity. Type', 		'Number',		'', 			'',				'321'	,		''					]],
 	[ "rating", 			['', 		'Numeric. Type',		'Number',		'', 			'',				'456',			''					]],
 	[ "salutation", 		['', 		'Text. type', 			'String',		'', 			'',				'Mr',			'Way the person is addressed e.g. Honorable etc'			]],
 	[ "schemeagencyname", 	['', 		'Text. type', 			'String',		'', 			'',				'Vodafone',		'The name of the agency that maintains the identification scheme.'	]],
 	[ "schemeid", 			['', 		'Identifier. Type', 	'Identifier',	'', 			'',				'Vodafone01',	'The identification of the identification scheme'			]],
 	[ "schemename", 		['', 		'Text. type', 			'String',		'', 			'',				'Vodafone',		'The name of the identification scheme'			]],
 	[ "score", 				['', 		'Numeric. Type',		'Number',		'', 			'',				'400',			''					]],
 	[ "size.value", 		['', 		'Quantity. Type', 		'Number',		'', 			'',				'234',			''					]],
 	[ "status", 			['', 		'Code. Type', 			'CodeType',		'', 			'',				'token',		''					]],
 	[ "status.value", 		['', 		'Code. Type', 			'CodeType',		'', 			'',				'Active',		''					]],
 	[ "surname", 			['', 		'Text. type', 			'String',		'', 			'',				'Smith',		'The surname of the person'	]],
 	[ "text", 				['', 		'Text. Type', 			'String',		'', 			'',				'',				''					]],
 	[ "thresold", 			['', 		'Numeric. Type',		'Number',		'', 			'',				'123',			''					]],
 	[ "time", 				['', 		'DateTime. Type', 		'DateTime',		'', 			'ISO8601',		'2015-04-01T00:00:00',	''			]],
 	[ "type.value", 		['', 		'Code. Type', 			'CodeType',		'', 			'',				'token',		''					]],
 	[ "version", 			['', 		'Text. Type', 			'String',		'', 			'',				'1.1',			''					]],
 	[ "verified", 			['', 		'Boolean. Type', 		'Boolean',		'', 			'',				'TRUE',			''					]],
 ]);

var globalAttributeArray =  
	// Hard to identify attributes from fields so this is a list of known attributes which are set as Attributes in the output
	// Taken from common basic components - 'name' has been left out deliberately
	// Used for json and XML/SOAP
[
	'actionCode', 'characteristicName', 'characterSetCode', 'currencyID', 'currencyCodeListVersionID', 'dateStringformat',
	'encodingCode', 'exponent', 'filename', 'format', 
	'languageID', 'languageLocaleID', 'listAgencyID', 'listAgencyName', 
	'listID', 'listHierarchyID', 'listHierarchyId',  'listName', 'listSchemeURI', 'listURI', 'listVersionID', 
	'mimeCode',
	'relationshipLocationCode','relationshipTypeCode', 'relationshipOrganisationCode',   
	'schemeAgencyID', 'schemeAgencyName', 'schemeName', 'schemeDataURI', 'schemeID', 'schemeURI', 'schemeVersionID',  
	'unitCode', 'unitCodeListID', 'unitCodeListAgencyID', 'unitCodeListAgencyName', 'unitCodeListVersionID', 'uri',
	'value'
] ;



/**
 * Reads a XSD file and returns it as a Json object
 * @param {string} filename - The name of the wadl file.
 * @return {Object} 
 */
function openXSDFile(filename){
    var json = null;
    var file = path.resolve(process.cwd(), filename);

        try {
            json = JSON.parse(convert.xml2json(fs.readFileSync(file).toString().trim(), optionsConvert));
        } 
        catch(JSONerr) {
                fns.logFatal('error 102: file not found ' + filename + ' ' + JSONerr.message);
        }

    return json;
}


function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}

function getLastName(name, isJson){
	//get the last part of a name, for json get from the last '.'; for XML get from last '/'
	// For example "/BankAccountVBO/Categories/Category" will return "Category"
	var nameParts;
	if(isJson) {
		nameParts = name.split('.');
	}
	else { // XML
		nameParts = name.split('/');
	}
	var lastPart = nameParts.pop();
	return lastPart;
}

function setColumnType (name, type, isJson) {
	// Set a column to be attribute or field by doing a look up of known attributes in globalAttributeArray
	var isXML = !(isJson);
	var columnType= type;
	var lastNamePart = getLastName(name, isJson);
	if(globalAttributeArray.includes(lastNamePart)) {
		columnType = 'Attribute';
	}
	else if(type == 'Attribute') { // Default any other Attributes to Field as these are 'guesses'
		columnType = 'Field';
	}
	// if the name ends with /name then set type to attribute
	if(isXML && ( (name.endsWith("/name"))  )) {
		columnType = 'Attribute';
	}
	// if the name ends with Extension then set type to Tag
	if(isXML && ( (name.endsWith("/Extension"))  )) {
		columnType = 'Tag';
	}

	return columnType;
}

function outputRow(text) {
	// Write to the file
	globalWriteStream.write(text + '\r\n');
}

function generateAttribute (name, type, jsonType, cardinality, description, isJson) {
	// Generate a single csv line made up of:
	//		name, columnType, coreDataType, jsonType, cardinality, validationRule, sampleData, description
	// Same call used for Json and XML
	var isXML = !(isJson);
	//console.log(name);
	var desc = ''; //description column
	var columnType = type; //type column (i.e. Attribute or Field or Tag or Collection or Object)
	var coreDataType = ' '; // core data type column
	var validationRule = ' ';
	var sampleData = '';
	// Set Description column
	if(description) { // remove commas and new lines
		desc = description.replace(/,/g , " ").replace(/\r?\n|\r/g, ' ');
	}
	// Set Column type
	columnType = setColumnType(name, columnType, isJson);

	// Set name 
	// Add @ to names of SOAP/XML arribute names
	if(isXML && (columnType=='Attribute')) {
		var nameParts = name.split('/');
		var lastPart = nameParts.pop();
		name = nameParts.join('/') + '/@' + lastPart;
	}
	var nameLower = name.toLowerCase();

	// Set values Using the values from the GlobalMap unless the values have been passed in
	globalDefaultMap.forEach((value, key) => {;
		if(nameLower && (nameLower.endsWith(key)) ) {
			// Apply dafaults from global data
			if(value[0] != ''){
				// Set type
				columnType = value[0];
			}
			if(value[1] != ''){
				// Set core data type
				coreDataType = value[1];
			}
			if(value[2] != ''){
				// Set json type
				jsonType = value[2];
			}
			if(value[3] != ''){
				// Set cardinality
				cardinality = value[3];
			}
			if(value[4] != ''){
				// Set validation
				validationRule = value[4];
			}
			if(value[5] != ''){
				// Set sample data
				sampleData = value[5];
			}
			if((value[6] != '') && (desc =='')){
				// Set description
				desc = value[6];
			}
		}
	});
	// if the type is a Collection or Tag set the coreDataType and jsonType to be empty
	if((columnType=='Collection') || (columnType=='Tag')) {
		coreDataType = ' ';
		jsonType = ' ';
	}

	// From description grab part of the path as the sample data - defaults the description!
	if( isJson && ((nameLower.endsWith('description')) ||   (nameLower.endsWith('desc'))) ) {
		sampleData = name.split('.').slice(0, -1).join('.'); //remove .description
		var path = require('path');
		sampleData = path.extname(sampleData).replace('[*]', '').replace('.', ''); // Set it to the last name part
	}
	// Add double quotes to sample data
	if(sampleData != '') sampleData = "'" + sampleData + "'";
	// Start with an empty column
	//outputRow(' ,' + name + ',' + columnType + ',' + coreDataType + ',' + jsonType + ',' + cardinality + ',' + 
	//				validationRule + ',' + sampleData + ',' + desc );
	outputRow(' ,' + name + ',' + columnType + ',' + coreDataType + ','  + cardinality + ','  + desc );
}


function generateJsonCSV (schemaJson, currentName, requiredList){
	// Traverse the json schema and output a csv row for each attribute
	//console.log('NAME=',currentName);
	//console.log(schemaJson.properties);
	var properties; // Grab the properties
	if (schemaJson.properties) 
		properties = Object.entries(schemaJson.properties);
	else if( (schemaJson.items) && (schemaJson.items.properties)) { // Sometimes extra layer has been added so ignore it
		properties = Object.entries(schemaJson.items.properties);
	}

	if(properties) {
		properties.forEach(function(property, value){ // Traverse each property looking for attributes

			// Identify any required attributes - applied in next iteration so will be passed on in recursive calls
			var required = [];
			if(property[1] && property[1].required) {
				required = property[1].required;
			}
			// Identify the cardinality
			var card = '0'; //defaults to zero
			if(property[1].minItems && (property[1].minItems == 1)){
				card = '1';
			}
			if(requiredList && (requiredList.length > 0) && (requiredList.indexOf(property[0]) > -1) ) {
				// If current property is in the required list passed from previous iteration then set card to 1
				card = '1';
			}
			// Process each property but ignore _links
			if(property[0] && (!(property[0].endsWith('_links')) ) ) { //Ignore _link
				switch (property[1].type) {
					case 'array':
						// Output an array entry
						generateAttribute(currentName + '.' + property[0] +  '[*]', 'Collection', 'Array', card + '..*', property[1].description, true);
						// Process array properties
						var arrayProperties;
						if((property[0]=='_links') && (property[1].items) && (property[1].items.additionalProperties) &&  
							(property[1].items.additionalProperties.oneOf)) { // Not called at present due to outer if-stmt
							arrayProperties = property[1].items.additionalProperties.oneOf;
							if(arrayProperties.required) {// Add required
									required = arrayProperties.required;
								}
							arrayProperties.forEach(function(arrayProperty){
									generateJsonCSV(arrayProperty, currentName + '.' + property[0] +'[*]', required);
								});
						}
						else if((property[1].items) && (property[1].items.allOf)) { // List of items - allOf
						 	arrayProperties = property[1].items.allOf;
						 	
							arrayProperties.forEach(function(arrayProperty){
									if(arrayProperty.required) {// Add required
										required = arrayProperty.required;
									}
									generateJsonCSV(arrayProperty, currentName + '.' + property[0] +'[*]', required);
								});
						} 
						else if ((property[1].items) && (property[1].items.properties)) { // Single object type
						 	arrayProperties = property[1].items;
						 	if(arrayProperties.required) {// Add required
								required = arrayProperties.required;
							}
							generateJsonCSV(arrayProperties, currentName + '.' + property[0] +'[*]', required);
						}
						else if ((property[1].items) && (property[1].items.items) && (property[1].items.items.properties)) { // Array of arrays
						 	arrayProperties = property[1].items;
						 	if(arrayProperties.required) {// Add required
								required = arrayProperties.required;
							}
							generateJsonCSV(arrayProperties, currentName + '.' + property[0] +'[*]', required);
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
						}
						break;
					case 'object' : 
						// Process an object 
						var objProperties;
						if((property[1]) && (property[1].allOf)) { // List of items - allOf
							// Output an object entry
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', property[1].description, true );
						 	objProperties = property[1].allOf;
							objProperties.forEach(function(objProp){
									if(objProp.required) {// Add required
										required = objProp.required;
									}
									generateJsonCSV(objProp, currentName + '.' + property[0], required);
								});
						} 
						else if (property[1].properties) { // Single object type
							// Output an array entry
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', property[1].description, true );
						 	objProperties = property[1];
						 	if(objProperties.required) {// Added required
								required = property[1].required;
							}
							generateJsonCSV(objProperties, currentName + '.' + property[0], required );
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
						}
						break;
					case 'string' :
						generateAttribute(currentName + '.' + property[0], 'Field', 'String', card +  '..1', property[1].description, true ); 
						break;
					case 'boolean' : 
						generateAttribute(currentName + '.' + property[0], 'Field', 'Boolean', card +  '..1', property[1].description, true );
						break;
					case 'number' : 
						generateAttribute(currentName + '.' + property[0], 'Field', 'Number', card +  '..1', property[1].description, true );
						break;
					default:
						if(property[0] == 'href') {
							generateAttribute(currentName + '.' + property[0], 'Field', 'String', card + '..1', property[1].description, true );
						}
						else if(property[1] && property[1].properties) {
							// Should probably be an object
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', '', true );
							objProperties = property[1];
							if(property[1].required) {// Added required
								required = property[1].required;
							}
							generateJsonCSV(objProperties, currentName + '.' + property[0], required );
						}
						else if(property[1] && property[1].allOf) {
							// Should probably be an Object
							generateAttribute(currentName + '.' + property[0], 'Field', 'Object', card + '..1', '', true );
							//fns.logWarn("Check json for this entry - assumed to be an object: " + property[0]);
							objProperties = property[1].allOf;
							objProperties.forEach(function(key){
									if(key.required) {// Add required
										required = key.required;
									}
									generateJsonCSV(key, currentName + '.' + property[0], required);
								});
						}
						else {
							generateAttribute(currentName + '.' + property[0], 'Field', property[1].type, card + '..1', property[1].description, true );
							fns.logWarn("Defaulted this entry:" + property[0]);
						}
						break;
				}
			}
		});
	}
	else {
			fns.logWarn("No entries for:" + currentName);
	}

}

/**
 * Generate csv. from json
 * @param {string} schemaFileName 
 */
function generateCSVFromJsonSchema (schemaFileName) {
	
	fns.logHeading("JSON CSV Generation");
	//Read json schme file
	var jsonSchema = utils.loadFile(schemaFileName);

	//set service name
	globalServiceName=getServiceName(schemaFileName);


	fns.logHeading(globalServiceName);

	//Create an output file
	var outputFileName = schemaFileName.split('.').slice(0, -1).join('.') + '_JSON.csv'; //remove extension

	globalWriteStream = fs.createWriteStream(outputFileName);
	globalWriteStream.on('error', 
		function(err) { 
			// Error
        fns.logError("Error: ");
        console.log(err);
        fns.logError("If permission denied make sure the generated file is closed.") 
		});
	fns.logHeading("Generating: " + outputFileName);

    outputRow(' ,Path, Type, Core Data Type, JSON Type, JSON Cardinality,	Validation Rule, Sample Data, Description');
    outputRow(' ,$,Tag, ,Object,1..1');

    // Generate based on a dereferenced schema
	$RefParser.dereference(schemaFileName)
    .then(function(schemaResult) {
        // Success
        generateJsonCSV(schemaResult, '$');
        fns.logHeading("Success: file " +  outputFileName + " generated.");
    })
    .catch(function(err) {
        // Error
        fns.logError("Error: ");
        console.log(err);
    });

}

function search(name, schema){
	//Search for the type called name in the XML schema and return the result
	var found = Object.keys(schema).filter(function(key) {
			if (schema[key].attributes && schema[key].attributes.name) {
	    		return schema[key].attributes.name == name;
	    	}
	    	else
	    		return false;
		// to cast back from an array of keys to the object, with just the passing ones
		}).reduce(function(obj, key){
		    obj[key] = schema[key];
		    return obj[key];
			}, {});
	return found;
}



function findType(typeName){
	// Find a type in an XML schema - searchs in all of the schemes below. Remember common is a join of several schemas
	var result;
	if(typeName.startsWith('extvbo:') && globalExtensionSchema) {
		// Search extension schema
		result = search(typeName.replace(/^(extvbo:)/,""), globalExtensionSchema);
	}
	else if(typeName.startsWith('cmn:')){
		// Search common 
		result = search(typeName.replace(/^(cmn:)/,""), globalCommonSchema);
	}
	else if(typeName.startsWith('tns:')){
		// Search current
		result = search(typeName.replace(/^(tns:)/,""), globalOriginalSchema);
	}
	else if(typeName.startsWith('cct:')){
		// Search common core type
		result = search(typeName.replace(/^(cct:)/,""), globalCommonTypeSchema);
	}
	else if(typeName.startsWith('ccts:')){
		// Search common core type
		result = search(typeName.replace(/^(ccts:)/,""), globalCommonTypeSchema);
	}
	if(isEmpty(result))  {
		// Search in original file, then extension and then common - this may not always work!!!!
		// Remove the type reference before ':'
		var index = typeName.indexOf(':');
		typeName = typeName.slice(index+1);
		// Search the original schema
		result = search(typeName, globalOriginalSchema);
		if(isEmpty(result)) {
			// Search the extension
			if(globalExtensionSchema) result = search(typeName, globalExtensionSchema);
			if(isEmpty(result)) {
				// Search common
				result = search(typeName, globalCommonSchema);
			}
		}
	}
	return result;
}

function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function getXMLDesc(current) {
	// Get the description text from the annotation
	var desc = '';
	if(current.elements && current.elements[0] && (current.elements[0].name == 'xsd:annotation') && current.elements[0].elements &&
		current.elements[0].elements[0] && current.elements[0].elements[0].elements && current.elements[0].elements[0].elements[0]){
		// Description can be at one of two levels
		var schema = current.elements[0].elements[0].elements[0];
		if(schema.elements && schema.elements[0]) {
			desc = schema.elements[0].text;
		}
		else
			desc = schema.text;
	}
	return desc;
}


function traverseXSD(current, currentName) {
	// Process the current XSD to generate csv row for each attribute
	//console.log(currentName, '-', current.name);
	switch (current.name) {
		case 'xsd:complexType' :
		case 'xsd:complexContent': 
		case 'xsd:simpleContent' :
		case 'xsd:simpleType' :
				if(current.elements) {
					current.elements.forEach(function(el){
						traverseXSD(el, currentName); 
					});
				}
				break;
		case 'xsd:choice' :
				if(current.elements) {
					current.elements.forEach(function(el){
						traverseXSD(el, currentName + '/[xsd:choice]');
				});
				}
				break;
		case 'xsd:extension' :
				var subSchema;
				if(current.attributes && current.attributes.base) {
					subSchema = findType(current.attributes.base);
					if(!(isEmpty(subSchema))) traverseXSD(subSchema, currentName);
					if(isEmpty(subSchema) && !(current.attributes.base.startsWith('xsd:')) ) {
							fns.logWarn('Extension Schema type not found ' +  current.attributes.base);
							//console.log(current);
					}
				}
				if(current.elements) {
					current.elements.forEach(function(el){
						traverseXSD(el, currentName); 
					});
				}
				break;
		case 'xsd:sequence':
				if(current.elements) {
					current.elements.forEach(function(el){
						traverseXSD(el, currentName); 
					});
				}
				break;
		case 'xsd:attribute':
		case 'attribute':
		case 'xsd:element':
		case 'element':
				// Get Name of attribute
				var newName = currentName;
				if(current.attributes && current.attributes.name) {
					newName = newName + '/' + current.attributes.name
				}
				// Get Cardinality; standard says this should default to 1 but ...
				var cardMin = 0;
				if(current.name.endsWith('element') ) cardMin = 1;
				if(current.attributes && current.attributes.minOccurs) {
					cardMin = current.attributes.minOccurs
				}
				var cardMax = 1;
				if(current.attributes && current.attributes.maxOccurs && (current.attributes.maxOccurs == 'unbounded')) {
					cardMax = '*';
				}
				// Get description
				var desc = getXMLDesc(current);
				//Get Type of attribute  and generate it as CSV
				var type = 'xsd:string';
				if(current.attributes && current.attributes.type) {
					if(current.attributes.type.startsWith('xsd:'))  {
						// Base type found
						type = current.attributes.type;
						generateAttribute(newName, 'Attribute', type, cardMin + '..' + cardMax, desc, false );
					}
					else {
						// Complex type so find it and then travser further for attributes
						subSchema = findType(current.attributes.type);
						if (desc=='') desc=getXMLDesc(subSchema); // Need to get the subSchema first as we need to get the description to pass to the attribute output
						generateAttribute(newName, 'Field', type, cardMin + '..' + cardMax, desc, false );
						if(isEmpty(subSchema)) {
							fns.logWarn('Schema type not found ' +  current.attributes.type);
							//console.log(current);
						}
						else {
							traverseXSD(subSchema, newName); 
						}

					}
				}
				else if(current.name.endsWith('element') && (cardMin != 1)) {
						generateAttribute(newName, 'Collection', type, cardMin + '..' + cardMax, desc, false );
					}
					else
						generateAttribute(newName, 'Field', type, cardMin + '..' + cardMax, desc, false );
				if(current.elements) { // Check for any remaining elements
					current.elements.forEach(function(el){
						traverseXSD(el, newName); 
					});
				}
				break;
		case 'xsd:annotation' : // Ignore
				break;
		case 'xsd:restriction' : // Ignore
				break;
		default:
				fns.logWarn('Unknown found ' +  current.attributes.type);
				console.log(current);
				break;
	}
}


function generateXSDCsv (mainSchema, currentName){
	// Generate CSV for an XSD
	mainSchema.forEach(function(current){
		//if it is a complextype then process as this is assumed to be the main type in the xsd
		if(current.name == 'xsd:complexType'){
			var name = current.attributes.name;
			if(name && name.toLowerCase() == globalServiceName.toLowerCase() +'type') {
				// Found the 'lead' element so traverse from there
				var desc = getXMLDesc(current);
				outputRow(' ,/' + globalServiceName + ' ,Business Object, Object ,1..1,'+ desc);
				if(current.elements) {
					current.elements.forEach(function(type) {
						if(type.type != 'xsd:annotation') {
							// Process the type
							traverseXSD(type, currentName);
						}
					});
				}
			}
		}
	})
}

function getSchema(name) {
	//Get the named schema
	var schema = openXSDFile(name).elements;
	if(isEmpty(schema)) {
		fns.logError("Schema " + name + " file not read");
		process.exit(1);
	}
	return schema[0].elements;

}

function createSingleCommonSchema (){
	// Join the schema for common based on several file names - makes it easier to search common types
	var schema = getSchema(globalCommonDirName + 'CommonComponents.xsd');
	schema = schema.concat(getSchema(globalCommonDirName + 'Batch.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'Fault.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'Meta.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'Header.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'CodeLists.xsd'));
	// Replace tns with cmn internally
	schema.forEach(function(value, key){
		if(value.attributes && value.attributes.type && value.attributes.type.startsWith('tns:')) {
			value.attributes.type.replace(/^(tns:)/,"cmn:");
		}
		return value;
	});
	return schema;
}


function getSchemaVersion(messageSchemaFileName) {
	let result;
	// check for the message schema
	if(messageSchemaFileName) {
		var schema = openXSDFile(messageSchemaFileName);
		if(isEmpty(schema)) {
			fns.logError("Schema " + messageSchemaFileName + " file not read");
			process.exit(1);
		}
		if(schema && schema.elements &&  schema.elements[0] && schema.elements[0].attributes && schema.elements[0].attributes.version) {
			result = schema.elements[0].attributes.version;
		}
	}
	else {
		fns.logWarn("No VBM file provided so version not defined");
	}
	return result;
}

function outputSchemaVersion(messageSchemaFileName){
	// Output the schema version to the csv
	let version = getSchemaVersion(messageSchemaFileName);
	fns.logHeading(globalServiceName + "  version: " + version);
	outputRow(" , " + globalServiceName + "  version: " + version);
	outputRow(" , " );
}

function outputHeadings() {
    outputRow(' ,Path, Type, Core Type, Cardinality, Description ');
}

function getExtensionName(schemaFileName) {
	// Based on the schema name identify the extension file name
	var result;
	var extensionFolder;
	var file = path.resolve(process.cwd(), schemaFileName);
  	var rootFolder = path.dirname(file); 
  	extensionFolder = rootFolder + "/Extension" + "/Extended" + globalServiceName + ".xsd" ;
  	if(!fs.existsSync(extensionFolder)) {
  		fns.logWarn("Extension file not found: " + extensionFolder);
  	}
  	else {
  		result = extensionFolder;
  	}
	return result;
}

function getVBMName(schemaFileName) {
	// Based on the schema name identify the VBM file name
	var result;
	var VBName;
	var file = path.resolve(process.cwd(), schemaFileName);
	var rootFolder = path.dirname(file); 
	VBMName = path.resolve(process.cwd(), rootFolder + "/" + globalServiceName.replace("VBO", "VBM") + ".xsd");
	if(!fs.existsSync(VBMName)) {
  		fns.logWarn("VBM file not found: " + VBMName);
  	}
  	else {
  		result = VBMName;
  	}
	return result;
	
}

function defineCommonLocation (schemaFileName) {
	// Set Common Location based on schema file location
	try {
		var file = path.resolve(process.cwd(), schemaFileName);
  		var rootFolder = path.dirname(file); 
  		globalCommonDirName = path.resolve(process.cwd(), rootFolder+"/../../../Common/V1/") + "/";
		globalCommonTypes = globalCommonDirName + "CoreComponentType_2p0.xsd";
	}
	catch (err) {
		fns.logFatal("Common directory not found at /../../../Common/V1/");
	}
}

/**
 * Generate csv for XML/SOAP .
 * @param {string} schemaFileName 
 * @param {string} serviceNameParamer  
 */
function generateCSVFromXMLSoapSchema (schemaFileName) {
	
	fns.logHeading("SOAP XSD CSV Generation");
	//set service name
	globalServiceName=getServiceName(schemaFileName);
	fns.logHeading(globalServiceName);

	defineCommonLocation(schemaFileName);

	var extensionSchemaFileName = getExtensionName(schemaFileName);
	var messageSchemaFileName = getVBMName(schemaFileName);

	// Read the XSD files as JSON
	globalOriginalSchema = getSchema(schemaFileName);
	globalCommonTypeSchema = getSchema(globalCommonTypes);
	globalCommonSchema = createSingleCommonSchema();
	// check for the extension schema
	if(extensionSchemaFileName) {
		globalExtensionSchema = getSchema(extensionSchemaFileName);
	}
	else {
		fns.logWarn("No extension file found");
	}

	//Create an output file
	var outputFileName = schemaFileName.split('.').slice(0, -1).join('.') + '_SOAP.csv'; //remove extension
	globalWriteStream = fs.createWriteStream(outputFileName);
	globalWriteStream.on('error', 
		function(err) { 
			// Error
        fns.logError("Error: ");
        console.log(err);
        fns.logError("If permission denied make sure the generated file is closed.") 
		});
	fns.logHeading("Generating: " + outputFileName);
	//Output version and headering row
	outputSchemaVersion(messageSchemaFileName);
    outputHeadings();

    // Generate CSV
    generateXSDCsv(globalOriginalSchema, '/' + globalServiceName);

}

module.exports = {

  generateJson: generateCSVFromJsonSchema,

  generateXML: generateCSVFromXMLSoapSchema
};


#!/bin/bash
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Excel====================================="
cd ${servicesLocation}
for service in ${servicesLocation}; do
	if [[ ${file} == Scripts ]]
	then :
	else 
		cd ${service}/SOAP/VBO/
		cd */
		cd */
		cd */
		# Should be at the SOAP VBO directory
		# Create soft links
		cp  -a  ${SOAP_COMMON} ../Common
		cp  -a  ${SOAP_COMMON} ../../Common
		node scripts/ValidateRest/index.js -S ${service}VBO.xsd
		# Remove soft links
		rm -rf   ../Common
		rm -rf   ../../Common
	fi
done


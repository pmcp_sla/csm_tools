const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  { name: 'generateJSONCSV', alias: 'j', type: String, multiple: false},
  { name: 'generateXMLCSV', alias: 'x', type: String, multiple: false}, 
  { name: 'extensionFile', alias: 'e', type: String, multiple: false},
  { name: 'messageFile', alias: 'm', type: String, multiple: false},
  { name: 'timeout', alias: 't', type: Number }
];

module.exports = {
  parseCommandLine: function()
  {
    try {
        return commandLineArgs(optionDefinitions);
    } catch (e) {
        console.error(e.message);
    } finally {
    }
    return null;
  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'Generate CSV',
        content: 'Generate CSV from SOAP and JSON schemas'
      },
      {
        header: 'Synopsis',
        content: '$ app <options>'
      },
      {
        header: 'Command List',
        content: [
          { name: 'help', summary: '-h Display help information ' },
          { name: 'generateJSONCSV', summary: '-j Generates csv file from json \n'+
                                                        '   Example: generateCSV -j CustomerPrivacyProfileVBO.json'
                                                      },
          { name: 'generateXMLCSV', summary: '-x Generates csv file from Soap XSD \n'+
                                                        '   Example: generateCSV -x CustomerPrivacyProfileVBO.xsd -e ExtendedCustomerPrivacyProfileVBS.xsd -m CustomerPrivacyProfileVBS.xsd'
                                                      }

        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

};

#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var generateCSV = require("./generateExample.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('generateJsonExample' in cmdArgs)
{
    generateCSV.generateJson(cmdArgs['generateJsonExample']);
}
else if ('generateXMLExample' in cmdArgs)
{
    generateCSV.generateXML(cmdArgs['generateXMLExample'], cmdArgs['extensionFile']);
}
else
{
    cmdObj.printHelp();
}

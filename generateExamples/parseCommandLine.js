const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  { name: 'generateJsonExample', alias: 'j', type: String, multiple: false},
  { name: 'generateXMLExample', alias: 'x', type: String, multiple: false}, 
  { name: 'extensionFile', alias: 'e', type: String, multiple: false},
  { name: 'timeout', alias: 't', type: Number }
];

module.exports = {
  parseCommandLine: function()
  {
    try {
        return commandLineArgs(optionDefinitions);
    } catch (e) {
        console.error(e.message);
    } finally {
    }
    return null;
  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'Validate CSM REST',
        content: 'Can perform different integrity [italic]{validations} (ajv, swagger, swagger-model, codegen)'
      },
      {
        header: 'Synopsis',
        content: '$ app <options>'
      },
      {
        header: 'Command List',
        content: [
          { name: 'help', summary: '-h Display help information ' },
          { name: 'generateJSONCSV', summary: '-j Generates Example file from json \n'+
                                                        '   Example: generateCSV -j "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Privacy Profile\\Trunk\\Docs\\CustomerPrivacyProfile\\REST\\VBO\\Customer\\CustomerPrivacyProfile\\V2\\CustomerPrivacyProfileVBS.json"'
                                                      },
          { name: 'generateXMLCSV', summary: '-x Generates Example file from Soap XSD -- NOT COMPLETED\n'+
                                                        '   Example: generateCSV -x "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Privacy Profile\\Trunk\\Docs\\CustomerPrivacyProfile\\REST\\VB)\\Customer\\CustomerPrivacyProfile\\V2\\CustomerPrivacyProfileVBS.xsd"'
                                                      }

        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

};



"use strict";
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
var xml2js = require('xml2js'); // Node module to parse XML schema into json
var $RefParser = require('json-schema-ref-parser'); // Node module which de-referecnes a json schema to one single schema
const convert = require('xml-js'); 
const optionsConvert = {object: false, ignoreComment: true, arrayNotation: true, trim: true};

var globalCommonDirName = process.env.SOAP_COMMON + "/V1/"; // Taken from external declarations file!
var globalCommonTypes = globalCommonDirName + "CoreComponentType_2p0.xsd";
// Schemas to be processed
var globalCommonSchema; // SOAP common schema
var globalCommonTypeSchema; // SOAP common types schema
var globalExtensionSchema;	// SOAP extension schema
var globalOriginalSchema; // SOAP original input schema
var globalServiceName; 
var globalWriteStream; // Output file
var globalNameSpace = []; // Used for XML to track the current name space - a stack
var globalGenerateAttribute = false;
var globalDefaultMap = new Map ( // Defaults for some common attributes; only used if data is not provided in the conversion process; used for json and SOAP
[ 	// Order is important in this list!!!
	// Copied from generateCVS so most is not needed for examples
	// Columns line up with csv columns which are output
 	// endswith Name : 	{ type, 		coreDataType,			jsonType, 		cardinality,	validation rule,	sample data ,	description}
 	[ "code", 				['', 		'Code. Type', 			'xsd:codeType',		'', 			'',				'token',		''					]],
 	[ "id", 				['', 		'Identifier. Type', 	'',					'', 			'',				'123',			''					]],
 	[ "name", 				['', 		'Text. Type', 			'',					'', 			'',				'',				''					]],
 	[ "value", 				['', 		'Code. Type', 			'',					'', 			'',				'',				''					]],
  	[ "unitcode", 			['', 		'Text. Type', 			'xsd:string',		'', 			'',				'token',		'The unit of the quantity or measure'	]],
	//alphabetic below this
 	[ "@actioncode", 		['', 		'', 					'xsd:decimal',		'',				'',				'ADD',			''					]],
 	[ "amount", 			['',	 	'Amount. Type', 		'xsd:decimal',		'', 			'',				'123.00',		'The Amount'		]],
 	[ "agencyname", 		['', 		'', 					'',					'', 			'',				'Vodafone',		'The name of the agency'	]],
 	[ "aristocratictitle", 	['', 		'Text. type', 			'xsd:string',		'', 			'',				'Sir',			'Aristocratic Title e.g. Duke, Knight'	]],
 	[ "binaryobject", 		['',	 	'BinaryObject. Type',	'xsd:base64Binary',	'', 			'',				'base64Binary',		'The Amount'		]],
 	[ "bloodtype", 			['', 		'Text. type', 			'',					'', 			'',				'B Neg.',		''					]],
	[ "content", 			['', 		'Text. Type', 			'',					'', 			'',				'Free text',	''					]],
	[ "code.value", 		['', 		'Code. Type', 			'',					'', 			'',				'token',		''					]],
 	[ "created", 			['', 		'DateTime. Type', 		'',					'', 			'ISO8601',		'2015-04-01T00:00:00',	''					]],
	[ "createdby", 			['', 		'Text. Type', 			'',					'', 			'',				'Person A',	''					]],
 	[ "currencyid", 		['', 		'', 					'xsd:decimal',		'', 			'ISO4127',		'EUR',			'The currency'					]],
 	[ "date", 				['', 		'Date. Type', 			'xsd:date',			'', 			'',				'2018-05-06',	''					]],
 	[ "datestring", 		['', 		'DateTime. Type', 		'',					'', 			'ISO8601',		'2018-05-06',	''					]],
 	[ "datestring.format", 	['', 		'Date. Format. Text',	'',					'', 			'',				'ISO8601',		''					]],
 	[ "daytype", 			['', 		'Text. Type', 			'xsd:string',		'', 			'',				'Monday',		''					]],
 	[ "desc", 				['', 		'Text. Type', 			'xsd:string',		'', 			'',				'Free text',	''					]],
 	[ "description", 		['', 		'Text. Type', 			'xsd:string',		'', 			'',				'Free text',	''					]],
 	[ "familyname", 		['', 		'Text. type', 			'xsd:string',		'', 			'',				'Smith',		'The family name of the person'		]],
 	[ "firstname", 			['', 		'Text. type', 			'xsd:string',		'', 			'',				'John',			'The first name of the person'			]],
 	[ "format", 			['', 		'Text. type', 			'xsd:string',		'', 			'',				'',				'The format of the content'			]],
 	[ "formattedname", 		['', 		'Text. type', 			'xsd:string',		'', 			'',				'Smith',		'The formatted name of the person'		]],
 	[ "gender", 			['', 		'Text. Type', 			'xsd:string',		'', 			'',				'male',			'The gender of the person'		]],
 	[ "href", 				['', 		'Text. Type', 			'',					'', 			'',				''	,			''					]],
 	[ "id.value", 			['', 		'Identifier. Type', 	'xsd:token',		'', 			'',				'123',			''					]],
 	[ "id[*].value", 		['', 		'Identifier. Type', 	'xsd:token',		'', 			'',				'123',			''					]],
 	[ "indicator", 			['', 		'Indicator. Type', 		'xsd:boolean',		'', 			'',				true,			'A flag indicator'		]],
 	[ "indicatorstring", 	['', 		'Indicator. Type', 		'xsd:boolean',		'', 			'',				true,			''					]],
 	[ "language", 			['', 		'Text. Type', 			'xsd:string',		'', 			'',				'en',			''					]],
 	[ "lastmodified", 			['', 		'DateTime. Type', 		'',					'', 			'ISO8601',		'2018-04-01T00:00:00',	''					]],
	[ "lastmodifiedby", 			['', 		'Text. Type', 			'',					'', 			'',				'Person B',	''					]],
 	[ "legalname", 			['', 		'Text. type', 			'xsd:string',		'', 			'',				'Joesph Smith',	'Legal Name by which the person is addressed'		]],
 	[ "maritalstatus", 		['', 		'Text. Type', 			'xsd:string',		'', 			'',				'single',		'The martial status of the person'		]],
 	[ "measure.value", 		['', 		'Measure. Type',		'xsd:number',		'', 			'',				'',				''					]],
 	[ "measure.unitcode", 	['', 		'Measure. Unit. Code',	'',					'', 			'',				'',				'The unit of the measure'		]],
 	[ "middlename", 		['', 		'Text. Type', 			'xsd:string',		'', 			'',				'Joe',			'The middle name of the person'		]],
 	[ "nationality", 		['', 		'Text. Type', 			'xsd:string',		'', 			'',				'British',		'The nationality of the subject'		]],
 	[ "number", 			['', 		'Numeric. Type',		'xsd:decimal',		'', 			'',				'123',			''					]],
 	[ "preferredgivenname", ['', 		'Text. type', 			'xsd:string',		'', 			'',				'Joe',			'Name by which the person prefers to be addressed'	]],
 	[ "price.value", 		['', 		'Amount. Type', 		'',					'', 			'',				'123.00',		''					]],
 	[ "quantity.value", 	['', 		'Quantity. Type', 		'xsd:number',		'', 			'',				'321'	,		''					]],
 	[ "rating", 			['', 		'Numeric. Type',		'xsd:decimal',		'', 			'',				'456',			''					]],
 	[ "salutation", 		['', 		'', 					'xsd:string',		'', 			'',				'Mr',			'Way the person is addressed e.g. Honorable etc'			]],
 	[ "schemeagencyname", 	['', 		'', 					'xsd:string',		'', 			'',				'Vodafone Agency',		'The name of the agency that maintains the identification scheme.'	]],
 	[ "schemeid", 			['', 		'Identifier. Type', 	'xsd:token',		'', 			'',				'Vodafone01',	'The identification of the identification scheme'			]],
 	[ "schemename", 		['', 		'', 					'xsd:string',		'', 			'',				'Scheme Default',		'The name of the identification scheme'			]],
 	[ "score", 				['', 		'Numeric. Type',		'xsd:decimal',		'', 			'',				'400',			''					]],
 	[ "size.value", 		['', 		'Quantity. Type', 		'',					'', 			'',				'234',			''					]],
 	[ "status", 			['', 		'Code. Type', 			'xsd:codeType',		'', 			'',				'active',		''					]],
 	[ "status.value", 		['', 		'Code. Type', 			'xsd:codeType',		'', 			'',				'active',		''					]],
 	[ "surname", 			['', 		'Text. type', 			'xsd:string',		'', 			'',				'Smith',		'The surname of the person'	]],
 	[ "text", 				['', 		'Text. Type', 			'',					'', 			'',				'Free text',				''					]],
 	[ "thresold", 			['', 		'Numeric. Type',		'xsd:decimal',		'', 			'',				'123',			''					]],
 	[ "time", 				['', 		'DateTime. Type', 		'',					'', 			'ISO8601',		'2015-04-01T00:00:00',	''			]],
 	[ "type.value", 		['', 		'Code. Type', 			'xsd:codeType',		'', 			'',				'token',		''					]],
 	[ "version", 			['', 		'Text. Type', 			'',					'', 			'',				'1.1',			''					]],
 	[ "verified", 			['', 		'Boolean. Type', 		'xsd:boolean',		'', 			'',				true,			''					]]
 ]);

var globalAttributeArray =  
	// Hard to identify attributes from fields so this is a list of known attributes which are set as Attributes in the output
	// Taken from common basic components - 'name' has been left out deliberately
	// Used for json and XML/SOAP
[
	'actionCode', 'characteristicName', 'characterSetCode', 'currencyID', 'currencyCodeListVersionID', 'dateStringformat',
	'encodingCode', 'exponent', 'filename', 'format', 
	'languageID', 'languageLocaleID', 'listAgencyID', 'listAgencyName', 
	'listID', 'listHierarchyID', 'listHierarchyId',  'listName', 'listSchemeURI', 'listURI', 'listVersionID', 
	'mimeCode',
	'relationshipLocationCode','relationshipTypeCode', 'relationshipOrganisationCode',   
	'schemeAgencyID', 'schemeAgencyName', 'schemeName', 'schemeDataURI', 'schemeID', 'schemeURI', 'schemeVersionID',  
	'unitCode', 'unitCodeListID', 'unitCodeListAgencyID', 'unitCodeListAgencyName', 'unitCodeListVersionID', 'uri',
	'value'
] ;



/**
 * Reads a XSD file and returns it as a Json object
 * @param {string} filename - The name of the wadl file.
 * @return {Object} 
 */
function openXSDFile(filename){
    var json = null;
    var file = path.resolve(process.cwd(), filename);

        try {
            json = JSON.parse(convert.xml2json(fs.readFileSync(file).toString().trim(), optionsConvert));
        } 
        catch(JSONerr) {
                fns.logFatal('error 102: file not found ' + filename + ' ' + JSONerr.message);
        }

    return json;
}

function removeVBO(name) {
	// REmoved VBO from end of a name
	var result = name;
	if(name.toLowerCase().endsWith('vbo')){
		result = name.slice(0, -3);
	}
	return result;
}


function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}

function getSecondLastName(name, isJson){
	//get the second last part of a name, for json get from the last '.'; for XML get from last '/'
	// For example "/BankAccountVBO/Categories/Category" will return "Categories"
	var nameParts;
	if(isJson) {
		nameParts = name.split('.');
	}
	else { // XML
		nameParts = name.split('/');
	}
	var lastPart =nameParts.pop(); // remove the last element
	lastPart = nameParts.pop(); // Grab new last element
	if(lastPart.endsWith("[*]")) {
		lastPart = lastPart.substring(0, lastPart.length - 3);
	}
	if(lastPart=="$"){
		lastPart = globalServiceName;
	}
	return lastPart;
}

function getLastName(name, isJson){
	//get the  last part of a name, for json get from the last '.'; for XML get from last '/'
	// For example "/BankAccountVBO/Categories/Category" will return "Category"
	var nameParts;
	if(isJson) {
		nameParts = name.split('.');
	}
	else { // XML
		nameParts = name.split('/');
	}
	var lastPart = nameParts.pop();
	return lastPart;
}


function removeLastName(name, isJson){
	//emove the  last part of a name, for json get from the last '.'; for XML get from last '/'
	// For example "/BankAccountVBO/Categories/Category" will return "/BankAccountVBO/Categories"
	var nameParts;
	console.log(name);
	if(isJson) {
		nameParts = name.split('.');
	}
	else { // XML
		nameParts = name.split('/');
	}
	var nameParts = nameParts.pop();
	console.log(nameParts);
	return nameParts;
}


function addDoubleQuotes (str, type) {
	// Adds double quotes to a string
	var result;
	switch (type) {
		case 'String' : 
			result = '"' + str + '"';
			break;
		case 'Number' : 
			result = str;
			break;
		case 'Boolean' : 
			result = str;
			break;
		default:
			result = '"' + str + '"';
			break;
	}
	return result;
}

function outputXMLHeading() {
	// For SOAP/XML output some standard heading info
	var name = removeVBO(globalServiceName);
	globalWriteStream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\r\n");
	globalWriteStream.write("<vbm:Create" + name + "VBMRequest" + "\r\n");
	globalWriteStream.write("xsi:schemaLocation=\"http://group.vodafone.com/schema/vbm/customer/" + name + "/v1 " + name + "VBM.xsd\"" + "\r\n");
	globalWriteStream.write("xmlns:vbm=\"http://group.vodafone.com/schema/vbm/customer/" + name + "/v1\"" + "\r\n");
	globalWriteStream.write("xmlns:cct=\"urn:un:unece:uncefact:documentation:standard:CoreComponentType:2\"" + "\r\n");
	globalWriteStream.write("xmlns:cmn=\"http://group.vodafone.com/schema/common/v1\"" + "\r\n");
	globalWriteStream.write("xmlns:vbo=\"http://group.vodafone.com/schema/vbo/customer/" + name + "/v1\"" + "\r\n");
	globalWriteStream.write("xmlns:extvbo=\"http://group.vodafone.com/schema/extensionvbo/customer/" + name + "v1\"" + "\r\n");
	globalWriteStream.write("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" + "\r\n");
	globalWriteStream.write("<!--  Replace " + name + "above with - separated name and correct version number!!!!!!!!!!!!!!!!!!!!! -->" + "\r\n");
	globalWriteStream.write("<vbm:" + globalServiceName + ">" + "\r\n");
}

function outputXMLTail() {
	// For SOAP/XML output the tail info associated with the above head
	var name = removeVBO(globalServiceName);
	globalWriteStream.write("</vbm:" + globalServiceName + ">"  + "\r\n");
	globalWriteStream.write("</vbm:Create" + name + "VBMRequest >" + "\r\n");
}

function outputOpeningXMLElement(name, type) {
	// For XML output the element opening statement such as "<entity>"
	var nameSpace = findNameSpace(type);
	var outputName = getLastName(name);
	globalWriteStream.write("<" + nameSpace + outputName + ">");
}

function outputClosingXMLElement(name, type) {
	// For XML output the element opening statement such as "</entity>"
	var nameSpace = findNameSpace(type);
	var outputName = getLastName(name);
	globalWriteStream.write("</" + nameSpace + outputName + ">" + "\r\n");
}

function outputExampleNoReturn(text) {
	// Output some text without a carriage return
	globalWriteStream.write(text);
}

function outputExample(text) {
	// Output some text with a carriage return
	globalWriteStream.write(text + '\r\n');
}

function outputOpeningObject () {
	// For Json output opening brace
	globalWriteStream.write('{ \r\n');
}

function outputClosingObject () {
	// For Json output closing brace
	globalWriteStream.write('} \r\n');
}

function outputOpeningArray () {
	// For Json output opening brace
	globalWriteStream.write('[ \r\n');
}

function outputClosingArray () {
	// For Json output closing brace
	globalWriteStream.write('] \r\n');
}

function outputSeparator () {
	// For Json output a comma
	globalWriteStream.write(', \r\n');
}

function generateJsonExample (name, type, jsonType, cardinality, description, isJson) {
	// Generate a single example line made up of:
	//		name : sampleData
	var isXML = !(isJson);
	//console.log(name);
	var desc = ''; //description column
	var columnType = type; //type column (i.e. Attribute or Field or Tag or Collection or Object)
	var coreDataType = ' '; // core data type column
	var validationRule = ' ';
	var sampleData = '';
	var nameLower = name.toLowerCase();

	// If the name end with "value" then use the earlier name part to get the sample data by setting nameLower to it
	if(getLastName(name, isJson) == "value"){
		nameLower = getSecondLastName(name, isJson).toLowerCase();
	}

	// Set values Using the values from the GlobalMap unless the values have been passed in
	globalDefaultMap.forEach((value, key) => {;
		if(nameLower && (nameLower.endsWith(key)) ) {
			// Apply dafaults from global data
			if(value[5] != ''){
				// Set sample data
				sampleData = value[5];
			}
		}
	});
	/// If no sample data set or date time format is used then use the defaults based on the json type
	if((sampleData=='') || ( ['date-time'].indexOf(jsonType) > -1 ) ) {
		switch (jsonType) {
			case 'String' : 
				sampleData = getLastName(name, isJson);
				sampleData = addDoubleQuotes(sampleData, jsonType);
				break;
			case 'Number' : 
				sampleData = 4
				break;
			case 'Boolean' : 
				sampleData = true
				break;
			case 'Object' : 
				sampleData = '{'
				break;
			case 'Array' : 
				sampleData = '['
				break;
			case 'date-time' : 
				sampleData = "2012-04-23T18:25:43.511Z";
				sampleData = addDoubleQuotes(sampleData, jsonType);
				break;
			case 'ISO8601' : 
				sampleData = "2015-10-01T00:00:00";
				sampleData = addDoubleQuotes(sampleData, jsonType);
				break;
			case 'base64' : 
				sampleData = "SGVsbG8sIFdvcmxkIQ==";
				sampleData = addDoubleQuotes(sampleData, jsonType);
				break;
			default:
				sampleData = getLastName(name, isJson);
				sampleData = addDoubleQuotes(sampleData,jsonType);
				break;
		}
	} 
	else {
		sampleData = addDoubleQuotes(sampleData, jsonType);
	}

	if(getLastName(name, isJson)=="name") {
		sampleData = getSecondLastName(name, isJson);
		sampleData = addDoubleQuotes(sampleData, jsonType);
	}
	if(getLastName(name, isJson)=="type") {
		sampleData = "Consumer";
		sampleData = addDoubleQuotes(sampleData, jsonType);
	}
	if(getLastName(name, isJson) == "format"){
		sampleData="string";
		sampleData = addDoubleQuotes(sampleData, jsonType);
	}

	var outputName = addDoubleQuotes(getLastName(name, isJson));

	if(isJson) {
		if(jsonType== 'Array') {
			sampleData = '[';
		}
		else if(jsonType== 'Object') {
			sampleData = '';
		}
		
		if((jsonType == 'Tag') ) {
			 // No example ouput
			 console.log(name)
		}
		else {
			outputExample(outputName + ' : ' +  sampleData );
		}
	}
	else { // XML - never called by XML
		outputExample(outputName + ' : ' +  sampleData );
	}
}

function getSampleData(name, jsonType) {
	var sampleData;
	var nameLower = name.toLowerCase();
	var nameLast = getLastName(name).toLowerCase();

	// Set values Using the values from the GlobalMap unless the values have been passed in
	globalDefaultMap.forEach((value, key) => {;
		if(nameLower && (nameLower.endsWith(key)) ) {
			// Apply dafaults from global data
			if(value[5] != ''){
				// Set sample data
				sampleData = value[5];
			}
		}
	}); 

	if(nameLast=="name") {
		sampleData = nameLast;
	}
	if(nameLast=="type") {
		sampleData = "Consumer";
	}	
	if (typeof sampleData === 'boolean'){
		sampleData = "true";
	}

	// If SampleData is empty then use the type to add a default
	if((!sampleData) && (jsonType)) {
		switch(jsonType) {
			case 'xsd:string' : 
				sampleData = nameLast;
				break;
			case 'xsd:decimal' : 
				sampleData = "4";
				break;
			case 'xsd:boolean' : 
				sampleData = "true";
				break;
			case 'AmountType' : 
				sampleData = 24.0;
				break;
			case 'xsd:decimal' : 
				sampleData = 23.0;
				break;
			case 'BinaryObjectType' : 
				sampleData = "base64Binary";
				break;
			case 'xsd:base64Binary' : 
				sampleData = "base64Binary";
				break;
			case 'CodeType' : 
				sampleData = "Code22";
				break;
			case 'DateTimeType' : 
				sampleData = "2015-04-01T00:00:00";
				break;
			case 'IDType' : 
				sampleData = "123";
				break;
			case 'IndicatorType' : 
				sampleData = "true";
				break;
			case 'IndicatorString' : 
				sampleData = "true";
				break;
			case 'MeasureType' : 
				sampleData = "35.2"	;			
				break;
			case 'NumericType' : 
				sampleData = "45";
				break;
			case 'QuantityType' : 
				sampleData = "12";
				break;
			case 'TextType' : 
				sampleData = "Text";
				break;
			case 'DateType' : 
				sampleData = "2018-04-01";
				break;
			case 'DateString' : 
				sampleData = "2018-04-01";
				break;
			case 'URIType' : 
				sampleData = "URI";
				break;
			case 'DurationType' : 
				sampleData = "P0Y0M2D";
				break;
			case 'xsd:duration' : 
				sampleData = "P0Y0M2D";
				break;
			case 'PercentType' : 
				sampleData = "23%";
				break;
			default:
				sampleData = nameLast;
				break;
		}
	}
	return sampleData;
}

function generateXMLExampleEntity (name, type, jsonType) {
	// Generate a single example line made up of:
	//		<cct:Date>2015-04-01</cct:Date>
	//console.log(name);
	//console.log(jsonType);
	var nameLower = name.toLowerCase();
	var nameLast = getLastName(name).toLowerCase();
	var sampleData = getSampleData(nameLast, jsonType);

	//console.log("sampleData=",sampleData);
	outputExampleNoReturn(sampleData );
}

function generateXMLExampleAttribute (name,  jsonType) {
	// Generate a single example line made up of:
	//		format="ISO8601
	//console.log(name);
	//console.log(jsonType);
	var nameLower = name.toLowerCase();
	var nameLast = getLastName(name).toLowerCase();
	var sampleData = getSampleData(nameLast, jsonType);
	
	//console.log("sampleData=",sampleData);
	outputExampleNoReturn(" " + getLastName(name) + "=" + sampleData + " " );
}

function joinProperties (prop1, prop2) {
	// Join two objects
	let p1 = prop1;
	let p2 = prop2
	let result =  Object.assign( {}, p1, p2);
	return result;
}

function processAllOf (schema, currentName, lastName){
	// Assumed to be only two items in the AllOf
	var first = true;
	// Join the properties
	var newSchema = {};
	if(schema[0] && schema[0].properties && schema[1] && schema[1].properties) {
		newSchema.properties = joinProperties(schema[0].properties, schema[1].properties);
		generateJsonCSV(newSchema, currentName + '.' + lastName +'[*]');
	}
	else {
			fns.logWarn("No allOf entries for:" + lastName);
	}
}

function contains(propertyArray, elementName) {
	// Return true if the property array contains the elementName
	let result = false;
	propertyArray.forEach(function(property, value){
		if(property[0] && (property[0]==elementName)){
			result = true;
		}
	});
	return result;
}

function removeAlternativeAttributes (propertyArray) {
	// When alternates exists such as indicatorString and indicator or dateString and date then remove one of the pairs
	let result =[];
	propertyArray.forEach(function(property, value){
		if(property[0] && (property[0]==='indicatorString') && contains(propertyArray, 'indicator')) {
			// Ignore
		}
		else if(property[0] && (property[0]==='dateString') && contains(propertyArray, 'date')) {
			// Ignore
		}
		else {
			result.push(property);
		}
	});
	return result;
}


function generateJsonCSV (schemaJson, currentName){
	// Traverse the json schema and output an example for each attribute
	//console.log('NAME=',currentName);
	//console.log(schemaJson.properties);
	var properties; // Grab the properties
	if (schemaJson.properties) 
		properties = Object.entries(schemaJson.properties);
	else if( (schemaJson.items) && (schemaJson.items.properties)) { 
		// Sometimes extra layer has been added so ignore it
		properties = Object.entries(schemaJson.items.properties);
	}

	if(properties) {
		let firstProperty = true; // Used to identify if a separate is needed between elements
		let propertyFound = false; // Used to identify an empty list of elements

		properties = removeAlternativeAttributes(properties);
		
		properties.forEach(function(property, value){ // Traverse each property looking for attributes

			// Process each property but ignore _links
			if(property[0] && (property[0]!='_links') ) { //Ignore _link
				if(!(firstProperty) ) { 
					outputSeparator();
				}
				if(firstProperty) {
					outputOpeningObject();
					propertyFound = true;
					firstProperty = false;
				}
				switch (property[1].type) {
					case 'array': 
						// Output an array entry
						generateJsonExample(currentName + '.' + property[0] , 'Collection', 'Array', '..*', property[1].description, true);
						// Process array properties;
						if((property[0]=='_links') && (property[1].items) && (property[1].items.additionalProperties) &&  
							(property[1].items.additionalProperties.oneOf)) { // Not called at present due to outer if-stmt
							let arrayProperties = property[1].items.additionalProperties.oneOf;
							let first= true;
							arrayProperties.forEach(function(arrayProperty){
									if(!(first) && (arrayProperty.properties)) outputSeparator();
									generateJsonCSV(arrayProperty, currentName + '.' + property[0] +'[*]');
									first = false;
								});
						}
						else if((property[1].items) && (property[1].items.allOf)) { // List of items - allOf
							processAllOf(property[1].items.allOf, currentName, property[0]);
						} 
						else if ((property[1].items) && (property[1].items.properties)) { // Single object type
						 	let arrayProperties = property[1].items;
							generateJsonCSV(arrayProperties, currentName + '.' + property[0] +'[*]');
						}
						else if ((property[1].items) && (property[1].items.items) && (property[1].items.items.properties)) { // Array of arrays
						 	let arrayProperties = property[1].items;
							outputOpeningArray();
							generateJsonCSV(arrayProperties, currentName + '.' + property[0] +'[*]');
							outputClosingArray();
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object!!!: " + property[0]);
						}
						outputClosingArray();
						break;
					case 'object' : 
						// Process an object 
						if((property[1]) && (property[1].allOf)) { // List of items - allOf
							// Output an object entry
							generateJsonExample(currentName + '.' + property[0], 'Field', 'Object',  '0..1', property[1].description, true );
							processAllOf(property[1].allOf, currentName, property[0]);
						}
						else if (property[1].properties) { // Single object type
							generateJsonExample(currentName + '.' + property[0], 'Field', 'Object',  '0..1', property[1].description, true );
						 	let objProperties = property[1];
							generateJsonCSV(objProperties, currentName + '.' + property[0] );
						}
						else {
							fns.logWarn("Check json for this entry - assumed to be an object!!!: " + property[0]);
						}
						break;
					case 'string' :
						if(property[1].format) {
							generateJsonExample(currentName + '.' + property[0], 'Field', property[1].format,   '0..1', property[1].description, true );
						}
						else
							generateJsonExample(currentName + '.' + property[0], 'Field', 'String',   '0..1', property[1].description, true ); 
						break;
					case 'boolean' : 
						generateJsonExample(currentName + '.' + property[0], 'Field', 'Boolean',  '0..1', property[1].description, true );
						break;
					case 'number' : 
						generateJsonExample(currentName + '.' + property[0], 'Field', 'Number',  '0..1', property[1].description, true );
						break;
					case 'integer' : 
						generateJsonExample(currentName + '.' + property[0], 'Field', 'Number',  '0..1', property[1].description, true );
						break;
					default:
						if(property[0] == 'href') {
							generateJsonExample(currentName + '.' + property[0], 'Field', 'String', '0..1', property[1].description, true );
						}
						else if(property[1] && property[1].properties) {
							// Should probably be an object
							generateJsonExample(currentName + '.' + property[0], 'Field', 'Object', '0..1', '', true );
							let objProperties = property[1];
							generateJsonCSV(objProperties, currentName + '.' + property[0] );
						}
						else if(property[1] && property[1].allOf) {
							// Should probably be an Object
							generateJsonExample(currentName + '.' + property[0], 'Field', 'Object', '0..1', '', true );
							processAllOf(property[1].allOf, currentName, property[0]);
						}
						else {
							generateJsonExample(currentName + '.' + property[0], 'Field', property[1].type, '0..1', property[1].description, true );
							fns.logWarn("Defaulted this entry:" + property[0]);
						}
						break;
				}
			}

		});
		if(propertyFound) outputClosingObject();
	}
	else {
			fns.logWarn("No entries for:" + currentName);
	}

}

/**
 * Generate examples from json
 * @param {string} schemaFileName 
 */
function generateExampleFromJsonSchema (schemaFileName) {
	
	fns.logHeading("JSON Example Generation");
	//Read json schme file
	var jsonSchema = utils.loadFile(schemaFileName);

	//set service name
	globalServiceName=getServiceName(schemaFileName);


	fns.logHeading(globalServiceName);

	//Create an output file
	var outputFileName = schemaFileName.split('.').slice(0, -1).join('.') + '_EXAMPLE.json'; //remove extension

	globalWriteStream = fs.createWriteStream(outputFileName);
	globalWriteStream.on('error', 
		function(err) { 
			// Error
        fns.logError("Error: ");
        console.log(err);
        fns.logError("If permission denied make sure the generated file is closed.") 
		});
	fns.logHeading("Generating: " + outputFileName);

    // Generate based on a dereferenced schema
	$RefParser.dereference(schemaFileName)
    .then(function(schemaResult) {
        // Success
        generateJsonCSV(schemaResult, '$');
        fns.logHeading("Success: file " +  outputFileName + " generated.");
    })
    .catch(function(err) {
        // Error
        fns.logError("Error: ");
        console.log(err);
    });

}


function currentGlobalNameSpace(){
	// Set the global name space to the previous value (XML)
		return globalNameSpace[globalNameSpace.length-1];
}


function pushGlobalNameSpace(type){
	// Update  the global name space stack 
	var nameSpace = findNameSpace(type);
	globalNameSpace.push(nameSpace);
	//console.log(type, "->", currentGlobalNameSpace());
}

function popGlobalNameSpace(){
	// Set the global name space to the previous value  (XML)
		globalNameSpace.pop();
}

function search(name, schema){
	//Search for the type called name in the XML schema and return the result
	var found = Object.keys(schema).filter(function(key) {
			if (schema[key].attributes && schema[key].attributes.name) {
	    		return schema[key].attributes.name == name;
	    	}
	    	else
	    		return false;
		// to cast back from an array of keys to the object, with just the passing ones
		}).reduce(function(obj, key){
		    obj[key] = schema[key];
		    return obj[key];
			}, {});
	return found;
}


function findNameSpace(typeName){
	// Find the name space for a type - searchs in all of the schemes below. Remember common is a join of several schemas
	var result;
	if(!typeName) {
		// Default
		typeName = currentGlobalNameSpace();
	}
	if(typeName.startsWith('extvbo:') && globalExtensionSchema) {
		//  extension schema
		result = "extvbo:";
	}
	else if(typeName.startsWith('cmn:')){
		//  common 
		result = "cmn:";
	}
	else if(typeName.startsWith('tns:') || typeName.startsWith('vbo:')){
		//  Use the global name space which should the current file/namespace
		if(currentGlobalNameSpace().startsWith("vbo:")) {
			result = "vbo:";
		}
		else  {
			// Else use the current global name space 
			result = currentGlobalNameSpace();
		}
	}
	else if(typeName.startsWith('cct:')){
		// common core type
		result = "cct:";
	}
	else if(typeName.startsWith('ccts:')){
		//  common core type
		result = "cct:";
	}
	else if(typeName.startsWith('xsd:')){
		//  use the current global name space 
			result = currentGlobalNameSpace();
	}
	if(isEmpty(result))  {
		// Search in original file, then extension and then common - this may not always work if same type name used in the main schama and the extension
		// Remove the type reference before ':'
		var index = typeName.indexOf(':');
		typeName = typeName.slice(index+1);
		// Search the original schema
		result = search(typeName, globalOriginalSchema);

		if(isEmpty(result)) {
			// Search the extension
			if(globalExtensionSchema) result = search(typeName, globalExtensionSchema);
			if(isEmpty(result)) {
				// Search common
				result = search(typeName, globalCommonSchema);
				if(isEmpty(result)) {
					//fns.logWarn("Type not found for type: ", typeName);
					result = "vbo:";
				}
				else {
					//  common 
					result = "cmn:";
				}
			}
			else {
			//  extension
			result = "extvbo:";
			}
		}
		else {
			//  current
			result = "vbo:";
		}
	}
	return result;
}

function findType(typeName){
	// Find a type in an XML schema - searchs in all of the schemes below. Remember common is a join of several schemas
	var result;
	if(typeName.startsWith('extvbo:') && globalExtensionSchema) {
		// Search extension schema
		result = search(typeName.replace(/^(extvbo:)/,""), globalExtensionSchema);
	}
	else if(typeName.startsWith('cmn:')){
		// Search common 
		result = search(typeName.replace(/^(cmn:)/,""), globalCommonSchema);
	}
	else if(typeName.startsWith('tns:')){
		// Check what the current name space is and use it instead of tns
		if(currentGlobalNameSpace().startsWith('tns:')) {
			result = search(typeName.replace(/^(tns:)/,""), globalOriginalSchema);
		} 
		else { // Replace the name space on the type!
			result = findType(typeName.replace(/^(tns:)/,currentGlobalNameSpace()));
		}
	}
	else if(typeName.startsWith('cct:')){
		// Search common core type
		result = search(typeName.replace(/^(cct:)/,""), globalCommonTypeSchema);
	}
	else if(typeName.startsWith('ccts:')){
		// Search common core type
		result = search(typeName.replace(/^(ccts:)/,""), globalCommonTypeSchema);
	}
	if(isEmpty(result))  {
		// Search in original file, then extension and then common - this may not always work if same type name used in the main schama and the extension
		// Remove the type reference before ':'
		var index = typeName.indexOf(':');
		typeName = typeName.slice(index+1);
		// Search the original schema
		result = search(typeName, globalOriginalSchema);
		if(isEmpty(result)) {
			// Search the extension
			if(globalExtensionSchema) result = search(typeName, globalExtensionSchema);
			if(isEmpty(result)) {
				// Search common
				result = search(typeName, globalCommonSchema);
			}
		}
	}
	return result;
}

function isEmpty(obj) {
	// Empty object test
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


function isBaseType(type) {
	//Returns true if the type is a base such as xsd or cct:
	return (type.startsWith("xsd:") || (type.indexOf(":") == -1) )
}


function traverseXSD(current, currentName) {
	// Process the current XSD to generate csv row for each attribute
	//console.log("currentName", '-', current.name);
	//console.log("current", '=', current);
	switch (current.name) {
		case 'xsd:complexType' :
		case 'xsd:complexContent': 
		case 'xsd:simpleContent' :
		case 'xsd:simpleType' :
				if(current.elements) {
					current.elements.forEach(function(el){
						traverseXSD(el, currentName); 
					});
				}
				break;
		case 'xsd:choice' :
				// Just process with the first choice!
				if(current.elements) {
						traverseXSD(current.elements[0], currentName);
				}
				break;
		case 'xsd:restriction' :
				// Use the restriction type
				if(current.attributes && current.attributes.base) {
					if(isBaseType(current.attributes.base) ) {
						// Base type found
						type = current.attributes.base;
						generateXMLExampleEntity(currentName, 'Field', type );
					}
				}
				break;
		case 'xsd:extension' :
				var subSchema;
				if(current.attributes && current.attributes.base) {
					var newName = currentName;
					if(current.attributes.name) {
						newName = newName + '/' + current.attributes.name
					}
					if(isBaseType(current.attributes.base) ) {
						// Base type found
						type = current.attributes.base;
						generateXMLExampleEntity(newName, 'Field', type );
					}
					else {
						subSchema = findType(current.attributes.base);
						if(!(isEmpty(subSchema))) { 
							pushGlobalNameSpace(current.attributes.base);
							traverseXSD(subSchema, currentName);
							popGlobalNameSpace();
						}
						if(isEmpty(subSchema) && !(current.attributes.base.startsWith('xsd:')) ) {
								fns.logWarn('Extension Schema type not found ' +  current.attributes.base);
								//console.log(current);
						}
					}
				}
				if(current.elements) {
					current.elements.forEach(function(el){
						traverseXSD(el, currentName); 
					});
				}
				break;
		case 'xsd:sequence':
				if(current.elements) {
					current.elements.forEach(function(el){
						traverseXSD(el, currentName); 
					});
				}
				break;
		case 'xsd:attribute':
		case 'attribute':
				if(globalGenerateAttribute){ // THIS NEEDS MORE WORK!!!
					// Get Name of attribute
					var newName = currentName;
					if(current.attributes && current.attributes.name) {
						newName = newName + '/' + current.attributes.name
					}
					//Get Type of attribute  and generate it as an example
					if(current.attributes && current.attributes.type) {
						if(isBaseType(current.attributes.type)) {
							// Base type found
							var type = current.attributes.type;
							generateXMLExampleAttribute(newName, type );
						}
						else {
								fns.logWarn('Expecting a basic type but found: ' +  current.attributes.type);
						}
					}
				}
				break;
		case 'xsd:element':
		case 'element':
				// Get Name of element
				var newName = currentName;
				if(current.attributes && current.attributes.name) {
					newName = newName + '/' + current.attributes.name
				}
				//Get Type of attribute  and generate it as an attribute
				var type = 'xsd:string';
				outputOpeningXMLElement(newName);
				pushGlobalNameSpace(type);
				if(current.attributes && current.attributes.type) {
					if(isBaseType(current.attributes.type)) {
						// Base type found
						type = current.attributes.type;
						generateXMLExampleEntity(newName, 'Field', type );
					}
					else {
						// Complex type so find it and then traverse further for attributes
						subSchema = findType(current.attributes.type);
						pushGlobalNameSpace(current.attributes.type);
						if(isEmpty(subSchema)) {
							fns.logWarn('Schema type not found ' +  current.attributes.type);
							//console.log(current);
						}
						else {
							traverseXSD(subSchema, newName); 
						}
						popGlobalNameSpace();
					}
				}
				else if(current.name.endsWith('element') ) {
						//generateAttribute(newName, 'Field', type, cardMin + '..' + cardMax, desc, false );
				}
				if(current.elements) { // Check for any remaining elements
					current.elements.forEach(function(el){
						traverseXSD(el, newName); 
					});
				}
				popGlobalNameSpace();
				outputClosingXMLElement(newName);
				break;
		case 'xsd:annotation' : // Ignore
				break;
		case 'xsd:restriction' : // Ignore
				break;
		default:
				fns.logWarn('Unknown found ' +  current.attributes.type);
				console.log(current);
				break;
	}
}


function generateXSDExample (mainSchema, currentName){
	// Generate Examples for an XSD
	mainSchema.forEach(function(current){
		//if it is a complextype then process as this is assumed to be the main type in the xsd
		if(current.name == 'xsd:complexType'){
			var name = current.attributes.name;
			if(name && name.toLowerCase() == globalServiceName.toLowerCase() +'type') {
				// Found the 'lead' element so traverse from there
				outputXMLHeading();
				if(current.elements) {
					current.elements.forEach(function(type) {
						if(type.type != 'xsd:annotation') {
							// Process the type
							traverseXSD(type, currentName);
						}
					});
				}
				outputXMLTail();
			}
		}
	})
}

function getSchema(name) {
	//Get the named schema
	var schema = openXSDFile(name).elements;
	if(isEmpty(schema)) {
		fns.logError("Schema " + name + " file not read");
		process.exit(1);
	}
	return schema[0].elements;

}

function createSingleCommonSchema (){
	// Join the schema for common based on several file names - makes it easier to search common types
	var schema = getSchema(globalCommonDirName + 'CommonComponents.xsd');
	schema = schema.concat(getSchema(globalCommonDirName + 'Batch.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'Fault.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'Meta.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'Header.xsd'));
	schema = schema.concat(getSchema(globalCommonDirName + 'CodeLists.xsd'));
	// Replace tns with cmn internally
	schema.forEach(function(value, key){
		if(value.attributes && value.attributes.type && value.attributes.type.startsWith('tns:')) {
			value.attributes.type.replace(/^(tns:)/,"cmn:");
		}
		return value;
	});
	return schema;
}

/**
 * Generate csv for XML/SOAP .
 * @param {string} schemaFileName 
 * @param {string} serviceNameParamer  
 */
function generateExampleFromXMLSoapSchema (schemaFileName, extensionSchemaFileName) {
	
	fns.logHeading("SOAP XSD Example Generation");

	// Read the XSD files as JSON
	globalOriginalSchema = getSchema(schemaFileName);
	globalCommonTypeSchema = getSchema(globalCommonTypes);
	globalCommonSchema = createSingleCommonSchema();
	// check for the extension schema
	if(extensionSchemaFileName) {
		globalExtensionSchema = getSchema(extensionSchemaFileName);
	}
	else
		fns.logWarn("No extension provided");

	//set service name
	globalServiceName=getServiceName(schemaFileName);
	globalNameSpace.push("vbo:" + globalServiceName + "Type");
	fns.logHeading(globalServiceName);

	//Create an output file
	var outputFileName = schemaFileName.split('.').slice(0, -1).join('.') + '_EXAMPLE.xml'; 
	globalWriteStream = fs.createWriteStream(outputFileName);
	globalWriteStream.on('error', 
		function(err) { 
			// Error
        if(err) {
        	fns.logError("Error: ");
        	console.log(err);
        	fns.logError("If permission denied make sure the generated file is closed.") 
    		}
		});
	fns.logHeading("Generating: " + outputFileName);
    // Generate CSV
    generateXSDExample(globalOriginalSchema, '/' + globalServiceName);

}

module.exports = {

  generateJson: generateExampleFromJsonSchema,

  generateXML: generateExampleFromXMLSoapSchema
};



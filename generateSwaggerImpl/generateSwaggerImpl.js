
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
var _ = require('underscore');

var globalAddress = "http://cim.vodafone.com/";
var globalServiceName; 
var globalTargetDirectory = "Impl" + path.sep;
var globalFileSet = new Set();

// When generating new files to reference the current Swagger parser has some limitation; use this param to say when we should stop generating subsfiles
var globalSubFileMaxLevel = 500;
var globalSubFileCurrentLevel = 0;
var globalFileCount = 1;

var globalFileMap = new Map();

//const $RefParser = require("json-schema-ref-parser");
const $SyncRefParser = require('json-schema-ref-parser-sync'); // Sync
const validateOptions = {
    allow: {
        json: false, // Don't allow JSON files
        empty: true      // Don't allow empty files
    },
    $refs: {
        internal: false   // Don't dereference internal $refs, only external
    },
    cache: {
        fs: 1, // Cache local files for 1 second
        http: 600         // Cache http URLs for 10 minutes
    },
    validate: {
        spec: true, // Don't validate against the Swagger 2.0 spec
        schema: true
    },
    dereference: {
        circular: false                 // Don't allow circular $refs
    }
};






function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbo')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}

function isObject (a) {
    return (!!a) && (a.constructor === Object);
}


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function replaceCommon(inputSchema) {
	//return JSON.parse(JSON.stringify(inputSchema).replace('"../../../Common/V1/', '"../../../../../Common/V1/Impl/Swagger/'));
	return JSON.parse(JSON.stringify(inputSchema).replace(/"..\/..\/..\/Common\/V1\//g, '"../../../../../Common/V1/Impl/Swagger/'));
}

function checkSubFileAllowed(filename){
	return (globalSubFileCurrentLevel < globalSubFileMaxLevel)
}

function incrementSubFileCount(){
	globalSubFileCurrentLevel ++;
}


function decrementSubFileCount(){
	globalSubFileCurrentLevel --;
}

function traverseJsonAllOf(propertiesSchema, name) {
	// Create a JSON file which contains the result of a traverse schema for allOf
	// 		allOf - { allOf : [ { property : value },  ...] }
	if(propertiesSchema && propertiesSchema.allOf){
		var result = {};
		// Create a swagger file for the AllOf Schema 
		var filename =  globalServiceName + 'VBO-' + capitalizeFirstLetter(name);
		incrementSubFileCount();
		if (checkSubFileAllowed(filename)) {
			filename = createJsonFile(filename, propertiesSchema, true);
			decrementSubFileCount();
			result["$ref"] = "./" + filename;
			return result; 
		}
		else {
			return replaceCommon(propertiesSchema);
		}
	}
	else {
		fns.logError(name + " schema has no allOf.");
		return replaceCommon(propertiesSchema);
	}
}

function traverseJsonObject(propertiesSchema, name) {
	// Create a JSON file which contains the result of properties
	// 		 { properties : { { property : value },  ...}, ... }
	// Consider adding an option to prevent file creation if only one attribute? See IncidentVBO-WorkfocedAppointmentSlot
	//console.log("OBJECT");
	if(propertiesSchema && propertiesSchema.properties){
		var result = {};
		// Create a swagger file for the properties Schema 
		var filename =  globalServiceName + 'VBO-' + capitalizeFirstLetter(name);
		incrementSubFileCount();
		if(checkSubFileAllowed(filename)) {
			// Only pass in the properties to be converted
			var paramProperties = {}
			paramProperties["properties"] = propertiesSchema.properties;
			if(propertiesSchema.description){
				// Add in the description
				paramProperties["description"] = propertiesSchema.description;
			}
			filename = createJsonFile(filename, paramProperties, true);
			decrementSubFileCount();
			result["$ref"] = "./" + filename;
			return result; 
		}
		else {
			return replaceCommon(propertiesSchema);
		}
	}
	else {
		fns.logError(name + " schema has no properties.");
		return replaceCommon(propertiesSchema);
	}
}

function traverseJson (inputSchema, parentName){
	// Traverse the json schema for parentName - which may be undefined
	// - Patterns expected: 
	// 1) properties - { properties : { property : value }, ... }
	// 2) items - { items :  { property : value }, ... }
	// 3) allOf - { allOf : [ { property : value },  ...] }
	// 4) { property : value }
	// 5) list of { property : value } - { { property : value }, { property : value }, ...}
	
	var result = {};
	if(inputSchema && (isObject(inputSchema)) && (Object.keys(inputSchema).length == 1)) {
		if(inputSchema.properties) {
			//nsole.log("CASE 1", parentName);
			var traverseResult={};
			var traverseProperties = inputSchema.properties
			Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
				traverseResult[key] = traverseJson(traverseProperties[key], key);
			});
			result["properties"] = traverseResult;
		}
		else if(inputSchema.items) {
			//console.log("CASE 2", parentName);
			var traverseResult={};
			var traverseProperties = inputSchema.items
			Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
				traverseResult[key] = traverseJson(traverseProperties[key], parentName);
			});
			result["items"] = traverseResult;
		}
		else if((inputSchema.allOf) && parentName) {
			//console.log("CASE 3A", parentName);
			result = traverseJsonAllOf(inputSchema, parentName);
		}
		else if((inputSchema.allOf) && !parentName) {
			//console.log("CASE 3B",);
			var traverseProperties = inputSchema.allOf // Returns an array
			var traverseResult = [];
			traverseProperties.forEach(function(value) { // Traverse each property
				traverseResult.push(traverseJson(value, parentName));
			});
			result["allOf"] = traverseResult;
		}
		else {
			// Assumed to be { property : value }
			//console.log("CASE 4", inputSchema);
			if(Object.keys(inputSchema)[0] && inputSchema[Object.keys(inputSchema)[0]].type){
				var traverseProperties = inputSchema
				var traverseResult={};
				Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
					traverseResult[key] = traverseJson(traverseProperties[key], key);
				});
				result = traverseResult;
			}
			else {
				result = replaceCommon(inputSchema);
			}
		}
	}
	else if(inputSchema  && (isObject(inputSchema)) && (inputSchema.type=="object") && (['category'].indexOf(parentName) == -1)  &&
		    (inputSchema.properties) && (Object.keys(inputSchema).length > 1)) {
		// Create a reference file for this object - ignore category
		//console.log("CASE 5A", parentName, "inputschema", inputSchema);
			
		var traverseResult={};
		var traverseProperties = inputSchema
		traverseResult = traverseJsonObject(inputSchema, parentName);
		result = traverseResult;
	}
	else if(inputSchema  && (isObject(inputSchema)) && (inputSchema.items) && (inputSchema.items.properties) && !(inputSchema.items.allOf) && 
			(['category'].indexOf(parentName) == -1)) {
		// An array without an allof so create a reference file
		//console.log("CASE 5C", parentName);
		var traverseResult={};
		traverseResult = traverseJsonObject(inputSchema.items, parentName);
		result = inputSchema;
		result["items"] = traverseResult;
	}
	else if(inputSchema  && (isObject(inputSchema)) && (Object.keys(inputSchema).length > 1)) {
		// Traverse each property adding it to the result
		//console.log("CASE 5B", parentName, "inputschema", inputSchema);
		var traverseResult={};
		var traverseProperties = inputSchema
		var removeRedunantProperties = false;
		Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
			//Set parent name for subsequent calls
			var localParent = parentName;
			if(['allOf', 'items'].indexOf(key) == -1 ) {
				localParent = key;
			}
			if(key=="allOf") { // Extract allOf and add it to its own object so we can pass it with the key
				var allOfProp = {};
				allOfProp["allOf"] = traverseProperties["allOf"];
				traverseResult = Object.assign(traverseResult, traverseJson(allOfProp, localParent));
				// Remove the type from the current result
				removeRedunantProperties = true;
			}
			else
			{
				traverseResult[key] = traverseJson(traverseProperties[key], localParent);
			}
		});
		if(removeRedunantProperties) {
			if(traverseResult["type"] != 'array') delete traverseResult["type"];
			delete traverseResult["description"];
		}
		result = traverseResult;
	}
	else {
		//fns.logError("Incorrect input schema " + inputSchema);
		//console.log("CASE 6", inputSchema);
		result = replaceCommon(inputSchema);
	}
	return result;
}

function deReference(schema, name){
	// Dereference the schema
	var result = {}
	validateOptions.$refs.internal = false;
    validateOptions.dereference.circular = false;
    try {
    	result = $SyncRefParser.dereference(schema, validateOptions); // Using synchronised version!
    } 
    catch(err)
        {
            console.error(err);
            console.log(chalk.red("Unable to flatten the reference, looks like the swagger file is invalid: " ,name));
            process.exit(1);
        }
	return result;
}


function combineSchemas(schemaList, name){
	// Given an array of schemas combine them into one property list
	var result = {};
	var attributes = {};
	var requiredList = [];
	schemaList.forEach(function (item){
		Object.keys(item).forEach(function (key){
			if(key == 'properties'){
				attributes = Object.assign({}, attributes, item[key]);
			}
			else if((key == 'type') || (key == 'description')){
				// Ignore
			}
			else if(key == 'required'){
				requiredList.push(item[key]); 
			}
			else {
				var property = {};
				property[key] = item[key];
				attributes = Object.assign({}, attributes, property);
			}
		});
	});
	result["properties"] = attributes
	if(requiredList.length > 0){ // Merge all Required Lists
		result["required"] = [].concat.apply([], requiredList);
	}
	return result;
}



function replaceAllOf (schema, name){
	// Replace the allOf pattern if found
	var result;
	var allOfList = [];
	if(schema.allOf){
		//console.log(schema);
		schema.allOf.forEach(function (item){
			if(item.$ref) {
				allOfList.push(deReference(item, name));
			}
			else {
				allOfList.push(item);
			}
		});
		//console.log(allOfList)
		result = combineSchemas(allOfList, name);
		//console.log("result=", result)
		if(schema.description && !result.description){
			// Add in the description
			result["description"] = schema.description;
		}
	}
	else {
		result = schema;
	}
	return result;
}

function setFilename(filename, schema){
	// Return a unique filename if it exists already
	var result = filename;
	if(sameFilenameDiffSchema(filename, schema)){
		result = result + "1";
		result = setFilename(result, schema);
	}
	return result;
}

function sameFileExists(filename, schema){
	return (globalFileMap.has(filename) &&  _.isEqual(globalFileMap.get(filename),schema)) //globalFileMap.get(filename) === schema)
}


function sameFilenameDiffSchema(filename, schema){
	return (globalFileMap.has(filename) &&  !(_.isEqual(globalFileMap.get(filename),schema)) )
}


function createJsonFile (filename, schema, addType) {
	// Traverse the schema and then create a Json file from the result;

	var outputSchema = traverseJson(schema);

	// Combine allOf
	outputSchema = replaceAllOf(outputSchema, filename);
	outputSchema = replaceCommon(outputSchema);

	//Check if the schema contains a type defn and add one if there is none
	if(!outputSchema.type && addType){
		outputSchema["type"] = "object";
	}
	
	// Write the file with the output schema
	var jsonFile;
	if(sameFileExists(filename,outputSchema)){
		// Ignore do not output
		jsonFile = filename + '.json';
	}
	else {
		let out = JSON.stringify(outputSchema, null, 4);
		let name = setFilename(filename, outputSchema);
		jsonFile = name + '.json';
		if(!fs.existsSync(globalTargetDirectory + jsonFile)){
			fns.logHeading("Generating file " +  jsonFile );
			try {
				fs.writeFileSync(globalTargetDirectory + jsonFile, out, { flag: 'wx' });
				globalFileMap.set(filename, outputSchema);
			}
			catch(err) {
				        fns.logError("Error: " + jsonFile);
				        console.log(err);
				        console.log(globalFileSet);
				        console.log(globalFileSet.has(filename));
				        console.log(filename);
		    }
		}
	}
	return 	jsonFile;
}

function prepTargetDirectory(){
	// Move existing files to Old dir
	let newDir = "OLD" + path.sep;
	if (!fs.existsSync(globalTargetDirectory + newDir)) {
    	fs.mkdirSync(globalTargetDirectory + newDir);
	}
	//Move files
	fs.readdirSync(globalTargetDirectory).forEach(file => {
  		//console.log(file);
  		try{
     		if(fs.lstatSync(globalTargetDirectory + file).isFile()){
				fs.rename(globalTargetDirectory + file, globalTargetDirectory + newDir + file, (err) => {
				  if (err) {
				  	throw err;
				  	console.log(err);
				  }
				});
     		}
		}catch(err){
			console.log(err);
		}
	});
	fns.logInfo("Existing swagger implementation files moved to " + globalTargetDirectory + newDir)
}


/**
 * Generate Swagger Impl  from json\schema
 * @param {string} schemaFileName 
 */
function generateSwaggerImplFromSchema (schemaFileName) {
	
	fns.logHeading("Swagger Implementation Generation");
	//set service name
	globalServiceName=getServiceName(schemaFileName);
	// Prepare target directory
	prepTargetDirectory();
	//Read json schema file
	var jsonSchema = utils.loadFile(schemaFileName);
	
	//Create an output file
	var outputFileName = schemaFileName.split('.').slice(0, -1).join('.') ; //remove extension
	fns.logHeading("Generating: " + outputFileName + ".json");

	// For initial call we only want the properties for the Swagger Impl
	if(jsonSchema.properties) {
		var schemaWithOnlyProperties = {};
		schemaWithOnlyProperties["properties"] = jsonSchema.properties;
		var filename = createJsonFile(outputFileName, 	schemaWithOnlyProperties, false);
		fns.logHeading("Generation complete");
	}
	else
		fns.logError(schemaFileName + " has no properties.");
}



function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


module.exports = {

  generateSwaggerImpl: generateSwaggerImplFromSchema
};
#!/bin/bash
#call declarations
source ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts/declarations.sh
echo "=======================Generate Excel====================================="
echo ${restCommonServiceName}
cd ${GENERATE_TOOL_LOCATION}
# Remove existing files
rm -rf Generated/*.*
echo "Generate JSON "
# Cp all of the files
find ${servicesLocation} -mindepth 8 -maxdepth 10  -path "*/REST/VBO/*"  -path "*/Examples/*" -prune -o -path "*/JSON-LD/*" -prune -o -path "*/Impl/*" -prune -o  -name "*VBO.json" -type f -exec cp {} Generated/ \;
# Create soft links
#cp  -a  ${REST_COMMON} /mnt/c/Users/pmcpa/
cp  -a  ${REST_COMMON} Common
cp  -a  ${REST_COMMON} ../Common
cp  -a  ${REST_COMMON} ../..
# Run the generate
for file in Generated/*.json; do
		node ./index.js -j ${file};
done
echo "Generate SOAP "
# cp all of the files
find ${servicesLocation} -mindepth 7  -path "*/SOAP/VBO/*" -path "*/Examples/*" -prune -o -name "*VBO.xsd" -type f -exec cp {} Generated/ \;
# Create soft links
cp  -a  ${SOAP_COMMON} Common
cp  -a  ${SOAP_COMMON} ../Common
# Run the generate
for file in Generated/*.xsd; do
	if [[ ${file} == Generated/Extended*.xsd ]]
	then :
	else 
		f=${file#"Generated/"}
		node ./index.js -x ${file} -e Generated/Extended${f}
	fi
done
/bin/echo -e "\e[1;34mTIDY UP\e[0m"
rm -rf   Common
rm -rf   ../Common
rm -rf   ../../Common

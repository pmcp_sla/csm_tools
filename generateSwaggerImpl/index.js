#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var generate = require("./generateSwaggerImpl.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('generateSwaggerImpl' in cmdArgs)
{
    generate.generateSwaggerImpl(cmdArgs['generateSwaggerImpl']);
}
else
{
    cmdObj.printHelp();
}

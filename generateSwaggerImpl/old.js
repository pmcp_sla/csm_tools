
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
var $RefParser = require('json-schema-ref-parser'); // Node module which de-referecnes a json schema to one single schema

var globalAddress = "http://cim.vodafone.com/";
var globalServiceName; 
var globalTargetDirectory = "Impl/" 

// When generating new files to reference the current Swagger parser has some limitation; use this param to say when we should stop generating subsfiles
var globalSubFileAllOfFound = false; 
var globalSubFileMaxLevel = 1;
var globalSubFileCurrentLevel = 0;
var globalFileCount = 1;




function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbo')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}

function isObject (a) {
    return (!!a) && (a.constructor === Object);
}


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function replaceCommon(inputSchema) {
	return JSON.parse(JSON.stringify(inputSchema).replace('"../../../Common/V1/', '"../../../../../Common/V1/Impl/Swagger/'));
}

function checkSubFileAllowed(filename){
	if(globalSubFileAllOfFound || (globalSubFileCurrentLevel > globalSubFileMaxLevel)) {
		//fns.logWarn(filename + ' will be generated but may need to be manually replaced in any referenced files with the file contents.(Add "type": "object".)');
	}
}

function incrementSubFileCount(){
	globalSubFileCurrentLevel ++;
}


function decrementSubFileCount(){
	globalSubFileCurrentLevel --;
}

function traverseJsonAllOf(propertiesSchema, name) {
	// Create a JSON file which contains the result of a traverse schema for allOf
	// 		allOf - { allOf : [ { property : value },  ...] }
	if(propertiesSchema && propertiesSchema.allOf){
		var result = {};
		// Create a swagger file for the AllOf Schema 
		var filename =  globalServiceName + 'VBO-' + capitalizeFirstLetter(name);
		incrementSubFileCount();
		checkSubFileAllowed(filename);
		globalSubFileAllOfFound = true; // Subfiles within an allOf may not work
		filename = createJsonFile(filename, propertiesSchema, true);
		globalSubFileAllOfFound = false;
		decrementSubFileCount();
		result["$ref"] = "./" + filename;
		return result; 
	}
	else {
		fns.logError(name + " schema has no allOf.");
		return replaceCommon(propertiesSchema);
	}
}

function traverseJsonObject(propertiesSchema, name) {
	// Create a JSON file which contains the result of properties
	// 		 { properties : { { property : value },  ...}, ... }
	// Consider adding an option to prevent file creation if only one attribute? See IncidentVBO-WorkfocedAppointmentSlot
	//console.log("OBJECT");
	if(propertiesSchema && propertiesSchema.properties){
		var result = {};
		// Create a swagger file for the properties Schema 
		var filename =  globalServiceName + 'VBO-' + capitalizeFirstLetter(name);
		incrementSubFileCount();
		checkSubFileAllowed(filename);
		// Only pass in the properties to be converted
		var paramProperties = {}
		paramProperties["properties"] = propertiesSchema.properties;
		filename = createJsonFile(filename, paramProperties, true);
		decrementSubFileCount();
		result["$ref"] = "./" + filename;
		return result; 
	}
	else {
		fns.logError(name + " schema has no properties.");
		return replaceCommon(propertiesSchema);
	}
}

function traverseJson (inputSchema, parentName){
	// Traverse the json schema for parentName - which may be undefined
	// - Patterns expected: 
	// 1) properties - { properties : { property : value }, ... }
	// 2) items - { items :  { property : value }, ... }
	// 3) allOf - { allOf : [ { property : value },  ...] }
	// 4) { property : value }
	// 5) list of { property : value } - { { property : value }, { property : value }, ...}
	var result = {};
	if(inputSchema && (isObject(inputSchema)) && (Object.keys(inputSchema).length == 1)) {
		if(inputSchema.properties) {
			//nsole.log("CASE 1", parentName);
			var traverseResult={};
			var traverseProperties = inputSchema.properties
			Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
				traverseResult[key] = traverseJson(traverseProperties[key], key);
			});
			result["properties"] = traverseResult;
		}
		else if(inputSchema.items) {
			//console.log("CASE 2", parentName);
			var traverseResult={};
			var traverseProperties = inputSchema.items
			Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
				traverseResult[key] = traverseJson(traverseProperties[key], parentName);
			});
			result["items"] = traverseResult;
		}
		else if((inputSchema.allOf) && parentName) {
			//console.log("CASE 3A", parentName);
			result = traverseJsonAllOf(inputSchema, parentName);
		}
		else if((inputSchema.allOf) && !parentName) {
			//console.log("CASE 3B",);
			var traverseProperties = inputSchema.allOf // Returns an array
			var traverseResult = [];
			traverseProperties.forEach(function(value) { // Traverse each property
				traverseResult.push(traverseJson(value, parentName));
			});
			result["allOf"] = traverseResult;
		}
		else {
			// Assumed to be { property : value }
			//console.log("CASE 4", inputSchema);
			result = replaceCommon(inputSchema);
		}
	}
	else if(inputSchema  && (isObject(inputSchema)) && (inputSchema.type=="object") && (['category'].indexOf(parentName) == -1)  &&
		    (inputSchema.properties) && (Object.keys(inputSchema).length > 1)) {
		// Create a reference file for this object - ignore category
		//console.log("CASE 5A", parentName, "inputschema", inputSchema);
		var traverseResult={};
		var traverseProperties = inputSchema
		traverseResult = traverseJsonObject(inputSchema, parentName);
		result = traverseResult;
	}
	else if(inputSchema  && (isObject(inputSchema)) && (inputSchema.items) && (inputSchema.items.properties) && !(inputSchema.items.allOf) && 
			(['category'].indexOf(parentName) == -1)) {
		// An array without an allof so create a reference file
		//console.log("CASE 5C", parentName);
		var traverseResult={};
		traverseResult = traverseJsonObject(inputSchema.items, parentName);
		result = inputSchema;
		result["items"] = traverseResult;
	}
	else if(inputSchema  && (isObject(inputSchema)) && (Object.keys(inputSchema).length > 1)) {
		// Traverse each property adding it to the result
		console.log("CASE 5B", parentName, "inputschema", inputSchema);
		var traverseResult={};
		var traverseProperties = inputSchema
		var removeRedunantProperties = false;
		Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
			//Set parent name for subsequent calls
			var localParent = parentName;
			if(['allOf', 'items'].indexOf(key) == -1 ) {
				localParent = key;
			}
			if(key=="allOf") { // Extract allOf and add it to its own object so we can pass it with the key
				var allOfProp = {};
				allOfProp["allOf"] = traverseProperties["allOf"];
				traverseResult = Object.assign(traverseResult, traverseJson(allOfProp, localParent));
				// Remove the type from the current result
				removeRedunantProperties = true;
			}
			else
			{
				traverseResult[key] = traverseJson(traverseProperties[key], localParent);
			}
		});
		if(removeRedunantProperties) {
			if(traverseResult["type"] != 'array') delete traverseResult["type"];
			delete traverseResult["description"];
		}
		result = traverseResult;
	}
	else {
		//fns.logError("Incorrect input schema " + inputSchema);
		//console.log("CASE 6", inputSchema);
		result = replaceCommon(inputSchema);
	}
	return result;
}



function createJsonFile (filename, schema, addType) {
	// Traverse the schema and then create a Json file from the result;
	var jsonFile = filename + '.json';
	if(fs.existsSync(globalTargetDirectory + jsonFile)) {
		fns.logWarn("File exists: " + jsonFile + " so creating " + filename + globalFileCount + '.json');
		jsonFile = filename + globalFileCount + '.json';
		globalFileCount++;
	}
	fns.logHeading("Generating file " +  jsonFile +  ".");
	// Traverse the schema to get the output swagger schema
	var outputSchema = traverseJson(schema);
	//Check if the schema contains a type defn and add one if there is none
	if(!outputSchema.type && addType){
		outputSchema["type"] = "object";
	}
	// Write the file with the output schema
	let out = JSON.stringify(outputSchema, null, 4);
	fs.writeFile(globalTargetDirectory + jsonFile, out, { flag: 'wx' }, 
		function(err) { 
			if(err) {
					// Error
		        fns.logError("Error: " + jsonFile);
		        console.log(err);
        	}
		});
	
	return 	jsonFile;
}

/**
 * Generate Swagger Impl  from json\schema
 * @param {string} schemaFileName 
 */
function generateSwaggerImplFromSchema (schemaFileName) {
	
	fns.logHeading("Swagger Implementation Generation");
	//Read json schema file
	var jsonSchema = utils.loadFile(schemaFileName);

	//set service name
	globalServiceName=getServiceName(schemaFileName);
	fns.logHeading("Remove existing files before execution");

	//Create an output file
	var outputFileName = schemaFileName.split('.').slice(0, -1).join('.') ; //remove extension
	fns.logHeading("Generating: " + outputFileName + ".json");

	// For initial call we only want the properties for the Swagger Impl
	if(jsonSchema.properties) {
		var schemaWithOnlyProperties = {};
		schemaWithOnlyProperties["properties"] = jsonSchema.properties;
		var filename = createJsonFile(outputFileName, 	schemaWithOnlyProperties, false);
		fns.logHeading("Generation complete");
	}
	else
		fns.logError(schemaFileName + " has no properties.");
}



function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


module.exports = {

  generateSwaggerImpl: generateSwaggerImplFromSchema
};
#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var validate = require("./operationConsistency.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('operationConsistency' in cmdArgs)
{
    validate.operationCheck(cmdArgs['operationConsistency']);
}
else
{
    cmdObj.printHelp();
}

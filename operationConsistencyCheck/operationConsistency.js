
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
const wadl = require('./getWadl.js');
const swagger = require('./getSwagger.js');
const wsdl = require('./getWsdl.js');
const Operation = require('./operation.js');


var globalAddress = "http://cim.vodafone.com/";
var globalServiceName; 
var globalWriteStream; // Output file

const EXPECTED_INPUTS = ["Source", "Correlation", "Cache", "Destination", "RouteInfo"];
const OPTIONAL_INPUTS = ["ResultStatus", "ServiceDocumentation"];

const EXPECTED_OUTPUTS = ["Source", "Correlation", "Cache", "ResultStatus", "Destination"];
const EXPECTED_LIST_OUTPUTS = ["Source", "Correlation", "Cache", "ResultStatus", "Destination", "QueryResult"];
const OPTIONAL_OUTPUTS = ["ServiceDocumentation"];


function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}

function checkOperationIds(operationMapSwagger, operationMapWadl){
	// Check that the operation Ids are in both wadl and swagger
	var swaggerIds = [];
	var wadlIds = [];
	var dupicates = [];
	operationMapSwagger.forEach(function(op) {swaggerIds.push(op.operationId)});
	operationMapWadl.forEach(function(op) {wadlIds.push(op.operationId)});

	// Check for any operation  id in swagger but not in wadl - 
	wadlIds.forEach(function (op) {
		if(!swaggerIds.includes(op)){
			fns.logError("Operation Id " + op + " found in WADL but not in Swagger");
		}
	});


	// Check for any operation id in swagger but not in wadl - however ignore search, hub, report, home and listener
	swaggerIds.forEach(function (op) {
		if(op.startsWith("hub") || op.startsWith("home") || op.startsWith("listener") || op.startsWith("report")|| 
			op.startsWith("monitor")  || op.endsWith("query") ) {
			// Ignore
		}
		else if(!wadlIds.includes(op)) {
			fns.logError("Operation " + op + " found in Swagger but not in WADL");
		}
	});
}

function compareSwaggerAndWadl (swaggerOps, wadlOps) {
	// Compare Swagger and Wadl operations

	// Check if all operations in wadl are in swagger
	let swaggerKeyList = Array.from( swaggerOps.keys() );
	let wadlKeyList = Array.from( wadlOps.keys() );
	wadlKeyList.forEach(function (op) {
		if(!swaggerKeyList.includes(op)){
			fns.logError("Operation " + op + " found in WADL but not in Swagger");
		}
	});

	// Check for any operation in swagger but not in wadl - however ignore search, hub, notify, report, home and listener
	swaggerKeyList.forEach(function (op) {
		if(op.startsWith("/hub") || op.startsWith("/home") || op.startsWith("/listener") || op.startsWith("/report") ||  
			op.startsWith("/monitor")  ) {
			// Ignore
		}
		else if(!wadlKeyList.includes(op)) {
			fns.logError("Operation " + op + " found in Swagger but not in WADL");
		}
	});

	// Check if operation id are the same in wadl and swagger
	checkOperationIds(swaggerOps, wadlOps);

}

function getSwaggerOperationInfo (swaggerJson) {
	var result = new Map();
	var swaggerOperations = swaggerJson.paths; //array of operations
	var keyPath, keyOperator =0;
	//Process each path
	for (keyPath in swaggerOperations){
		//Process each operation with a path
		for (keyOperator in swaggerOperations[keyPath]){
				if(keyOperator != 'parameters') {
				var nameIndex = keyPath + '_' + keyOperator.toLowerCase();
				var newOperation = new Operation(keyOperator, keyPath, swaggerOperations[keyPath][keyOperator], 'swagger', globalServiceName);
				result.set(nameIndex, newOperation);
			}	
		}
	}
	return result;
}

function getWadlOperationInfo (wadlJson) {
	var result = new Map();
	var wadlOperations = wadlJson.elements[0].elements[2].elements; //array of operations
	var keyPath, keyOperator = 0;
	//Process each Operation
	for (keyPath in wadlOperations){
		var opName = wadlOperations[keyPath].attributes.path;
		var opWadl = wadlOperations[keyPath].elements;

		for (key in opWadl){
		    var name = opWadl[key].name;
		    var id = opWadl[key].id;
            var attributes = opWadl[key].attributes;
            var elements = opWadl[key].elements;

            //Grab the responses
            if (name == 'wadl:method'){
            	var keyEl = 0;
            	var wadlResponses = [];
                for (keyEl in elements){
                    var current = elements[keyEl];
                    if(current.name == 'wadl:response') {
                        Array.prototype.push.apply(wadlResponses, current.attributes.status.split(' '));
                    }
                }
            	var newOperation = new Operation(attributes.name, opName, elements, 'wadl',  globalServiceName, attributes.id);
				var nameIndex = opName + '_' + attributes.name.toLowerCase();
				if(!nameIndex.startsWith("/")) {
					nameIndex = "/" + nameIndex;
				}
            	result.set(nameIndex, newOperation);
            }
        }
    }
	return result;
}

function getWsdlOp(operation) {
	// Get info for a single operation
	//console.log(operation);
	var result;
	if(operation.attributes.name) {
		//fns.logInfo(operation.attributes.name);
		result = operation.attributes.name;
	}
	return result;
}

function getWsdlOperationInfo (wsdlSchema) {
	// Get the operation name and param names for each operation
	var result = new Map();
	if(wsdlSchema && wsdlSchema.elements && wsdlSchema.elements[0].elements) {
		// Traverse elements looking for operation
		wsdlSchema.elements[0].elements.forEach(function (el) {
			if(el.name == "wsdl:binding") {
				// Look within the binding for the operations
				if(el.elements) {
					//Travse for operations
					el.elements.forEach(function (op) {
						if(op.name == "wsdl:operation") {
							result.set(getWsdlOp(op), []);
						}
					});
				}
			}
		});
	} 
	else {
		fns.logWarn("No WSDL operations found");
	}
	return result;
}

function getFileContents (filename, type) {
	// read a file 
	var result;
	if(fs.existsSync(filename)) {
		switch (type){
			case "swagger" :
				result = swagger.openJsonFile(filename);
				break;
			case "wadl" :
				result = wadl.openWadlFile(filename);
				break;
			case "wsdl" :
				result = wsdl.openWsdlFile(filename);
				break;
			default: 
		}
	}
	return result; 
}

function display(iterator, type) {
	let list = Array.from( iterator.keys() );
	fns.logHeading(type + " Operations");
	list.forEach(function(op) {
		fns.logInfo(op);
	});
}


/**
 * Valiate that the operations files are consistent - wsdl, wadl and swagger - wsdl and wadl may not always exist 
 * @param {string} fileName 
 */
function operationCheck (filenames) {
	
	fns.logHeading("Valiate Operations across Swagger, WADL and WSDL");
	var swaggerFilename = filenames[0];
	var wadlFilename = filenames[1];
	var wsdlFilename = filenames[2];

	//set service name
	globalServiceName=getServiceName(swaggerFilename);
	fns.logHeading(globalServiceName);
	//Read  files
	var swaggerOperations = getSwaggerOperationInfo(getFileContents(swaggerFilename, "swagger"));
	display(swaggerOperations, "Swagger");
	var wadlOperations = getWadlOperationInfo(getFileContents(wadlFilename, "wadl"));
	display(wadlOperations, "WADL");
	var wsdlOperations = getWsdlOperationInfo(getFileContents(wsdlFilename, "wsdl"));
	display(wsdlOperations, "WSDL");

	if(swaggerOperations && wadlOperations) {
		compareSwaggerAndWadl(swaggerOperations, wadlOperations);
	}

}



function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


module.exports = {

  operationCheck: operationCheck
};


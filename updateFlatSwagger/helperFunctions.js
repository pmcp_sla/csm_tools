/**
 * Global helper function module.
 * @module helperFunction
 */

const chalk = require('chalk');

/**
 * Displays an error message
 * @param {string} display - The error message.
 */
function logError(display){
        console.log("\tError: ", chalk.red.bold(display));
  }

/**
 * Displays an info message
 * @param {string} display - The error message.
 */
function logInfo(display){
        console.log(display);
  }

/**
 * Displays a warning message
 * @param {string} display - The error message.
 */
function logWarn(display){
        console.log("\tWarning: ", chalk.yellow.bold(display));
  }

/**
 * Displays a fatal error message
 * @param {string} display - The error message.
 */
function logFatal(display){
        console.log("Fatal Error: ", chalk.red.bold(display));
        process.exit(2);
  }

function logHeading(display){
        console.log(chalk.bgYellow.bold(display));
  }

module.exports = {

  logError: logError,
  logInfo: logInfo,
  logWarn: logWarn,
  logFatal: logFatal,
  logHeading: logHeading
};

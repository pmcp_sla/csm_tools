const path = require('path');
const SwaggerParser = require('swagger-parser');
var chalk = require('chalk');
const utils = require('./utils.js');
var fs = require('fs');
const fns = require('./helperFunctions');
var check = require('./validateFlatSwagger.js');
const $RefParser = require("json-schema-ref-parser");
var jsonQuery = require('json-query');
var globalReferences = new Set();
var globalReferenceDefns = new Map();
var globalServiceName;
var globalAllOfLocations = new Set();
var globalAllOfLocationsDefns = new Map();

const validateOptions = {
    allow: {
        json: false, // Don't allow JSON files
        empty: true      // Don't allow empty files
    },
    $refs: {
        internal: false   // Don't dereference internal $refs, only external
    },
    cache: {
        fs: 1, // Cache local files for 1 second
        http: 600         // Cache http URLs for 10 minutes
    },
    validate: {
        spec: true, // Don't validate against the Swagger 2.0 spec
        schema: true
    },
    dereference: {
        circular: false                 // Don't allow circular $refs
    },
    resolve: {
        //file: false                 // Don't dereference internal $refs, only external
    }
};

function toDotNotation (name) {
    return name.replace(/#\//g, "").replace(/\//g, ".");
}

function traverseJson (schemaJson, currentName){
    // Traverse the json schema and find $Refs
    //console.log('NAME=',currentName);
    //console.log(schemaJson.properties);
    // Check for {} generation which is a swagger gen bug
    var nameDepth = (currentName.match(/\./g) || []).length; 
    if( nameDepth > 0 && schemaJson.allOf){
        fns.logWarn("allOf found at " + currentName + " which may cause empty schema {} to be displayed - manually dereference the swagger to repace the allOf");
        if(currentName) 
            globalAllOfLocations.add("#/definitions/" + currentName.replace(/\./g, ".properties.").replace(/\[\*\]/g, ".items"));
    }
    var properties; 
    if (schemaJson.properties) 
        properties = Object.entries(schemaJson.properties);
    else if( (schemaJson.items) && (schemaJson.items.properties)) { // Look inside items
        properties = Object.entries(schemaJson.items.properties);
    }
    else if( (schemaJson.items)) { // Items with no properties
        properties = Object.entries(schemaJson.items);
    }
    else if( (schemaJson.allOf) && (schemaJson.allOf[1]) && (schemaJson.allOf[1]).properties) { // Look inside allOf
        properties = Object.entries(schemaJson.allOf);
        //traverseJson(schemaJson.allOf[1], currentName);
    }
    else {
        properties = Object.entries(schemaJson);
    }

    if(properties) { //Properties found
        properties.forEach(function(property, value){ // Traverse each property looking for attributes
            //if(property[0].endsWith("specification")) console.log("property1=", property[1]);
            if(property[0] ) { 
                //console.log(property[1].type);
                switch (property[1].type) {
                    case 'array':
                        // Process array properties
                        var arrayProperties;
                        if((property[0].endsWith('_links')) && (property[1].items) ) { 
                            arrayProperties = property[1].items;
                            if(arrayProperties.properties){
                                traverseJson(arrayProperties, currentName + '.' + property[0] +'[*]');
                            }
                            else {
                                traverseJson(property[1], currentName + '.' + property[0] +'[*]');
                            }
                        }
                        else if((property[1].items) && (property[1].items.allOf)) { // List of items - allOf
                            arrayProperties = property[1].items;
                            traverseJson(arrayProperties, currentName + '.' + property[0] +'[*]');
                            /*arrayProperties.forEach(function(arrayProperty){
                                    traverseJson(arrayProperty, currentName + '.' + property[0] +'[*]');
                                });*/
                        } 
                        else if ((property[1].items) && (property[1].items.properties)) { // Single object type
                            arrayProperties = property[1].items;
                            traverseJson(arrayProperties, currentName + '.' + property[0] +'[*]');
                        }
                        else if ((property[1].items) && (property[1].items.items) && (property[1].items.items.properties)) { // Array of arrays
                            arrayProperties = property[1].items;
                            traverseJson(arrayProperties, currentName + '.' + property[0] +'[*]');
                        }
                        else if ((property[1].items) ) { // Single object type
                            traverseJson(property[1], currentName + '.' + property[0] +'[*]');
                        }
                        else {
                            //console.log("Check json for this entry - assumed to be an object: " + Object.entries(property[1]));
                            traverseJson(property[1], currentName + '.' + property[0]);
                        }
                        break;
                    case 'object' : 
                        // Process an object 
                        var objProperties;
                        if((property[1]) && (property[1].allOf)) { // List of items - allOf
                            objProperties = property[1];
                            traverseJson(objProperties, currentName + '.' + property[0] );
                            /*objProperties.forEach(function(objProp){
                                    traverseJson(objProp, currentName + '.' + property[0]);
                                });*/
                        } 
                        else if (property[1].properties) { // Single object type
                            objProperties = property[1];
                            traverseJson(objProperties, currentName + '.' + property[0] );
                        } 
                        else if (property[1].$ref) { // Single object type
                            objProperties = property[1];
                            traverseJson(objProperties, currentName + '.' + property[0] );
                        }
                        else {
                            //console.log("Check json for this entry - assumed to be an object: " + Object.entries(property[1]));
                            traverseJson(property[1], currentName + '.' + property[0]);
                        }
                        break;
                    case 'string' :
                        break;
                    case 'boolean' : 
                        break;
                    case 'number' : 
                        break;
                    default:
                        if(property[1] && property[1].$ref){
                            // Found a reference
                            var depth = ((property[1].$ref).match(/\//g) || []).length;
                            if(depth>2){
                                //console.log("$ref= ", property[1].$ref);
                                globalReferences.add(property[1].$ref);
                            }
                        }
                        else if(property[0] && property[0]=="$ref"){
                            // Found a reference
                            var depth = ((property[1]).match(/\//g) || []).length;
                                //console.log("$ref= ", property[1]);
                            if(depth>2){
                                globalReferences.add(property[1]);
                            }
                        }
                        else if(property[1] && property[1].properties) {
                            // Should probably be an object
                            objProperties = property[1];
                            traverseJson(objProperties, currentName + '.' + property[0] );
                        }
                        else if(property[1] && property[1].allOf) {
                            // Should probably be an Object
                            objProperties = property[1].allOf;
                            objProperties.forEach(function(key){
                                    traverseJson(key, currentName + '.' + property[0]);
                                });
                        }
                        else {
                            //console.log("Defaulted this entry:" + property[0]);
                        }
                        break;
                }
            }
        });
    }
    else {
            //console.log("No entries for:" + currentName);
    }

}



/**
 * Perform a validation of the swagger file but using future&promise (async)
 *
 * @param {type} swaggerFile    - input file location
 * @return
 */
function validateProm(swaggerFile)
{
    var apiObj = null;

    var file = path.resolve(process.cwd(), swaggerFile);
    rootFolder = path.dirname(file);

    fns.logHeading("Validating the swagger file: " + swaggerFile);

    var promise = new Promise(function (resolve, reject) {
        SwaggerParser.validate(file, validateOptions)
                .then(function (api) {
                    resolve(api);
                })
                .catch(function (err) {
                    console.error(err);
                    fns.logFatal("------------->: [!!!NOT VALID!!! - Please resolve the above errors ]");
                    reject(Error(err));
                });
    });

    return promise;
}

/**
 * Perform a validation of swagger file
 *
 * @param {type} swaggerFile
 * @return {undefined}
 */
function validate(swaggerFile)
{
    var allValid = true;

    var file = path.resolve(process.cwd(), swaggerFile);
    rootFolder = path.dirname(file);

    fns.logHeading("Validating the swagger file: " + swaggerFile);

    SwaggerParser.validate(file, validateOptions
            )
            .then(function (api) {
                console.log("API name: %s, Version: %s: [%s]", api.info.title, api.info.version, chalk.green("VALID"));
            })
            .catch(function (err) {
                console.error(err);
                allValid = false;
                fns.logFatal("------------->: [!!!NOT VALID!!! - Please resolve the above errors ]");
            });
}

/**
 *
 *
 */
function bundleAll(startPath, filter, level){

    //console.log('Starting from('+level+') dir '+startPath+'/');

    if (!fs.existsSync(startPath)){
        console.log("No directory with path: ",startPath);
        return;
    }

    var files=fs.readdirSync(startPath);
    for(var i=0;i<files.length;i++){
        var filename=path.join(startPath,files[i]);

        if (filename.indexOf('Trunk')>=0)
        {
          var stat = fs.lstatSync(filename);
          if (stat.isDirectory()){
              bundleAll(filename,filter, ++level); //recurse
          }
          else if (filename.indexOf(filter)>=0) {
              //generate the flat swagger file
              var baseName = path.basename(filename, path.extname(filename));
              var destinationFile = baseName + ".flat.swagger";
              bundle(filename, destinationFile);
          };
        }
        else
        {
          //we increase level
          var stat = fs.lstatSync(filename);
          if (stat.isDirectory()){
              bundleAll(filename,filter, ++level); //recurse
          }
        }
    };
};


function removeAllOfs (jsonObj) {
    // There is an issue with the swagger generator where by if an allOf occurs within a definition then it generates an empty object {}
    // See https://github.com/swagger-api/swagger-codegen/issues/796 and other such issues
    // The traverse Json call above identified the offend allOf calls and added their location to globalAllOfLocations
    // This function replaces these allOfs with a reference to a definitions
    var result = jsonObj;
    console.log(globalAllOfLocations);
    // For each reference found grab its defn - jsonQuery uses dot notation to search
    globalAllOfLocations.forEach(function(ref){
        globalAllOfLocationsDefns[ref] = jsonQuery(toDotNotation(ref), {data: jsonObj}).value;
    });
    console.log(globalAllOfLocationsDefns);
    // For each location defn add a definition to the json object result
    var newDefinitions = jsonObj.definitions;
    globalAllOfLocations.forEach(function(ref){
        if(globalAllOfLocationsDefns[ref]){
            var pieces = ref.split("\.");
            var newDefinitionName = pieces[pieces.length-1]; //Name becomes the last part
            if(ref.endsWith("_links/items")) {
                // Hard code link
                newDefinitionName = "_linksItems"
            }
            else if(ref.endsWith("items")) {
                // Add second last name to ref link
                newDefinitionName = pieces[pieces.length-2] + "Items"
                //fns.logInfo("Items reference found - " + newDefinitionName)
            }
            //Assumption that two reference names with the same name will have same definition
            if(newDefinitionName in newDefinitions) {
                fns.logInfo("Duplicate found " + newDefinitionName + " - manually check flat swagger!");
            }
            else {
                fns.logInfo("Added defn " + newDefinitionName)
            }
            newDefinitions[newDefinitionName] = globalAllOfLocationsDefns[ref];
        }
        else{ 
            fns.logWarn("No definition found for ", ref)
        }
    });
    result["definitions"] = newDefinitions;
    return result;
}


function derefJson (jsonObj){
    // Review the definitions and deref any $refs which have a path longer than 2
    // e.g. $ref to #/definitions/BasicComponents/properties/IDType replaced by #/definitions/IDType
    var result = jsonObj;
    if(jsonObj.definitions) {
        const definitions = jsonObj.definitions;
        const definitionsKeys = Object.keys(definitions);
        // Traverse Json looking for references - places them in globalReferences
        definitionsKeys.forEach(function(defnName){
            traverseJson(definitions[defnName], defnName);
        });
        // For each reference found grab its defn - jsonQuery uses dot notation to search
        globalReferences.forEach(function(ref){
            globalReferenceDefns[ref] = jsonQuery(toDotNotation(ref), {data: jsonObj}).value;
        });
        console.log("References updated=" , globalReferences);
        // For each reference add a definition to the json object result
        var newDefinitions = definitions;
        globalReferences.forEach(function(ref){
            if(globalReferenceDefns[ref]){
                var pieces = ref.split("\/");
                var newDefinitionName = pieces[pieces.length-1]; //Name becomes the last part
                if(ref.endsWith("_links/items")) {
                    // Hard code link
                    newDefinitionName = "_linksItems"
                }
                else if(ref.endsWith("items")) {
                    // Add second last name to ref link
                    newDefinitionName = pieces[pieces.length-2] + "Items"
                    //fns.logInfo("Items reference found - " + newDefinitionName)
                }
                //Assumption that two reference names with the same name will have same definition
                if(newDefinitionName in newDefinitions) {
                    fns.logInfo("Duplicate found " + newDefinitionName + " - manually check flat swagger!");
                }
                newDefinitions[newDefinitionName] = globalReferenceDefns[ref];
            }
            else{ // Since reference not found there are a few cases associated with roles where there is an issue in the existing definitions
                  // Try each existing definition to find the reference definition
                  var newDef;
                  definitionsKeys.forEach(function(defn){
                    // Check if the current defn has the same name as the current reference and try to use it
                    var pieces = ref.split("\/");
                    searchDefName = defn + "\/" + "properties" + "\/" + pieces[pieces.length-1];
                    var refDot =  searchDefName.replace(/#\//g, "");
                    refDot = "definitions." + refDot.replace(/\//g, ".");
                    if(!newDef) { // Try to define new definition based on an existing name
                        newDef = jsonQuery(refDot, {data: jsonObj}).value; // Returns undefined if not found
                        newDefinitionName = pieces[pieces.length-1]; //Name becomes the last part
                    }
                  });
                  // Alternative definition found so use it
                  if(newDef){
                    newDefinitions[newDefinitionName] = newDef;
                    fns.logWarn("Reference " +  ref + " has been updated to #/definitions/" + newDefinitionName + " - manually check definition!");
                  }
                  else {
                    fns.logError("Reference definition not found for: " + ref + " - manually add missing definition " + setNewReferencename(ref) + 
                                    " by finding the equivalent attribute or dereference it using an equivalent definition");
                  }
            }
        });
        result["definitions"] = newDefinitions;
    }
    return result;
}

function setNewReferencename (ref){
    var pieces = ref.split("\/");
    var newRef = "#/definitions/" + pieces[pieces.length-1];

    if(ref.endsWith("_links/items")) {
        // Hard code a link
        newRef = "#/definitions/_linksItems"
    }
    else if(ref.endsWith("items")) {
        // Add second last name to ref link
        newRef = "#/definitions/" + pieces[pieces.length-2] + "Items"
    }
    return newRef;
}

function replaceGlobalReferences (jsonStr){
    // replace any references in globalReferences with the reference to its new defn
    var result = jsonStr;
    globalReferences.forEach(function(ref){
        var newRef = setNewReferencename(ref);
        var regex = new RegExp(ref, "g");
        result = result.replace(regex, newRef);
    });
    return result;
}



function getServiceName (schemaFileName){    
    //Get the resource Collection Name - assumed to be the filename before the extension
    var serviceName = path.basename(schemaFileName);
    serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
    if(serviceName.toLowerCase().endsWith('vbo')){
        serviceName = serviceName.slice(0, -3);
    }
    else if(serviceName.toLowerCase().endsWith('vbs')){
        serviceName = serviceName.slice(0, -3);
    }
    return serviceName.toLowerCase()
}

/**
 * Create a swagger flat file
 *
 * @param {type} swaggerFile    - swagger file with external references
 * @param {type} outputFile     - flat swagger file
 * @return
 */
function bundle(swaggerFile, outputFile)
{
  /*Hardcoded Functionality - start */
   var folderPath = path.resolve(process.cwd(), swaggerFile);
    var outputFileName = path.basename(outputFile);
    var outputFullPath = path.resolve(process.cwd(), outputFile);

   /* Hardcoded Functionality - end */
   // Usual functionality - START
    var allValid = true;
    var file = path.resolve(process.cwd(), swaggerFile);
    rootFolder = path.dirname(file);

    console.log("%s %s", chalk.yellow("Creating the swagger flatten file for:"), chalk.green(swaggerFile));
    //set service name
    globalServiceName=getServiceName(swaggerFile);


    validateOptions.$refs.internal = false;
    validateOptions.dereference.circular = false;

    $RefParser.bundle(file, validateOptions)
            .then(function (api) {
                console.log(chalk.yellow("File successfully generated in memory"));
                var newJson = removeAllOfs(derefJson(api));
                var json = JSON.stringify(newJson, null, 4);
                json = replaceGlobalReferences(json);
                // Update flat swagger file names to $refs to add flat swagger file name
                //json = json.replace(/\"#\/definitions\//g, '\".\/' + outputFileName + '#\/definitions\/');
                //fs.writeFile(outputFile, json);
                //json = JSON.stringify(api, null, 4);
                fs.writeFile(outputFile, json, function (err) {
                    if (err)
                        return console.log("%s %s", chalk.red("Unable to save the file:"), outputFile);
                    else
                        return console.log("%s %s", chalk.yellow("File successfully saved to disk:"), chalk.green(outputFile));
                });
            })
            .catch(function (err)
            {
                console.error(err);
                console.log(chalk.red("Unable to flatten the file, looks like the swagger file is invalid"));
                exit(1);
            });

    // Check the generated flat swagger
    //check.validateFlatSwagger(outputFullPath);
}

module.exports = {
    validate: function (swaggerFile) {
        return validate(swaggerFile);
    },
    validatePromise: function (swaggerFile)
    {
        return validateProm(swaggerFile);
    },
    bundle: function (params)
    {
        return bundle(params[0], params[1]);
    },
    bundleAll: function (rootFolder)
    {
        return bundleAll(rootFolder,'.swagger', 0);
    }
};

This repo contains tools to help with the development of the Vodafone CSM (Common Service Model). For each service based on the TM Forum services there is a set 
of documents and technical artefacts.

CSM services are delivered with SOAP and REST realisations. For SOAP there are xsd and wsdl files. For REST there are JSON and Swagger files. 

As well as the tools described here there is also a Vodafone set of tools stored and managed on their GitHub. The tools in this repo are related to these Vodafone libraries. In some cases the tools in this Repo have been replaced by the Vodafone version as the SLA functionality has been merged in.

The tools are listed below.

* __diffSwagWadl__ - DEPRECATED diff tool between swagger and wadl files - wadl is being depreciated - tool was not completed but is 90% there

* __generateCSV__ - DEPRECATED (use Vodafone utils) - generate the CSV files based on the xsd and json schema files - the CSV can then be cut and pasted into the Excel files which are required to list every attribute; this functionality migrated to the Vodafone CSM Utilities

* __generateCSVFromFlatSwagger__ - DEPRECATED (use Vodafone utils)

* __generateExamples__ - generate the example files based on the xsd and json schema files - generated examples should be reviewed as more specific actuals may be required

* generateFlatSwagger - DEPRECATED (use Vodafone utils)

* __generateJSON_LD__ - generates a json LD file - not sure they are used but still required 

* __generateSwaggerImpl__ - generates the Swagger implementation files used with Spring Boot - will not always work due to limitations with the swagger-codegen-cli.jar but is a good first cut; the limitation means that generated subfiles will not always work in which case the subfile needs removed and it contents used instead; see https://github.com/swagger-api/swagger-codegen/issues/3614 and https://github.com/swagger-api/swagger-parser/issues/671 

* __operationConsistencyCheck__ - completes some basic checks across Swagger, wWadl and Wsdl files - mainly checks operation names

* __updateSwagImpl__ - adds a type object to the swagger implementation files 

* __scripts__ - scripts to execute these tools and some other checks 

* __version__ - sets the version number in some of the REST and SOAP files - saves time doing a manual edit 

* __wadlValidateRest_1.2.4__ - local copy of the Vodafone validation tool with update for wadl; the wadl piece has not been merged into the Vodafone CSM uUilies. Better to use the Vodafone version unless you are interested in Wadl. DEPRECATED (use Vodafone utils)

* __wsdlValidate__ - checks the wsdl file for correct headers in each operation

The tools should be deployed in directory: ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools

The services to be checked should be in directory: ${HOME}/Documents/GitHub
A key service dependency is on the CommonSchemas location. This is required to be installed under ${HOME}/Documents/GitHub; review the declarations.sh file under scripts directory.

The scripts are executed from within directories:
${technicalLocation}/ValidateSoap/ for SOAP
${technicalLocation}/CurrentValidateRest for REST
where technicalLocation is defined in the declarations file under ${HOME}/Documents/CSM_Interfaces/Technical/SLA_Tools/scripts. 

Warning: The node implementation directories contain the node_modules on which each implementation is based. In the future this could cause issues if 
the node modules are out of step with the version of node being used (developed against node v8.9.4).

Please install the following tools under linux (the tools should work on Windows 10):

* npm

* node

* ajv - node tool (see below)

* xmllint - may already be installed otherwise for Ubuntu use "apt-get install libxml2-utils"

* appache-cxf for the wsdlvalidator - http://cxf.apache.org/download.html; install at ${technicalLocation}/appache-cxf

* maven

* spring boot

* swagger-codegen-cli.jar needs to be somewhere like ${technicalLocation}/CurrentValidateRest (this is the location of where JSON based commands are executed) - install version 2.2.3 but call the file swagger-codegen-cli.jar

* jq (for JSON formatting) 


node modules:

* npm install -g command-line-args

* npm install -g command-line-usage

* npm install –g swagger-parser

* npm install –g chalk

* npm install –g ajv

* npm install –g findit

* npm install –g child_process

The readme under Git/scripts/CSMUtilies has the latest instructions on installing the utilities and using them. Many of the scripts created here are to make it easier to run these utilies.


The ValidateRest folder (currently wadlValidateRest_1.2.4) will contain several references to local files which will need updated for any new install. These are Vodafone tools developed Windows so manually updates are required. Search for 
user pmcpa. One example is in file generateCode.js.


The detailed commands to use are in the script files - they call the modules above.

The main scripts are:

* setUpValidateSoap.sh to check a SOAP specification for a service - checks the schema and the example files; should be executed in a local directory away from the services

* setUpValidateRest.sh to check the REST specification for a service - checks the schema, the swagger and the example files and generates Output for Swagger UI; should be executed in a local directory away from the services which has a local copy of swagger-codegen-cli.jar

* runRest.sh executes the generated Swagger file for a REST service; the result can be viewed at for example: 	http://localhost:8080/%7Baccess%7D/subscriptionAPI/v2/swagger-ui.html#/subscription; 

* checkAll.sh is a combined script to check most aspects of a service package; SOAP and REST


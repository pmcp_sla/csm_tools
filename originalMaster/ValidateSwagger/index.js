#! /usr/bin/env node

const fs = require('fs');
const cmdObj = require("./parseCommandLine.js");
const cmdArgs = cmdObj.parseCommandLine();
const val = require("./validateSwagger.js");

if ('help' in cmdArgs){
  cmdObj.printHelp();
}
else if (('print' in cmdArgs) && ('input' in cmdArgs))
{
  val.print(cmdArgs['input']);
}
else if (('input' in cmdArgs) && ('service' in cmdArgs)) {
  val.validate(cmdArgs['input'], cmdArgs['service']);
}
else if ('input' in cmdArgs){
  val.validate(cmdArgs['input']);
}
else{
  cmdObj.printHelp();
}


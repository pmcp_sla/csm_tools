const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  {
    name: 'input',
    type: String,
    multiple: false,
    description: 'The swagger input file to process',
    typeLabel: '<file>' 
  },
  {
    name: 'service',
    type: String,
    multiple: false,
    description: 'The name of the service to process (use camel case); if not provided then the input swagger filename is used.',
    typeLabel: '<file>' 
  },
  {
    name: 'print',
    type: String,
    multiple: false,
    description: 'Summary print for swagger and wadl file'
  }
];


module.exports = {
  parseCommandLine: function()
  {

    try {
        return commandLineArgs(optionDefinitions, { partial: true });
    } catch (e) {
        console.error(e.message);
    } finally {
    }

    return null;

  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'Validate a swagger file',
        content: 'Validates a swagger file \n\t Usage: validateSwagger  --input <filename.swagger> '
      },
      {
        header: 'Options',
        optionList: [
          {
            name: 'input',
            typeLabel: '{underline file}',
            description: 'The swagger file.'
          },
          {
            name: 'service',
            typeLabel: '{underline name}',
            description: 'The name of the service to process (use camel case); if not provided then the input swagger filename is used.'
          },
          {
            name: 'print',
            description: 'Print a summary of the swagger file.'
          },
          {
            name: 'help',
            description: 'Print this usage guide.'
          }
        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

}

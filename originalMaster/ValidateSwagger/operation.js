"use strict";
const chalk = require('chalk');
const fns = require('./helperFunctions');
const path = require('path');
const REFERENCE = 'reference';
const COLLECTION = 'COLLECTION';
const RESOURCE_TASK = 'RESOURCE_TASK';
const RESOURCE_ENTITY = 'RESOURCE_ENTITY';

/** Class representing an Operation. */
class Operation {
    
    /**
     * Create an operation.
     * @param {string} method - The method name, GET, POST, PUT, PATCH or DELETE.
     * @param {string} scope - The operation path 
     * @param {json Object} json - The json object.
     * @param {string} type - The type of json obejct swagger or wadl.
     * @param {string} sName - The name of the service
     */
    constructor(method, scope, json, type, sName) {
    	if (type == 'swagger') {
            this.method = method.toUpperCase();         // The method name, GET, POST, PUT, PATCH or DELETE.
            this.scope = scope;                         // The operation path such as /listener
            this.name = this.method + ' ' + this.scope; // Overall operation name which is a combo of the method and scope
            this.params = new Map();                    // The operation parameters
            this.responses = new Map();                 // The operation responses
            this.operationId = json.operationId;        // The operation Id 
            this.serviceName = sName;                   // The name of the service - comes from the swagger file name or passed in as a param
            this.resourceType = "";                     // Defines an operation to be on a Collection, Resource Task or a Resource Entity
            this.tags = "";                             // Tags associated with the operation
            this.setResourceType();
    		this.createOperationFromSwagger(json, sName);
    	}
    	else {
    		fns.logFatal("Unable to create operation" + this.name);
    	}
    }



    /**
     * Create an operation from Swagger.
     * @param {json Object} json - The json object.
     * @param {sname} - the service name
     * @return {operation object} operation
     */
    createOperationFromSwagger (jsonObj, sname){
        // Move params to a list of names such as ID, string, type, string, ...
        var params = jsonObj.parameters;
        var key = 0;
        for (key in params){
            if (typeof params[key].name === 'string' || params[key].name instanceof String) {
                if (['default', 'undefined', 'query', 'X-Notify-Business-EventCode', 'X-Total-Count', 'hub', '$ref'].indexOf(params[key].name) == -1 ) {
                    if (params[key].type)
                       this.params.set(params[key].name, params[key].type);
                }
                else if((params[key].name == 'query') && (params[key].schema) && (params[key].schema.$ref)){
                    //Push a reference on for the param
                    this.params.set('query', params[key].schema.$ref);
                }
            }
            else if(params[key].$ref){
                this.params.set(params[key].$ref, params[key].$ref);
            }
        }
        // Move response to a map from number to properties
        var responses = jsonObj.responses;
        var key = 0;
        for (key in responses){
            if (['default', 'undefined', 'query'].indexOf(key) == -1 ) {
                this.responses.set(key, responses[key]);
            }
        }
        // Read the tags
        this.tags = jsonObj.tags;
        if ( (!(this.tags)) || this.tags.length == 0) {
            fns.logError('Tags entry not provided or empty');
        }
    }

    /**
     * Print a summary of the operation
     */
    print() {
        console.log('\nOperation:', chalk.bold(this.name));
        console.log('\toperationId:', chalk.bold(this.operationId));
        console.log('\tmethod: ', this.method);
        console.log('\tscope: ',this.scope);
        console.log('\tservice name: ',this.serviceName);
        console.log('\tresourceType: ',this.resourceType );
        console.log('\ttags: ',this.tags );
        if (this.params.size==0)
        {
            console.log("Parameters: []");
        }
        else {
            console.log('Parameters');
            this.params.forEach(function(value, key){console.log('\t', key, ':', value);});
        }
        if (this.responses.size==0)
        {
            console.log("Responses: []");
        }
        else {
            console.log('Responses');
            this.responses.forEach(function(value, key){
                console.log('\t', key, ':', value);
            });
        }
    }

    setResourceType () {
        //set the resource type to be a collection or a resource task or resource entity
        if((this.scope.toLowerCase() == '/hub') || (this.scope.toLowerCase() == '/home')) {
            this.resourceType=RESOURCE_ENTITY;
        }
        else if (this.scope.toLowerCase() == '/' + this.serviceName.toLowerCase() ) { // DEFAULT for /{collection}
                this.resourceType=COLLECTION;
        }
        else if (this.scope.toLowerCase().endsWith('id}')) {
            this.resourceType=RESOURCE_ENTITY;
        }
        else {
            this.resourceType = RESOURCE_TASK;
        }
    }

    checkErrorResponseCodes (responseList, responses){
        responseList.forEach(function(code){
                if (!(responses.has(code)) ) {
                    fns.logError("Response code " + code + " is missing");
                }
            });
    }


    checkErrorResponseOneOffCodesExist (responseList,  responses){
        var result = 0;
         responseList.forEach(function(code){
                if (responses.has(code)) {
                    result=1;
                }
            });
         if (result == 0) fns.logError("One of response codes " + responseList + " is required");
    }

    checkWarnResponseCodes (responseList, responses){
        responseList.forEach(function(code){
                if (!(responses.has(code)) ) {
                    fns.logWarn("Response code " + code + " is missing");
                }
            });
    }

    checkShouldNotHaveResponseCodes (responseList, responses){
        responseList.forEach(function(code){
                if (responses.has(code)) {
                    fns.logWarn("Response code " + code + " is not required");
                }
            });
    }

    checkResponseIsNotAnArray(responseList, responses){
        if ((this.method != 'GET') && (this.method!='PATCH')){
            responseList.forEach(function(code){
                if((responses.has(code)) && (responses.get(code).schema) && (responses.get(code).schema.type == 'array')) {
                    // Array only allowed on GET or PATCH
                        fns.logError('Response type array not expected on code ' + code);
                }
            });
        }   
    }

    checkExpectedHeaders(responseProperties, code){
        if(responseProperties.headers) {
            if(code == '200') {
                //Check that the correct properties are included
                if (!(responseProperties.headers["X-Rate-Limit-Limit"])) {
                    fns.logError('Header X-Rate-Limit-Limit is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Remaining"])) {
                    fns.logError('Header X-Rate-Limit-Remaining is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Reset"])) {
                    fns.logError('Header X-Rate-Limit-Reset is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Total-Count"])) {
                    fns.logError('Header X-Total-Count is missing from response ' + code);
                }
            }
            else if (code == '206') {
                //Check that the correct properties are included
                if (!(responseProperties.headers["X-Rate-Limit-Limit"])) {
                    fns.logError('Header X-Rate-Limit-Limit is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Remaining"])) {
                    fns.logError('Header X-Rate-Limit-Remaining is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Reset"])) {
                    fns.logError('Header X-Rate-Limit-Reset is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Total-Count"])) {
                    fns.logError('Header X-Total-Count is missing from response ' + code);
                }
                if (!(responseProperties.headers["Content-Range"])) {
                    fns.logError('Header Content-Range is missing from response ' + code);
                }
            }
        }
        else {
            fns.logError('No headers for response code ' + code);
        }
    }

    checkNotExpectedHeaders(responseProperties, code){ 
        //No pagination
        if(responseProperties.headers) {
                //Check that the correct properties are NOT included
                if ((responseProperties.headers["X-Rate-Limit-Limit"])) {
                    fns.logError('Header X-Rate-Limit-Limit is not required for response ' + code);
                }
                if ((responseProperties.headers["X-Rate-Limit-Remaining"])) {
                    fns.logError('Header X-Rate-Limit-Remaining is not required for response ' + code);
                }
                if ((responseProperties.headers["X-Rate-Limit-Reset"])) {
                    fns.logError('Header X-Rate-Limit-Reset is not required for response ' + code);
                }
                if ((responseProperties.headers["X-Total-Count"])) {
                    fns.logError('Header X-Total-Count is not required for response ' + code);
                 }
        }
    }


    checkResponseProperties(responseProperties, code){
        //Check that a search, query or list returns an array
        if((this.operationId.toLowerCase().endsWith('search')) || (this.operationId.toLowerCase().endsWith('list')) || 
            (this.operationId.toLowerCase().endsWith('query')) ){
            if((responseProperties.schema) && (responseProperties.schema.type == 'array')) {
                //Check that the items exists
                if((responseProperties.schema) && (responseProperties.schema.items)) {
                    if(responseProperties.schema.items.$ref){
                        //Get the service name
                        if(responseProperties.schema.items.$ref.toLowerCase() != '#/definitions/' + this.serviceName.toLowerCase() + 'vbo') {
                            fns.logWarn('Operation does not define "$ref" as #/definitions/' + this.serviceName.toLowerCase() + 'VBO' + '  for response ' + code)
                        }
                    }
                    else {
                        // Not items defn for the array
                        fns.logError('Operation does not define "$ref" for an array for response ' + code)
                    }
                }
                else {
                    // Not items defn for the array
                    fns.logError('Operation does not define "items" for an array for response ' + code)
                }
            }
            else {
                //Type not specified or not array
                fns.logError('Operation does not return an array for response ' + code)
            }
        }
        // Check the response headers include pagination
        if (this.scope.endsWith('/search') ) {
                this.checkExpectedHeaders(responseProperties, code);
        }
        else if ( (this.scope.endsWith('/report')) || (this.scope.endsWith('/notification')) || (this.scope.endsWith('/listener')) ||  
                   (this.scope.endsWith('/notifications')) ||
                   (this.scope.endsWith('/home')) ||   (this.scope.toLowerCase().endsWith('/hub/{id}')) || (this.scope.toLowerCase().endsWith('id}') ) ||
                   (this.scope.toLowerCase().endsWith('/hub')) ) {
            if(code=='206') {
                if ((responseProperties.headers) && (responseProperties.headers["Content-Range"])) {
                    // okay
                } 
                else {
                    fns.logError('Header Content-Range is missing from response ' + code);
                }
            }   
        }
        else if(this.scope.endsWith('/monitor')) {
            //Okay
        }
        else {
            //collection
             this.checkExpectedHeaders(responseProperties, code);
        }
    }

    confirmResponseReturnType(responseProperties, code) {
        //Check that the type of a 200 or 206 response is made up of method + VBO + a name within the scope
        // Basically checking "$ref": "#/definitions/customerPrivacyProfileVBORelatedCustomerPrivacyProfile"
        // ajv check that the main type exists so we are just checking for a match between RelatedCustomerPrivacyProfile
        // and the scope of the method - would pick up cut and paste errors
        if ((code == '200') || (code == '206')){
            var type = ''; //stores type of definition
            if((responseProperties.schema) && (responseProperties.schema.$ref)) {
                type = responseProperties.schema.$ref;
            }
            else if ((responseProperties.schema) && (responseProperties.schema.items) && (responseProperties.schema.items.$ref)){
                type = responseProperties.schema.items.$ref;
            }
            var originalType = type;
            // Strip off unneeded characters such as vbo and check it is not set to the service name
            type = type.toLowerCase().replace('#/definitions/', '');
            if(type == this.serviceName.toLowerCase() + 'vbo') {
                type = type.replace('vbo', '');
            } 
            else {
                    type = type.replace(this.serviceName.toLowerCase() + 'vbo', '');
            }
            var typeList = this.scope.split('/');
            // Remove the first element if it is a ''
            if(typeList[0] == '') {
                typeList.shift();
            }
            typeList = typeList.map(function(x){ return x.toLowerCase() });

            // Check it the type appears in the list taken from the scope but ignore some cases
            if(this.scope.endsWith('/home'))  {
                if (type != '_links') {
                    fns.logWarn('Return type for /home  for code ' + code + ' should be _links');
                }
            }
            else if (typeList.indexOf(type) > -1 )  {
                // Okay type found in scope
            } 
            else {
                fns.logWarn('Check if type ' + originalType + ' for code ' + code + ' is correct');
            }
        }
    }


    validateResponseCodes () {
        // Check the response codes exist
        if (this.method=="GET") {
            if ((this.resourceType==COLLECTION) || (this.scope.endsWith('/monitor')) ) {
                this.checkErrorResponseCodes(['200', '204', '206', '400', '404', '405', '500'], this.responses);
            } 
            else if (this.scope == '/hub')  {
                this.checkErrorResponseCodes(['200', '204', '206', '400', '404', '405', '500'], this.responses);
                //this.checkShouldNotHaveResponseCodes(['206'], this.responses);
            }   
            else if (this.resourceType==RESOURCE_ENTITY) {
                this.checkErrorResponseCodes(['200', '206', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['204'], this.responses);
            } 
            else if (this.resourceType==RESOURCE_TASK) {
                this.checkErrorResponseCodes(['200', '206', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['204'], this.responses);
                fns.logWarn('Resource Task should not use GET');
            } 

            // Check body of response codes for types
            if((this.responses.has('200')) && (!(this.scope.endsWith('/report'))) ) 
                    this.checkResponseProperties(this.responses.get('200'), '200'); 
            if((this.responses.has('206')) && (!(this.scope.endsWith('/report'))) )
                    this.checkResponseProperties(this.responses.get('206'), '206'); 
        }
        if (this.method=="POST"){
            if((this.scope.endsWith('/search')) || (this.scope.endsWith('/report')) ) {
                this.checkErrorResponseCodes(['200', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['201', '202'], this.responses);
            }
            else if((this.scope.endsWith('/monitor')) || (this.scope.endsWith('/listener')) ) {
                this.checkErrorResponseCodes(['201', '202', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['204'], this.responses);
            }
            else if(this.scope.endsWith('/hub')) {
                this.checkErrorResponseCodes(['200', '201', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['202', '204'], this.responses);
            }
            else if((this.scope.endsWith('/notification')) || (this.scope.endsWith('/notifications')) ) {
                this.checkErrorResponseCodes(['201', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['200', '202', '204', '206'], this.responses);
                //Check the response text
                if((this.responses.has('201')) && (this.responses.get('201').description != "201, Created.") ) {
                    fns.logError('Response body for 201 description on /notification should be "201, Created."');
                }
            }
            else if ((this.resourceType==COLLECTION) ) { 
                this.checkErrorResponseCodes(['200', '201', '202', '400', '404', '405', '500'], this.responses);
            }   
            else if (this.resourceType==RESOURCE_ENTITY) {
                fns.logWarn('Resource Entity should not use POST');
            } 
            else if (this.resourceType==RESOURCE_TASK) {
                this.checkErrorResponseCodes(['200', '201', '202', '204', '400', '404', '405', '500'], this.responses);
            }  
            // Check that array is not used
            this.checkResponseIsNotAnArray(['200', '206'], this.responses);

            // Check pagination is not used
            if(this.responses.has('200') ) 
                    this.checkNotExpectedHeaders(this.responses.get('200'), '200'); 
            if(this.responses.has('206') )
                    this.checkNotExpectedHeaders(this.responses.get('206'), '206');  
        }
        // Check the response codes for a  PUT
        if  (this.method=="PUT") {
            if (this.resourceType==RESOURCE_ENTITY)  { 
                this.checkErrorResponseCodes(['200', '202', '204', '400', '404', '405', '500'], this.responses);
            } 
            else if (this.resourceType==COLLECTION) {
                fns.logWarn('Collection should not use PUT');
            }
            else if (this.resourceType==RESOURCE_TASK) {
                fns.logWarn('Resource Task should not use PUT');
            }
        }
        // Check the response codes for a PATCH
        if  (this.method=="PATCH") {
            if ((this.resourceType==COLLECTION) || (this.resourceType==RESOURCE_ENTITY) ) { 
                this.checkErrorResponseCodes(['200', '202', '204', '400', '404', '405', '409', '412','500'], this.responses);
            } 
            else if (this.resourceType==RESOURCE_TASK) {
                fns.logWarn('Resource Task should not use PATCH');
            }
            // Check pagination is not used
            if(this.responses.has('200') ) 
                    this.checkNotExpectedHeaders(this.responses.get('200'), '200'); 
            if(this.responses.has('206') )
                    this.checkResponseProperties(this.responses.get('206'), '206');
        }
        // Check the response codes for a delete
        if  (this.method=="DELETE") {
            if (this.resourceType==RESOURCE_ENTITY)  { 
                this.checkErrorResponseCodes(['200', '202', '204', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['409'], this.responses);
            } 
            else if (this.resourceType==COLLECTION) {
                fns.logWarn('Collection should not use DELETE');
            }
            else if (this.resourceType==RESOURCE_TASK) {
                fns.logWarn('Resource Task should not use DELETE');
            }
        }

        //Check type of return in response code
        if(this.responses.has('200') ) 
                this.confirmResponseReturnType(this.responses.get('200'), '200'); 
        if(this.responses.has('206') )
                this.confirmResponseReturnType(this.responses.get('206'), '206'); 

        //Check response text for 404 - remove extra spaces
        if((this.responses.has('404')) && (this.responses.get('404').description.replace(/\s+/g,' ').trim() != "404, Not Found.") ) {
            fns.logError('Response body for 404 description should be "404, Not Found."');
        }

     }

     paramsHasReference(name){
        var foundFields = 0; //false
        //Need to traverse all of map to look at each REFERENCE
        this.params.forEach(function(key, value){
            if((key == REFERENCE) && (value == name) ){
                foundFields == 1;
            }
        });
        return foundFields;
     }

     validateParams(){
        // Parameters  required for GET!
        if(this.method == "GET") { 
            // The following operations should have no params: /home, /seach, /hub/{id}, /report, /notification /listener
            if( (this.scope.endsWith('/home')) || (this.scope.endsWith('/search')) || (this.scope.toLowerCase().endsWith('/hub/{id}')) ||
                (this.scope.endsWith('/report')) || (this.scope.endsWith('/notification')) || (this.scope.endsWith('/listener')) ||  (this.scope.endsWith('/notifications')) ) {
                    //Params whoulc be empty
                if(this.params.length > 0){
                    fns.logError('Empty parameters expected."');
                }
             }
             // The following operations should have limit, count and sort
             else if( (this.scope.endsWith('/hub'))  ) {
                if( (this.params.get('#/parameters/limit')) && (this.params.get('#/parameters/count')) && 
                    (this.params.get('#/parameters/sort'))) {
                    //okay but should not include fields
                    if(this.params.get('#/parameters/fields')) {
                        fns.logError('Parameters must not include fields.');
                    }
                }
                else {
                    fns.logError('Parameters must include limit, count and sort.');
                }
            }
             // The following operations should have fields, limit, count and sort
            else if( (this.scope.endsWith('/monitor'))  ) { 
                if( (this.params.get('#/parameters/fields')) &&  (this.params.get('#/parameters/limit')) && 
                    (this.params.get('#/parameters/count')) &&  (this.params.get('#/parameters/sort')) ) {
                    //okay
                }
                else {
                    fns.logError('Parameters must include fields, limit, count and sort.');
                }
            }
             // The following operations should have fields
             else if( (this.scope.toLowerCase().endsWith('id}')) ) {
                if(this.params.get('#/parameters/fields')) {
                    //okay
                }
                else {
                    fns.logError('Parameters must include fields.');
                }
            }
            // The following assumed to be a collection and should have fields, limit, count and sort
            else if (this.scope.toLowerCase() == '/' + this.serviceName.toLowerCase() ) { // DEFAULT for /{collection}
                if( (this.params.get('#/parameters/fields')) && (this.params.get('#/parameters/limit')) && 
                    (this.params.get('#/parameters/count')) &&  (this.params.get('#/parameters/sort')) ) {
                    //okay
                }
                else {
                    fns.logError('Parameters must include fields, limit, count and sort.');
                }
            }
        }
        else {
            if(this.params.size > 0){
                    fns.logWarn('Parameters fields, sort, limit and count not expected for POST, DELETE, PUT and PATCH. Found:');
                    this.params.forEach(function(value, key){console.log('\t\t' ,  value);});
                }
        }
     }

     validateTags(headerTags) {
        // Check that the operation tags occur in the header tags
        if(this.tags) {
            this.tags.forEach(function(tag){
                    if(!(headerTags.has(tag))) {
                        fns.logError('Tag ' + tag + ' not found in file header list of tags');
                    }
                });
        }
        else {
            fns.logError('Header tags not defined');
        }
     }

    /**
     * Validate an operation for common issues
     */
     validate(headerTags){
        fns.logInfo("Validating: " + chalk.bold(this.name) + " Resource type: " + this.resourceType);

        //Validate path starts with servicename
        var sName = '/' + this.serviceName.charAt(0).toLowerCase() + this.serviceName.substr(1);
        if((this.scope.startsWith(sName)) || (this.scope.startsWith('/hub')) || (this.scope.startsWith('/listener')) 
            || (this.scope.startsWith('/home')) || (this.scope.startsWith('/monitor'))  ){
            // Okay
        }
        else {
            fns.logError('Operation path ' + this.scope + ' does not start with ' + sName)
        }

        this.validateParams();

        this.validateResponseCodes();

        this.validateTags(headerTags);
    }

}



module.exports = Operation;

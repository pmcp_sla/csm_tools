const chalk = require('chalk');
const Ajv = require('ajv');
const find = require('findit');
const path = require('path');
const fs = require('fs');
const utils = require('./utils.js');
var _ = require('lodash');

//globals as they are re-used across different scopes
var schemaLinks = null;
var schemaBasicComponents = null;
var schemaCommonComponents = null;
var schemaReportRequest = null;
var schemaReportResponse = null;
var schemaPatchRequest = null;
var schemaListener = null;
var schemaQuery = null;
var schemaHub = null;

function openFile(filename, suffix){
    var json = null;
    var file = path.resolve(process.cwd(), filename);
    try {
        try {
            json = JSON.parse(fs.readFileSync(file).toString());
        } catch(JSONerr) {
            json = require(file);
        }
    } catch(err) {
        console.error('error:  ' + err.message.replace(' module', ' ' + suffix));
        process.exit(2);
    }
    return json;
}

function getBaseURI(rootFolder)
{
  var baseURI = rootFolder.replace(/ /g,"%20");
  baseURI= "file:///"+baseURI.replace(/\\/g,"/")+"/";
  return baseURI;
}

function validateSchema(schemaFile)
{
  var allValid = true;

  var file = path.resolve(process.cwd(), schemaFile);
  rootFolder = path.dirname(file);

  console.log("Validating the json schema file: ", schemaFile);

  //load the file to validate
  var schema = utils.loadFile(file);
  var fileBaseURI = getBaseURI(rootFolder) + path.basename(file);
  schema.$id=fileBaseURI;

  rootFolder = path.resolve(process.cwd(), rootFolder+"/../../../Common/V1/");



  //loading the Common library schemes
  loadCommonScheme(rootFolder);

  //create the AJV context
  var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}

  //set the draft 04 format
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));

  //add the Common schemes to the validator
  ajv.addSchema(schemaLinks);
  ajv.addSchema(schemaBasicComponents);
  ajv.addSchema(schemaCommonComponents);
  ajv.addSchema(schemaReportRequest);
  ajv.addSchema(schemaReportResponse);

  //try to compile the scheme
  try {
    var validate = ajv.compile(schema);
  } catch (e) {
    console.error(e);
    allValid=false;
  } finally {

  }

  console.log("Here!");

  if (allValid)
    console.log("SchemaID [%s]: [%s]",schema.$id, chalk.green("VALID"));
  else
    console.log(chalk.red("------------->: [!!!NOT VALID!!! - Please resolve the above errors ]"));

  console.log();

  return allValid;
};



function loadCommonScheme(commonFolder)
{
  //check if the common folder exists
  if (!fs.existsSync(commonFolder))
  {
    console.error("Folder: [%s] doesn't exists", commonFolder);
    throw new Error("Folder specified does not exist on the disk");
  }

  baseURI = getBaseURI(commonFolder);

  //start loading the JSON Scheme
  schemaLinks = utils.loadFile(commonFolder+"/Links.json");
  schemaLinks.$id=baseURI+"Links.json";
  //schemaLinks.$id="../../../Common/V1/Links.json";
  //schemaLinks.$id="file:///C:/Users/TeneF/Documents/work/SVN/Services/Customer%20Party/Trunk/Docs/CustomerParty/REST/VBO/Common/V1/Links.json"
  //schemaLinks.$id="Links.json"


  schemaBasicComponents = utils.loadFile(commonFolder+'/BasicComponents.json');
  schemaBasicComponents.$id=baseURI+"BasicComponents.json";
  //schemaBasicComponents.$id="../../../Common/V1/BasicComponents.json";
  //schemaBasicComponents.$id="BasicComponents.json";
  //working
  //schemaBasicComponents.$id="file:///C:/Users/TeneF/Documents/work/SVN/Services/Workforce%20Appointment%20Slot/Trunk/Docs/WorkforceAppointmentSlot/REST/VBO/Common/V1/BasicComponents.json";


  schemaCommonComponents = utils.loadFile(commonFolder+'/CommonComponents.json');
  schemaCommonComponents.$id=baseURI+"CommonComponents.json";

  schemaReportRequest = utils.loadFile(commonFolder+'/ReportRequest.json');
  schemaReportRequest.$id=baseURI+"ReportRequest.json";

  schemaReportResponse = utils.loadFile(commonFolder+'/ReportResponse.json');
  schemaReportResponse.$id=baseURI+"ReportResponse.json";

  schemaPatchRequest = utils.loadFile(commonFolder+'/Patch.json');
  schemaPatchRequest.$id=baseURI+"Patch.json";

  schemaListener = utils.loadFile(commonFolder+'/Listener.json');
  schemaListener.$id=baseURI+"Listener.json";

  schemaQuery = utils.loadFile(commonFolder+'/Query.json');
  schemaQuery.$id=baseURI+"Query.json";

  schemaHub = utils.loadFile(commonFolder+'/Hub.json');
  schemaHub.$id=baseURI+"Hub.json";
}

/**
  Validate an example file against the provided validation scheme
  Method requires a valid AJV context with Common library loaded
*/
function validateExample(example, ajvContextValidator)
{
  //TODO: add a validateion for ajvContextValidator
  var valid = ajvContextValidator(example);
  console.log("The file[%s] validation against schema: [%s] is: [%s] \n", exampleBaseName, schemaBaseName, (valid?chalk.green("Valid"):chalk.red("Invalid")));

  if (!valid)
  {
    console.error(ajvContextValidator.errors);
  }
};


/**
* We have to compile a dedicated AJV context for the Patch schema
*/
function validatePatchRequest(exampleFile)
{
  var sourceFile      = utils.loadFile(exampleFile);
  var exampleBaseName = path.basename(exampleFile);
  var schemaBaseName  = path.basename(schemaPatchRequest.$id);

  var ajv = new Ajv();
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));

  var ajvContextValidator = ajv.compile(schemaPatchRequest);
  var valid = ajvContextValidator(sourceFile);
  console.log("The file[%s] validation against schema: [%s] is: [%s]", exampleBaseName, schemaBaseName, (valid?chalk.green("Valid"):chalk.red("Invalid")));

  if (!valid)
    console.error(ajvContextValidator.errors);

}


function validateString(data, exampleSchema, ajvContextValidator)
{
  var valid = ajvContextValidator(data);

  if (!valid)
    console.error(ajvContextValidator.errors);

  return valid;
};


function validateOneByOne(_exampleFile, _schemaVBO)
{
  console.log("Example file provided: [%s]", _exampleFile);
  console.log("VBO Scheme: [%s]", _schemaVBO);

  //normalize the example folder
  var exampleFile = path.resolve(_exampleFile);
  var exampleFolder = path.dirname(exampleFile)
  console.log("Normalized folder is: [%s]", exampleFolder);

  var validationSchemaPath = path.resolve(_schemaVBO);
  console.log("Normalized validation VBO Scheme is: [%s]", validationSchemaPath);

  //Note: assumption here is that we have the CSM common folder structure
  var commonFolder = path.resolve(exampleFolder+"/../../../../Common/V1/");
  console.log("Common folder is: [%s]\n", commonFolder);

  var ajvContextValidator = exampleAJVContext(_exampleFile, _schemaVBO);

  validateExample(exampleFile, validationSchemaPath, ajvContextValidator);
}






function validateAllExamples(_exampleFolder)
{
    console.log("Example folder provided: [%s]", _exampleFolder);

    //normalize the example folder
    var exampleFolder = path.resolve(_exampleFolder);
    console.log("Normalized folder is: [%s]", exampleFolder);

    var implCommonFolder = path.resolve(exampleFolder+"/../../../../Common/V1/Impl/Swagger");

    var index = 0;
    fs.readdirSync(exampleFolder).forEach(file => {
      console.log("Index:[%s], File:[%s]",++index,file);
      var examplePath = path.resolve(exampleFolder+"/"+file);
      //find the equivalent schema in the impl folder
      var exampleSchema = path.resolve(path.dirname(exampleFolder)+"/Impl/Swagger/"+file);

        //exceptions list: Home, Hub, Listener, Patch, Search to the general rule
          if (file.indexOf('-Hub')>-1)
              exampleSchema = path.resolve(implCommonFolder+"/Hub.json");
          else if (file.indexOf('-Home')>-1)
              exampleSchema = path.resolve(implCommonFolder+"/Links.json");
          else if (file.indexOf('-PatchRequest')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/Patch.json");
          else if (file.indexOf('-PatchCreateListRequest')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/Patch.json");
          else if (file.indexOf('-PatchDeleteListRequest')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/Patch.json");
          else if (file.indexOf('-ReportRequest')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/ReportRequest.json");
          else if (file.indexOf('-ReportResponse')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/ReportResponse.json");
          else if (file.indexOf('-SearchRequest')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/Query.json");
          else if (file.indexOf('-Monitor')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/Monitor.json");
          else if (file.indexOf('-Listener')>-1)
                exampleSchema = path.resolve(implCommonFolder+"/Listener.json");
          else if (file.indexOf('-SearchResponse')>-1 || file.indexOf('-HistoryResponse')>-1)
                exampleSchema = path.resolve(path.dirname(exampleFolder)+"/Impl/Swagger/"+file.substring(0,file.indexOf('-'))+".json");

          //load the validation context
          var ajvContextValidator = exampleAJVContext(examplePath, exampleSchema);

          //load the example file
          var exampleObj = utils.loadFile(path.resolve(exampleFolder+"/"+file));

          //check if the file is an array
          if (file.indexOf('-SearchResponse')>-1 || file.indexOf('-HistoryResponse')>-1 || file.indexOf('-PatchResponse')>-1 || file.indexOf('-PatchCreateListResponse')>-1 || file.indexOf('-PatchDeleteListResponse')>-1)
          {
                  var allValid = true;
                  if (Array.isArray(exampleObj))
                  {
      			           console.log("%s", chalk.green("Validating an array of objects..."));
                       exampleObj.forEach(function(s){
                         var result = validateString(s, exampleSchema, ajvContextValidator);
                         if (!result)
                          allValid = false;
                        });
                  }
                  else
                  {
      			        console.log("%s", chalk.yellow("Most likely this should be an array, please double check manually"));
                     var result = validateString(exampleObj, exampleSchema, ajvContextValidator);
                     if (!result)
                       allValid = false;
                  }
          }
          else
          {


            var valid = ajvContextValidator(exampleObj);
            console.log("The file[%s] is: [%s] \n", file,  (valid?chalk.green("Valid"):chalk.red("Invalid")));

            if (!valid)
            {
              console.log("The file[%s] validation against schema: [%s] is: [%s] \n", file, exampleSchema, (valid?chalk.green("Valid"):chalk.red("Invalid")));
              console.error(ajvContextValidator.errors);
            }
          }
    });
}
    // //create the AJV context
    // var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
    // ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));
    //
    // ajv.addSchema(schemaLinks);
    // ajv.addSchema(schemaBasicComponents);
    // ajv.addSchema(schemaCommonComponents);
    // ajv.addSchema(schemaReportRequest);
    // ajv.addSchema(schemaReportResponse);
    // ajv.addSchema(schemaPatchRequest);
    //
    // fs.readdirSync(exampleFolder).forEach(file => {
    //   var schemePath = validationSchemaPath;
    //
	  //   console.log("%s %s", chalk.green("Parsing file: "), file);
    //
    //   //each example folder should have a home and a hub end points
    //   //which require a different validation scheme
    //   if (file.indexOf('-Hub')>-1)
    //       schemePath = path.resolve(commonFolder+"/Hub.json");
    //   else if (file.indexOf('-Home')>-1)
    //       schemePath = path.resolve(commonFolder+"/Links.json");
    //   else if (file.indexOf('-PatchRequest')>-1)
    //         schemePath = path.resolve(commonFolder+"/Patch.json");
    //   else if (file.indexOf('-PatchCreateListRequest')>-1)
    //         schemePath = path.resolve(commonFolder+"/Patch.json");
    //   else if (file.indexOf('-PatchDeleteListRequest')>-1)
    //         schemePath = path.resolve(commonFolder+"/Patch.json");
    //   else if (file.indexOf('-ReportRequest')>-1)
    //         schemePath = path.resolve(commonFolder+"/ReportRequest.json");
    //   else if (file.indexOf('-ReportResponse')>-1)
    //         schemePath = path.resolve(commonFolder+"/ReportResponse.json");
    //   else if (file.indexOf('-SearchRequest')>-1)
    //         schemePath = path.resolve(commonFolder+"/Query.json");
    //   else if (file.indexOf('-Monitor')>-1)
    //         schemePath = path.resolve(commonFolder+"/Monitor.json");
    //
    //   else if (file.indexOf('-Summary')>-1)
    //   {
    //         console.log(chalk.yellow('This example might need to be validated against a specific \"Summary Scheme\"'));
    //   }
    //
    //   var schemaObj = utils.loadFile(schemePath);
    //   var fileBaseURI = getBaseURI(path.dirname(schemePath)) + path.basename(schemePath);
    //   //console.log("fileBaseURI: %s", fileBaseURI);
    //   schemaObj.$id=fileBaseURI;
    //
    //
    //   var ajvContextValidator = ajv.compile(schemaObj);
    //   //var ajvContextValidator = ajv.compile(schemaBasicComponents);
    //
    //   //validate example with the context providedw
    //   if (file.indexOf('-SearchResponse')>-1 || file.indexOf('-HistoryResponse')>-1 || file.indexOf('-PatchResponse')>-1 || file.indexOf('-PatchCreateListResponse')>-1 || file.indexOf('-PatchDeleteListResponse')>-1)
    //   {
    //         var allValid = true;
    //         var sourceFile = utils.loadFile(exampleFolder+"/"+file);
    //         if (Array.isArray(sourceFile))
    //         {
		// 	           console.log("%s", chalk.green("Validating an array of objects..."));
    //              sourceFile.forEach(function(s){
    //                var result = validateString(s, schemePath, ajvContextValidator);
    //                if (!result)
    //                 allValid = false;
    //               });
    //         }
    //         else
    //         {
		// 	        //console.log("%s", chalk.yellow("Most likely this should be an array, please double check manually"));
    //           var result = validateString(sourceFile, schemePath, ajvContextValidator);
    //           if (!result)
    //             allValid = false;
    //         }
    //
    //         console.log("The [%s] validation against schema: [%s] is: [%s]\n", file, path.basename(schemePath), (allValid?chalk.green("Valid"):chalk.red("Invalid")));
    //         //schemePath = path.resolve(commonFolder+"/ReportResponse.json");
    //   }
    //   else
    //   {
    //     validateExample(exampleFolder+"/"+file, schemePath, ajvContextValidator);
    //   }
    // })
//}



/**
 * Method will be deprecated
 */
function _validateAllExamples(_exampleFolder, _schemaVBO)
{
    console.log("Example folder provided: [%s]", _exampleFolder);
    //console.log("VBO Scheme: [%s]", _schemaVBO);

    //normalize the example folder
    var exampleFolder = path.resolve(_exampleFolder);
    console.log("Normalized folder is: [%s]", exampleFolder);

    //var validationSchemaPath = path.resolve(_schemaVBO);
    //console.log("Normalized validation VBO Scheme is: [%s]", validationSchemaPath);

    //Note: assumption here is that we have the CSM common folder structure
    var commonFolder = path.resolve(exampleFolder+"/../../../../Common/V1/");
    console.log("Common folder is: [%s]\n", commonFolder);

    //load the scheme
    try {
      loadCommonScheme(commonFolder);
    } catch (e) {
      console.error(e);
      console.log(chalk.red("Unable to load the Common library folder, abortin..."));
      return;
    } finally {

    }

    //create the AJV context
    var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
    ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));

    ajv.addSchema(schemaLinks);
    ajv.addSchema(schemaBasicComponents);
    ajv.addSchema(schemaCommonComponents);
    ajv.addSchema(schemaReportRequest);
    ajv.addSchema(schemaReportResponse);
    ajv.addSchema(schemaPatchRequest);

    fs.readdirSync(exampleFolder).forEach(file => {
      var schemePath = validationSchemaPath;

	    console.log("%s %s", chalk.green("Parsing file: "), file);

      //each example folder should have a home and a hub end points
      //which require a different validation scheme
      if (file.indexOf('-Hub')>-1)
          schemePath = path.resolve(commonFolder+"/Hub.json");
      else if (file.indexOf('-Home')>-1)
          schemePath = path.resolve(commonFolder+"/Links.json");
      else if (file.indexOf('-PatchRequest')>-1)
            schemePath = path.resolve(commonFolder+"/Patch.json");
      else if (file.indexOf('-PatchCreateListRequest')>-1)
            schemePath = path.resolve(commonFolder+"/Patch.json");
      else if (file.indexOf('-PatchDeleteListRequest')>-1)
            schemePath = path.resolve(commonFolder+"/Patch.json");
      else if (file.indexOf('-ReportRequest')>-1)
            schemePath = path.resolve(commonFolder+"/ReportRequest.json");
      else if (file.indexOf('-ReportResponse')>-1)
            schemePath = path.resolve(commonFolder+"/ReportResponse.json");
      else if (file.indexOf('-SearchRequest')>-1)
            schemePath = path.resolve(commonFolder+"/Query.json");
      else if (file.indexOf('-Monitor')>-1)
            schemePath = path.resolve(commonFolder+"/Monitor.json");

      else if (file.indexOf('-Summary')>-1)
      {
            console.log(chalk.yellow('This example might need to be validated against a specific \"Summary Scheme\"'));
      }

      var schemaObj = utils.loadFile(schemePath);
      var fileBaseURI = getBaseURI(path.dirname(schemePath)) + path.basename(schemePath);
      //console.log("fileBaseURI: %s", fileBaseURI);
      schemaObj.$id=fileBaseURI;


      var ajvContextValidator = ajv.compile(schemaObj);
      //var ajvContextValidator = ajv.compile(schemaBasicComponents);

      //validate example with the context providedw
      if (file.indexOf('-SearchResponse')>-1 || file.indexOf('-HistoryResponse')>-1 || file.indexOf('-PatchResponse')>-1 || file.indexOf('-PatchCreateListResponse')>-1 || file.indexOf('-PatchDeleteListResponse')>-1)
      {
            var allValid = true;
            var sourceFile = utils.loadFile(exampleFolder+"/"+file);
            if (Array.isArray(sourceFile))
            {
			           console.log("%s", chalk.green("Validating an array of objects..."));
                 sourceFile.forEach(function(s){
                   var result = validateString(s, schemePath, ajvContextValidator);
                   if (!result)
                    allValid = false;
                  });
            }
            else
            {
			        //console.log("%s", chalk.yellow("Most likely this should be an array, please double check manually"));
              var result = validateString(sourceFile, schemePath, ajvContextValidator);
              if (!result)
                allValid = false;
            }

            console.log("The [%s] validation against schema: [%s] is: [%s]\n", file, path.basename(schemePath), (allValid?chalk.green("Valid"):chalk.red("Invalid")));
            //schemePath = path.resolve(commonFolder+"/ReportResponse.json");
      }
      else
      {
        validateExample(exampleFolder+"/"+file, schemePath, ajvContextValidator);
      }
    })
}

/**
 * Method will load the AJV context for example validations.
 * It will:
 *  - load the Common schemes
 *  - Change the Common ID references to match the ones from the example
 *  - Load&Parse the example schema and pre-load all the dependencies
 */
function exampleAJVContext(_exampleFile, _schemaVBO)
{
  //console.log("Example file provided: [%s]", _exampleFile);
  //console.log("VBO Scheme: [%s]", _schemaVBO);

  //normalize the example folder
  var exampleFile = path.resolve(_exampleFile);
  var exampleFolder = path.dirname(exampleFile)
  //console.log("Normalized folder is: [%s]", exampleFolder);

  var validationSchemaPath = path.resolve(_schemaVBO);
  //console.log("Normalized validation VBO Scheme is: [%s]", validationSchemaPath);

  //Note: assumption here is that we have the CSM common folder structure
  var commonFolder = path.resolve(exampleFolder+"/../../../../Common/V1/");
  //console.log("Common folder is: [%s]\n", commonFolder);

  //workout the swagger impl folder
  implCommonFolder = path.resolve(exampleFolder+"/../../../../Common/V1/Impl/Swagger");
  //console.log(implCommonFolder);

  //load the scheme
  try {
    loadCommonScheme(commonFolder);
  } catch (e) {
    console.error(e);
    console.log(chalk.red("Unable to load the Common library folder, abortin..."));
    return;
  } finally {
  }

  //create the AJV context
  var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));

  //for objects like "home" the validation schema is the same "links" as such we don't load it but let AJV to compile it later
  if (_schemaVBO.indexOf("Links.json")==-1)
  {
    schemaLinks.$id=getBaseURI(implCommonFolder)+'Links.json';
    ajv.addSchema(schemaLinks);
  }

  schemaBasicComponents.$id=getBaseURI(implCommonFolder)+'BasicComponents.json';
  ajv.addSchema(schemaBasicComponents);

  schemaCommonComponents.$id=getBaseURI(implCommonFolder)+'CommonComponents.json';
  ajv.addSchema(schemaCommonComponents);

  schemaReportRequest.$id=getBaseURI(implCommonFolder)+'ReportRequest.json';
  ajv.addSchema(schemaReportRequest);

  schemaReportResponse.$id=getBaseURI(implCommonFolder)+'ReportResponse.json';
  ajv.addSchema(schemaReportResponse);

  //schemaPatchRequest.$id=getBaseURI(implCommonFolder)+'Patch.json';
  //ajv.addSchema(schemaPatchRequest);

//  schemaHub.$id=getBaseURI(implCommonFolder)+'Hub.json';
//  ajv.addSchema(schemaHub);

  //load the schema
  var schemaObject = utils.loadFile(_schemaVBO);
  schemaObject.$id=getBaseURI(path.dirname(_schemaVBO))+path.basename(_schemaVBO);

  //load the external references
  loadExternalRef(ajv, schemaObject, exampleFolder);

  var ajvContextValidator = ajv.compile(schemaObject);
  if (!ajvContextValidator)
    logError("Unable to create the AJV context validator, validate manually ");

  return ajvContextValidator;
}

function loadExternalRef(ajv, schemaObj, exampleFolder)
{
  var finalResults = new Array();
  utils.findObjects(schemaObj, "$ref", 'targetValue', finalResults);
  _.forEach(finalResults, function(ref)
  {
    if (ref.$ref.indexOf('Common')==-1)
    {
      //load the referenced schema back in the context
      var schemaPath = path.resolve(exampleFolder+'/../Impl/Swagger/'+ref.$ref);
      var schema = utils.loadFile(schemaPath);
      loadExternalRef(ajv, schema, exampleFolder);
      schema.$id = getBaseURI(path.dirname(schemaPath))+path.basename(schemaPath);
      if (!ajv.getSchema(schema.$id))
        ajv.addSchema(schema);
    }
  });
}

/**
* The method will be deprecated
*/
function validateCustom(_exampleFile, _schemaVBO)
{
  console.log("Example file provided: [%s]", _exampleFile);
  console.log("VBO Scheme: [%s]", _schemaVBO);

  //normalize the example folder
  var exampleFile = path.resolve(_exampleFile);
  var exampleFolder = path.dirname(exampleFile)
  console.log("Normalized folder is: [%s]", exampleFolder);

  var validationSchemaPath = path.resolve(_schemaVBO);
  console.log("Normalized validation VBO Scheme is: [%s]", validationSchemaPath);

  //Note: assumption here is that we have the CSM common folder structure
  var commonFolder = path.resolve(exampleFolder+"/../../../../Common/V1/");
  console.log("Common folder is: [%s]\n", commonFolder);

  //workout the swagger impl folder
  implCommonFolder = path.resolve(exampleFolder+"/../../../../Common/V1/Impl/Swagger");
  console.log(implCommonFolder);

  //load the scheme
  try {
    loadCommonScheme(commonFolder);
  } catch (e) {
    console.error(e);
    console.log(chalk.red("Unable to load the Common library folder, abortin..."));
    return;
  } finally {
  }

  //create the AJV context
  var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-06.json'));

  schemaLinks.$id=getBaseURI(implCommonFolder)+'Links.json';
  ajv.addSchema(schemaLinks);

  schemaBasicComponents.$id=getBaseURI(implCommonFolder)+'BasicComponents.json';
  ajv.addSchema(schemaBasicComponents);

  schemaCommonComponents.$id=getBaseURI(implCommonFolder)+'CommonComponents.json';
  ajv.addSchema(schemaCommonComponents);

  schemaReportRequest.$id=getBaseURI(implCommonFolder)+'ReportRequest.json';
  ajv.addSchema(schemaReportRequest);

  schemaReportResponse.$id=getBaseURI(implCommonFolder)+'ReportResponse.json';
  ajv.addSchema(schemaReportResponse);

  schemaPatchRequest.$id=getBaseURI(implCommonFolder)+'Patch.json';
  ajv.addSchema(schemaPatchRequest);

  //load the schema
  var schemaObject = utils.loadFile(_schemaVBO);
  schemaObject.$id=getBaseURI(path.dirname(_schemaVBO))+path.basename(_schemaVBO);

  var finalResults = new Array();
  utils.findObjects(schemaObject, "$ref", 'targetValue', finalResults)
  _.forEach(finalResults, function(ref)
  {
    if (ref.$ref.indexOf('Common')==-1)
    {
      //load the referenced schema back in the context
      var schemaPath = path.resolve(exampleFolder+'/../Impl/Swagger/'+ref.$ref);
      var schema = utils.loadFile(schemaPath);
      schema.$id = getBaseURI(path.dirname(schemaPath))+path.basename(schemaPath);
      ajv.addSchema(schema);
    }
  })

  var ajvContextValidator = ajv.compile(schemaObject);

  validateExample(exampleFile, validationSchemaPath, ajvContextValidator);
}



module.exports = {
  validate: function(vbos)
  {
    var allValid = true;

    if (vbos.length==0)
      console.error("Nothing to validate!");

    vbos.forEach(function(value)
    {
      console.log("\n");
      if (!validateSchema(value))
        allValid = false;
    });

    return allValid;
  },

  //validates
  validateVBOSchema: function(jsonSchema)
  {
      return validateSchema(jsonSchema);
  },

  validateAllExamples: function(exampleFolder)
  {
    validateAllExamples(exampleFolder[0], exampleFolder[1]);
  },

  validateExample: function(examples)
  {
    if (examples.length==0)
      console.error("Nothing to validate!");

      var size = examples.length;

      if (size-1>0)
        var validationScheme = examples[size-1];
      else
        console.error(chalk.red("Missing the validation scheme"));

      for (i=0; i < size-1; ++i)
      {
          //validateCustom
          validateOneByOne(examples[i], validationScheme);
      }

      /*
      examples.forEach(function(value)
      {
        console.log("\n");
        validateOneByOne(examples[0], examples[1]);
      });
      */
  }
}


/*  var data =  utils.loadFile(exampleFile);
var valid = validate(data);
  if (!valid) console.log(validate.errors);
  */
//


//Working version
/*  var data =  utils.loadFile(exampleFile);
  var validate = ajv.validate(schemaReportResponse, data);
  if (!validate)
  {
    console.log(ajv.errorsText());
  }
*/

package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.CommunicationVBOAttachment;
import io.swagger.model.CommunicationVBOCommunicationsignature;
import io.swagger.model.CommunicationVBOExtraId;
import io.swagger.model.CommunicationVBOLastModified;
import io.swagger.model.CommunicationVBOLinks;
import io.swagger.model.CommunicationVBOReceiver;
import io.swagger.model.CommunicationVBORelatedEntity;
import io.swagger.model.CommunicationVBOSender;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommunicationVBO
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBO   {
  @JsonProperty("_links")
  private List<CommunicationVBOLinks> links = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("extraId")
  private List<CommunicationVBOExtraId> extraId = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("lastModified")
  private CommunicationVBOLastModified lastModified = null;

  @JsonProperty("Callback")
  private String callback = null;

  @JsonProperty("relatedEntity")
  private List<CommunicationVBORelatedEntity> relatedEntity = null;

  @JsonProperty("Policy")
  private String policy = null;

  @JsonProperty("characteristic")
  private  characteristic = null;

  @JsonProperty("sender")
  private List<CommunicationVBOSender> sender = null;

  @JsonProperty("receiver")
  private List<CommunicationVBOReceiver> receiver = null;

  @JsonProperty("attachment")
  private List<CommunicationVBOAttachment> attachment = null;

  @JsonProperty("communicationsignature")
  private CommunicationVBOCommunicationsignature communicationsignature = null;

  public CommunicationVBO links(List<CommunicationVBOLinks> links) {
    this.links = links;
    return this;
  }

  public CommunicationVBO addLinksItem(CommunicationVBOLinks linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<CommunicationVBOLinks>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOLinks> getLinks() {
    return links;
  }

  public void setLinks(List<CommunicationVBOLinks> links) {
    this.links = links;
  }

  public CommunicationVBO id(String id) {
    this.id = id;
    return this;
  }

   /**
   * unique identifier of the Communication
   * @return id
  **/
  @ApiModelProperty(value = "unique identifier of the Communication")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CommunicationVBO extraId(List<CommunicationVBOExtraId> extraId) {
    this.extraId = extraId;
    return this;
  }

  public CommunicationVBO addExtraIdItem(CommunicationVBOExtraId extraIdItem) {
    if (this.extraId == null) {
      this.extraId = new ArrayList<CommunicationVBOExtraId>();
    }
    this.extraId.add(extraIdItem);
    return this;
  }

   /**
   * Get extraId
   * @return extraId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOExtraId> getExtraId() {
    return extraId;
  }

  public void setExtraId(List<CommunicationVBOExtraId> extraId) {
    this.extraId = extraId;
  }

  public CommunicationVBO href(String href) {
    this.href = href;
    return this;
  }

   /**
   * URI where to query and perform operations on the Communication
   * @return href
  **/
  @ApiModelProperty(value = "URI where to query and perform operations on the Communication")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public CommunicationVBO name(String name) {
    this.name = name;
    return this;
  }

   /**
   *  The name of the instance of a business object or component.
   * @return name
  **/
  @ApiModelProperty(value = " The name of the instance of a business object or component.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CommunicationVBO description(String description) {
    this.description = description;
    return this;
  }

   /**
   *  A free text description of a business object or component.
   * @return description
  **/
  @ApiModelProperty(value = " A free text description of a business object or component.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public CommunicationVBO type(String type) {
    this.type = type;
    return this;
  }

   /**
   * A character string (letters, figures, or symbols) that for brevity and/or languange independence may be used to represent or replace a definitive value or text of an attribute together with relevant supplementary information. Code string Should not be used if the character string identifies an instance of an object class or an object in the real world, in which case the Identifier. Type should be used.
   * @return type
  **/
  @ApiModelProperty(value = "A character string (letters, figures, or symbols) that for brevity and/or languange independence may be used to represent or replace a definitive value or text of an attribute together with relevant supplementary information. Code string Should not be used if the character string identifies an instance of an object class or an object in the real world, in which case the Identifier. Type should be used.")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public CommunicationVBO status(String status) {
    this.status = status;
    return this;
  }

   /**
   *  The lifecycle state of the business object or component. This field is used to track the specific state an instance of an object or component is in
   * @return status
  **/
  @ApiModelProperty(value = " The lifecycle state of the business object or component. This field is used to track the specific state an instance of an object or component is in")


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public CommunicationVBO lastModified(CommunicationVBOLastModified lastModified) {
    this.lastModified = lastModified;
    return this;
  }

   /**
   * Get lastModified
   * @return lastModified
  **/
  @ApiModelProperty(value = "")

  @Valid

  public CommunicationVBOLastModified getLastModified() {
    return lastModified;
  }

  public void setLastModified(CommunicationVBOLastModified lastModified) {
    this.lastModified = lastModified;
  }

  public CommunicationVBO callback(String callback) {
    this.callback = callback;
    return this;
  }

   /**
   * Call back details of the communication
   * @return callback
  **/
  @ApiModelProperty(value = "Call back details of the communication")


  public String getCallback() {
    return callback;
  }

  public void setCallback(String callback) {
    this.callback = callback;
  }

  public CommunicationVBO relatedEntity(List<CommunicationVBORelatedEntity> relatedEntity) {
    this.relatedEntity = relatedEntity;
    return this;
  }

  public CommunicationVBO addRelatedEntityItem(CommunicationVBORelatedEntity relatedEntityItem) {
    if (this.relatedEntity == null) {
      this.relatedEntity = new ArrayList<CommunicationVBORelatedEntity>();
    }
    this.relatedEntity.add(relatedEntityItem);
    return this;
  }

   /**
   * The set of related Entityto this one. The relationship is defined with the relation type code attribute.
   * @return relatedEntity
  **/
  @ApiModelProperty(value = "The set of related Entityto this one. The relationship is defined with the relation type code attribute.")

  @Valid
 @Size(min=1)
  public List<CommunicationVBORelatedEntity> getRelatedEntity() {
    return relatedEntity;
  }

  public void setRelatedEntity(List<CommunicationVBORelatedEntity> relatedEntity) {
    this.relatedEntity = relatedEntity;
  }

  public CommunicationVBO policy(String policy) {
    this.policy = policy;
    return this;
  }

   /**
   * Policy of the communication
   * @return policy
  **/
  @ApiModelProperty(value = "Policy of the communication")


  public String getPolicy() {
    return policy;
  }

  public void setPolicy(String policy) {
    this.policy = policy;
  }

  public CommunicationVBO characteristic( characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public CommunicationVBO addCharacteristicItem( characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = ;
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

   /**
   * The set characteristics associated with the Communication.
   * @return characteristic
  **/
  @ApiModelProperty(value = "The set characteristics associated with the Communication.")

  @Valid
 @Size(min=0)
  public  getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic( characteristic) {
    this.characteristic = characteristic;
  }

  public CommunicationVBO sender(List<CommunicationVBOSender> sender) {
    this.sender = sender;
    return this;
  }

  public CommunicationVBO addSenderItem(CommunicationVBOSender senderItem) {
    if (this.sender == null) {
      this.sender = new ArrayList<CommunicationVBOSender>();
    }
    this.sender.add(senderItem);
    return this;
  }

   /**
   * The sender parties of the Communication.
   * @return sender
  **/
  @ApiModelProperty(value = "The sender parties of the Communication.")

  @Valid
 @Size(min=1)
  public List<CommunicationVBOSender> getSender() {
    return sender;
  }

  public void setSender(List<CommunicationVBOSender> sender) {
    this.sender = sender;
  }

  public CommunicationVBO receiver(List<CommunicationVBOReceiver> receiver) {
    this.receiver = receiver;
    return this;
  }

  public CommunicationVBO addReceiverItem(CommunicationVBOReceiver receiverItem) {
    if (this.receiver == null) {
      this.receiver = new ArrayList<CommunicationVBOReceiver>();
    }
    this.receiver.add(receiverItem);
    return this;
  }

   /**
   * The receiver parties of the Communication.
   * @return receiver
  **/
  @ApiModelProperty(value = "The receiver parties of the Communication.")

  @Valid
 @Size(min=1)
  public List<CommunicationVBOReceiver> getReceiver() {
    return receiver;
  }

  public void setReceiver(List<CommunicationVBOReceiver> receiver) {
    this.receiver = receiver;
  }

  public CommunicationVBO attachment(List<CommunicationVBOAttachment> attachment) {
    this.attachment = attachment;
    return this;
  }

  public CommunicationVBO addAttachmentItem(CommunicationVBOAttachment attachmentItem) {
    if (this.attachment == null) {
      this.attachment = new ArrayList<CommunicationVBOAttachment>();
    }
    this.attachment.add(attachmentItem);
    return this;
  }

   /**
   * A document or other file attached to the information about the Communication.
   * @return attachment
  **/
  @ApiModelProperty(value = "A document or other file attached to the information about the Communication.")

  @Valid
 @Size(min=1)
  public List<CommunicationVBOAttachment> getAttachment() {
    return attachment;
  }

  public void setAttachment(List<CommunicationVBOAttachment> attachment) {
    this.attachment = attachment;
  }

  public CommunicationVBO communicationsignature(CommunicationVBOCommunicationsignature communicationsignature) {
    this.communicationsignature = communicationsignature;
    return this;
  }

   /**
   * Get communicationsignature
   * @return communicationsignature
  **/
  @ApiModelProperty(value = "")

  @Valid

  public CommunicationVBOCommunicationsignature getCommunicationsignature() {
    return communicationsignature;
  }

  public void setCommunicationsignature(CommunicationVBOCommunicationsignature communicationsignature) {
    this.communicationsignature = communicationsignature;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBO communicationVBO = (CommunicationVBO) o;
    return Objects.equals(this.links, communicationVBO.links) &&
        Objects.equals(this.id, communicationVBO.id) &&
        Objects.equals(this.extraId, communicationVBO.extraId) &&
        Objects.equals(this.href, communicationVBO.href) &&
        Objects.equals(this.name, communicationVBO.name) &&
        Objects.equals(this.description, communicationVBO.description) &&
        Objects.equals(this.type, communicationVBO.type) &&
        Objects.equals(this.status, communicationVBO.status) &&
        Objects.equals(this.lastModified, communicationVBO.lastModified) &&
        Objects.equals(this.callback, communicationVBO.callback) &&
        Objects.equals(this.relatedEntity, communicationVBO.relatedEntity) &&
        Objects.equals(this.policy, communicationVBO.policy) &&
        Objects.equals(this.characteristic, communicationVBO.characteristic) &&
        Objects.equals(this.sender, communicationVBO.sender) &&
        Objects.equals(this.receiver, communicationVBO.receiver) &&
        Objects.equals(this.attachment, communicationVBO.attachment) &&
        Objects.equals(this.communicationsignature, communicationVBO.communicationsignature);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links, id, extraId, href, name, description, type, status, lastModified, callback, relatedEntity, policy, characteristic, sender, receiver, attachment, communicationsignature);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBO {\n");
    
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    extraId: ").append(toIndentedString(extraId)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    lastModified: ").append(toIndentedString(lastModified)).append("\n");
    sb.append("    callback: ").append(toIndentedString(callback)).append("\n");
    sb.append("    relatedEntity: ").append(toIndentedString(relatedEntity)).append("\n");
    sb.append("    policy: ").append(toIndentedString(policy)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    sender: ").append(toIndentedString(sender)).append("\n");
    sb.append("    receiver: ").append(toIndentedString(receiver)).append("\n");
    sb.append("    attachment: ").append(toIndentedString(attachment)).append("\n");
    sb.append("    communicationsignature: ").append(toIndentedString(communicationsignature)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


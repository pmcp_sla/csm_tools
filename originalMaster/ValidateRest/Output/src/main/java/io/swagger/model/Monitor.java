package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Monitor
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class Monitor   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("request")
  private Object request = null;

  @JsonProperty("response")
  private Object response = null;

  /**
   * The monitor state of the resource.
   */
  public enum StateEnum {
    INPROGRESS("InProgress"),
    
    INERROR("InError"),
    
    COMPLETED("Completed");

    private String value;

    StateEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("state")
  private StateEnum state = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("sourceHref")
  private String sourceHref = null;

  public Monitor id(String id) {
    this.id = id;
    return this;
  }

   /**
   *  The Identifier for the Monitor component. These should be used to uniquely identify instance of the Monitor component.
   * @return id
  **/
  @ApiModelProperty(value = " The Identifier for the Monitor component. These should be used to uniquely identify instance of the Monitor component.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Monitor type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Used in the URIs as the identifier for specific instance of a type.
   * @return type
  **/
  @ApiModelProperty(value = "Used in the URIs as the identifier for specific instance of a type.")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Monitor name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Name of a resource, i.e., monitor.
   * @return name
  **/
  @ApiModelProperty(value = "Name of a resource, i.e., monitor.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Monitor request(Object request) {
    this.request = request;
    return this;
  }

   /**
   * Represents the request.
   * @return request
  **/
  @ApiModelProperty(value = "Represents the request.")


  public Object getRequest() {
    return request;
  }

  public void setRequest(Object request) {
    this.request = request;
  }

  public Monitor response(Object response) {
    this.response = response;
    return this;
  }

   /**
   * Represents the response.
   * @return response
  **/
  @ApiModelProperty(value = "Represents the response.")


  public Object getResponse() {
    return response;
  }

  public void setResponse(Object response) {
    this.response = response;
  }

  public Monitor state(StateEnum state) {
    this.state = state;
    return this;
  }

   /**
   * The monitor state of the resource.
   * @return state
  **/
  @ApiModelProperty(value = "The monitor state of the resource.")


  public StateEnum getState() {
    return state;
  }

  public void setState(StateEnum state) {
    this.state = state;
  }

  public Monitor href(String href) {
    this.href = href;
    return this;
  }

   /**
   * A hyperlink URL
   * @return href
  **/
  @ApiModelProperty(value = "A hyperlink URL")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Monitor sourceHref(String sourceHref) {
    this.sourceHref = sourceHref;
    return this;
  }

   /**
   * A hyperlink URL
   * @return sourceHref
  **/
  @ApiModelProperty(value = "A hyperlink URL")


  public String getSourceHref() {
    return sourceHref;
  }

  public void setSourceHref(String sourceHref) {
    this.sourceHref = sourceHref;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Monitor monitor = (Monitor) o;
    return Objects.equals(this.id, monitor.id) &&
        Objects.equals(this.type, monitor.type) &&
        Objects.equals(this.name, monitor.name) &&
        Objects.equals(this.request, monitor.request) &&
        Objects.equals(this.response, monitor.response) &&
        Objects.equals(this.state, monitor.state) &&
        Objects.equals(this.href, monitor.href) &&
        Objects.equals(this.sourceHref, monitor.sourceHref);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, type, name, request, response, state, href, sourceHref);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Monitor {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    request: ").append(toIndentedString(request)).append("\n");
    sb.append("    response: ").append(toIndentedString(response)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    sourceHref: ").append(toIndentedString(sourceHref)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


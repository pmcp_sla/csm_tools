package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.CommunicationVBOpropertiesLinksitems;
import io.swagger.model.CommunicationVBOpropertiesextraIditems;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommunicationVBORelatedEntity
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBORelatedEntity   {
  @JsonProperty("_links")
  private List<CommunicationVBOpropertiesLinksitems> links = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("extraId")
  private List<CommunicationVBOpropertiesextraIditems> extraId = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("characteristic")
  private List<Object> characteristic = null;

  public CommunicationVBORelatedEntity links(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
    return this;
  }

  public CommunicationVBORelatedEntity addLinksItem(CommunicationVBOpropertiesLinksitems linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<CommunicationVBOpropertiesLinksitems>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesLinksitems> getLinks() {
    return links;
  }

  public void setLinks(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
  }

  public CommunicationVBORelatedEntity id(String id) {
    this.id = id;
    return this;
  }

   /**
   * unique identifier of the Resource
   * @return id
  **/
  @ApiModelProperty(value = "unique identifier of the Resource")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CommunicationVBORelatedEntity extraId(List<CommunicationVBOpropertiesextraIditems> extraId) {
    this.extraId = extraId;
    return this;
  }

  public CommunicationVBORelatedEntity addExtraIdItem(CommunicationVBOpropertiesextraIditems extraIdItem) {
    if (this.extraId == null) {
      this.extraId = new ArrayList<CommunicationVBOpropertiesextraIditems>();
    }
    this.extraId.add(extraIdItem);
    return this;
  }

   /**
   * Get extraId
   * @return extraId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesextraIditems> getExtraId() {
    return extraId;
  }

  public void setExtraId(List<CommunicationVBOpropertiesextraIditems> extraId) {
    this.extraId = extraId;
  }

  public CommunicationVBORelatedEntity href(String href) {
    this.href = href;
    return this;
  }

   /**
   * URI where to query and perform operations on the resource
   * @return href
  **/
  @ApiModelProperty(value = "URI where to query and perform operations on the resource")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public CommunicationVBORelatedEntity name(String name) {
    this.name = name;
    return this;
  }

   /**
   *  The name of the instance of a business object or component.
   * @return name
  **/
  @ApiModelProperty(value = " The name of the instance of a business object or component.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CommunicationVBORelatedEntity characteristic(List<Object> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public CommunicationVBORelatedEntity addCharacteristicItem(Object characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<Object>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

   /**
   * The set characteristics associated with the Communication.
   * @return characteristic
  **/
  @ApiModelProperty(value = "The set characteristics associated with the Communication.")

 @Size(min=0)
  public List<Object> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<Object> characteristic) {
    this.characteristic = characteristic;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBORelatedEntity communicationVBORelatedEntity = (CommunicationVBORelatedEntity) o;
    return Objects.equals(this.links, communicationVBORelatedEntity.links) &&
        Objects.equals(this.id, communicationVBORelatedEntity.id) &&
        Objects.equals(this.extraId, communicationVBORelatedEntity.extraId) &&
        Objects.equals(this.href, communicationVBORelatedEntity.href) &&
        Objects.equals(this.name, communicationVBORelatedEntity.name) &&
        Objects.equals(this.characteristic, communicationVBORelatedEntity.characteristic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links, id, extraId, href, name, characteristic);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBORelatedEntity {\n");
    
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    extraId: ").append(toIndentedString(extraId)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


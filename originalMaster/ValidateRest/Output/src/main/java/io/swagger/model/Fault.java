package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.FaultCharacteristic;
import io.swagger.model.FaultDescription;
import io.swagger.model.FaultErrorCode;
import io.swagger.model.FaultFailure;
import io.swagger.model.FaultOriginator;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Fault
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class Fault   {
  @JsonProperty("timestamp")
  private String timestamp = null;

  @JsonProperty("originator")
  private List<FaultOriginator> originator = null;

  @JsonProperty("errorCode")
  private List<FaultErrorCode> errorCode = null;

  @JsonProperty("description")
  private List<FaultDescription> description = null;

  @JsonProperty("faultCause")
  private Object faultCause = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("severity")
  private String severity = null;

  @JsonProperty("category")
  private String category = null;

  @JsonProperty("reasonCode")
  private String reasonCode = null;

  @JsonProperty("message")
  private String message = null;

  @JsonProperty("characteristic")
  private List<FaultCharacteristic> characteristic = null;

  @JsonProperty("failure")
  private List<FaultFailure> failure = null;

  public Fault timestamp(String timestamp) {
    this.timestamp = timestamp;
    return this;
  }

   /**
   * Get timestamp
   * @return timestamp
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public Fault originator(List<FaultOriginator> originator) {
    this.originator = originator;
    return this;
  }

  public Fault addOriginatorItem(FaultOriginator originatorItem) {
    if (this.originator == null) {
      this.originator = new ArrayList<FaultOriginator>();
    }
    this.originator.add(originatorItem);
    return this;
  }

   /**
   * Get originator
   * @return originator
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<FaultOriginator> getOriginator() {
    return originator;
  }

  public void setOriginator(List<FaultOriginator> originator) {
    this.originator = originator;
  }

  public Fault errorCode(List<FaultErrorCode> errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  public Fault addErrorCodeItem(FaultErrorCode errorCodeItem) {
    if (this.errorCode == null) {
      this.errorCode = new ArrayList<FaultErrorCode>();
    }
    this.errorCode.add(errorCodeItem);
    return this;
  }

   /**
   * Get errorCode
   * @return errorCode
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<FaultErrorCode> getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(List<FaultErrorCode> errorCode) {
    this.errorCode = errorCode;
  }

  public Fault description(List<FaultDescription> description) {
    this.description = description;
    return this;
  }

  public Fault addDescriptionItem(FaultDescription descriptionItem) {
    if (this.description == null) {
      this.description = new ArrayList<FaultDescription>();
    }
    this.description.add(descriptionItem);
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<FaultDescription> getDescription() {
    return description;
  }

  public void setDescription(List<FaultDescription> description) {
    this.description = description;
  }

  public Fault faultCause(Object faultCause) {
    this.faultCause = faultCause;
    return this;
  }

   /**
   * The legacy fault from the backend system.
   * @return faultCause
  **/
  @ApiModelProperty(value = "The legacy fault from the backend system.")


  public Object getFaultCause() {
    return faultCause;
  }

  public void setFaultCause(Object faultCause) {
    this.faultCause = faultCause;
  }

  public Fault name(String name) {
    this.name = name;
    return this;
  }

   /**
   * A human readable short text name for the fault. Required if the Error Code >= 100, may be omitted or set to 
   * @return name
  **/
  @ApiModelProperty(value = "A human readable short text name for the fault. Required if the Error Code >= 100, may be omitted or set to ")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Fault severity(String severity) {
    this.severity = severity;
    return this;
  }

   /**
   * The degree of severity and impact the fault has inccured, provides an indication of the level that may be used to trigger different behaviours in the consumer. Required if the Error Code >= 100, may be omitted or set to Information if the object is being used to indicate success.
   * @return severity
  **/
  @ApiModelProperty(value = "The degree of severity and impact the fault has inccured, provides an indication of the level that may be used to trigger different behaviours in the consumer. Required if the Error Code >= 100, may be omitted or set to Information if the object is being used to indicate success.")


  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }

  public Fault category(String category) {
    this.category = category;
    return this;
  }

   /**
   * A classification of the fault indicating whether it has been generated through some technical aspect of the interaction or on account of a failure of business logic, rules or processing. It provides an indication of the type that may be used to trigger different behaviours in the consumer. Required if the Error Code >= 100, may be omitted or set to Business if the object is being used to indicate success.
   * @return category
  **/
  @ApiModelProperty(value = "A classification of the fault indicating whether it has been generated through some technical aspect of the interaction or on account of a failure of business logic, rules or processing. It provides an indication of the type that may be used to trigger different behaviours in the consumer. Required if the Error Code >= 100, may be omitted or set to Business if the object is being used to indicate success.")


  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Fault reasonCode(String reasonCode) {
    this.reasonCode = reasonCode;
    return this;
  }

   /**
   * An enumeration of the reasons why the fault has been generated by the provider. Currently this is not used.
   * @return reasonCode
  **/
  @ApiModelProperty(value = "An enumeration of the reasons why the fault has been generated by the provider. Currently this is not used.")


  public String getReasonCode() {
    return reasonCode;
  }

  public void setReasonCode(String reasonCode) {
    this.reasonCode = reasonCode;
  }

  public Fault message(String message) {
    this.message = message;
    return this;
  }

   /**
   * A human readable description of the fault as identified by the Error Code. Required if the Error Code >= 100, may be omitted or set to 
   * @return message
  **/
  @ApiModelProperty(value = "A human readable description of the fault as identified by the Error Code. Required if the Error Code >= 100, may be omitted or set to ")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Fault characteristic(List<FaultCharacteristic> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public Fault addCharacteristicItem(FaultCharacteristic characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<FaultCharacteristic>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

   /**
   * Get characteristic
   * @return characteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<FaultCharacteristic> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<FaultCharacteristic> characteristic) {
    this.characteristic = characteristic;
  }

  public Fault failure(List<FaultFailure> failure) {
    this.failure = failure;
    return this;
  }

  public Fault addFailureItem(FaultFailure failureItem) {
    if (this.failure == null) {
      this.failure = new ArrayList<FaultFailure>();
    }
    this.failure.add(failureItem);
    return this;
  }

   /**
   * Get failure
   * @return failure
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<FaultFailure> getFailure() {
    return failure;
  }

  public void setFailure(List<FaultFailure> failure) {
    this.failure = failure;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Fault fault = (Fault) o;
    return Objects.equals(this.timestamp, fault.timestamp) &&
        Objects.equals(this.originator, fault.originator) &&
        Objects.equals(this.errorCode, fault.errorCode) &&
        Objects.equals(this.description, fault.description) &&
        Objects.equals(this.faultCause, fault.faultCause) &&
        Objects.equals(this.name, fault.name) &&
        Objects.equals(this.severity, fault.severity) &&
        Objects.equals(this.category, fault.category) &&
        Objects.equals(this.reasonCode, fault.reasonCode) &&
        Objects.equals(this.message, fault.message) &&
        Objects.equals(this.characteristic, fault.characteristic) &&
        Objects.equals(this.failure, fault.failure);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, originator, errorCode, description, faultCause, name, severity, category, reasonCode, message, characteristic, failure);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Fault {\n");
    
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    originator: ").append(toIndentedString(originator)).append("\n");
    sb.append("    errorCode: ").append(toIndentedString(errorCode)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    faultCause: ").append(toIndentedString(faultCause)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    severity: ").append(toIndentedString(severity)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    reasonCode: ").append(toIndentedString(reasonCode)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    failure: ").append(toIndentedString(failure)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


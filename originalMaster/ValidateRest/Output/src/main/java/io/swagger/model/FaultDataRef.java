package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * A reference to the specific field or component within the VBO that generated the failure.
 */
@ApiModel(description = "A reference to the specific field or component within the VBO that generated the failure.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class FaultDataRef   {
  @JsonProperty("pathName")
  private String pathName = null;

  @JsonProperty("pathValueText")
  private String pathValueText = null;

  public FaultDataRef pathName(String pathName) {
    this.pathName = pathName;
    return this;
  }

   /**
   * The pseudo-JSON Path reference to the specific field or component within the VBO.
   * @return pathName
  **/
  @ApiModelProperty(value = "The pseudo-JSON Path reference to the specific field or component within the VBO.")


  public String getPathName() {
    return pathName;
  }

  public void setPathName(String pathName) {
    this.pathName = pathName;
  }

  public FaultDataRef pathValueText(String pathValueText) {
    this.pathValueText = pathValueText;
    return this;
  }

   /**
   * The value of the field referenced by the Path Name.
   * @return pathValueText
  **/
  @ApiModelProperty(value = "The value of the field referenced by the Path Name.")


  public String getPathValueText() {
    return pathValueText;
  }

  public void setPathValueText(String pathValueText) {
    this.pathValueText = pathValueText;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FaultDataRef faultDataRef = (FaultDataRef) o;
    return Objects.equals(this.pathName, faultDataRef.pathName) &&
        Objects.equals(this.pathValueText, faultDataRef.pathValueText);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pathName, pathValueText);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FaultDataRef {\n");
    
    sb.append("    pathName: ").append(toIndentedString(pathName)).append("\n");
    sb.append("    pathValueText: ").append(toIndentedString(pathValueText)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


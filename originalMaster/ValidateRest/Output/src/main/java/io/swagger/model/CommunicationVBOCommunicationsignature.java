package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.CommunicationVBOpropertiesLinksitems;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommunicationVBOCommunicationsignature
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOCommunicationsignature   {
  @JsonProperty("_links")
  private List<CommunicationVBOpropertiesLinksitems> links = null;

  @JsonProperty("text")
  private String text = null;

  public CommunicationVBOCommunicationsignature links(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
    return this;
  }

  public CommunicationVBOCommunicationsignature addLinksItem(CommunicationVBOpropertiesLinksitems linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<CommunicationVBOpropertiesLinksitems>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesLinksitems> getLinks() {
    return links;
  }

  public void setLinks(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
  }

  public CommunicationVBOCommunicationsignature text(String text) {
    this.text = text;
    return this;
  }

   /**
   * Text of the Signature
   * @return text
  **/
  @ApiModelProperty(value = "Text of the Signature")


  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOCommunicationsignature communicationVBOCommunicationsignature = (CommunicationVBOCommunicationsignature) o;
    return Objects.equals(this.links, communicationVBOCommunicationsignature.links) &&
        Objects.equals(this.text, communicationVBOCommunicationsignature.text);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links, text);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOCommunicationsignature {\n");
    
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


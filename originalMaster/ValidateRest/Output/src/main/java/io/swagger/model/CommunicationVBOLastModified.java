package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * A particular point in the progression of time together with the relevant supplementary information.Date Time string
 */
@ApiModel(description = "A particular point in the progression of time together with the relevant supplementary information.Date Time string")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOLastModified   {
  @JsonProperty("value")
  private String value = null;

  @JsonProperty("format")
  private String format = null;

  public CommunicationVBOLastModified value(String value) {
    this.value = value;
    return this;
  }

   /**
   * A particular point in the progression of time together with the relevant supplementary information.
   * @return value
  **/
  @ApiModelProperty(required = true, value = "A particular point in the progression of time together with the relevant supplementary information.")
  @NotNull


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public CommunicationVBOLastModified format(String format) {
    this.format = format;
    return this;
  }

   /**
   * Get format
   * @return format
  **/
  @ApiModelProperty(value = "")


  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOLastModified communicationVBOLastModified = (CommunicationVBOLastModified) o;
    return Objects.equals(this.value, communicationVBOLastModified.value) &&
        Objects.equals(this.format, communicationVBOLastModified.format);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, format);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOLastModified {\n");
    
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    format: ").append(toIndentedString(format)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.CommunicationVBOpropertiesLinksitems;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommunicationVBOAttachment
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOAttachment   {
  @JsonProperty("_links")
  private List<CommunicationVBOpropertiesLinksitems> links = null;

  public CommunicationVBOAttachment links(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
    return this;
  }

  public CommunicationVBOAttachment addLinksItem(CommunicationVBOpropertiesLinksitems linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<CommunicationVBOpropertiesLinksitems>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesLinksitems> getLinks() {
    return links;
  }

  public void setLinks(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOAttachment communicationVBOAttachment = (CommunicationVBOAttachment) o;
    return Objects.equals(this.links, communicationVBOAttachment.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOAttachment {\n");
    
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


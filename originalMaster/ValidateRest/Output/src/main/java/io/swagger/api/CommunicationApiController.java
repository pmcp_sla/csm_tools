package io.swagger.api;

import io.swagger.model.CommunicationVBO;
import io.swagger.model.CommunicationVBOAttachment;
import io.swagger.model.CommunicationVBOReceiver;
import io.swagger.model.CommunicationVBORelatedEntity;
import io.swagger.model.CommunicationVBOSender;
import io.swagger.model.Fault;
import io.swagger.model.Query;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

@Controller
public class CommunicationApiController implements CommunicationApi {



    public ResponseEntity<CommunicationVBOAttachment> getAttachment(@ApiParam(value = "The identifier of the communication to search for, configured as the $.ids[0].$ value in the business object.",required=true ) @PathVariable("id") String id,
        @ApiParam(value = "The primary ID for the attachment component",required=true ) @PathVariable("attachmentId") String attachmentId,
        @ApiParam(value = "The field filters on the response attributes is enabled on all attributes, specified by ?fields=id,name,desc. Always the first query parameter.") @RequestParam(value = "fields", required = false) String fields) {
        // do some magic!
        return new ResponseEntity<CommunicationVBOAttachment>(HttpStatus.OK);
    }

    public ResponseEntity<CommunicationVBO> getCommunication(@ApiParam(value = "The primary ID for the resource",required=true ) @PathVariable("id") String id,
        @ApiParam(value = "The field filters on the response attributes is enabled on all attributes, specified by ?fields=id,name,desc. Always the first query parameter.") @RequestParam(value = "fields", required = false) String fields) {
        // do some magic!
        return new ResponseEntity<CommunicationVBO>(HttpStatus.OK);
    }

    public ResponseEntity<List<CommunicationVBO>> getList(@ApiParam(value = "The identifier of the communication to search for, configured as the $.ids[0].$ value in the business object.") @RequestParam(value = "id", required = false) String id,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the type") @RequestParam(value = "type", required = false) String type,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the customer id") @RequestParam(value = "customerId", required = false) String customerId,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the customer name") @RequestParam(value = "customerName", required = false) String customerName,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the sender id") @RequestParam(value = "senderId", required = false) String senderId,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the sender name") @RequestParam(value = "senderName", required = false) String senderName,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the receiver id") @RequestParam(value = "receiverId", required = false) String receiverId,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the receiver name") @RequestParam(value = "receiverName", required = false) String receiverName,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the sent date") @RequestParam(value = "sentDate", required = false) String sentDate,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the received date") @RequestParam(value = "receivedDate", required = false) String receivedDate,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the subscriber number") @RequestParam(value = "subscriberNumber", required = false) String subscriberNumber,
        @ApiParam(value = "Identifies zero or multiple communication resources based on the msisdn") @RequestParam(value = "msisdn", required = false) String msisdn,
        @ApiParam(value = "The field filters on the response attributes is enabled on all attributes, specified by ?fields=id,name,desc. Always the first query parameter.") @RequestParam(value = "fields", required = false) String fields,
        @ApiParam(value = "This limits the queries allowed to supporting AND only, equality matching for the operator code and simple naming of the parameters themselves.") @RequestParam(value = "limit", required = false) Integer limit,
        @ApiParam(value = "Support pagination. The size of the page to return.") @RequestParam(value = "count", required = false) Integer count,
        @ApiParam(value = "Supports sorting. The field and direction on which to sort the returned set of Network Access Profiles. ") @RequestParam(value = "sort", required = false) String sort) {
        // do some magic!
        return new ResponseEntity<List<CommunicationVBO>>(HttpStatus.OK);
    }

    public ResponseEntity<CommunicationVBOReceiver> getReceiverParty(@ApiParam(value = "The identifier of the communication to search for, configured as the $.ids[0].$ value in the business object.",required=true ) @PathVariable("id") String id,
        @ApiParam(value = "The primary ID for the Receiver Party component",required=true ) @PathVariable("partyId") String partyId,
        @ApiParam(value = "The field filters on the response attributes is enabled on all attributes, specified by ?fields=id,name,desc. Always the first query parameter.") @RequestParam(value = "fields", required = false) String fields) {
        // do some magic!
        return new ResponseEntity<CommunicationVBOReceiver>(HttpStatus.OK);
    }

    public ResponseEntity<CommunicationVBORelatedEntity> getRelatedEntity(@ApiParam(value = "The identifier of the communication to search for, configured as the $.ids[0].$ value in the business object.",required=true ) @PathVariable("id") String id,
        @ApiParam(value = "The primary ID for the related entity component",required=true ) @PathVariable("relatedEntityId") String relatedEntityId,
        @ApiParam(value = "The field filters on the response attributes is enabled on all attributes, specified by ?fields=id,name,desc. Always the first query parameter.") @RequestParam(value = "fields", required = false) String fields) {
        // do some magic!
        return new ResponseEntity<CommunicationVBORelatedEntity>(HttpStatus.OK);
    }

    public ResponseEntity<CommunicationVBOSender> getSenderParty(@ApiParam(value = "The identifier of the communication to search for, configured as the $.ids[0].$ value in the business object.",required=true ) @PathVariable("id") String id,
        @ApiParam(value = "The primary ID for the Sender Party component",required=true ) @PathVariable("partyId") String partyId,
        @ApiParam(value = "The field filters on the response attributes is enabled on all attributes, specified by ?fields=id,name,desc. Always the first query parameter.") @RequestParam(value = "fields", required = false) String fields) {
        // do some magic!
        return new ResponseEntity<CommunicationVBOSender>(HttpStatus.OK);
    }

    public ResponseEntity<List<CommunicationVBO>> query(@ApiParam(value = "" ,required=true )  @Valid @RequestBody Query query) {
        // do some magic!
        return new ResponseEntity<List<CommunicationVBO>>(HttpStatus.OK);
    }

    public ResponseEntity<CommunicationVBO> send(@ApiParam(value = "" ,required=true )  @Valid @RequestBody CommunicationVBO communication) {
        // do some magic!
        return new ResponseEntity<CommunicationVBO>(HttpStatus.OK);
    }

}

package io.swagger.api;

import io.swagger.model.Fault;
import io.swagger.model.Hub;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

@Controller
public class HubApiController implements HubApi {



    public ResponseEntity<Hub> hubCreate(@ApiParam(value = "" ,required=true )  @Valid @RequestBody Hub hub) {
        // do some magic!
        return new ResponseEntity<Hub>(HttpStatus.OK);
    }

    public ResponseEntity<Hub> hubDelete(@ApiParam(value = "The primary ID for the resource",required=true ) @PathVariable("id") String id) {
        // do some magic!
        return new ResponseEntity<Hub>(HttpStatus.OK);
    }

    public ResponseEntity<List<Hub>> hubFind(@ApiParam(value = "This limits the queries allowed to supporting AND only, equality matching for the operator code and simple naming of the parameters themselves.") @RequestParam(value = "limit", required = false) Integer limit,
        @ApiParam(value = "Support pagination. The size of the page to return.") @RequestParam(value = "count", required = false) Integer count,
        @ApiParam(value = "Supports sorting. The field and direction on which to sort the returned set of Network Access Profiles. ") @RequestParam(value = "sort", required = false) String sort) {
        // do some magic!
        return new ResponseEntity<List<Hub>>(HttpStatus.OK);
    }

    public ResponseEntity<Hub> hubGet(@ApiParam(value = "The primary ID for the resource",required=true ) @PathVariable("id") String id,
        @ApiParam(value = "The field filters on the response attributes is enabled on all attributes, specified by ?fields=id,name,desc. Always the first query parameter.") @RequestParam(value = "fields", required = false) String fields) {
        // do some magic!
        return new ResponseEntity<Hub>(HttpStatus.OK);
    }

}

package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.QueryQueryOptionsPagination;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * QueryQueryOptions
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class QueryQueryOptions   {
  @JsonProperty("fields")
  private String fields = null;

  @JsonProperty("filter")
  private String filter = null;

  @JsonProperty("pagination")
  private QueryQueryOptionsPagination pagination = null;

  @JsonProperty("sorting")
  private String sorting = null;

  public QueryQueryOptions fields(String fields) {
    this.fields = fields;
    return this;
  }

   /**
   * The query parameter used to specify the fields for a partial response.
   * @return fields
  **/
  @ApiModelProperty(value = "The query parameter used to specify the fields for a partial response.")


  public String getFields() {
    return fields;
  }

  public void setFields(String fields) {
    this.fields = fields;
  }

  public QueryQueryOptions filter(String filter) {
    this.filter = filter;
    return this;
  }

   /**
   * Get filter
   * @return filter
  **/
  @ApiModelProperty(value = "")


  public String getFilter() {
    return filter;
  }

  public void setFilter(String filter) {
    this.filter = filter;
  }

  public QueryQueryOptions pagination(QueryQueryOptionsPagination pagination) {
    this.pagination = pagination;
    return this;
  }

   /**
   * Get pagination
   * @return pagination
  **/
  @ApiModelProperty(value = "")

  @Valid

  public QueryQueryOptionsPagination getPagination() {
    return pagination;
  }

  public void setPagination(QueryQueryOptionsPagination pagination) {
    this.pagination = pagination;
  }

  public QueryQueryOptions sorting(String sorting) {
    this.sorting = sorting;
    return this;
  }

   /**
   * Get sorting
   * @return sorting
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getSorting() {
    return sorting;
  }

  public void setSorting(String sorting) {
    this.sorting = sorting;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QueryQueryOptions queryQueryOptions = (QueryQueryOptions) o;
    return Objects.equals(this.fields, queryQueryOptions.fields) &&
        Objects.equals(this.filter, queryQueryOptions.filter) &&
        Objects.equals(this.pagination, queryQueryOptions.pagination) &&
        Objects.equals(this.sorting, queryQueryOptions.sorting);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fields, filter, pagination, sorting);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class QueryQueryOptions {\n");
    
    sb.append("    fields: ").append(toIndentedString(fields)).append("\n");
    sb.append("    filter: ").append(toIndentedString(filter)).append("\n");
    sb.append("    pagination: ").append(toIndentedString(pagination)).append("\n");
    sb.append("    sorting: ").append(toIndentedString(sorting)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


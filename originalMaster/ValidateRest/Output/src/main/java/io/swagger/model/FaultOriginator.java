package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * FaultOriginator
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class FaultOriginator   {
  @JsonProperty("address")
  private String address = null;

  @JsonProperty("reference-parameters")
  private List<Object> referenceParameters = null;

  @JsonProperty("metadata")
  private List<Object> metadata = null;

  public FaultOriginator address(String address) {
    this.address = address;
    return this;
  }

   /**
   * 
   * @return address
  **/
  @ApiModelProperty(value = "")


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public FaultOriginator referenceParameters(List<Object> referenceParameters) {
    this.referenceParameters = referenceParameters;
    return this;
  }

  public FaultOriginator addReferenceParametersItem(Object referenceParametersItem) {
    if (this.referenceParameters == null) {
      this.referenceParameters = new ArrayList<Object>();
    }
    this.referenceParameters.add(referenceParametersItem);
    return this;
  }

   /**
   * Get referenceParameters
   * @return referenceParameters
  **/
  @ApiModelProperty(value = "")


  public List<Object> getReferenceParameters() {
    return referenceParameters;
  }

  public void setReferenceParameters(List<Object> referenceParameters) {
    this.referenceParameters = referenceParameters;
  }

  public FaultOriginator metadata(List<Object> metadata) {
    this.metadata = metadata;
    return this;
  }

  public FaultOriginator addMetadataItem(Object metadataItem) {
    if (this.metadata == null) {
      this.metadata = new ArrayList<Object>();
    }
    this.metadata.add(metadataItem);
    return this;
  }

   /**
   * Get metadata
   * @return metadata
  **/
  @ApiModelProperty(value = "")


  public List<Object> getMetadata() {
    return metadata;
  }

  public void setMetadata(List<Object> metadata) {
    this.metadata = metadata;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FaultOriginator faultOriginator = (FaultOriginator) o;
    return Objects.equals(this.address, faultOriginator.address) &&
        Objects.equals(this.referenceParameters, faultOriginator.referenceParameters) &&
        Objects.equals(this.metadata, faultOriginator.metadata);
  }

  @Override
  public int hashCode() {
    return Objects.hash(address, referenceParameters, metadata);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FaultOriginator {\n");
    
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    referenceParameters: ").append(toIndentedString(referenceParameters)).append("\n");
    sb.append("    metadata: ").append(toIndentedString(metadata)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.LinksHub;
import io.swagger.model.LinksListener;
import io.swagger.model.LinksMonitor;
import io.swagger.model.LinksSearch;
import io.swagger.model.LinksSelf;
import io.swagger.model.LinksSend;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Links
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class Links   {
  @JsonProperty("self")
  private LinksSelf self = null;

  @JsonProperty("send")
  private LinksSend send = null;

  @JsonProperty("search")
  private LinksSearch search = null;

  @JsonProperty("hub")
  private LinksHub hub = null;

  @JsonProperty("listener")
  private LinksListener listener = null;

  @JsonProperty("monitor")
  private LinksMonitor monitor = null;

  public Links self(LinksSelf self) {
    this.self = self;
    return this;
  }

   /**
   * Get self
   * @return self
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LinksSelf getSelf() {
    return self;
  }

  public void setSelf(LinksSelf self) {
    this.self = self;
  }

  public Links send(LinksSend send) {
    this.send = send;
    return this;
  }

   /**
   * Get send
   * @return send
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LinksSend getSend() {
    return send;
  }

  public void setSend(LinksSend send) {
    this.send = send;
  }

  public Links search(LinksSearch search) {
    this.search = search;
    return this;
  }

   /**
   * Get search
   * @return search
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LinksSearch getSearch() {
    return search;
  }

  public void setSearch(LinksSearch search) {
    this.search = search;
  }

  public Links hub(LinksHub hub) {
    this.hub = hub;
    return this;
  }

   /**
   * Get hub
   * @return hub
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LinksHub getHub() {
    return hub;
  }

  public void setHub(LinksHub hub) {
    this.hub = hub;
  }

  public Links listener(LinksListener listener) {
    this.listener = listener;
    return this;
  }

   /**
   * Get listener
   * @return listener
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LinksListener getListener() {
    return listener;
  }

  public void setListener(LinksListener listener) {
    this.listener = listener;
  }

  public Links monitor(LinksMonitor monitor) {
    this.monitor = monitor;
    return this;
  }

   /**
   * Get monitor
   * @return monitor
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LinksMonitor getMonitor() {
    return monitor;
  }

  public void setMonitor(LinksMonitor monitor) {
    this.monitor = monitor;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Links links = (Links) o;
    return Objects.equals(this.self, links.self) &&
        Objects.equals(this.send, links.send) &&
        Objects.equals(this.search, links.search) &&
        Objects.equals(this.hub, links.hub) &&
        Objects.equals(this.listener, links.listener) &&
        Objects.equals(this.monitor, links.monitor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(self, send, search, hub, listener, monitor);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Links {\n");
    
    sb.append("    self: ").append(toIndentedString(self)).append("\n");
    sb.append("    send: ").append(toIndentedString(send)).append("\n");
    sb.append("    search: ").append(toIndentedString(search)).append("\n");
    sb.append("    hub: ").append(toIndentedString(hub)).append("\n");
    sb.append("    listener: ").append(toIndentedString(listener)).append("\n");
    sb.append("    monitor: ").append(toIndentedString(monitor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


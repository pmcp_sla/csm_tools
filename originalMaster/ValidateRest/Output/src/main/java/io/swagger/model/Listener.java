package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Listener
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class Listener   {
  @JsonProperty("eventType")
  private String eventType = null;

  @JsonProperty("eventTime")
  private String eventTime = null;

  @JsonProperty("eventId")
  private String eventId = null;

  @JsonProperty("event")
  private Object event = null;

  public Listener eventType(String eventType) {
    this.eventType = eventType;
    return this;
  }

   /**
   * The type associated with the event.
   * @return eventType
  **/
  @ApiModelProperty(value = "The type associated with the event.")


  public String getEventType() {
    return eventType;
  }

  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  public Listener eventTime(String eventTime) {
    this.eventTime = eventTime;
    return this;
  }

   /**
   * The time event occurs.
   * @return eventTime
  **/
  @ApiModelProperty(value = "The time event occurs.")


  public String getEventTime() {
    return eventTime;
  }

  public void setEventTime(String eventTime) {
    this.eventTime = eventTime;
  }

  public Listener eventId(String eventId) {
    this.eventId = eventId;
    return this;
  }

   /**
   * The unique identifier associated with the event.
   * @return eventId
  **/
  @ApiModelProperty(value = "The unique identifier associated with the event.")


  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public Listener event(Object event) {
    this.event = event;
    return this;
  }

   /**
   * The event body.
   * @return event
  **/
  @ApiModelProperty(value = "The event body.")


  public Object getEvent() {
    return event;
  }

  public void setEvent(Object event) {
    this.event = event;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Listener listener = (Listener) o;
    return Objects.equals(this.eventType, listener.eventType) &&
        Objects.equals(this.eventTime, listener.eventTime) &&
        Objects.equals(this.eventId, listener.eventId) &&
        Objects.equals(this.event, listener.event);
  }

  @Override
  public int hashCode() {
    return Objects.hash(eventType, eventTime, eventId, event);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Listener {\n");
    
    sb.append("    eventType: ").append(toIndentedString(eventType)).append("\n");
    sb.append("    eventTime: ").append(toIndentedString(eventTime)).append("\n");
    sb.append("    eventId: ").append(toIndentedString(eventId)).append("\n");
    sb.append("    event: ").append(toIndentedString(event)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


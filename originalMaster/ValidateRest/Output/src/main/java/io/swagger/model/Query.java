package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.QueryQueries;
import io.swagger.model.QueryQueryOptions;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Query
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class Query   {
  @JsonProperty("queries")
  private List<QueryQueries> queries = new ArrayList<QueryQueries>();

  @JsonProperty("queryOptions")
  private QueryQueryOptions queryOptions = null;

  public Query queries(List<QueryQueries> queries) {
    this.queries = queries;
    return this;
  }

  public Query addQueriesItem(QueryQueries queriesItem) {
    this.queries.add(queriesItem);
    return this;
  }

   /**
   * 
   * @return queries
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
 @Size(min=1)
  public List<QueryQueries> getQueries() {
    return queries;
  }

  public void setQueries(List<QueryQueries> queries) {
    this.queries = queries;
  }

  public Query queryOptions(QueryQueryOptions queryOptions) {
    this.queryOptions = queryOptions;
    return this;
  }

   /**
   * Get queryOptions
   * @return queryOptions
  **/
  @ApiModelProperty(value = "")

  @Valid

  public QueryQueryOptions getQueryOptions() {
    return queryOptions;
  }

  public void setQueryOptions(QueryQueryOptions queryOptions) {
    this.queryOptions = queryOptions;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Query query = (Query) o;
    return Objects.equals(this.queries, query.queries) &&
        Objects.equals(this.queryOptions, query.queryOptions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(queries, queryOptions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Query {\n");
    
    sb.append("    queries: ").append(toIndentedString(queries)).append("\n");
    sb.append("    queryOptions: ").append(toIndentedString(queryOptions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


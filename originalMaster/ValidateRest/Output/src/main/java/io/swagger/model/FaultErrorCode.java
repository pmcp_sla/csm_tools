package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * FaultErrorCode
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class FaultErrorCode   {
  @JsonProperty("value")
  private String value = null;

  @JsonProperty("dialect")
  private String dialect = null;

  public FaultErrorCode value(String value) {
    this.value = value;
    return this;
  }

   /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public FaultErrorCode dialect(String dialect) {
    this.dialect = dialect;
    return this;
  }

   /**
   * Get dialect
   * @return dialect
  **/
  @ApiModelProperty(value = "")


  public String getDialect() {
    return dialect;
  }

  public void setDialect(String dialect) {
    this.dialect = dialect;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FaultErrorCode faultErrorCode = (FaultErrorCode) o;
    return Objects.equals(this.value, faultErrorCode.value) &&
        Objects.equals(this.dialect, faultErrorCode.dialect);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, dialect);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FaultErrorCode {\n");
    
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    dialect: ").append(toIndentedString(dialect)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


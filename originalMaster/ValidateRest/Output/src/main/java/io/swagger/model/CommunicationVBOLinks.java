package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommunicationVBOLinks
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOLinks   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("templated")
  private Boolean templated = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("deprecation")
  private Boolean deprecation = null;

  public CommunicationVBOLinks name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Name of a resource, link, action, etc.
   * @return name
  **/
  @ApiModelProperty(value = "Name of a resource, link, action, etc.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CommunicationVBOLinks href(String href) {
    this.href = href;
    return this;
  }

   /**
   * A hyperlink URL
   * @return href
  **/
  @ApiModelProperty(value = "A hyperlink URL")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public CommunicationVBOLinks templated(Boolean templated) {
    this.templated = templated;
    return this;
  }

   /**
   * Specifies whether or not a link is using URI templates
   * @return templated
  **/
  @ApiModelProperty(value = "Specifies whether or not a link is using URI templates")


  public Boolean getTemplated() {
    return templated;
  }

  public void setTemplated(Boolean templated) {
    this.templated = templated;
  }

  public CommunicationVBOLinks type(String type) {
    this.type = type;
    return this;
  }

   /**
   * A media type
   * @return type
  **/
  @ApiModelProperty(value = "A media type")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public CommunicationVBOLinks deprecation(Boolean deprecation) {
    this.deprecation = deprecation;
    return this;
  }

   /**
   * Specifies whether or not a link is deprecated
   * @return deprecation
  **/
  @ApiModelProperty(value = "Specifies whether or not a link is deprecated")


  public Boolean getDeprecation() {
    return deprecation;
  }

  public void setDeprecation(Boolean deprecation) {
    this.deprecation = deprecation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOLinks communicationVBOLinks = (CommunicationVBOLinks) o;
    return Objects.equals(this.name, communicationVBOLinks.name) &&
        Objects.equals(this.href, communicationVBOLinks.href) &&
        Objects.equals(this.templated, communicationVBOLinks.templated) &&
        Objects.equals(this.type, communicationVBOLinks.type) &&
        Objects.equals(this.deprecation, communicationVBOLinks.deprecation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, href, templated, type, deprecation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOLinks {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    templated: ").append(toIndentedString(templated)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    deprecation: ").append(toIndentedString(deprecation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


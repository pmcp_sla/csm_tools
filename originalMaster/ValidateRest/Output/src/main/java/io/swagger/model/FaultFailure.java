package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.FaultDataRef;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * A component that describes individual failures within the fault. This component is used to support multiple causes to the fault; i.e. where the fault is generated from one or more API calls or one or more validation failures.
 */
@ApiModel(description = "A component that describes individual failures within the fault. This component is used to support multiple causes to the fault; i.e. where the fault is generated from one or more API calls or one or more validation failures.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class FaultFailure   {
  @JsonProperty("code")
  private String code = null;

  @JsonProperty("text")
  private String text = null;

  @JsonProperty("dataRef")
  private FaultDataRef dataRef = null;

  @JsonProperty("Severity")
  private String severity = null;

  public FaultFailure code(String code) {
    this.code = code;
    return this;
  }

   /**
   * The code identifying the specific failure.
   * @return code
  **/
  @ApiModelProperty(value = "The code identifying the specific failure.")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public FaultFailure text(String text) {
    this.text = text;
    return this;
  }

   /**
   * The human-readable text of the specific failure.
   * @return text
  **/
  @ApiModelProperty(value = "The human-readable text of the specific failure.")


  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public FaultFailure dataRef(FaultDataRef dataRef) {
    this.dataRef = dataRef;
    return this;
  }

   /**
   * Get dataRef
   * @return dataRef
  **/
  @ApiModelProperty(value = "")

  @Valid

  public FaultDataRef getDataRef() {
    return dataRef;
  }

  public void setDataRef(FaultDataRef dataRef) {
    this.dataRef = dataRef;
  }

  public FaultFailure severity(String severity) {
    this.severity = severity;
    return this;
  }

   /**
   * The degree of severity and impact the fault has inccured, provides an indication of the level that may be used to trigger different behaviours in the consumer. Required if the Error Code >= 100, may be omitted or set to Information if the object is being used to indicate success.
   * @return severity
  **/
  @ApiModelProperty(value = "The degree of severity and impact the fault has inccured, provides an indication of the level that may be used to trigger different behaviours in the consumer. Required if the Error Code >= 100, may be omitted or set to Information if the object is being used to indicate success.")


  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FaultFailure faultFailure = (FaultFailure) o;
    return Objects.equals(this.code, faultFailure.code) &&
        Objects.equals(this.text, faultFailure.text) &&
        Objects.equals(this.dataRef, faultFailure.dataRef) &&
        Objects.equals(this.severity, faultFailure.severity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, text, dataRef, severity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FaultFailure {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    dataRef: ").append(toIndentedString(dataRef)).append("\n");
    sb.append("    severity: ").append(toIndentedString(severity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Hub
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class Hub   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("callback")
  private String callback = null;

  @JsonProperty("query")
  private String query = null;

  @JsonProperty("fields")
  private String fields = null;

  public Hub id(String id) {
    this.id = id;
    return this;
  }

   /**
   * The unique identifier of the hub.
   * @return id
  **/
  @ApiModelProperty(value = "The unique identifier of the hub.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Hub callback(String callback) {
    this.callback = callback;
    return this;
  }

   /**
   * The URI for the callback.
   * @return callback
  **/
  @ApiModelProperty(value = "The URI for the callback.")


  public String getCallback() {
    return callback;
  }

  public void setCallback(String callback) {
    this.callback = callback;
  }

  public Hub query(String query) {
    this.query = query;
    return this;
  }

   /**
   * The query expression may be used to filter specific event types and/or any content of the event.
   * @return query
  **/
  @ApiModelProperty(value = "The query expression may be used to filter specific event types and/or any content of the event.")


  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public Hub fields(String fields) {
    this.fields = fields;
    return this;
  }

   /**
   * An attribute selector directive used to specify the attributes to be returned as part of a partial representation of a resource
   * @return fields
  **/
  @ApiModelProperty(value = "An attribute selector directive used to specify the attributes to be returned as part of a partial representation of a resource")


  public String getFields() {
    return fields;
  }

  public void setFields(String fields) {
    this.fields = fields;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Hub hub = (Hub) o;
    return Objects.equals(this.id, hub.id) &&
        Objects.equals(this.callback, hub.callback) &&
        Objects.equals(this.query, hub.query) &&
        Objects.equals(this.fields, hub.fields);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, callback, query, fields);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Hub {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    callback: ").append(toIndentedString(callback)).append("\n");
    sb.append("    query: ").append(toIndentedString(query)).append("\n");
    sb.append("    fields: ").append(toIndentedString(fields)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


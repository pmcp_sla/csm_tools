package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.CommunicationVBOSenderEmail;
import io.swagger.model.CommunicationVBOpropertiesLinksitems;
import io.swagger.model.CommunicationVBOpropertiescharacteristicitems;
import io.swagger.model.CommunicationVBOpropertiesextraIditems;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommunicationVBOSender
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOSender   {
  @JsonProperty("_links")
  private List<CommunicationVBOpropertiesLinksitems> links = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("extraId")
  private List<CommunicationVBOpropertiesextraIditems> extraId = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("characteristic")
  private List<CommunicationVBOpropertiescharacteristicitems> characteristic = null;

  @JsonProperty("relatedParty")
  private  relatedParty = null;

  @JsonProperty("email")
  private CommunicationVBOSenderEmail email = null;

  public CommunicationVBOSender links(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
    return this;
  }

  public CommunicationVBOSender addLinksItem(CommunicationVBOpropertiesLinksitems linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<CommunicationVBOpropertiesLinksitems>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesLinksitems> getLinks() {
    return links;
  }

  public void setLinks(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
  }

  public CommunicationVBOSender id(String id) {
    this.id = id;
    return this;
  }

   /**
   * unique identifier of the Resource
   * @return id
  **/
  @ApiModelProperty(value = "unique identifier of the Resource")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CommunicationVBOSender extraId(List<CommunicationVBOpropertiesextraIditems> extraId) {
    this.extraId = extraId;
    return this;
  }

  public CommunicationVBOSender addExtraIdItem(CommunicationVBOpropertiesextraIditems extraIdItem) {
    if (this.extraId == null) {
      this.extraId = new ArrayList<CommunicationVBOpropertiesextraIditems>();
    }
    this.extraId.add(extraIdItem);
    return this;
  }

   /**
   * Get extraId
   * @return extraId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesextraIditems> getExtraId() {
    return extraId;
  }

  public void setExtraId(List<CommunicationVBOpropertiesextraIditems> extraId) {
    this.extraId = extraId;
  }

  public CommunicationVBOSender href(String href) {
    this.href = href;
    return this;
  }

   /**
   * URI where to query and perform operations on the resource
   * @return href
  **/
  @ApiModelProperty(value = "URI where to query and perform operations on the resource")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public CommunicationVBOSender name(String name) {
    this.name = name;
    return this;
  }

   /**
   *  The name of the instance of a business object or component.
   * @return name
  **/
  @ApiModelProperty(value = " The name of the instance of a business object or component.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CommunicationVBOSender characteristic(List<CommunicationVBOpropertiescharacteristicitems> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public CommunicationVBOSender addCharacteristicItem(CommunicationVBOpropertiescharacteristicitems characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<CommunicationVBOpropertiescharacteristicitems>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

   /**
   * The set characteristics associated with the Communication.
   * @return characteristic
  **/
  @ApiModelProperty(value = "The set characteristics associated with the Communication.")

  @Valid
 @Size(min=0)
  public List<CommunicationVBOpropertiescharacteristicitems> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<CommunicationVBOpropertiescharacteristicitems> characteristic) {
    this.characteristic = characteristic;
  }

  public CommunicationVBOSender relatedParty( relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public CommunicationVBOSender addRelatedPartyItem( relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = ;
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

   /**
   * A reference to the Related Party associated with the Resource Configuration.
   * @return relatedParty
  **/
  @ApiModelProperty(value = "A reference to the Related Party associated with the Resource Configuration.")

  @Valid
 @Size(min=1)
  public  getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty( relatedParty) {
    this.relatedParty = relatedParty;
  }

  public CommunicationVBOSender email(CommunicationVBOSenderEmail email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")

  @Valid

  public CommunicationVBOSenderEmail getEmail() {
    return email;
  }

  public void setEmail(CommunicationVBOSenderEmail email) {
    this.email = email;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOSender communicationVBOSender = (CommunicationVBOSender) o;
    return Objects.equals(this.links, communicationVBOSender.links) &&
        Objects.equals(this.id, communicationVBOSender.id) &&
        Objects.equals(this.extraId, communicationVBOSender.extraId) &&
        Objects.equals(this.href, communicationVBOSender.href) &&
        Objects.equals(this.name, communicationVBOSender.name) &&
        Objects.equals(this.characteristic, communicationVBOSender.characteristic) &&
        Objects.equals(this.relatedParty, communicationVBOSender.relatedParty) &&
        Objects.equals(this.email, communicationVBOSender.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links, id, extraId, href, name, characteristic, relatedParty, email);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOSender {\n");
    
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    extraId: ").append(toIndentedString(extraId)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


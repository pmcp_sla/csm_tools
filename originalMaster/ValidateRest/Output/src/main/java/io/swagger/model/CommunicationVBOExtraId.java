package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * A character string to identify and distinguish uniquely, one instance of an object in an identification scheme from all other objects in the same scheme together with relevant supplementary information.Identifier string
 */
@ApiModel(description = "A character string to identify and distinguish uniquely, one instance of an object in an identification scheme from all other objects in the same scheme together with relevant supplementary information.Identifier string")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOExtraId   {
  @JsonProperty("value")
  private String value = null;

  @JsonProperty("schemeID")
  private String schemeID = null;

  @JsonProperty("schemeName")
  private String schemeName = null;

  @JsonProperty("schemeAgencyName")
  private String schemeAgencyName = null;

  public CommunicationVBOExtraId value(String value) {
    this.value = value;
    return this;
  }

   /**
   * The value of the field.
   * @return value
  **/
  @ApiModelProperty(required = true, value = "The value of the field.")
  @NotNull


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public CommunicationVBOExtraId schemeID(String schemeID) {
    this.schemeID = schemeID;
    return this;
  }

   /**
   * Get schemeID
   * @return schemeID
  **/
  @ApiModelProperty(value = "")


  public String getSchemeID() {
    return schemeID;
  }

  public void setSchemeID(String schemeID) {
    this.schemeID = schemeID;
  }

  public CommunicationVBOExtraId schemeName(String schemeName) {
    this.schemeName = schemeName;
    return this;
  }

   /**
   * Get schemeName
   * @return schemeName
  **/
  @ApiModelProperty(value = "")


  public String getSchemeName() {
    return schemeName;
  }

  public void setSchemeName(String schemeName) {
    this.schemeName = schemeName;
  }

  public CommunicationVBOExtraId schemeAgencyName(String schemeAgencyName) {
    this.schemeAgencyName = schemeAgencyName;
    return this;
  }

   /**
   * Get schemeAgencyName
   * @return schemeAgencyName
  **/
  @ApiModelProperty(value = "")


  public String getSchemeAgencyName() {
    return schemeAgencyName;
  }

  public void setSchemeAgencyName(String schemeAgencyName) {
    this.schemeAgencyName = schemeAgencyName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOExtraId communicationVBOExtraId = (CommunicationVBOExtraId) o;
    return Objects.equals(this.value, communicationVBOExtraId.value) &&
        Objects.equals(this.schemeID, communicationVBOExtraId.schemeID) &&
        Objects.equals(this.schemeName, communicationVBOExtraId.schemeName) &&
        Objects.equals(this.schemeAgencyName, communicationVBOExtraId.schemeAgencyName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, schemeID, schemeName, schemeAgencyName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOExtraId {\n");
    
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    schemeID: ").append(toIndentedString(schemeID)).append("\n");
    sb.append("    schemeName: ").append(toIndentedString(schemeName)).append("\n");
    sb.append("    schemeAgencyName: ").append(toIndentedString(schemeAgencyName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Email as contact point
 */
@ApiModel(description = "Email as contact point")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOSenderEmail   {
  @JsonProperty("fullAddress")
  private String fullAddress = null;

  @JsonProperty("domainName")
  private String domainName = null;

  @JsonProperty("userName")
  private String userName = null;

  @JsonProperty("emailVerified")
  private Boolean emailVerified = null;

  public CommunicationVBOSenderEmail fullAddress(String fullAddress) {
    this.fullAddress = fullAddress;
    return this;
  }

   /**
   * The full email address e.g. peter.jackson@vodafone.com
   * @return fullAddress
  **/
  @ApiModelProperty(value = "The full email address e.g. peter.jackson@vodafone.com")


  public String getFullAddress() {
    return fullAddress;
  }

  public void setFullAddress(String fullAddress) {
    this.fullAddress = fullAddress;
  }

  public CommunicationVBOSenderEmail domainName(String domainName) {
    this.domainName = domainName;
    return this;
  }

   /**
   * The domain e.g. vodafone.com
   * @return domainName
  **/
  @ApiModelProperty(value = "The domain e.g. vodafone.com")


  public String getDomainName() {
    return domainName;
  }

  public void setDomainName(String domainName) {
    this.domainName = domainName;
  }

  public CommunicationVBOSenderEmail userName(String userName) {
    this.userName = userName;
    return this;
  }

   /**
   * The username e.g. peter.jackson
   * @return userName
  **/
  @ApiModelProperty(value = "The username e.g. peter.jackson")


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public CommunicationVBOSenderEmail emailVerified(Boolean emailVerified) {
    this.emailVerified = emailVerified;
    return this;
  }

   /**
   * Indicates whether the email address is verified.
   * @return emailVerified
  **/
  @ApiModelProperty(value = "Indicates whether the email address is verified.")


  public Boolean getEmailVerified() {
    return emailVerified;
  }

  public void setEmailVerified(Boolean emailVerified) {
    this.emailVerified = emailVerified;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOSenderEmail communicationVBOSenderEmail = (CommunicationVBOSenderEmail) o;
    return Objects.equals(this.fullAddress, communicationVBOSenderEmail.fullAddress) &&
        Objects.equals(this.domainName, communicationVBOSenderEmail.domainName) &&
        Objects.equals(this.userName, communicationVBOSenderEmail.userName) &&
        Objects.equals(this.emailVerified, communicationVBOSenderEmail.emailVerified);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fullAddress, domainName, userName, emailVerified);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOSenderEmail {\n");
    
    sb.append("    fullAddress: ").append(toIndentedString(fullAddress)).append("\n");
    sb.append("    domainName: ").append(toIndentedString(domainName)).append("\n");
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("    emailVerified: ").append(toIndentedString(emailVerified)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.CommunicationVBOSenderpropertiesemail;
import io.swagger.model.CommunicationVBOSenderpropertiesrelatedPartyitems;
import io.swagger.model.CommunicationVBOpropertiesLinksitems;
import io.swagger.model.CommunicationVBOpropertiescharacteristicitems;
import io.swagger.model.CommunicationVBOpropertiesextraIditems;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CommunicationVBOReceiver
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

public class CommunicationVBOReceiver   {
  @JsonProperty("_links")
  private List<CommunicationVBOpropertiesLinksitems> links = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("extraId")
  private List<CommunicationVBOpropertiesextraIditems> extraId = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("characteristic")
  private List<CommunicationVBOpropertiescharacteristicitems> characteristic = null;

  @JsonProperty("relatedParty")
  private List<CommunicationVBOSenderpropertiesrelatedPartyitems> relatedParty = null;

  @JsonProperty("email")
  private CommunicationVBOSenderpropertiesemail email = null;

  public CommunicationVBOReceiver links(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
    return this;
  }

  public CommunicationVBOReceiver addLinksItem(CommunicationVBOpropertiesLinksitems linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<CommunicationVBOpropertiesLinksitems>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesLinksitems> getLinks() {
    return links;
  }

  public void setLinks(List<CommunicationVBOpropertiesLinksitems> links) {
    this.links = links;
  }

  public CommunicationVBOReceiver id(String id) {
    this.id = id;
    return this;
  }

   /**
   * unique identifier of the Resource
   * @return id
  **/
  @ApiModelProperty(value = "unique identifier of the Resource")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CommunicationVBOReceiver extraId(List<CommunicationVBOpropertiesextraIditems> extraId) {
    this.extraId = extraId;
    return this;
  }

  public CommunicationVBOReceiver addExtraIdItem(CommunicationVBOpropertiesextraIditems extraIdItem) {
    if (this.extraId == null) {
      this.extraId = new ArrayList<CommunicationVBOpropertiesextraIditems>();
    }
    this.extraId.add(extraIdItem);
    return this;
  }

   /**
   * Get extraId
   * @return extraId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CommunicationVBOpropertiesextraIditems> getExtraId() {
    return extraId;
  }

  public void setExtraId(List<CommunicationVBOpropertiesextraIditems> extraId) {
    this.extraId = extraId;
  }

  public CommunicationVBOReceiver href(String href) {
    this.href = href;
    return this;
  }

   /**
   * URI where to query and perform operations on the resource
   * @return href
  **/
  @ApiModelProperty(value = "URI where to query and perform operations on the resource")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public CommunicationVBOReceiver name(String name) {
    this.name = name;
    return this;
  }

   /**
   *  The name of the instance of a business object or component.
   * @return name
  **/
  @ApiModelProperty(value = " The name of the instance of a business object or component.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CommunicationVBOReceiver characteristic(List<CommunicationVBOpropertiescharacteristicitems> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public CommunicationVBOReceiver addCharacteristicItem(CommunicationVBOpropertiescharacteristicitems characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<CommunicationVBOpropertiescharacteristicitems>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

   /**
   * The set characteristics associated with the Communication.
   * @return characteristic
  **/
  @ApiModelProperty(value = "The set characteristics associated with the Communication.")

  @Valid
 @Size(min=0)
  public List<CommunicationVBOpropertiescharacteristicitems> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<CommunicationVBOpropertiescharacteristicitems> characteristic) {
    this.characteristic = characteristic;
  }

  public CommunicationVBOReceiver relatedParty(List<CommunicationVBOSenderpropertiesrelatedPartyitems> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public CommunicationVBOReceiver addRelatedPartyItem(CommunicationVBOSenderpropertiesrelatedPartyitems relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<CommunicationVBOSenderpropertiesrelatedPartyitems>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

   /**
   * A reference to the Related Party associated with the Resource Configuration.
   * @return relatedParty
  **/
  @ApiModelProperty(value = "A reference to the Related Party associated with the Resource Configuration.")

  @Valid
 @Size(min=1)
  public List<CommunicationVBOSenderpropertiesrelatedPartyitems> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<CommunicationVBOSenderpropertiesrelatedPartyitems> relatedParty) {
    this.relatedParty = relatedParty;
  }

  public CommunicationVBOReceiver email(CommunicationVBOSenderpropertiesemail email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")

  @Valid

  public CommunicationVBOSenderpropertiesemail getEmail() {
    return email;
  }

  public void setEmail(CommunicationVBOSenderpropertiesemail email) {
    this.email = email;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommunicationVBOReceiver communicationVBOReceiver = (CommunicationVBOReceiver) o;
    return Objects.equals(this.links, communicationVBOReceiver.links) &&
        Objects.equals(this.id, communicationVBOReceiver.id) &&
        Objects.equals(this.extraId, communicationVBOReceiver.extraId) &&
        Objects.equals(this.href, communicationVBOReceiver.href) &&
        Objects.equals(this.name, communicationVBOReceiver.name) &&
        Objects.equals(this.characteristic, communicationVBOReceiver.characteristic) &&
        Objects.equals(this.relatedParty, communicationVBOReceiver.relatedParty) &&
        Objects.equals(this.email, communicationVBOReceiver.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links, id, extraId, href, name, characteristic, relatedParty, email);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommunicationVBOReceiver {\n");
    
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    extraId: ").append(toIndentedString(extraId)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package io.swagger.api;

import io.swagger.model.Fault;
import io.swagger.model.Listener;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-07T11:15:32.558Z")

@Controller
public class ListenerApiController implements ListenerApi {



    public ResponseEntity<Listener> listener(@ApiParam(value = "" ,required=true )  @Valid @RequestBody Listener hub) {
        // do some magic!
        return new ResponseEntity<Listener>(HttpStatus.OK);
    }

}

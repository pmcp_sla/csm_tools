@ECHO OFF
SETLOCAL

CALL :createlink "../../../Common Schemas/Trunk/Docs/Common/REST/VBO/Common/V1"

EXIT /B %ERRORLEVEL%

:createlink
set local=%*
Pushd %local%
echo %local%
cmd /c ajv compile -s BasicComponents.json
cmd /c ajv compile -s CommonComponents.json -r BasicComponents.json
cmd /c ajv compile -s Fault.json
cmd /c ajv compile -s Hub.json
cmd /c ajv compile -s Links.json
cmd /c ajv compile -s Listener.json
cmd /c ajv compile -s Patch.json
cmd /c ajv compile -s Query.json
cmd /c ajv compile -s ReportRequest.json -r Links.json
cmd /c ajv compile -s ReportResponse.json -r CommonComponents.json -r BasicComponents.json -r Links.json
Popd )

EXIT /B 0

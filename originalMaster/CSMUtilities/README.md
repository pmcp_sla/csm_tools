# CSM Utilities

This package contains tools to help with the development of the SOAP and REST services. For each service based on the TM Forum services there is a set 
of documents and technical artefacts.

Services are delivered with SOAP and REST realisations. For SOAP there are xsd and wsdl files. For REST there are JSON and Swagger files. 


## Installation

Install a local copy of GitHub/Scripts.

Ensure that node version is v8.11 or higher by using:

* node -v	 

Ensure that npm version is v5.6 or higher by using:

* npm -v

You will also need copies of:

* swagger-codegen-cli-2.2.3.jar (available at: GitHub/Scripts/SwaggerCodeGen)

* json-schema-ref-parser node version 4.0.4 (optional but available at: GitHub/Scripts/Utils)

Navigate to your local copy of GitHub/Scripts/CSMUtilities and install the node modules you need using:

* npm update -g

* npm install -g

Module json-schema-ref-parser must be version 4.0.4! Check using command:

* npm list json-schema-ref-parser -g 

and this should show:


	+-- json-schema-ref-parser@4.0.4

		-- swagger-parser@5.0.6

	  		-- json-schema-ref-parser@4.0.4 invalid    <-- This one needs to be 4.0.4 with invalid


If @4.0.4 is not installed then manually copy the module to the *swagger-parser* node_module:

* Linux under /usr/local/lib/node or /usr/local/lib/node_modules
(For example: /usr/local/lib/node_modules/swagger-parser/node_modules/)
* Windows 7, 8 and 10 under %USERPROFILE%\AppData\Roaming\npm\node_modules\swagger-parser\node_modules
(For example: C:\Users\USERNAME\AppData\Roaming\npm\node_modules\csmutilities\node_modules\swagger-parser\node_modules\json-schema-ref-parser)


To execute the utilities use:

* csmutilities -c "C:\TEST\swagger-codegen-cli-2.2.3.jar" to do a configuration for your codegen.

* csmutilities -h



## Example commands Windows:

Navigate to your local GitHub directory

**REST**

Validate a REST Schema:
* csmutilities -a CheckServiceFeasibility\REST\VBO\Service\ServiceFeasibility\V2\ServiceFeasibilityVBO.json

Validate swagger file
* csmutilities -s CheckServiceFeasibility\REST\VBS\Service\CheckServiceFeasibility\V2\CheckServiceFeasibilityVBS.swagger

Validate REST examples
* csmutilities -v CheckServiceFeasibility\REST\VBO\Service\ServiceFeasibility\V2\Examples

Validate REST package
* csmutilities -p CheckServiceFeasibility

Validate VBM swagger operations
* csmutilities -o CheckServiceFeasibility\REST\VBS\Service\CheckServiceFeasibility\V2\CheckServiceFeasibilityVBS.swagger

Generate Flat Swagger file
* csmutilities -b CheckServiceFeasibility\REST\VBS\Service\CheckServiceFeasibility\V2\CheckServiceFeasibilityVBS.swagger CheckServiceFeasibility\REST\VBS\Service\CheckServiceFeasibility\V2\CheckServiceFeasibilityVBS.flat.swagger


	Check that the generated file, CheckServiceFeasibilityVBS.flat.swagger, has fully de-referenced the Common files
	i.e. in the generated file a definition exists for "BasicComponents": ...; if it does not then you need to update 
	your json-schema-ref-parser as described above


Generate csv 
* csmutilities -J CheckServiceFeasibility\REST\VBS\Service\CheckServiceFeasibility\V2\CheckServiceFeasibilityVBS.flat.swagger -N CheckServiceFeasibility\REST\VBO\Service\ServiceFeasibility\V2\ServiceFeasibilityVBO.json


	* CSV file copied to CheckServiceFeasibility\REST\VBO\Service\ServiceFeasibility\V2\ServiceFeasibilityVBO.csv


**SOAP**

Generate csv from SOAP schema:
* csmutilities -S CheckServiceFeasibility\SOAP\VBO\Service\ServiceFeasibility\V1\ServiceFeasibilityVBO.xsd


	* CSV file copied to CheckServiceFeasibility\SOAP\VBO\Service\ServiceFeasibility\V1\ServiceFeasibilityVBO.csv




## Example commands Linux:

Navigate to your local GitHub directory

**REST**

Validate a REST Schema:
* csmutilities -a CheckServiceFeasibility/REST/VBO/Service/ServiceFeasibility/V2/ServiceFeasibilityVBO.json

Validate swagger file
* csmutilities -s CheckServiceFeasibility/REST/VBS/Service/CheckServiceFeasibility/V2/CheckServiceFeasibilityVBS.swagger

Validate REST examples
* csmutilities -v CheckServiceFeasibility/REST/VBO/Service/ServiceFeasibility/V2/Examples/

Validate REST package
* csmutilities -p CheckServiceFeasibility/

Validate VBM swagger operations
* csmutilities -o CheckServiceFeasibility/REST/VBS/Service/CheckServiceFeasibility/V2/CheckServiceFeasibilityVBS.swagger

Generate Flat Swagger file
* csmutilities -b CheckServiceFeasibility/REST/VBS/Service/CheckServiceFeasibility/V2/CheckServiceFeasibilityVBS.swagger CheckServiceFeasibility/REST/VBS/Service/CheckServiceFeasibility/V2/CheckServiceFeasibilityVBS.flat.swagger


	Check that the generated file, CheckServiceFeasibilityVBS.flat.swagger, has fully de-referenced the Common files
	i.e. in the generated file a definition exists for "BasicComponents": ...; if it does not then you need to update 
	your json-schema-ref-parser as described above


Generate csv 
* csmutilities -J CheckServiceFeasibility/REST/VBS/Service/CheckServiceFeasibility/V2/CheckServiceFeasibilityVBS.flat.swagger -N CheckServiceFeasibility/REST/VBO/Service/ServiceFeasibility/V2/ServiceFeasibilityVBO.json


	* CSV file copied to  CheckServiceFeasibility/REST/VBO/Service/ServiceFeasibility/V2/ServiceFeasibilityVBO.csv



**SOAP**

Generate csv from SOAP schema:
* csmutilities -S CheckServiceFeasibility/SOAP/VBO/Service/ServiceFeasibility/V1/ServiceFeasibilityVBO.xsd


	* CSV file copied to CheckServiceFeasibility/SOAP/VBO/Service/ServiceFeasibility/V1/ServiceFeasibilityVBO.csv



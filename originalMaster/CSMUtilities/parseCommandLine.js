const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  { name: 'readme', alias: 'r', type: module.exports.printHelp },
  { name: 'validateAJV', alias:'a', type: String, multiple: true},
  { name: 'validateSwagger', alias:'s', type: String, multiple: false},
  { name: 'generateCode', alias: 'g', type: String, multiple: false },
  { name: 'validateExample', alias: 'e', type: String, multiple: true },
  { name: 'validateAllExamples', alias: 'v', type: String, multiple: true },
  { name: 'validatePackage', alias: 'p', type: String, multiple: true },
  { name: 'configureCodegen', alias: 'c', type: String, multiple: false },
  { name: 'generateFlatSwagger', alias: 'b', type: String, multiple: true },
  { name: 'validateSwaggerOperations', alias: 'o', type: String, multiple: false},
  { name: 'generateAllFlatSwaggers', alias: 'B', type: String, multiple: false},
  { name: 'generateFlatSwaggerCSV', alias: 'J', type: String, multiple: false},
  { name: 'generateCSVFromSoapVBO', alias: 'S', type: String, multiple: false},
  { name: 'serviceName', alias: 'N', type: String, multiple: false},
  { name: 'timeout', alias: 't', type: Number }
];

module.exports = {
  parseCommandLine: function()
  {
    try {
        return commandLineArgs(optionDefinitions);
    } catch (e) {
        console.error(e.message);
    } finally {
    }
    return null;
  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'CSM utilities for SOAP and REST',
        content: 'Can perform different integrity [italic]{validations} (ajv, swagger, swagger-model, codegen)'
      },
      {
        header: 'Synopsis',
        content: '$ app <options>'
      },
      {
        header: 'Command List',
        content: [
          { name: 'help', summary: '-h Display help information ' },
          { name: 'validateAJV', summary: '-a <path to JSON Schema VBO> Performs a validation of the JSON Schema VBO file against JSON Schema Draft 04/06 \n' +
                                          'Example: CSMUtilities -a "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Interaction\\Trunk\\Docs\\CustomerInteraction\\REST\\VBO\\Customer\\CustomerInteraction\\V2\\CustomerInteractionVBO.json"'},
          { name: 'validateSwagger', summary: '-s <path to Swagger VBS> Performs a validation of the Swagger file against the swagger/openAPI 2.0'+
                                              'Example: CSMUtilities -s "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Interaction\\Trunk\\Docs\\CustomerInteraction\\REST\\VBS\\Customer\\CustomerInteraction\\V2\\CustomerInteractionVBS.swagger"'},
          { name: 'generateCode', summary: '-g Calls swagger-codegen and generates a spring-boot project'+
                                           'Example: CSMUtilities -g "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Interaction\\Trunk\\Docs\\CustomerInteraction\\REST\\VBS\\Customer\\CustomerInteraction\\V2\\CustomerInteractionVBS.swagger"'},
          { name: 'validateExample', summary: '-e Performs a validation of an example file (JSON) against the specified scheme (loads behind the scene the Common library)'+
                                              'Example: CSMUtilities -e "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Interaction\\Trunk\\Docs\\CustomerInteraction\\REST\\VBO\\Customer\\CustomerInteraction\\V2\\Examples\\CustomerInteractionVBO.json" "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Interaction\\Trunk\\Docs\\CustomerInteraction\\REST\\VBO\\Customer\\CustomerInteraction\\V2\\CustomerInteractionVBO.json"'},
          { name: 'validateAllExamples', summary: '-v Performs a validation of all examples against the specified scheme (behind the scene it loads the Common library and assigns hub & links)'+
                                                  'Example: CSMUtilities -v "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Interaction\\Trunk\\Docs\\CustomerInteraction\\REST\\VBO\\Customer\\CustomerInteraction\\V2\\Examples" "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Interaction\\Trunk\\Docs\\CustomerInteraction\\REST\\VBO\\Customer\\CustomerInteraction\\V2\\CustomerInteractionVBO.json"'},
          { name: 'validatePackage', summary: '-p Performs a complete validation of the package starting from the location of the VBS file (runs all the other commands) \n'+
                                              '   First parameter is the Swagger, Second parameter is the JSON Schema file \n'+
                                              '   Example: CSMUtilities -p ./VBS/Customer/CustomerPrivacyProfile/V2/CustomerPrivacyProfileVBS.swagger ./VBO/Customer/CustomerPrivacyProfile/V2/CustomerPrivacyProfileVBO.json'},
          { name: 'configureCodegen', summary: '-c Saves the path to the codegen jar exec. \n'+
                                              '   Example: CSMUtilities -c \"C:\\Users\\TeneF\\Documents\\work\\SVN\\Liquid\\Scripts\\SwaggerCodeGen\\swagger-codegen-cli-2.2.3.jar\"'},
          { name: 'generateFlatSwagger', summary: '-b Bundle all definitions from swagger files in a single flat one. \n'+
                                                  '   Example: CSMUtilities -b "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Privacy Profile\\Trunk\\Docs\\CustomerPrivacyProfile\\REST\\VBS\\Customer\\CustomerPrivacyProfile\\V2\\CustomerPrivacyProfileVBS.swagger" outputfile'},
          { name: 'generateAllFlatSwaggers', summary: '-B Generate flat swagger files for every .swagger file found. \n'+
                                                      '   Example: CSMUtilities -B "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services"'},
          { name: 'validateSwaggerOperations', summary: '-o Perform a validation on the swagger operations return codes, headers, etc. \n'+
                                                        '   Example: CSMUtilities -o "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Privacy Profile\\Trunk\\Docs\\CustomerPrivacyProfile\\REST\\VBS\\Customer\\CustomerPrivacyProfile\\V2\\CustomerPrivacyProfileVBS.swagger"'},
          { name: 'generateFlatSwaggerCSV', summary: '-J Generate csv file from flat swagger; saves the file with the REST VBO schema \n'+
                                                     '-N Optional service name parameter needed to identify service name in some cases\n' +
                                                        '   Example: CSMUtilities -J "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Privacy Profile\\Trunk\\Docs\\CustomerPrivacyProfile\\REST\\VBS\\Customer\\CustomerPrivacyProfile\\V2\\CustomerPrivacyProfileVBS.flat.swagger"\n '+ 
                                                        '                     -N "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Privacy Profile\\Trunk\\Docs\\CustomerPrivacyProfile\\REST\\VBO\\Customer\\CustomerPrivacyProfile\\V2\\CustomerPrivacyProfileVBO.json"'},
          { name: 'generateCSVFromSoapVBO', summary: '-S Generate csv file from SOAP VBO schema; saves the file with the SOAP VBO schema \n'+ 
                                                        '   Example: CSMUtilities -S "C:\\Users\\TeneF\\Documents\\work\\SVN\\Services\\Customer Privacy Profile\\Trunk\\Docs\\CustomerPrivacyProfile\\SOAP\\VBO\\Customer\\CustomerPrivacyProfile\\V2\\CustomerPrivacyProfileVBO.xsd"' }

        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

};

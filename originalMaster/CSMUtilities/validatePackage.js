const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const ajvValidator = require("./validateAJV.js");
const swaggerValidator = require("./validateSwagger.js");
const genCodeValidator = require("./generateCode.js");
const swaggerValidateOperations = require("./validateSwaggerOperations.js");
const utils = require("./utils.js");
const util = require('util');
const glob = require('glob');

var inputObj = Object();
var validationResult = new Object();
validationResult.vboSchema = false;
validationResult.swagger = false;
validationResult.examples = false;
validationResult.codegen = false;


function locateSwaggerFile(projectPath, inputObj)
{
  var swaggerFiles = glob.sync(projectPath + '/**/*.swagger');
  if (swaggerFiles.length>1)
  {
    return new Error("Multiple swagger file, please check the path");
  }
  else
  {
      inputObj.swaggerFile = path.resolve(swaggerFiles[0]);
      inputObj.vbsFolder = path.resolve(path.dirname(swaggerFiles[0]));
  }
}

function locateVBOSchema(projectPath, inputObj)
{
  var implFolder = glob.sync(projectPath +'/**/Impl');
  if (implFolder.length==0)
    return new Error("Unable to locate the Impl folder, required for validation(1) "+ projectPath);

  //part of the search option there might be multiple /Impl folders (one from Common, one from package, we ignore the one from Common)
  if (implFolder.length>=2)
  {
      for (i=0; i<implFolder.length; ++i){

        //ignore the Common Impl
        if (implFolder[i].indexOf("Common")==-1)
            inputObj.VBOfolder = path.resolve(implFolder[i]+"/../");
      }
  }
  else
    inputObj.VBOfolder = path.resolve(implFolder[0]+"/../");

  inputObj.exampleFolder = path.resolve(inputObj.VBOfolder+"/Examples/");

  var jsonSchemaFile = glob.sync(inputObj.VBOfolder + '/*.json');
  if (jsonSchemaFile.length==0 || jsonSchemaFile.length>1)
    return new Error("Unable to locate the JSON schema, required for validation "+inputObj.VBOfolder);

  inputObj.VBOSchemaFile = jsonSchemaFile[0];

}


/**
 Method performs a complete validation of the package:
 1. Validates the JSON schemes
 2. Validates the Swagger file
 3. Validates the example files
 4. Generates the code based on the swagger file

 */
function validatePackage(input)
{
    var inputObj = Object();
    inputObj.swaggerFile = '';  //input swagger file
    inputObj.vbsFolder = '';    //input swagger directory
    inputObj.VBOfolder = '';
    inputObj.VBOSchemaFile = '';

    var swaggerFileN;
    var vbsFolder;

    var projectPath = path.resolve(input);

    //check the location of the swagger file
    var result = locateSwaggerFile(projectPath, inputObj);
    if (result instanceof Error)
    {
      console.error(chalk.red(result));
      return;
    }

    result = locateVBOSchema(projectPath, inputObj);
    if (result instanceof Error)
    {
      console.error(chalk.red(result));
      return;
    }

    //we assume that all the project follow the same pattern
    var swaggerFileN = inputObj.swaggerFile;
    var vbsFolder = inputObj.vbsFolder;
    var jsonSchemaFileN = inputObj.VBOSchemaFile;
    console.log("VBS Folder: [%s]", vbsFolder);
    if (!utils.checkExists(vbsFolder))
        return;

    var vboFolder = inputObj.VBOfolder;
    console.log("VBO Folder: [%s]", vboFolder);
    if (!utils.checkExists(vboFolder))
        return;


    //locate the example folder
    var exampleFolder = inputObj.exampleFolder;
    console.log("Exm Folder: [%s]", exampleFolder);
    if (!utils.checkExists(vboFolder))
        return;

    //we start by validating the JSON Schemes
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Validating the VBO scheme...."));
    console.log(chalk.green("#############################"));
    if (!ajvValidator.validateVBOSchema(jsonSchemaFileN))
    {
        console.error(chalk.red("JSON Schema is not valid"));
        console.error(chalk.red("Aborting..."));
        return;
    } else
        validationResult.vboSchema = true;

    //validate the swagger file
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Validating the Swagger file.."));
    console.log(chalk.green("#############################"));
    var promise = swaggerValidator.validatePromise(swaggerFileN);
    promise.then(function (api) {
        console.log("API name: %s, Version: %s: [%s]", api.info.title, api.info.version, chalk.green("VALID"));
        validationResult.swagger = true;

        //validate all the return codes
        console.log(chalk.green("#############################"));
        console.log(chalk.green("Validating swagger operations.."));
        console.log(chalk.green("#############################"));
        swaggerValidateOperations.validate(swaggerFileN);

        //validate the examples
        console.log(chalk.green("#############################"));
        console.log(chalk.green("Validating all the examples.."));
        console.log(chalk.green("#############################"));
        console.log("Example folder: [%s]", exampleFolder);
        var validateExample = [exampleFolder, jsonSchemaFileN];
        ajvValidator.validateAllExamples(validateExample);

        //generate the code
        console.log(chalk.green("#############################"));
        console.log(chalk.green("Validate & generate java code.."));
        console.log(chalk.green("#############################"));
        genCodeValidator.validate(swaggerFileN);

    }).catch(function (err) {
        console.log("Error: [%o]", err);

    });
}

module.exports = {
    validate: function (swaggerFile)
    {
        validatePackage(swaggerFile[0]);
    }
};

var fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const utils = require("./utils.js");
var JSZip = require('jszip');
var glob = require("glob");

function replaceStringOld(documentBuffer, position, oldString, newString)
{
  /*
  console.log("Position: %d", position);
  console.log("OldString: %s", oldString);
  console.log("NewString: %s", newString);
  console.log("Before: %s", documentBuffer.substring(position-50, position+50));
  console.log("After: %s", documentBuffer.substring(position+oldString.length, position+oldString.length+50));

  var before = documentBuffer.substring(position, position-50);
  var after = documentBuffer.substring(position+oldString.length, position+oldString.length+50);
  var all = before + newString + after;

  console.log("All: %s", all);*/
  documentBuffer=documentBuffer.substring(0, position) + newString + documentBuffer.substring(position+oldString.length, documentBuffer.length);
  return documentBuffer;
}

function replaceString(documentBuffer, position, newString)
{
  documentBuffer=documentBuffer.substring(0, position) + newString + documentBuffer.substring(position, documentBuffer.length);
  return documentBuffer;
}

/*
* This method is very error prone but for the moment it will do
*/
function replaceDocumentStatus(documentBuffer, newStatusValue)
{
  const endTag              = "</w:r>";
  const statusKey           = 'DOCPROPERTY  \"Doc Status\"  \\\* MERGEFORMAT';
  const statusKeyBkp        = 'DOCPROPERTY  &quot;Doc Status&quot;  \\\* MERGEFORMAT';  //inside some of our word documents the doc status is escaped with quotes! not sure why
  const placeHolderGID      = "<w:t>GIDDocStatus</w:t>";
  const placeHolderDraft    = "<w:t>DRAFT</w:t>";
  const placeHolderApproved = "<w:t>APPROVED</w:t>";

  newStatusValue = "<w:t>"+newStatusValue+"</w:t>";

  //get the position of the common element
  var pos = documentBuffer.indexOf(statusKey);
  if (pos==-1)
  {
    pos = documentBuffer.indexOf(statusKeyBkp);
    if (pos==-1)
    {
      console.log(chalk.red("Unable to find the Document Status placeholder!"));
      return documentBuffer;
    }
  }

  //console.log("Document status placeholder, found at position: [%s]", pos);

  //find the next </w:r>
  var firstTagPos = documentBuffer.indexOf(endTag, pos);
  //console.log("Document status, fistTag position: [%s]", firstTagPos);

  var secondTagPos = documentBuffer.indexOf(endTag, firstTagPos+endTag.length);
  //console.log("Document status, secondTag position: [%s]", secondTagPos);

  var thirdTagPos = documentBuffer.indexOf(endTag, secondTagPos+endTag.length);
  //console.log("Document status, thirdTag position: [%s]", thirdTagPos);

  //check if we find the generic placeholder
  var wkPos = documentBuffer.lastIndexOf(placeHolderGID, thirdTagPos);

  if (wkPos!=-1)
  {
      //console.log("Found generic placeholder: GIDDocStatus, at position: [%s]", wkPos);
      documentBuffer = replaceStringOld(documentBuffer, wkPos, placeHolderGID, newStatusValue);
      console.log(chalk.yellow("Found and replaced the GIDDocStatus with the new value:", newStatusValue));
  }
  else
  {
      //console.log("NOT Found generic placeholder: GIDDocStatus, at position: [%s]", wkPos);
      //check if we find the APPROVED placeholder
      wkPos = documentBuffer.lastIndexOf(placeHolderApproved, thirdTagPos);
      if (wkPos!=-1)
      {
        //console.log("Found generic placeholder: APPROVED, at position: [%s]", wkPos);
        documentBuffer = replaceStringOld(documentBuffer, wkPos, placeHolderApproved, newStatusValue);
        console.log(chalk.yellow("Found and replaced the APPROVED  with the new value:", newStatusValue));
      }
      else
      {
        //console.log("NOT Found generic placeholder: APPROVED, at position: [%s]", wkPos);
        //check if we find the DRAFT placeholder
        wkPos = documentBuffer.lastIndexOf(placeHolderDraft, thirdTagPos);
        /*console.log("thirdTagPos: %d", thirdTagPos);
        console.log("PlaceHolderDraft:%s", placeHolderDraft);
        console.log("Position: %s", wkPos);
        */

        if (wkPos!=-1)
        {
          //console.log("Found generic placeholder: STATUS, at position: [%s]", wkPos);
          documentBuffer = replaceStringOld(documentBuffer, wkPos, placeHolderDraft, newStatusValue);
          console.log(chalk.yellow("Found and replaced the DRAFT with the new value:", newStatusValue));
        }
        else
        {
          console.log(chalk.red("Unable to find one of the possible placeholders (GIDDocStatus, Approved, Draft), skipping document change"));
        }
      }
  }

  return documentBuffer;
}

/*
* This method is very error prone but for the moment it will do
*/
function replaceReleaseVersion(documentBuffer, newReleaseVersion)
{
  const endTag              = "</w:r>";
  const statusKey           = 'DOCPROPERTY  \"Release\"  \\\* MERGEFORMAT';
  const statusKeyBkp        = 'DOCPROPERTY  &quot;Release&quot;  \\\* MERGEFORMAT';  //inside some of our word documents the doc status is escaped with quotes! not sure why

  newReleaseVersion = "<w:t>"+newReleaseVersion+"</w:t>";

  //get the position of the common element
  var pos = documentBuffer.indexOf(statusKey);
  if (pos==-1)
  {
    console.log("Unable to find the Release tag...");
    pos = documentBuffer.indexOf(statusKeyBkp);
    if (pos==-1)
    {
      console.log("Unable to find the Release tag(2)...");
      //let's try to solve the split occurences
      var statusKeySplit = "DOCPROPERTY  \"Release";
      var statusKeySpliBkp = "DOCPROPERTY  &quot;Release";

      pos = documentBuffer.indexOf(statusKeySplit);
      if (pos!=-1)
      {
          //ok, we managed to find the release tag inside the document
          //we start looking for a "Mergeformat"
          var newPos = documentBuffer.indexOf("MERGEFORMAT", pos);
          if (newPos!=-1)
            pos = newPos;
      }
      else{
        console.log(chalk.red("Unable to find the Release tag placeholder!"));
        return documentBuffer;
      }
    }
    else
    {
      console.log("Found second key backup: [%s]", pos);
    }
  }

  //console.log("Document status placeholder, found at position: [%s]", pos);

  //find the next </w:r>
  var firstTagPos = documentBuffer.indexOf(endTag, pos);
  //console.log("Document status, fistTag position: [%s]", firstTagPos);

  var secondTagPos = documentBuffer.indexOf(endTag, firstTagPos+endTag.length);
  //console.log("Document status, secondTag position: [%s]", secondTagPos);

  var thirdTagPos = documentBuffer.indexOf(endTag, secondTagPos+endTag.length);
  //console.log("Document status, thirdTag position: [%s]", thirdTagPos);

  //retrieve the last <w:t> </w:t> tag
  var endWtTagPos = documentBuffer.lastIndexOf("</w:t>", thirdTagPos);
  var startWtTagPos = documentBuffer.lastIndexOf("<w:t>", thirdTagPos);

  /*console.log("endTagPos:", endWtTagPos);
  console.log("startTagPos:", startWtTagPos);
  console.log("Buffer: ", documentBuffer.substring(startWtTagPos, endWtTagPos+6));
*/
  //replace the release version
  documentBuffer = documentBuffer.substring(0, startWtTagPos) + newReleaseVersion + documentBuffer.substring(endWtTagPos+6, documentBuffer.length);
  console.log(chalk.yellow("Found and replaced the release version with the new value:", newReleaseVersion));

  return documentBuffer;
}

function replaceDocProperties(documentBuffer, tagName, newValue)
{
  var startTag="<vt:lpwstr>";
  var endTag = "</vt:lpwstr>";
  var updatedValue = startTag+newValue+endTag;

  var tagNamePos = documentBuffer.indexOf(tagName);
  if (tagNamePos==-1)
  {
    console.log(chalk.red("Unable to find the %s in the document properties", tagName));
    return;
  }

  var startTagPos = documentBuffer.indexOf(startTag, tagNamePos);
  var endTagPos = documentBuffer.indexOf(endTag, tagNamePos);

  var documentBuffer = documentBuffer.substring(0, startTagPos) + updatedValue + documentBuffer.substring(endTagPos+endTag.length, documentBuffer.length);
  return documentBuffer;
}


function processZipDoc(config, docFileInput, docFileOutput)
{
  var globalFileInput = docFileInput;
  fs.readFile(docFileInput, function(err, data) {
      if (err) throw err;
      JSZip.loadAsync(data).then(function (zip)
      {
        //DocumentStatus({DOC_STATUS}), DocumentVersion({RELEASE_VERSION}), {RELEASE_DATE}
        //DocumentProperties, MainDocument

        var promiseUpdate = zip.file("docProps/custom.xml").async("text");
        promiseUpdate.then(function(txt){
          var statusTagName = "Doc Status";
          var releaseTagName = "Release";
          var result = txt;
          if (config.release.wordUpdate.updateDocumentStatus==true)
            result = replaceDocProperties(result, statusTagName, config.release.wordUpdate.updateDocumentStatusValue);

          if (config.release.wordUpdate.updateReleaseVersion==true)
            result = replaceDocProperties(result, releaseTagName, config.release.version);

          if (config.release.wordUpdate.updateReleaseDate =true)
            result = result.replace(/GIDReleaseDate/g,config.release.wordUpdate.updateReleaseDateValue);

          return result;
        }).then(function(result){
          zip.file("docProps/custom.xml", result);
        });

        //word/document
        var promiseDocument = zip.file("word/document.xml").async("text");
        promiseDocument.then(function(txt){
          var result = txt;
          if (config.release.wordUpdate.updateDocumentStatus==true)
            result = replaceDocumentStatus(result, config.release.wordUpdate.updateDocumentStatusValue);

          if (config.release.wordUpdate.updateReleaseVersion==true)
          {
            if (globalFileInput.indexOf('Technical Service Specification-SOAP')!=-1)
            {
              var soapVersion = config.release.version.substring(0, config.release.version.indexOf("-"));
              result = replaceReleaseVersion(result, soapVersion);
              result = result.replace(/GIDReleaseVersion/g,soapVersion);
            }
            else if (globalFileInput.indexOf('Technical Service Specification-API')!=-1)
            {
              var restVersion = config.release.version.substring(config.release.version.indexOf("-")+1, config.release.version.length);
              result = replaceReleaseVersion(result, restVersion);
              result = result.replace(/GIDReleaseVersion/g,restVersion);
            }
            else
            {
              result = result.replace(/GIDReleaseVersion/g,config.release.version);
              result = replaceReleaseVersion(result, config.release.version)
            }
          }

          if (config.release.wordUpdate.updateReleaseDate =true)
            result = result.replace(/GIDReleaseDate/g,config.release.wordUpdate.updateReleaseDateValue);
          return result;
        }).then(function(result){
          zip.file("word/document.xml", result);
        });

        Promise.all([promiseUpdate, promiseDocument]).then(function()
        {
          zip
          .generateNodeStream(
            {type:'nodebuffer',
            compression: "DEFLATE",
            compressionOptions: {
                level: 9
            },
            streamFiles:true})
          .pipe(fs.createWriteStream(docFileOutput))
          .on('finish', function () {
            // JSZip generates a readable stream with a "end" event,
            // but is piped here in a writable stream which emits a "finish" event.
            console.log("[%s] written: [OK]", docFileOutput);
          });
        });
      });
  });
}

function processExcelDoc(config, docFileInput, docFileOutput)
{
  fs.readFile(docFileInput, function(err, data) {
      if (err) throw err;
      JSZip.loadAsync(data).then(function (zip)
      {
        //DocumentStatus({DOC_STATUS}), DocumentVersion({RELEASE_VERSION}), {RELEASE_DATE}
        //DocumentProperties, MainDocument

        var promiseUpdate = zip.file("xl/sharedStrings.xml").async("text");
        promiseUpdate.then(function(txt){
          var result = txt;

          if (config.release.wordUpdate.updateReleaseVersion==true)
            result = result.replace(/GIDReleaseVersion/g,config.release.version);

          if (config.release.wordUpdate.updateReleaseDate =true)
            result = result.replace(/GIDReleaseDate/g,config.release.wordUpdate.updateReleaseDateValue);

          return result;
        }).then(function(result){
          zip.file("xl/sharedStrings.xml", result);
        });

        Promise.all([promiseUpdate]).then(function()
        {
          zip
          .generateNodeStream(
            {type:'nodebuffer',
            compression: "DEFLATE",
            compressionOptions: {
                level: 9
            },
            streamFiles:true})
          .pipe(fs.createWriteStream(docFileOutput))
          .on('finish', function () {
            // JSZip generates a readable stream with a "end" event,
            // but is piped here in a writable stream which emits a "finish" event.
            console.log("[%s] written: [OK]", docFileOutput);
          });
        });
      });
  });
}

function processSwaggerFile(config, fileInput)
{
  //check if we have a REST realisation
  fs.readFile(fileInput, function(err, data) {
      if (err)
      {
          console.log(chalk.red(err));
          throw err;
      }

      console.log("Working on file: %s", fileInput);
      var result = data.toString();
      var newVersion = "\"version\": \""+config.release.version.substr(config.release.version.indexOf("-")+2, config.release.version.length)+"\""; //-R is stripped out

      result = result.replace(/\"version\":.*\".*"/g,newVersion);

      fs.writeFile(fileInput, result, function(err) {
          if(err) {
            return console.log(err);
          }

          console.log("Update completed for: [%s]", fileInput);
        });
  });
}

function processWADLFile(config, fileInput)
{
  //check if we have a REST realisation
  fs.readFile(fileInput, function(err, data) {
      if (err)
      {
          console.log(chalk.red(err));
          throw err;
      }

      console.log("Working on file: %s", fileInput);
      var result = data.toString();
      var newVersion = "<tns:version>" + config.release.version.substr(config.release.version.indexOf("-")+2, config.release.version.length)+"</tns:version>";//-R is stripped out
      result = result.replace(/<tns:version>.*<\/tns:version>/g,newVersion);

      fs.writeFile(fileInput, result, function(err) {
          if(err) {
            return console.log(err);
          }

          console.log("Update completed for: [%s]", fileInput);
        });
  });
}

function processWSDLFile(config, fileInput)
{
  //check if we have a REST realisation
  fs.readFile(fileInput, function(err, data) {
      if (err)
      {
          console.log(chalk.red(err));
          throw err;
      }

      console.log("Working on file: %s", fileInput);
      var result = data.toString();
      var newVersion = "<wsdl:documentation>version:" + config.release.version.substr(1, config.release.version.indexOf("-")-1)+"</wsdl:documentation>";//S is stripped out
      result = result.replace(/<wsdl:documentation>.*version:.*<\/wsdl:documentation>/g,newVersion);

      fs.writeFile(fileInput, result, function(err) {
          if(err) {
            return console.log(err);
          }

          console.log("Update completed for: [%s]", fileInput);
        });
  });
}

function processTechnicalArtefacts(fileInput)
{
  //check if we have a REST realisation
  fs.readFile(fileInput, function(err, data) {
      if (err)
      {
          console.log(chalk.red(err));
          throw err;
      }
  });
}

function processChanges(config, resolve, reject)
{
    var directory = config.sourceFolder;
    //normalize the path
    if (!utils.checkExists(directory))
      reject();


    //look inside the current folder and go through all the word documents
    fs.readdirSync(directory).forEach(file => {
      if (file.indexOf('.doc')>-1)
      {
          console.log("Working on file: [%s]", file);
          var inputFile = path.resolve(config.sourceFolder, file);
          var outputFile = inputFile;

          processZipDoc(config, inputFile, outputFile);
      }


      if (file.indexOf('.xlsx')>-1)
      {
          console.log("Working on file: [%s]", file);
          var inputFile = path.resolve(config.sourceFolder, file);
          var outputFile = inputFile;

          processExcelDoc(config, inputFile, outputFile);
      }

    });

    resolve(true);
}

function updateTechnicalArtefacts(config, resolve, reject)
{
    var promiseSwagger;
    var promiseWSDL;
    var promiseWADL;
      glob("**/*.swagger",  function (err, files) {
        if (err)
        {
          console.log(chalk.red("Unable to locate the .swagger file, if none present the message can be ignored"));
          return;
        }

        if (files!=null && files.length>0)
        {
          for (var i in files)
          {
            var filePathNormalized = path.resolve(files[i]);
            processSwaggerFile(config, filePathNormalized);
          }
        }
      });

      glob("**/*.wadl",  function (err, files) {
        if (err)
        {
          console.log(chalk.red("Unable to locate the .wadl file, if none present the message can be ignored"));
          return;
        }

        if (files!=null && files.length>0)
        {
          for (var i in files)
          {
            var filePathNormalized = path.resolve(files[i]);
            processWADLFile(config, filePathNormalized);
          }
        }
      });

      glob("**/*.wsdl",  function (err, files) {
        if (err)
        {
          console.log(chalk.red("Unable to locate the .wsdl file, if none present the message can be ignored"));
          return;
        }

        if (files!=null && files.length>0)
        {
          for (var i in files)
          {
            var filePathNormalized = path.resolve(files[i]);
            processWSDLFile(config, filePathNormalized);
          }
        }
      });

      resolve(true);
}


module.exports = {

  processWord: function(config)
  {
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Processing the word document changes"));
    console.log(chalk.green("#############################"));

    return new Promise(function (resolve, reject) {
      processChanges(config, resolve, reject);
    });
  },

  processTechnical: function(config)
  {
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Processing the word document changes"));
    console.log(chalk.green("#############################"));

    return new Promise(function (resolve, reject) {
      updateTechnicalArtefacts(config, resolve, reject);
    });
  }
}

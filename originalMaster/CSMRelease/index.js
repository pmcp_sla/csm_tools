#! /usr/bin/env node

const fs = require('fs');
const program = require('commander');
const inquirer = require ('inquirer');
const path = require('path');
const chalk = require('chalk');
const generatePDF = require('./generatePDF.js');
const svnLib = require("./svnLib.js");
const zipLib = require("./zipLib.js");
const wordLib = require("./wordLib.js");


function getTodayDate()
{
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  if (mm<10) mm="0"+mm;
  if (dd<10) dd="0"+dd;
  var yyyy = today.getFullYear();

  return dd+'/'+mm+'/'+yyyy;
}

function generateZipFromTag(config, tagFolder)
{
  var zipTagResult;
  var inputFolder = path.resolve(tagFolder+'/'+config.release.version);
  if (config.zipTagFolder.zipName!='default')
    zipTagResult = zipLib.createReleaseZip(inputFolder, config.zipTagFolder.zipName);
  else
      zipTagResult = zipLib.createReleaseZip(inputFolder);

  Promise.all([zipTagResult]).then(values=>{
      console.log("ZIP completed: [OK]");
  }).catch(reason=>{
    console.log(chalk.red("ZIP File generation failed:", reason))
  });
}

function processTagOperation(config)
{
  var tagResult = svnLib.tagRelease(config.sourceFolder,config.tag.message, config.release.version);

  Promise.all([tagResult]).then(tagFolder=>{
      console.log("Tag completed: [OK]");
      console.log("Tag folfder: [%s]", tagFolder);
      if (config.operation.zipTagFolder==true)
          generateZipFromTag(config, tagFolder);
  }).catch(reason => {
      console.log(chalk.red("Tagging failed(1):", reason))
  });
}

function commitLocalChanges(config)
{
  var promiseResult = svnLib.commitLocalChanges(config.commit.message);
  promiseResult.then(function (data)
  {
      //check if we need to tag anything
      if (config.operation.tag==true)
      {
        processTagOperation(config);
      }
  }, function (error)
  {
    console.log(chalk.red("commit completed with error", error));
  });
}

function generatePDFFiles(config)
{
  var promiseGeneratorPDF = generatePDF.generateForFolder(config.sourceFolder);

  Promise.all([promiseGeneratorPDF]).then(values=>{
    if (config.operation.commit==true)
    {
        commitLocalChanges(config);
    }
  });
}

function processWordChanges(config)
{
  console.log("Process word changes");
  var promiseResult = wordLib.processWord(config);
  promiseResult.then(function (data)
  {
      if (config.operation.technicalArtefacts==true)
          processTechnicalArtefacts(config);
  }, function (error)
  {
    console.log(chalk.red("Unable to update word documents", error));
  });
}

function processTechnicalArtefacts(config)
{
  console.log("Process technical artefacts");
  var promiseResult = wordLib.processTechnical(config);
  promiseResult.then(function (data)
  {
      if (config.operation.generatePDF==true)
        generatePDFFiles(config);
  }, function (error)
  {
    console.log(chalk.red("Unable to update technical artefacts", error));
  });
}

function processingCommands(config)
{
  //check if we want to update the word documents
  if (config.operation.wordUpdate==true)
  {
    processWordChanges(config);
  }
  else if (config.operation.technicalArtefacts==true)
  {
    processTechnicalArtefacts(config);
  }
  else if (config.operation.generatePDF==true)
  {
    generatePDFFiles(config);
  }
  else if (config.operation.commit==true)
  {
    commitLocalChanges(config);
  }
  else if (config.operation.tag==true)
  {
    processTagOperation(config);
  }


}


//import inquirer
function list(val) {
  //return val.replace(/^\s+|\s+$/g,"").split(/\s*,\s*/);
  return val.split(',');
}

var svnCommit = new Object();
svnCommit.required = false;

var promise;
var promiseGeneratorPDF;


program
  .version('0.0.1')
  .parse(process.argv)

if (process.argv.length==2) console.log(program.helpInformation());

/*
.option('-p, --packageFolder [folderPath]', 'specify the package folder path up to the trunk')
.option('-f, --generateFolderPDF [folderPath]', 'generate the PDF\'s for all the .doc, .docx files from the specified folder')
.option('-c, --commitLocalChanges [Message]', 'commit the local changed files on SVN')
.option('-t, --tagRelease [ReleaseVersion]', 'create a tag on SVN for the specified release')
.option('-g, --generatePDF [items]', 'generate the PDF\'s for the provided list of word or for all documents in the current folder', list)
.option('-l, --list [list]', 'list of customers in CSV file')
*/


//we execute the commands in a specific order: Update DOCS, Update WSDL/Swagger, GeneratePDF, SVNCommit

let questions = [
  { type: 'input',
    name: 'sourceFolder',
    message: 'Please provide the source package folder ?',
    default: './'
  },
  {
    type: 'input',
    name: 'release.version',
    message: 'Please provide the Release Version (e.g. S1.0.0-R1.0.0): ',
    default: 'SX.X.X-RY.Y.D'
  },
  { type: 'confirm',
    name: 'operation.wordUpdate',
    message: 'Do you want to update word documents ?'
  },
  { when: function (response) {
      return response.operation.wordUpdate;
    },
    type: 'confirm',
    name: 'release.wordUpdate.updateReleaseVersion',
    message: 'Do you want to update the ReleaseVersion inside the word documents ?',
  },
  { when: function (response) {
      return response.operation.wordUpdate;
    },
    type: 'confirm',
    name: 'release.wordUpdate.updateReleaseDate',
    message: 'Do you want to update the ReleaseDate inside the word documents ?',
  },
  { when: function (response) {
      return response.operation.wordUpdate && response.release.wordUpdate.updateReleaseDate;
    },
    type: 'input',
    name: 'release.wordUpdate.updateReleaseDateValue',
    message: 'Please specify the release date (format dd/mm/yyyy):',
    default: getTodayDate(),
  },

  { when: function (response) {
      return response.operation.wordUpdate;
    },
    type: 'confirm',
    name: 'release.wordUpdate.updateDocumentStatus',
    message: 'Do you want to update the DocumentStatus(Draft/Approved) inside the word documents ?',
  },
  { when: function (response) {
      return response.operation.wordUpdate && response.release.wordUpdate.updateDocumentStatus;
    },
    type: 'list',
    name: 'release.wordUpdate.updateDocumentStatusValue',
    message: 'Do you want to update the DocumentStatus(Draft/Approved) inside the word documents ?',
    choices: ['DRAFT', 'APPROVED']
  },
  { type: 'confirm',
    name: 'operation.technicalArtefacts',
    message: 'Do you want to update the technical artefacts(swagger, wsdl, wadl) ?',
  },
  { type: 'confirm',
    name: 'operation.generatePDF',
    message: 'Do you want to generate the PDF documents ?',
  },
  { type: 'confirm',
    name: 'operation.commit',
    message: 'Do you want to commit the changes to SVN ?',
  },
  { when: function (response) {
      return response.operation.commit;
    },
    type: 'input',
    name: 'commit.message',
    message: 'Please provide the message for the commit on SVN ?',
  },
  { type: 'confirm',
    name: 'operation.tag',
    message: 'Do you want to TAG the changes on SVN ?',
  },
  { when: function (response) {
      return response.operation.tag;
    },
    type: 'input',
    name: 'tag.message',
    message: 'Please provide the message for the TAG operation on SVN ?',
  },
  { when: function (response) {
      return response.operation.tag;
    },
    type: 'confirm',
    name: 'operation.zipTagFolder',
    message: 'Do you want to create the output ZIP file from the tag  ?',
  },
  { when: function (response) {
      return response.operation.tag && response.operation.zipTagFolder;
    },
    type: 'input',
    name: 'zipTagFolder.zipName',
    message: 'Please provide the ZIP file name or use the default one (default is the package name)?',
    default: 'default'
  }
];


inquirer.prompt(questions).then(function (answers) {
      console.log(answers);
      processingCommands(answers);
    });


/*
if (svnCommit.tagRelease==true)
{
    if (svnCommit.packageFolder==null)
    {
      console.log(chalk.red("The path to the working folder must be specified using the -p option"));
      return;
    }

    if (svnCommit.releaseVersion==null)
    {
      console.log(chalk.red("No release version specified"));
      return;
    }

    if (svnCommit.commitMessage==null)
    {

      svnCommit.commitMessage = "Commit for release:"+svnCommit.releaseVersion;
      console.log(chalk.yellow("No commit message specified, using the default template one:", svnCommit.commitMessage));
    }

    var tagResult = svnLib.tagRelease(svnCommit.packageFolder,svnCommit.commitMessage, svnCommit.releaseVersion);

    Promise.all([tagResult]).then(values=>{
        console.log("tag result");
    }).catch(reason => {
        console.log(reason)
    });
}
*/


/*
if (program.generatePDF!=null)
{
  generatePDF.generateForFiles(program.generatePDF);
}
else */

//if we don't have a pdf generation required but still we want to commit something
//if (program.generateFolderPDF==null && program.commitLocalChanges!=null)



/*
let questions = [
  { type: 'confirm',
    name: 'release.version.update',
    message: 'Do you want to update the release version ?',
  },
  { when: function (response) {
      return response.release.version.update;
    },
    type: 'input',
    name: 'release.version.value',
    message: 'Release Version (e.g. S1.0.0-R1.0.0): ',
  },
  { type: 'confirm',
    name: 'release.date.update',
    message: 'Do you want to update the release date ?',
  },
  { when: function (response) {
      return response.release.date.update;
    },
    type: 'input',
    name: 'release.date.value',
    message: "Release Date (e.g. 23/11/2017): "
  },

  { type: 'confirm',
    name: 'release.status.update',
    message: 'Do you want to update the document status (draft/approved) ?',
  },
  { when: function (response) {
      return response.release.status.update;
    },
    type: 'list',
    name: 'release.status.value',
    message : "Document Status(DRAFT/APPROVED): ",
    choices: ['DRAFT', 'APPROVED']
  }
];

inquirer.prompt(questions).then(function (answers) {
      console.log(answers);
    });

*/




//console.log(program.list);


/*
    0. present the console parameters
    1. read the parameters from the console
        - TEMP_DOC_STATUS       : update the doc status (y,n)
        - TEMP_RELEASE_VERSION  : update the release version (y,n)
        - TEMP_RELEASE_DATE     : update the release date (y,n)
        - SVN Commit            : commit the changes on SVN (y,n)
        - SVN tag               : create a tag/branch on SVN (y,n)
        - package type: custom or VSGB or Release
          - Custom: every parameter is confirmed
          - VSGB:
            - TEMP_DOC_STATUS:Y
            - TEMP_RELEASE_VERSION: Y
            - TEMP_RELEASE_DATE: N
            - SVN Commit: N
            - SVN Tag: N
          - Release:
            - TEMP_DOC_STATUS:Y
            - TEMP_RELEASE_VERSION: Y
            - TEMP_RELEASE_DATE: Y
            - SVN Commit: Y
            - SVN Tag: Y
        -
    3. Update the word documents

*/













/*
var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var ajvValidator = require("./validateAJV.js");
var swaggerValidator = require("./validateSwagger.js");
var genCodeValidator = require("./generateCode.js");
var packageValidator = require("./validatePackage.js");
var configuration = require("./configuration.js")
var pdfGenerator = require("./generatePDF.js")
const path = require('path');


var JSZip = require('jszip');

var zipDocument;

function processZipDoc(docFileInput, docFileOutput)
{
  fs.readFile(docFileInput, function(err, data) {
      if (err) throw err;
      JSZip.loadAsync(data).then(function (zip)
      {
        //DocumentStatus({DOC_STATUS}), DocumentVersion({RELEASE_VERSION}), {RELEASE_DATE}
        //DocumentProperties, MainDocument

        var promiseUpdate = zip.file("docProps/custom.xml").async("text");
        promiseUpdate.then(function(txt){
          var result = txt.replace(/{TEMP_DOC_STATUS}/g,"APPROVED");
          result = result.replace(/{TEMP_RELEASE_VERSION}/g, "S111-R222");
          result = result.replace(/{TEMP_RELEASE_DATE}/g,"19.19.1999");
          return result;
        }).then(function(result){
          zip.file("docProps/custom.xml", result);
        });

        //word/document
        var promiseDocument = zip.file("word/document.xml").async("text");
        promiseDocument.then(function(txt){
          var result = txt.replace(/{TEMP_DOC_STATUS}/g,"APPROVED");
          result = result.replace(/{TEMP_RELEASE_VERSION}/g, "S111-R222");
          result = result.replace(/{TEMP_RELEASE_DATE}/g,"19.19.1999");
          return result;
        }).then(function(result){
          zip.file("word/document.xml", result);
        });

        Promise.all([promiseUpdate, promiseDocument]).then(function()
        {
          zip
          .generateNodeStream({type:'nodebuffer',streamFiles:true})
          .pipe(fs.createWriteStream(docFileOutput))
          .on('finish', function () {
            // JSZip generates a readable stream with a "end" event,
            // but is piped here in a writable stream which emits a "finish" event.
            console.log("out.zip written.");
          });
        });
      });
  });
}

*/
/*processZipDoc("working.docx","working_updated.docx");
processZipDoc("working2.docx","working2_updated.docx");
processZipDoc("working3.docx","working3_updated.docx"); */



//return;
/*
//test-florin
var msopdf = require('node-msoffice-pdf');

//test-florin 2
var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');
var path = require('path');

var exampleBaseName = path.basename("test.docx");
//console.log("File: %s", exampleBaseName);

//Load the docx file as a binary
var content = fs
    .readFileSync(path.resolve('./test.docx'), 'binary');


var zip = new JSZip(content);

var doc = new Docxtemplater();
doc.loadZip(zip);



//set the templateVariables
doc.setData({
    placeholder: 'A MERSSS!!!',
    status: 'XXXX'
});

try {
    // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    doc.render()
}
catch (error) {
    var e = {
        message: error.message,
        name: error.name,
        stack: error.stack,
        properties: error.properties,
    }
    console.log(JSON.stringify({error: e}));
    // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
    throw error;
}

var buf = doc.getZip()
             .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
fs.writeFileSync(path.resolve('./output.docx'), buf);



msopdf(null, function(error, office) {

    if (error) {
      console.log("Init failed", error);
      return;
    }


  //   There is a queue on the background thread, so adding things is non-blocking.
   office.word({input: "output.docx", output: "outfile.pdf"}, function(error, pdf) {
      if (error) {

          //     Sometimes things go wrong, re-trying usually gets the job done
          //     Could not get remoting to repiably not crash on my laptop

           console.log("Woops", error);
       } else {
           console.log("Saved to", pdf);
       }
   });



  //   Word/PowerPoint/Excel remain open (for faster batch conversion)
  // To clean them up, and to wait for the queue to finish processing

   office.close(null, function(error) {
       if (error) {
           console.log("Woops", error);
       } else {
           console.log("Finished & closed");
       }
   });
});
*/

/*
//generic print console help
if ('help' in cmdArgs)
  cmdObj.printHelp();
else if ('validateAJV' in cmdArgs) //check if we need to perform an AJV validation
{
  ajvValidator.validate(cmdArgs['validateAJV']);
}
else if ('validateSwagger' in cmdArgs) //check if we need to perform a swagger validation
{
  swaggerValidator.validate(cmdArgs['validateSwagger']);
}
else if ('generateCode' in cmdArgs)
{
  genCodeValidator.validate(cmdArgs['generateCode']);
}
else if ('validateAllExamples' in cmdArgs)
{
  ajvValidator.validateAllExamples(cmdArgs['validateAllExamples']);
}
else if ('validateExample' in cmdArgs)
{
  ajvValidator.validateExample(cmdArgs['validateExample']);
}
else if ('validatePackage' in cmdArgs)
{
  packageValidator.validate(cmdArgs['validatePackage']);
}
else if ('configureCodegen' in cmdArgs)
{
  configuration.validate(cmdArgs['configureCodegen']);
}
else if ('generateFlatSwagger' in cmdArgs)
{
  swaggerValidator.bundle(cmdArgs['generateFlatSwagger'])
}
else if ('generatePDF' in cmdArgs)
{
  pdfGenerator.generate(cmdArgs['generatePDF'])
}
else
  cmdObj.printHelp();

//console.log(cmdArgs);
*/

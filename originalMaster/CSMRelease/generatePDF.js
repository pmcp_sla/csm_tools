var fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const utils = require("./utils.js");
const svnLib = require("./svnLib.js");

var donePromise;
var promisesArray = new Array();

function generateForDirectory(directory)
{
  var inputFileList = new Array();

  console.log("Generating PDFs for all files in: [%s]", directory);
  if (!utils.checkExists(directory))
    return;

    fs.readdirSync(directory).forEach(file => {
      if (file.indexOf('.doc')>-1)
      {
        console.log("Found word file: [%s]", file);
        inputFileList.push(file);
      }
    });

    generateForFiles(inputFileList);
}


function generateForFiles(fileList)
{
  var inputFileList = new Array();

  //check if all the files are correct
  for (var idx in fileList)
  {
    //check the file sanity
    var filePath = path.resolve(process.cwd(), fileList[idx].trim());
    if (utils.checkExists(filePath))
        inputFileList.push(filePath);
  }

  //start the pdf writer
  var msopdf = require('node-msoffice-pdf');
  msopdf(null, function(error, office)
  {
    if (error) {
      console.log("Initialization of the MSWord engine failed", error);
      return;
    }

    for (var idx in inputFileList)
    {
      var fileName = inputFileList[idx];
      //get the new file name
      var outputFile = fileName.substr(0, fileName.lastIndexOf(".")) + ".pdf";

      var workPromise = new Promise((resolve, reject) => {
        office.word({input: fileName, output: outputFile}, function(error, pdf) {
           if (error) {

               //     Sometimes things go wrong, re-trying usually gets the job done
               //     Could not get remoting to repiably not crash on my laptop

                console.log("%s %s", chalk.red("The following error occured:"), chalk.red(error));
                return reject(error);
            } else {
                console.log("%s %s", chalk.green("Saved PDF file:"), chalk.green(pdf));
                resolve(pdf);
            }
        });
      });
      promisesArray.push(workPromise);
    }

    var closePromise = new Promise((resolve, reject) => {
      office.close(null, function(error) {
          if (error) {
              console.log("%s: %s", chalk.red("Unable to successfully close the engine!"), error);
              return reject(error);
          } else {
              console.log("PDF file generation completed: [%s]", chalk.green("OK"));
              resolve(true);
          }
      });
    });

    promisesArray.push(closePromise);
  });

}



module.exports = {

  generateForFolder: function(directory)
  {
    var result = false;
    //svnCommit = commitRequest;
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Generating PDFs for all files"));
    console.log(chalk.green("#############################"));

    generateForDirectory(directory);

    //resolve all promises before leaving the function
    return new Promise((resolve, reject) => {
        Promise.all(promisesArray).then(values=>{
          console.log(values);
          resolve(true);
        });

    });
  },
  generateForFiles: function(fileList)
  {
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Generating PDFs...."));
    console.log(chalk.green("#############################"));
    generateForFiles(fileList);
  }


}

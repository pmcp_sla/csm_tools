var chalk = require('chalk');
var Ajv = require('ajv');
const path = require('path');
const fs = require('fs');


function openFile(filename){
    var json = null;
    var file = path.resolve(process.cwd(), filename);
    try {
        try {
            json = JSON.parse(fs.readFileSync(file).toString());
        } catch(JSONerr) {
            json = require(file);
        }
    } catch(err) {
        console.error('error:  ' + err.message);
        process.exit(2);
    }
    return json;
}

function copyLocalFile(_sourceFile, _destinationFile)
{
  //normalize the file path
  var sourceFile = path.resolve(_sourceFile);
  var destinationFile = path.resolve(_destinationFile);

  console.log("copyFile->SourceFile: [%s]", sourceFile);
  console.log("copyFile->DestinationFile: [%s]", destinationFile);

  //check if the file exists on the disk
  if (!fs.existsSync(sourceFile))
  {
    console.error("File: [%s] doesn't exists", sourceFile);
    throw new Error("File specified does not exist on the disk");
  }

  //this will throw errors, is up for the upper layers to deal with the exceptions
  fs.createReadStream(sourceFile).pipe(fs.createWriteStream(destinationFile));
}

/**
  Check if a file exists on the disk
*/
function checkPath(filePath)
{
  var pathNormalized = path.resolve(filePath);

  //for the moment the swagger codegen is hardcoded to: swagger-codegen-cli-2.2.3.jar
  if (!fs.existsSync(pathNormalized))
  {
    console.error("%s: [%s]",chalk.red("Unable to locate the path:"), pathNormalized);
    return false;
  }
  else
    return true;

}

module.exports = {
  loadFile: function(fileName)
  {
    return openFile(fileName);
  },

  copyFile: function(sourceFile, destinationFile)
  {
      return copyLocalFile(sourceFile, destinationFile);
  },

  checkExists: function(filePath)
  {
      return checkPath(filePath);
  }
}

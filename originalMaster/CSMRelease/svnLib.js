var fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const utils = require("./utils.js");
const { URL } = require('url');
var svnUltimate = require('node-svn-ultimate');
var Client = require('svn-spawn');
var client = new Client();

function commitLocalChanges(commitMessage, resolve, reject)
{
  var changedFileList = new Array();

  //get the current repository status vs the target one
  client.getStatus(function(err, data) {

    for (var idx in data)
    {
        var obj = data[idx];
        var objStatus = obj['wc-status'];
        if (objStatus.$.item=='modified')
        {
            console.log("The following file is changed: %s", obj.$.path);
            changedFileList.push(obj.$.path);
        }
    }

    var commitArray = new Array();
    //set the commit Message
    commitArray.push(commitMessage);
    for (var idx in changedFileList)
      commitArray.push(changedFileList[idx]);

    //confirmation on commit ?
    client.commit(commitArray, function(err, data) {
      if (err)
      {
        reject(err);
      }
      else
      {
            console.log('committed %s file(s)', commitArray.length-1);
            resolve(data);
      }
    });
  });
}

function tagRelease(packageFolder, commitMessage, releaseVersion, resolve, reject)
{
  var sourceURL;
  var destinationURL;

  console.log("Tagging the files in the folder: [%s]", packageFolder);
  if (!utils.checkExists(packageFolder))
  {
    reject("The specified path is incorrect");
  }

  var sourceFolder = path.resolve(process.cwd(), packageFolder);
  var destinationFolder = path.resolve(process.cwd(), packageFolder+"/../../Tags");

  client.getInfo(sourceFolder, function(err, data) {

    if (err)
      reject(err);

    var sourceURLObj = new URL(data.url);
    var destinationURLObj = new URL(data.url);
    destinationURLObj.pathname = path.join(destinationURLObj.pathname, '..', '..', '/Tags', releaseVersion);

    //extract the update paths
    sourceURL = data.url;
    destinationURL = destinationURLObj.href;

    console.log(chalk.green("Source SVN folder:", sourceURL));
    console.log(chalk.green("Destination SVN folder:", destinationURL));

    svnUltimate.commands.copy(sourceURL, destinationURL, commitMessage, function( err )
    {
      if (err)
        reject(err);

      console.log(chalk.green("#############################"));
      console.log(chalk.green("Tagging completed: [OK]"));
      console.log(chalk.green("#############################"));
      console.log("Updating local repository")
      client.update(destinationFolder, function(err, data) {
        if (err)
          reject(err);
        console.log(chalk.green("#############################"));
        console.log(chalk.green("Update completed: [OK]"));
        console.log(chalk.green("#############################"));
        resolve(destinationFolder); //we return the local TAG folder
      });
    });

  });

  //resolve(true);
}

module.exports = {

  commitLocalChanges: function(commitMessage)
  {
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Commiting the local changes on SVN"));
    console.log(chalk.green("#############################"));
    //generateForDirectory(directory);

    return new Promise(function (resolve, reject) {
      commitLocalChanges(commitMessage, resolve, reject);

    });
  },
  tagRelease: function(packageFolder, commitMessage, releaseVersion)
  {
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Tag Release...."));
    console.log(chalk.green("#############################"));

    return new Promise(function (resolve, reject) {
        return tagRelease(packageFolder, commitMessage, releaseVersion, resolve, reject);
    });
  }


}




/*
function generateForDirectory(directory)
{
  var inputFileList = new Array();

  console.log("Generating PDFs for all files in: [%s]", directory);
  if (!utils.checkExists(directory))
    return;

    fs.readdirSync(directory).forEach(file => {
      if (file.indexOf('.doc')>-1)
      {
        console.log("Found word file: [%s]", file);
        inputFileList.push(file);
      }
    });

    generateForFiles(inputFileList);
}


function generateForFiles(fileList)
{
  var inputFileList = new Array();

  //check if all the files are correct
  for (var idx in fileList)
  {
    //check the file sanity
    var filePath = path.resolve(process.cwd(), fileList[idx].trim());
    if (utils.checkExists(filePath))
        inputFileList.push(filePath);
  }

  //start the pdf writer
  var msopdf = require('node-msoffice-pdf');
  msopdf(null, function(error, office)
  {
    if (error) {
      console.log("Initialization of the MSWord engine failed", error);
      return;
    }

    for (var idx in inputFileList)
    {
      var fileName = inputFileList[idx];
      //get the new file name
      var outputFile = fileName.substr(0, fileName.lastIndexOf(".")) + ".pdf";

      office.word({input: fileName, output: outputFile}, function(error, pdf) {
         if (error) {

             //     Sometimes things go wrong, re-trying usually gets the job done
             //     Could not get remoting to repiably not crash on my laptop

              console.log("%s %s", chalk.red("The following error occured:"), chalk.red(error));
          } else {
              console.log("%s %s", chalk.green("Saved PDF file:"), chalk.green(pdf));
          }
      });
    }

    office.close(null, function(error) {
        if (error) {
            console.log("%s: %s", chalk.red("Unable to successfully close the engine!"), error);
        } else {
            console.log("PDF file generation completed: [%s]", chalk.green("OK"));
        }
    });

  });
}
*/

var fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const utils = require("./utils.js");
var JSZip = require('jszip');
var zipdir = require('zip-dir');

/*
* If no output file name was provided the VBO/VBS parent folder will be used for naming
*/
function getZipName(directory)
{
  var zipFileName = "output.zip";

  //check if the path exists
  if (!utils.checkExists(directory))
    return;

  //reading the zip file name - we read the foder and
  fs.readdirSync(directory).forEach(file => {
      if (fs.lstatSync(path.resolve(directory, file)).isDirectory())
        zipFileName=file+".zip";
    });

  zipFileName = path.resolve(directory+"/"+zipFileName);

  return zipFileName;
}


/*
* Create a zip file out of the tag release
*/
function createReleaseZip(directory, zipName, resolve, reject)
{
  //normalize the path
  var filePath = path.resolve(process.cwd(), directory);
  var outputFilePath = filePath;


  console.log("Generating the zip file from the folder:[%s]", directory);
  //check if the path exists
  if (!utils.checkExists(directory))
    return;

  //check if the output path exists
  if (!utils.checkExists(outputFilePath))
    return;

  if (zipName==null || zipName=='')
    zipName = getZipName(directory);

  console.log("Output file: \t\t\t\t[%s]", zipName);
  zipdir(directory, { saveTo: zipName }, function (err, buffer) {
    if (err)
        reject(err);
    else
    {
        console.log(chalk.green("Zip file generated: [OK]"));
        resolve();
    }
});


}

module.exports = {

  createReleaseZip: function(directory, zipName)
  {
    console.log(chalk.green("#############################"));
    console.log(chalk.green("Creating the ZIP file"));
    console.log(chalk.green("#############################"));

    return new Promise(function (resolve, reject) {
      createReleaseZip(directory, zipName, resolve, reject);
    });
  }

}

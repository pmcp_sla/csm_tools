
const chalk = require('chalk');
//Local files
const swag = require('./getSwagger.js');
const Operation = require('./operation.js');
const path = require('path');
const fns = require('./helperFunctions');

var globalServiceName;
var globalTags = new Map(); // Mapping from Tag to description


function parseSwagger (swaggerJson, sName) {
	var swaggerObjectMap = new Map();
	var swaggerOperations = swaggerJson.paths; //array of operations
	var keyPath, keyOperator =0;
	//Process each path
	for (keyPath in swaggerOperations){
		//Process each operation with a path 
		for (keyOperator in swaggerOperations[keyPath]){
				// Create an Operation instance
				if(keyOperator != 'parameters') {
					var newOperation = new Operation(keyOperator, keyPath, swaggerOperations[keyPath][keyOperator], 'swagger', sName, '');
					swaggerObjectMap.set(newOperation.name,  newOperation);
				}	
		}
	}
	return swaggerObjectMap;
}


function getServiceName (swaggerFileName, swaggerJson){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(swaggerFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	//Alternative for serviceName is taken from the base path - "basePath": "/{access}/partyRoleAPI"
	var servicePath = swaggerJson.basePath;
	servicePath = servicePath.replace("/{access}/", "");
	servicePath = servicePath.replace("API", "");
	if(servicePath && servicePath.includes("/v")) {
		//remove version - works up to version 9
		servicePath = servicePath.slice(0, -3);
	}
	if(serviceName.toLowerCase()!==servicePath.toLowerCase()) {
		fns.logInfo("filename differs from basePath so using basePath, " + servicePath + ", as API name");
	}

	if(servicePath) return servicePath;
	else return serviceName
}

function parseTags (swaggerJson){
	// Parse and store the tags from the header
	if( (!(swaggerJson.tags)) || (swaggerJson.tags.length == 0) ) {
		fns.logError('Tags entry not provided or empty');
	}
	else {
		swaggerJson.tags.forEach(function(tag){
                globalTags.set(tag.name, tag.description)
            });
	}

}

function validateFileHeaderInfo (swaggerJson){

	//Get the base path
	var servicePath = swaggerJson.basePath;
	var serviceHost = swaggerJson.host;
	//Get the version number
	var serviceVersion = swaggerJson.info.version;
	fns.logHeading("SWAGGER VALIDATION");
	fns.logHeading(serviceHost);
	fns.logHeading(globalServiceName);
	fns.logHeading(servicePath);
	const HOST = 'api.vodafone.com';
	const SCHEMES = [ 'https' ];

	if(serviceHost != HOST){
		fns.logError('Host not set to: ' + HOST);
	}

	//Check that the primary version number is used unless it is v1
	if((serviceVersion.charAt(0) != servicePath.slice(-1)) && (serviceVersion.charAt(0) != '1') ){
		fns.logError('Service Version ' + serviceVersion.charAt(0) + ' does not match Service path ' + servicePath);
	}

	//Check the schema is https
	if(swaggerJson.schemes[0] != SCHEMES[0]) {
		fns.logError('Schemas not set to: ' + SCHEMES);
	}

	// Parse the tags
	parseTags(swaggerJson);

}


function checkOperationIds(operationMap){
	// Check that the operation Ids are unqiue
	var ids = [];
	var dupicates = [];
	operationMap.forEach(function(op) {ids.push(op.operationId)});
	ids.forEach(function(id) {
		var occurrences = ids.filter(v => v === id).length
		if(occurrences>1)
			fns.logError('Duplicate operation id ' + id + ' found');
	});
}

/**
 * Validate a swagger file.
 * @param {string} swaggerFileName 
 * @param {string} serviceNameParamer  
 */
function validateSwagger (swaggerFileName, serviceNameParameter) {
	
	//Read swagger file
	var swagger = swag.openJsonFile(swaggerFileName);
	var swaggerObjectMap = new Map();

	//set service name
	if(serviceNameParameter){
		globalServiceName = serviceNameParameter;
	}
	else {
		globalServiceName=getServiceName(swaggerFileName, swagger);
	}

	validateFileHeaderInfo(swagger);

	//Parse the Operations
	swaggerObjectMap = parseSwagger(swagger, globalServiceName);

	// Check that the operation ids are unique
	checkOperationIds(swaggerObjectMap);

	//Run some checks on the swagger operations
	swaggerObjectMap.forEach(function(op){ op.validate(globalTags);});

}

function printSummary (swaggerFileName) {
	var swagger = swag.openJsonFile(swaggerFileName);
	var swaggerObjectMap = new Map();
	globalServiceName=getServiceName(swaggerFileName);

	swaggerObjectMap = parseSwagger(swagger, globalServiceName);
	//print the Swagger operators
	console.log(chalk.yellow.bold('Swagger File operations'));
	swaggerObjectMap.forEach(function(op){ op.print();});
}


module.exports = {

  validate: validateSwagger,
  print: printSummary
};


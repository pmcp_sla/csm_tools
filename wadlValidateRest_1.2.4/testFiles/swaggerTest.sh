#!/bin/bash
node ../index.js -o ReferenceSwagger.json 
echo -e "\e[1;34mExpected result - headers missing on GET /partyRole\e[0m"
echo -e "\e[1;34mExpected result - tag missing on GET /monitor\e[0m"
echo -e "\e[1;34mExpected result - tag missing on GET /monitor{id}\e[0m"
node  ../index.js -o CustomerPrivacyProfileVBS.swagger
echo -e "\e[1;34mWarning please review:  Response code 206 is not required ib GET hub/id\e[0m"
node  ../index.js -o ServiceBalanceVBS.swagger
echo -e "\e[1;34mError:  Response code 200 is missing on listener\e[0m"
node ../index.js -o ServiceOrderVBS.swagger
echo -e "\e[1;34mOperation does not return an array for response 200 on Search\e[0m"
echo -e "\e[1;34mError:  Response code 200 is missing on listener\e[0m"
node ../index.js -o CustomerAgreementVBS.swagger
echo -e "\e[1;34mError:  Consumes not set to application/json-patch+json\e[0m"
echo -e "\e[1;34mWarning Response code 206 is not required\e[0m"
node ../index.js -o ProcessPortContactVBS.swagger
echo -e "\e[1;34mNo errors just info on filename\e[0m"


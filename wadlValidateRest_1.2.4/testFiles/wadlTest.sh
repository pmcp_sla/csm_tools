#!/bin/bash
node ../index.js -w CheckCustomerCreditRatingVBS.wadl
echo -e "\e[1;34mExpected result - Content-Range is missing from response 206\e[0m"
echo -e "\e[1;34mExpected result - Parameters must include fields.\e[0m"
node ../index.js -w CommunicationVBS.wadl
echo -e "\e[1;34mWExpected result - Operation path //communication does not start with /communication\e[0m"
echo -e "\e[1;34mExpected result - Parameters fields, sort, limit and count not expected for POST, DELETE, PUT and PATCH.\e[0m"
echo -e "\e[1;34mExpected result - Content-Range is missing from response 206.\e[0m"
echo -e "\e[1;34mExpected result - Mutiple response code errors.\e[0m"
node ../index.js -w CustomerBillVBS.wadl
echo -e "\e[1;34mExpected result - Duplicate operation id get found\e[0m"
echo -e "\e[1;34mExpected result - Parameters fields, sort, limit and count not expected for POST, DELETE, PUT and PATCH.\e[0m"
echo -e "\e[1;34mExpected result - Content-Range is missing from response 206.\e[0m"
echo -e "\e[1;34mExpected result - Mutiple response code errors.\e[0m"
node ../index.js -w CustomerPartyVBS.wadl
echo -e "\e[1;34mExpected result -  id getList should be set to get-list for a Collection\e[0m"
echo -e "\e[1;34mExpected result - wadl:representation supplied not required for 202, 204, 500\e[0m"
echo -e "\e[1;34mExpected result - Content-Range is missing from response 206.\e[0m"
echo -e "\e[1;34mExpected result - Operation path /customer/customerParty does not start with /customerPart\e[0m"
node ../index.js -w ProductOfferingVBS.wadl
echo -e "\e[1;34mNo errors just info on filename\e[0m"


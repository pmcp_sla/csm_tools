#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var ajvValidator = require("./validateAJV.js");
var swaggerValidator = require("./validateSwagger.js");
var genCodeValidator = require("./generateCode.js");
var packageValidator = require("./validatePackage.js");
var configuration = require("./configuration.js");
var swaggerValidatorOperations = require("./validateSwaggerOperations.js")
var wadlValidatorOperations = require("./validateWadlOperations.js")

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('validateAJV' in cmdArgs) //check if we need to perform an AJV validation
{
    ajvValidator.validate(cmdArgs['validateAJV']);
} else if ('validateSwagger' in cmdArgs) //check if we need to perform a swagger validation
{
    swaggerValidator.validate(cmdArgs['validateSwagger']);
} else if ('generateCode' in cmdArgs)
{
    genCodeValidator.validate(cmdArgs['generateCode']);
} else if ('validateAllExamples' in cmdArgs)
{
    ajvValidator.validateAllExamples(cmdArgs['validateAllExamples']);
} else if ('validateExample' in cmdArgs)
{
    ajvValidator.validateExample(cmdArgs['validateExample']);
} else if ('validatePackage' in cmdArgs)
{
    packageValidator.validate(cmdArgs['validatePackage']);
} else if ('configureCodegen' in cmdArgs)
{
    configuration.validate(cmdArgs['configureCodegen']);
} else if ('generateFlatSwagger' in cmdArgs)
{
    swaggerValidator.bundle(cmdArgs['generateFlatSwagger']);
} else if ('validateSwaggerOperations' in cmdArgs)
{
    swaggerValidatorOperations.validate(cmdArgs['validateSwaggerOperations']);
} else if ('validateWadlOperations' in cmdArgs)
{
    wadlValidatorOperations.validate(cmdArgs['validateWadlOperations']);
} else if ('generateAllFlatSwaggers' in cmdArgs)
{
    swaggerValidator.bundleAll(cmdArgs['generateAllFlatSwaggers']);
}
else
{
    cmdObj.printHelp();
}

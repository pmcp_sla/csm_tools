
const chalk = require('chalk');
//Local files
const swag = require('./getSwagger.js');
const wadl = require('./getWadl.js');
const Operation = require('./operation.js');  // Swagger Operation
const path = require('path');
const fns = require('./helperFunctions');

var globalServiceName;
var globalTags = new Map(); // Mapping from Tag to description
var globalSearch = false;


function parseWadl (wadlJson){
	var wadlObjectMap =new Map();
	var wadlOperations = wadlJson.elements[0].elements[2].elements; //array of operations
	var keyPath, keyOperator = 0;
	//Process each Operation
	for (keyPath in wadlOperations){
		var opName = wadlOperations[keyPath].attributes.path;
		var opWadl = wadlOperations[keyPath].elements;

		for (key in opWadl){
		    var name = opWadl[key].name;
		    var id = opWadl[key].id;
            var attributes = opWadl[key].attributes;
            var elements = opWadl[key].elements;

            //Grab the responses
            if (name == 'wadl:method'){
            	var keyEl = 0;
            	var wadlResponses = [];
                for (keyEl in elements){
                    var current = elements[keyEl];
                    if(current.name == 'wadl:response') {
                        //Array.prototype.push.apply(wadlResponses, current.attributes.status.split(' '));
                    }
                    if((opName.endsWith('search')) && (current.name == 'wadl:request')) {
                        checkSearchOperation(opName, attributes.id, current.elements);
                        globalSearch = true;
                    }
                }
            	var newOperation = new Operation(attributes.name, opName, elements, 'wadl',  globalServiceName, attributes.id);
            	wadlObjectMap.set(newOperation.name, newOperation);
            }
        }
    }
    return wadlObjectMap;
}

function checkSearchOperation (opName, opId, requests){
	// Search operation should use query
	fns.logInfo("Initial Validation of: " + chalk.bold('search'));
		// Check the id is a query
	if(opId !== 'query') {
		fns.logError('search operation does not have id set to \"query\": ' + opId);
	}
	//console.log(requests);
	requests.forEach(function(req){
		if(req.name == 'wadl:representation') {
			if(req.attributes && req.attributes.href){
				if((req.attributes.href=='#query.json') || (req.attributes.href=='#query.xml')) {
					//Okay
				}
				else {
					fns.logError('search operation request href expedt to be #query.json or #query.xml but found ": ' + req.attributes.href);
				}
			}
		}
	});
}

function checkVersionAndDes (wadlJson){ 
	var docJson = wadlJson.elements[0].elements[0].elements;
	var versionFound, descFound = 0;
	var item = 0;
	if(wadlJson.elements[0].elements[0] && wadlJson.elements[0].elements[0].name && 
		wadlJson.elements[0].elements[0].name == 'wadl:doc' ) {
		for (item in docJson) {
			if(docJson[item].name && (docJson[item].name == 'tns:description')) {
				descFound = 1;
			}
			if(docJson[item].name && (docJson[item].name == 'tns:version')) {
				versionFound = 1;
			}
		}
		if(versionFound != 1) {
			fns.logError('tns:version number not found');
		}
		if(descFound != 1) {
			fns.logError('tns:description not found');
		}
	}
	else {
			fns.logError('wadl:doc entry not found');
	}

}

function checkGrammars (wadlJson){ 
	var grammarJson = wadlJson.elements[0].elements[1].elements;
	//console.log(grammarJson);
	var faultFound = 0; 
	var metaFound = 0;
	var VBOFound = 0;
	var queryFound= 0;
	var item = 0;
	if(wadlJson.elements[0].elements[1] && wadlJson.elements[0].elements[1].name && 
		wadlJson.elements[0].elements[1].name == 'wadl:grammars' ) {
		for (item in grammarJson) {
			if(grammarJson[item].attributes && grammarJson[item].attributes.href && 
				(grammarJson[item].attributes.href.endsWith('Fault.xsd') )) {
				faultFound = 1;
			}
			if(grammarJson[item].attributes && grammarJson[item].attributes.href && 
				(grammarJson[item].attributes.href.endsWith('Meta.xsd'))) { // Should this be query?
				metaFound = 1;
			}
			if(grammarJson[item].attributes && grammarJson[item].attributes.href && 
				(grammarJson[item].attributes.href.endsWith(globalServiceName + 'VBO.xsd'))) { 
				VBOFound = 1;
			}
			if(grammarJson[item].attributes && grammarJson[item].attributes.href && 
				(grammarJson[item].attributes.href.endsWith('Query.xsd'))) { 
				queryFound = 1;
			}
		}
		if(faultFound != 1) {
			fns.logError('Fault.xsd number not found in wadl:grammars');
		}
		if(metaFound != 1) {
			fns.logError('Meta.xsd not found in wadl:grammars');
		}
		if(VBOFound != 1) {
			fns.logWarn(globalServiceName + 'VBO  not found in wadl:grammars');
		}
		if(queryFound != 1 && globalSearch) {
			fns.logError('Query.xsd not found in wadl:grammars - only needed if search operation used');
		}
	}
	else {
			fns.logError('wadl:grammars entry not found');
	}

}

function getServiceName (wadlFileName, wadlJson){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(wadlFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	
	//Alternative for serviceName is taken from the base path - base="http://api.vodafone.com/{access}/salesOrderAPI/v3/"
	var servicePath = wadlJson.elements[0].elements[2].attributes.base;

	//Check base path is correct
	if(servicePath.startsWith("http://api.vodafone.com/{access}/")){
		servicePath = servicePath.replace("http://api.vodafone.com/{access}/", "");
		servicePath = servicePath.replace("API", "");
		if(servicePath && servicePath.endsWith('/')) servicePath = servicePath.slice(0, -1);
		if(servicePath && servicePath.includes("/v")) {
			//remove version - works up to version 9
			servicePath = servicePath.slice(0, -3);
		}
		if(servicePath)  servicePath = servicePath.charAt(0).toUpperCase() + servicePath.slice(1);
		if(serviceName.toLowerCase()!==servicePath.toLowerCase()) {
			fns.logInfo("filename differs from basePath so using basePath, " + servicePath + ", as API name");
		}
	}
	else {
		fns.logWarn("base does not start with http://api.vodafone.com/{access}/");
		servicePath = undefined;
	}

	if(servicePath) return servicePath;
	else return serviceName
}


function checkOperationIds(operationMap){
	// Check that the operation Ids are unqiue
	var ids = [];
	var dupicates = [];
	operationMap.forEach(function(op) {ids.push(op.operationId)});
	ids.forEach(function(id) {
		var occurrences = ids.filter(v => v === id).length
		if(occurrences>1)
			fns.logError('Duplicate operation id ' + id + ' found');
	});
}

function checkRepresentatons (wadl){
	// Check that the wadl file has the correct list of representations
	var representations = wadl.elements[0].elements;
	var representationIds = []
	representations.forEach(function(element){
		//Check each elemt to see if it is a representation
		if(element.name && element.name == 'wadl:representation' && element.attributes){
			var attributes = element.attributes;
			if(attributes.id){
				representationIds.push(attributes.id);
			}
		}
	});

	if (!representationIds.includes('faultObject.json')) {
		fns.logError('wadl:representation not found for faultObject.json');
	}
	if (!representationIds.includes('faultObject.xml')) {
		fns.logError('wadl:representation not found for faultObject.xml');
	}
	if (!representationIds.includes('patchObject.json')) {
		fns.logError('wadl:representation not found for patchObject.json');
	}
	if (!representationIds.includes('patchObject.xml')) {
		fns.logError('wadl:representation not found for patchObject.xml');
	}
	if (!representationIds.includes(globalServiceName.toLowerCase() + '.json')) {
		fns.logError('wadl:representation not found for ' + globalServiceName.toLowerCase() + '.json');
	}
	if (!representationIds.includes(globalServiceName.toLowerCase() + '.xml')) {
		fns.logError('wadl:representation not found for ' + globalServiceName.toLowerCase() + '.json');
	}
	if (!representationIds.includes('query.json') && globalSearch) {
		fns.logError('wadl:representation not found for query.json');
	}
	if (!representationIds.includes('query.xml') && globalSearch) {
		fns.logError('wadl:representation not found for query.xml');
	}
	
}




/**
 * Validate a wadl file.
 * @param {string} wadlFileName 
 * @param {string} serviceNameParamer  
 */
function validateWadl (wadlFileName, serviceNameParameter) {

	fns.logHeading("WADL VALIDATION");

	//Read wadl file
	var wadlXML = wadl.openWadlFile(wadlFileName);

	//set service name
	if(serviceNameParameter){
		globalServiceName = serviceNameParameter;
	}
	else {
		globalServiceName=getServiceName(wadlFileName, wadlXML);
	}
	fns.logHeading(globalServiceName);

	//Check the a version number exists - 
	checkVersionAndDes(wadlXML);

	//validate the operations
	wadlObjectMap = parseWadl(wadlXML);

	// Check the Grammar
	checkGrammars(wadlXML);

	// Check that the operation ids are unique
	checkOperationIds(wadlObjectMap);

	//Run some checks on the swagger operations
	wadlObjectMap.forEach(function(op){ 
		op.validate(globalTags);});

	checkRepresentatons(wadlXML);
}



module.exports = {

  validate: validateWadl,
};


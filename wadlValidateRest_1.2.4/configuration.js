var fs = require('fs');

var options = {
    codegenExec: ''
  };

function saveConfig(filePath){
  options.codegenExec = filePath;
  var data = JSON.stringify(options);
   fs.writeFile(process.env.APPDATA+'/npm/node_modules/validationrest/config.json', data, function (err) {
     if (err) {
       console.log('There has been an error saving your configuration data.');
       console.log(err.message);
       return;
     }
     console.log('Configuration saved successfully.');
   });
}



var base = process.cwd();

function config(filePath)
{
  saveConfig(filePath);
}


module.exports = {

  validate: function(filePath)
  {
    config(filePath);
  }

};

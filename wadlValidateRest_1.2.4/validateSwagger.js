const path = require('path');
const SwaggerParser = require('swagger-parser');
var chalk = require('chalk');
var fs = require('fs');

const validateOptions = {
    allow: {
        json: false, // Don't allow JSON files
        empty: true      // Don't allow empty files
    },
    $refs: {
        internal: false   // Don't dereference internal $refs, only external
    },
    cache: {
        fs: 1, // Cache local files for 1 second
        http: 600         // Cache http URLs for 10 minutes
    },
    validate: {
        spec: true, // Don't validate against the Swagger 2.0 spec
        schema: true
    },
    dereference: {
        circular: false                 // Don't allow circular $refs
    }
};

/**
 * Perform a validation of the swagger file but using future&promise (async)
 *
 * @param {type} swaggerFile    - input file location
 * @return
 */
function validateProm(swaggerFile)
{
    var apiObj = null;

    var file = path.resolve(process.cwd(), swaggerFile);
    rootFolder = path.dirname(file);

    console.log("Validating the swagger file: %s", swaggerFile);

    var promise = new Promise(function (resolve, reject) {
        SwaggerParser.validate(file, validateOptions)
                .then(function (api) {
                    resolve(api);
                })
                .catch(function (err) {
                    console.error(err);
                    console.log(chalk.red("------------->: [!!!NOT VALID!!! - Please resolve the above errors ]"));
                    reject(Error(err));
                });
    });

    return promise;
}

/**
 * Perform a validation of swagger file
 *
 * @param {type} swaggerFile
 * @return {undefined}
 */
function validate(swaggerFile)
{
    var allValid = true;

    var file = path.resolve(process.cwd(), swaggerFile);
    rootFolder = path.dirname(file);

    console.log("Validating the swagger file: %s", swaggerFile);

    SwaggerParser.validate(file, validateOptions
            )
            .then(function (api) {
                console.log("API name: %s, Version: %s: [%s]", api.info.title, api.info.version, chalk.green("VALID"));
            })
            .catch(function (err) {
                console.error(err);
                allValid = false;
                console.log(chalk.red("------------->: [!!!NOT VALID!!! - Please resolve the above errors ]"));
            });
}

/**
 *
 *
 */
function bundleAll(startPath, filter, level){

    //console.log('Starting from('+level+') dir '+startPath+'/');

    if (!fs.existsSync(startPath)){
        console.log("No directory with path: ",startPath);
        return;
    }

    var files=fs.readdirSync(startPath);
    for(var i=0;i<files.length;i++){
        var filename=path.join(startPath,files[i]);

        if (filename.indexOf('Trunk')>=0)
        {
          var stat = fs.lstatSync(filename);
          if (stat.isDirectory()){
              bundleAll(filename,filter, ++level); //recurse
          }
          else if (filename.indexOf(filter)>=0) {
              //generate the flat swagger file
              var baseName = path.basename(filename, path.extname(filename));
              var destinationFile = baseName + ".flat.swagger";
              bundle(filename, destinationFile);
          };
        }
        else
        {
          //we increase level
          var stat = fs.lstatSync(filename);
          if (stat.isDirectory()){
              bundleAll(filename,filter, ++level); //recurse
          }
        }
    };
};


/**
 * Create a swagger flat file
 *
 * @param {type} swaggerFile    - swagger file with external references
 * @param {type} outputFile     - flat swagger file
 * @return
 */
function bundle(swaggerFile, outputFile)
{
  /*Hardcoded Functionality - start */
   var folderPath = path.resolve(process.cwd(), swaggerFile);



   /* Hardcoded Functionality - end */
   // Usual functionality - START
    var allValid = true;
    var file = path.resolve(process.cwd(), swaggerFile);
    rootFolder = path.dirname(file);

    console.log("%s %s", chalk.yellow("Creating the swagger flatten file for:"), chalk.green(swaggerFile));
    validateOptions.$refs.internal = false;
    validateOptions.dereference.circular = false;

    SwaggerParser.bundle(file, validateOptions)
            .then(function (api) {
                console.log(chalk.yellow("File successfully generated in memory"));
                var json = JSON.stringify(api, null, 4);
                //fs.writeFile(outputFile, json);
                fs.writeFile(outputFile, json, function (err) {
                    if (err)
                        return console.log("%s %s", chalk.red("Unable to save the file:"), outputFile);
                    else
                        return console.log("%s %s", chalk.yellow("File successfully saved to disk:"), chalk.green(outputFile));
                });
            })
            .catch(function (err)
            {
                console.error(err);
                console.log(chalk.red("Unable to flatten the file, looks like the swagger file is invalid"));
            });
      // Usual functionality - END

}

module.exports = {
    validate: function (swaggerFile) {
        return validate(swaggerFile);
    },
    validatePromise: function (swaggerFile)
    {
        return validateProm(swaggerFile);
    },
    bundle: function (params)
    {
        return bundle(params[0], params[1]);
    },
    bundleAll: function (rootFolder)
    {
        return bundleAll(rootFolder,'.swagger', 0);
    }
};



"use strict";
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
var xml2js = require('xml2js'); // Node module to parse XML schema into json
var $RefParser = require('json-schema-ref-parser'); // Node module which de-referecnes a json schema to one single schema

var globalServiceName; 
var globalWriteStream; // Output file



function removeVBO(name) {
	// REmoved VBO from end of a name
	var result = name;
	if(name.toLowerCase().endsWith('vbo')){
		result = name.slice(0, -3);
	}
	return result;
}


function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}



function createFile (filename, outputSchema) {
	// Write the file with the output schema
	let out = JSON.stringify(outputSchema, null, 4);
	fs.writeFile(filename, out, { flag: 'wx' }, 
		function(err) { 
			if(err) {
					// Error
		        fns.logError("Error: " + filename);
		        console.log(err);
        	}
		});
}

/**
 * Generate Swagger Impl  from swagger file
 * @param {string} schemaFileName 
 */
function generateFlatSwagger (swaggerFileName) {
	
	fns.logHeading("Flat Swagger  Generation for ", swaggerFileName);
	//Read swagger  file
	var jsonSchema = utils.loadFile(swaggerFileName);

	//set service name
	globalServiceName=getServiceName(swaggerFileName);


	fns.logHeading(globalServiceName);

	//Create an output file
	var outputFileName = swaggerFileName.split('.').slice(0, -1).join('.') + '.flatten.swagger'; //remove extension
	fns.logHeading("Generating: " + outputFileName);

    // Generate based on a dereferenced schema
	$RefParser.dereference(swaggerFileName)
    .then(function(schemaResult) {
        // Success
        createFile(outputFileName, schemaResult);
        fns.logHeading("Success: file " +  outputFileName + " generated.");
    })
    .catch(function(err) {
        // Error
        fns.logError("Error: ");
        console.log(err);
    });
}

module.exports = {

  generate: generateFlatSwagger

};



"use strict";
const chalk = require('chalk');
const fns = require('./helperFunctions');
const path = require('path');
const REFERENCE = 'reference';
const COLLECTION = 'COLLECTION';
const RESOURCE_TASK = 'RESOURCE_TASK';
const RESOURCE_ENTITY = 'RESOURCE_ENTITY';

/** Class representing an Operation. */
class Operation {

    /**
     * Create an operation.
     * @param {string} method - The method name, GET, POST, PUT, PATCH or DELETE.
     * @param {string} scope - The operation path
     * @param {json Object} json - The json object.
     * @param {string} type - The type of json obejct swagger or wadl.
     * @param {string} sName - The name of the service
     */
    constructor(method, scope, json, type, sName) {
    	if (type == 'swagger') {
            this.method = method.toUpperCase();         // The method name, GET, POST, PUT, PATCH or DELETE.
            this.scope = scope;                         // The operation path such as /listener
            this.name = this.method + ' ' + this.scope; // Overall operation name which is a combo of the method and scope
            this.params = new Map();                    // The operation parameters
            this.responses = new Map();                 // The operation responses
            this.operationId = json.operationId;        // The operation Id
            this.serviceName = sName;                   // The name of the service - comes from the swagger file name or passed in as a param
            this.resourceType = "";                     // Defines an operation to be on a Collection, Resource Task or a Resource Entity
            this.tags = "";                             // Tags associated with the operation
            this.setResourceType();
    		this.createOperationFromSwagger(json, sName);
    	}
    	else {
    		fns.logFatal("Unable to create operation" + this.name);
    	}
    }



    /**
     * Create an operation from Swagger.
     * @param {json Object} json - The json object.
     * @param {sname} - the service name
     * @return {operation object} operation
     */
    createOperationFromSwagger (jsonObj, sname){
        // Move params to a list of names such as ID, string, type, string, ...
        var params = jsonObj.parameters;
        var key = 0;
        for (key in params){
            if (typeof params[key].name === 'string' || params[key].name instanceof String) {
            	//Ignore these params
                if (['default', 'undefined', 'query', 'X-Notify-Business-EventCode', 'X-Notify-EventCode', 'hub', '$ref'].indexOf(params[key].name) == -1 ) {
                    if (params[key].type)
                       this.params.set(params[key].name, params[key].type);
                }
                else if((params[key].name == 'query') && (params[key].schema) && (params[key].schema.$ref)){
                    //Push a reference on for the param
                    this.params.set('query', params[key].schema.$ref);
                }
            }
            else if(params[key].$ref){
                this.params.set(params[key].$ref, params[key].$ref);
            }
        }
        // Move response to a map from number to properties
        var responses = jsonObj.responses;
        var key = 0;
        for (key in responses){
            if (['default', 'undefined', 'query'].indexOf(key) == -1 ) {
                this.responses.set(key, responses[key]);
            }
        }
        // Read the tags
        this.tags = jsonObj.tags;
        if ( (!(this.tags)) || this.tags.length == 0) {
            fns.logError('Tags entry not provided or empty');
        }

        //Read the input type
        this.consumes = jsonObj.consumes;

        //Read the output type
        this.produces = jsonObj.produces;
    }

    /**
     * Print a summary of the operation
     */
    print() {
        console.log('\nOperation:', chalk.bold(this.name));
        console.log('\toperationId:', chalk.bold(this.operationId));
        console.log('\tmethod: ', this.method);
        console.log('\tscope: ',this.scope);
        console.log('\tservice name: ',this.serviceName);
        console.log('\tresourceType: ',this.resourceType );
        console.log('\ttags: ',this.tags );
        if (this.params.size==0)
        {
            console.log("Parameters: []");
        }
        else {
            console.log('Parameters');
            this.params.forEach(function(value, key){console.log('\t', key, ':', value);});
        }
        if (this.responses.size==0)
        {
            console.log("Responses: []");
        }
        else {
            console.log('Responses');
            this.responses.forEach(function(value, key){
                console.log('\t', key, ':', value);
            });
        }
    }

    setResourceType () {
        //set the resource type to be a collection or a resource task or resource entity
        if((this.scope.toLowerCase() == '/hub') || (this.scope.toLowerCase() == '/home')) {
            this.resourceType=RESOURCE_ENTITY;
        }
        else if (this.scope.toLowerCase() == '/' + this.serviceName.toLowerCase() ) { // DEFAULT for /{collection}
                this.resourceType=COLLECTION;
        }
        else if (this.scope.toLowerCase().endsWith('id}')) {
            this.resourceType=RESOURCE_ENTITY;
        }
        else {
            this.resourceType = RESOURCE_TASK;
        }
    }

    checkErrorResponseCodes (responseList, responses){
    	//Check that the responses occur in the responseList
        responseList.forEach(function(code){
                if (!(responses.has(code)) ) {
                    fns.logError("Response code " + code + " is missing");
                }
            });
    }


    checkShouldNotHaveResponseCodes (responseList, responses){
    	//Check that the responses do NOT occur in the responseList
        responseList.forEach(function(code){
                if (responses.has(code)) {
                    fns.logWarn("Response code " + code + " is not required");
                }
            });
    }

    checkResponseIsNotAnArray(responseList, responses){
    	// Check that the response type is NOT an array
        if ((this.method != 'GET') && (this.method!='PATCH')){
            responseList.forEach(function(code){
                if((responses.has(code)) && (responses.get(code).schema) && (responses.get(code).schema.type == 'array')) {
                    // Array only allowed on GET or PATCH
                        fns.logError('Response type array not expected on code ' + code);
                }
            });
        }
    }


    checkForIllegalHeaders(responses) {
        // Only a few headers are allowed - return an error if any others are used; headers only allowed on 200 and 206!
        //But also seen some headers on 202 (Link) so just raising a warning for now
        responses.forEach(function(value, resp, mapObj) {
            if(value.headers){
                if((resp=='200') || (resp == '206')) {
                    for(var key in value.headers ) {
                        if (["X-Rate-Limit-Limit", "X-Rate-Limit-Remaining", "X-Rate-Limit-Reset", "X-Total-Count", "Content-Range", "Link"
                            ].indexOf(key) == -1 ) {
                                fns.logError('Header  ' + key + ' is not allowed for response ' + resp);
                        }
                    }
                }
                else if (value.headers) {
                     fns.logWarn('Check if headers required for ' + resp);
                }
            }
        });
    }

     checkForIllegalRepresentations(responses) {
        // Only response codes 200, 206 and error responses should have schemas
            responses.forEach(function(value, resp, mapObj) {
                if((responses.has(resp)) && (responses.get(resp).schema)) {
                    if((resp=='200') || (resp == '206') || (resp == '201') || ((parseInt(resp) > 399) && (parseInt(resp) <= 500)) )  {
                        // Headers allowed
                    }
                    else  {
                                fns.logError('schema supplied not required for ' + resp);
                    }
                }
            });
    }

    checkForFaultSchemas(responses){
        // Error codes between 400 and 500 should have a fault schema
        responses.forEach(function(value, resp, mapObj) {
            if((parseInt(resp) > 399) && (parseInt(resp) <= 500)) {
                // Check that a schema exists and references fault
                if(!value.schema) {
                        fns.logError('schema not supplied for ' + resp);
                }
                else {
                    if((value.schema.$ref) && (value.schema.$ref.toLowerCase().includes("fault")) ) {
                        // Ignore all good
                    }
                    else {
                            fns.logError('fault schema not supplied for ' + resp);
                    }
                }
            }
        });
    }

    checkExpectedHeaders(responseProperties, code){
        if(responseProperties.headers) {
            if(code == '200') {
                //Check that the correct properties are included
                if (!(responseProperties.headers["X-Rate-Limit-Limit"])) {
                    fns.logError('Header X-Rate-Limit-Limit is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Remaining"])) {
                    fns.logError('Header X-Rate-Limit-Remaining is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Reset"])) {
                    fns.logError('Header X-Rate-Limit-Reset is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Total-Count"])) {
                    fns.logError('Header X-Total-Count is missing from response ' + code);
                }
            }
            else if (code == '206') {
                //Check that the correct properties are included
                if (!(responseProperties.headers["X-Rate-Limit-Limit"])) {
                    fns.logError('Header X-Rate-Limit-Limit is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Remaining"])) {
                    fns.logError('Header X-Rate-Limit-Remaining is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Rate-Limit-Reset"])) {
                    fns.logError('Header X-Rate-Limit-Reset is missing from response ' + code);
                }
                if (!(responseProperties.headers["X-Total-Count"])) {
                    fns.logError('Header X-Total-Count is missing from response ' + code);
                }
                if (!(responseProperties.headers["Content-Range"])) {
                    fns.logError('Header Content-Range is missing from response ' + code);
                }
            }
            else
                    fns.logError('Headers for response code ' + code + ' not expected');
        }
        else {
            fns.logError('No headers for response code ' + code);
        }

    }


    checkNotExpectedHeaders(responseProperties, code){
        //No pagination should be included
        if(responseProperties.headers) {
                //Check that the correct properties are NOT included
                if ((responseProperties.headers["X-Rate-Limit-Limit"])) {
                    fns.logError('Header X-Rate-Limit-Limit is not required for response ' + code);
                }
                if ((responseProperties.headers["X-Rate-Limit-Remaining"])) {
                    fns.logError('Header X-Rate-Limit-Remaining is not required for response ' + code);
                }
                if ((responseProperties.headers["X-Rate-Limit-Reset"])) {
                    fns.logError('Header X-Rate-Limit-Reset is not required for response ' + code);
                }
                if ((responseProperties.headers["X-Total-Count"])) {
                    fns.logError('Header X-Total-Count is not required for response ' + code);
                 }
        }
    }


    checkResponseIsArray(responseProperties, code){
        //Check that a search, query or list returns an array
        if((this.operationId.toLowerCase().endsWith('search')) || (this.operationId.toLowerCase().endsWith('list')) ||
            (this.operationId.toLowerCase().endsWith('query')) ){
            if((responseProperties.schema) && (responseProperties.schema.type == 'array')) {
                //Check that the items exists
                if((responseProperties.schema) && (responseProperties.schema.items)) {
                    if(responseProperties.schema.items.$ref){
                        //Get the service name
                        if(responseProperties.schema.items.$ref.toLowerCase() != '#/definitions/' + this.serviceName.toLowerCase() + 'vbo') {
                            //fns.logWarn('Operation does not define "$ref" as #/definitions/' + this.serviceName.toLowerCase() + 'VBO' + '  for response ' + code)
                        }
                    }
                    else {
                        // Not items defn for the array
                        fns.logError('Operation does not define "$ref" for an array for response ' + code)
                    }
                }
                else {
                    // Not items defn for the array
                    fns.logError('Operation does not define "items" for an array for response ' + code)
                }
            }
            else {
                //Type not specified or not array
                fns.logError('Operation does not return an array for response ' + code)
            }
        }
    }

    reviewResponseHeaders(responseProperties, code){
        // Check the response headers include pagination
        if (this.scope.endsWith('/search') ) {
                this.checkExpectedHeaders(responseProperties, code);
        }
        else if ( (this.scope.endsWith('/report')) || (this.scope.endsWith('/notification')) || (this.scope.endsWith('/listener')) ||
                   (this.scope.endsWith('/notifications')) ||
                   (this.scope.endsWith('/home')) ||   (this.scope.toLowerCase().endsWith('/hub/{id}')) || (this.scope.toLowerCase().endsWith('id}') ) ||
                   (this.scope.toLowerCase().endsWith('/hub')) ) {
            if(code=='206') {
                if ((responseProperties.headers) && (responseProperties.headers["Content-Range"])) {
                    // okay
                }
                else {
                    fns.logError('Header Content-Range is missing from response ' + code);
                }
            }
        }
        else if(this.scope.endsWith('/monitor')) {
            //Okay
        }
        else {
            //collection
             this.checkExpectedHeaders(responseProperties, code);
        }
    }

    confirmResponseReturnType(responseProperties, code) {
        //Check that the type of a 200 or 206 response is made up of method + VBO + a name within the scope
        // Basically checking "$ref": "#/definitions/customerPrivacyProfileVBORelatedCustomerPrivacyProfile"
        // ajv check that the main type exists so we are just checking for a match between RelatedCustomerPrivacyProfile
        // and the scope of the method - would pick up cut and paste errors
        if ((code == '200') || (code == '206')){
            if(responseProperties.schema) {
                var type = ''; //stores type of definition
                if((responseProperties.schema) && (responseProperties.schema.$ref)) {
                    type = responseProperties.schema.$ref;
                }
                else if ((responseProperties.schema) && (responseProperties.schema.items) && (responseProperties.schema.items.$ref)){
                    type = responseProperties.schema.items.$ref;
                }
                var originalType = type;
                // Strip off unneeded characters such as vbo and check it is not set to the service name
                type = type.toLowerCase().replace('#/definitions/', '');
                if(type == this.serviceName.toLowerCase() + 'vbo') {
                    type = type.replace('vbo', '');
                }
                else {
                        type = type.replace(this.serviceName.toLowerCase() + 'vbo', '');
                }
                var typeList = this.scope.split('/');
                // Remove the first element if it is a ''
                if(typeList[0] == '') {
                    typeList.shift();
                }
                typeList = typeList.map(function(x){ return x.toLowerCase() });

                // Check it the type appears in the list taken from the scope but ignore some cases
                if(this.scope.endsWith('/home'))  {
                    if (type != '_links') {
                        fns.logWarn('Return type for /home  for code ' + code + ' should be _links');
                    }
                }
                else if (typeList.indexOf(type) > -1 )  {
                    // Okay type found in scope
                }
                else {
                    fns.logWarn('Check if type ' + originalType + ' for code ' + code + ' is correct');
                }
            }
            else {
                    fns.logWarn('No return type for code ' + code + ', check if this is correct');
            }
        }
    }


    validateResponseCodes () {
        // Check the response codes exist
        if (this.method=="GET") {
            if ((this.resourceType==COLLECTION) || (this.scope.endsWith('/monitor')) ) {
                this.checkErrorResponseCodes(['200', '204', '206', '400', '404', '405', '500'], this.responses);
            }
            else if (this.scope == '/hub')  {
                this.checkErrorResponseCodes(['200', '204', '206', '400', '404', '405', '500'], this.responses);
            }
            else if (this.resourceType==RESOURCE_ENTITY) {
                if (this.scope =='/hub/{id}') {
                    this.checkErrorResponseCodes(['200', '400', '404', '405', '500'], this.responses);
                    this.checkShouldNotHaveResponseCodes(['206'], this.responses);
                }
                else
                  this.checkErrorResponseCodes(['200', '206', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['204'], this.responses);
            }
            else if (this.resourceType==RESOURCE_TASK) {
                this.checkErrorResponseCodes(['200', '206', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['204'], this.responses);
                fns.logWarn('Resource Task should not use GET');
            }

            // Check body of response codes for types
            if((this.responses.has('200')) && (!(this.scope.endsWith('/report'))) ) {
                    this.checkResponseIsArray(this.responses.get('200'), '200');
                    this.reviewResponseHeaders(this.responses.get('200'), '200');
            }
            if((this.responses.has('206')) && (!(this.scope.endsWith('/report'))) ) {
                    this.checkResponseIsArray(this.responses.get('206'), '206');
                    this.reviewResponseHeaders(this.responses.get('206'), '206');
            }
        }
        if (this.method=="POST"){
            if((this.scope.endsWith('/search')) || (this.scope.endsWith('/report')) ) {
                this.checkErrorResponseCodes(['200', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['201', '202'], this.responses);
            }
            else if(this.scope.endsWith('/monitor')) {
                this.checkErrorResponseCodes(['201', '202', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['204'], this.responses);
            }
            else if(this.scope.endsWith('/listener')) {
                this.checkErrorResponseCodes(['200', '201', '202', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['204'], this.responses);
            }
            else if(this.scope.endsWith('/hub')) {
                this.checkErrorResponseCodes(['200', '201', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['202', '204'], this.responses);
            }
            else if((this.scope.endsWith('/notification')) || (this.scope.endsWith('/notifications')) ) {
                this.checkErrorResponseCodes(['201', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['200', '202', '204', '206'], this.responses);
                //Check the response text
                if((this.responses.has('201')) && (this.responses.get('201').description != "201, Created.") ) {
                    fns.logError('Response body for 201 description on /notification should be "201, Created."');
                }
            }
            else if ((this.resourceType==COLLECTION) ) {
                this.checkErrorResponseCodes(['200', '201', '202', '400', '404', '405', '500'], this.responses);
            }
            else if (this.resourceType==RESOURCE_ENTITY) {
                fns.logWarn('Resource Entity should not use POST');
            }
            else if (this.resourceType==RESOURCE_TASK) {
                this.checkErrorResponseCodes(['200', '201', '202', '204', '400', '404', '405', '500'], this.responses);
            }
            // Check that array is  used for search and for nothing else
            if (this.scope.endsWith('/search')) {
            	if(this.responses.has('200'))  {
	                    this.checkResponseIsArray(this.responses.get('200'), '200');
                        this.checkExpectedHeaders(this.responses.get('200'), '200');
	            }
	            if(this.responses.has('206')) {
	                    this.checkResponseIsArray(this.responses.get('206'), '206');
	            }
            }
            else {
            	this.checkResponseIsNotAnArray(['200', '206'], this.responses);
            }

            // Check pagination is not used
            if( (this.responses.has('200') )  && (!(this.scope.endsWith('/search'))) )
                    this.checkNotExpectedHeaders(this.responses.get('200'), '200');
            if( (this.responses.has('206') )  && (!(this.scope.endsWith('/search'))) )
                    this.checkNotExpectedHeaders(this.responses.get('206'), '206');
        }
        // Check the response codes for a  PUT
        if  (this.method=="PUT") {
            if (this.resourceType==RESOURCE_ENTITY)  {
                this.checkErrorResponseCodes(['200', '202', '204', '400', '404', '405', '500'], this.responses);
            }
            else if (this.resourceType==COLLECTION) {
                fns.logWarn('Collection should not use PUT');
            }
            else if (this.resourceType==RESOURCE_TASK) {
                fns.logWarn('Resource Task should not use PUT');
            }
            //Check response headers
            this.checkResponseIsNotAnArray(['200'], this.responses);
            if( (this.responses.has('200') )   )
                    this.checkNotExpectedHeaders(this.responses.get('200'), '200');
        }
        // Check the response codes for a PATCH
        if  (this.method=="PATCH") {
            if ((this.resourceType==COLLECTION) || (this.resourceType==RESOURCE_ENTITY) ) {
                this.checkErrorResponseCodes(['200', '202', '204', '400', '404', '405', '409', '412','500'], this.responses);
            }
            else if (this.resourceType==RESOURCE_TASK) {
                fns.logWarn('Resource Task should not use PATCH');
            }
            // Check pagination is not used
            if(this.responses.has('200') ) {
                    this.checkNotExpectedHeaders(this.responses.get('200'), '200');
            }
            if(this.responses.has('206') ) {
                    this.checkResponseIsArray(this.responses.get('206'), '206');
                    this.reviewResponseHeaders(this.responses.get('206'), '206');
            }
            //Check response headers
            this.checkResponseIsNotAnArray(['200'], this.responses);
            if( (this.responses.has('200') )   )
                    this.checkNotExpectedHeaders(this.responses.get('200'), '200');
        }
        // Check the response codes for a delete
        if  (this.method=="DELETE") {
            if (this.resourceType==RESOURCE_ENTITY)  {
                this.checkErrorResponseCodes(['200', '202', '204', '400', '404', '405', '500'], this.responses);
                this.checkShouldNotHaveResponseCodes(['409'], this.responses);
            }
            else if (this.resourceType==COLLECTION) {
                fns.logWarn('Collection should not use DELETE');
            }
            else if (this.resourceType==RESOURCE_TASK) {
                fns.logWarn('Resource Task should not use DELETE');
            }
            //Check response headers
            this.checkResponseIsNotAnArray(['200'], this.responses);
            if( (this.responses.has('200') )   )
                    this.checkNotExpectedHeaders(this.responses.get('200'), '200');
        }

        //Check type of return in response code
        if(this.responses.has('200') )
                this.confirmResponseReturnType(this.responses.get('200'), '200');
        if(this.responses.has('206') )
                this.confirmResponseReturnType(this.responses.get('206'), '206');

        //Check response text for 404 - remove extra spaces
        if((this.responses.has('404')) && (this.responses.get('404').description.replace(/\s+/g,' ').trim() != "404, Not Found.") ) {
            fns.logError('Response body for 404 description should be "404, Not Found."');
        }

        //Validate any response headers
        this.checkForIllegalHeaders(this.responses);
        this.checkForFaultSchemas(this.responses);

     }

     paramsHasReference(name){
        var foundFields = 0; //false
        //Need to traverse all of map to look at each REFERENCE
        this.params.forEach(function(key, value){
            if((key == REFERENCE) && (value == name) ){
                foundFields == 1;
            }
        });
        return foundFields;
     }

     hasStandardParam(param, parameterList){
            //Add #/parameters/
        param = '#/parameters/' + param;
        return this.params.get(param);
     }

     validateParams(){
        // Parameters  required for GET!
        if(this.method == "GET") {
            // The following operations should have no params: /home, /seach, /hub/{id}, /report, /notification /listener
            if( (this.scope.endsWith('/home')) || (this.scope.endsWith('/search')) || (this.scope.toLowerCase().endsWith('/hub/{id}')) ||
                (this.scope.endsWith('/report')) || (this.scope.endsWith('/notification')) || (this.scope.endsWith('/listener')) ||
                (this.scope.endsWith('/notifications')) ) {
                    //Params would be empty
                if(this.params.length > 0){
                    fns.logError('Empty parameters expected."');
                }
             }
             // The following operations should have limit, count and sort
             else if( (this.scope.endsWith('/hub'))  ) {
                if( (this.hasStandardParam('limit')) && (this.hasStandardParam('count')) &&
                    (this.hasStandardParam('sort'))) {
                    //okay but should not include fields
                    if(this.hasStandardParam('fields')) {
                        fns.logError('Parameters must not include fields.');
                    }
                }
                else {
                    fns.logError('Parameters must include limit, count and sort.');
                }
            }
             // The following operations should have fields, limit, count and sort
            else if( (this.scope.endsWith('/monitor'))  ) {
                if( (this.hasStandardParam('fields')) &&  (this.hasStandardParam('limit')) &&
                    (this.hasStandardParam('count')) &&  (this.hasStandardParam('sort')) ) {
                    //okay
                }
                else {
                    fns.logError('Parameters must include fields, limit, count and sort.');
                }
            }
             // The following operations should have fields
             else if( (this.scope.toLowerCase().endsWith('id}')) ) {
                if(this.hasStandardParam('fields')) {
                    //okay
                }
                else {
                    fns.logError('Parameters must include fields.');
                }
            }
            // The following assumed to be a collection and should have fields, limit, count and sort
            else if (this.scope.toLowerCase() == '/' + this.serviceName.toLowerCase() ) { // DEFAULT for /{collection}
                if( (this.hasStandardParam('fields')) &&  (this.hasStandardParam('limit')) &&
                    (this.hasStandardParam('count')) &&  (this.hasStandardParam('sort')) ) {
                    //okay
                }
                else {
                    fns.logError('Parameters must include fields, limit, count and sort.');
                }
            }
        }
        else {
            if((this.params.size > 0) && (!(this.scope.endsWith('/search'))) ){
                    fns.logWarn('Parameters fields, sort, limit and count not expected for POST, DELETE, PUT and PATCH. Found:');
                    this.params.forEach(function(value, key){console.log('\t\t' ,  key);});
                }
        }
     }


    validateTags(headerTags) {
        // Check that the operation tags occur in the header tags
        if(this.tags) {
            this.tags.forEach(function(tag){
                    if(!(headerTags.has(tag))) {
                        fns.logError('Tag ' + tag + ' not found in file header list of tags');
                    }
                });
        }
        else {
            fns.logError('Header tags not defined');
        }
     }

    validateInputType() {
        // Check that the operation consumes the correct type for a patch - 'application/json-patch+json'
        if(this.method== 'PATCH') {
            if((this.consumes) && (this.consumes.indexOf('application/json-patch+json') >= 0)) {
                // Okay
            }
            else {
                fns.logError('Consumes not set to application/json-patch+json');
            }
        }
     }

    validateProduceType() {
        // Check that the operation produce the correct type if used - 'application/json'
        // Should this check POST and PATCH only?
        if((this.produces) && (this.produces.length > 0)) {
            if(this.produces.indexOf('application/json') >= 0) {
                // Okay
            }
            else {
                fns.logError('Produces not set to application/json');
            }
        }
     }


     validateMethodId (){ //wadl only!
        if(this.method == 'GET') {
            if ((this.resourceType == COLLECTION) && (this.operationId != 'get-list') ){
                fns.logWarn('id ' + this.operationId + ' should be set to get-list for a Collection');
            }
            else if ((this.resourceType == RESOURCE_ENTITY) && (this.operationId == 'get') ){
                fns.logWarn('id ' + this.operationId + ' should be a unique id');
            }

        }
        else if(this.method == 'PUT') {
            if ((this.resourceType == RESOURCE_ENTITY) && (this.operationId != 'update-replace') && (this.operationId != 'update-partial') ){
                fns.logWarn('id ' + this.operationId + ' should be set to update-replace or update-partial for a Resource Entity');
            }

        }

     }


    /**
     * Validate an operation for common issues
     */
     validate(headerTags){
        fns.logInfo("Validating: " + chalk.bold(this.name) + " Resource type: " + this.resourceType);

         this.validateTags(headerTags);
         this.validateInputType();

         this.validateProduceType();

        //Validate path starts with servicename
        var sName = this.serviceName.charAt(0).toLowerCase() + this.serviceName.substr(1);
        if(this.scope.startsWith('/'))
            sName = '/' + sName;
        if((this.scope.startsWith(sName)) || (this.scope.startsWith('/hub')) || (this.scope.startsWith('/listener'))
            || (this.scope.startsWith('/home')) || (this.scope.startsWith('/monitor'))  ){
            // Okay
        }
        else {
            fns.logWarn('Operation path ' + this.scope + ' does not start with ' + sName)
        }

        this.validateParams();

        this.validateResponseCodes();
    }

}



module.exports = Operation;

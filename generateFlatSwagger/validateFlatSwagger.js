
const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
const swag = require('./getSwagger.js');
var pointer = require('json-pointer');
var $RefParser = require('json-schema-ref-parser'); // Node module which de-referecnes a json schema to one single schema

var globalSchema; 
var globalDerefCheckLen;


function getReference(reference){
	var result;
	try {
		result = pointer.get(globalSchema, reference);
	} catch (error) {
    // handle the error safely
    	//console.log(error)
    	fns.logError("Invalid reference " + reference);
	}
	return result;
}


function isObject (a) {
    return (!!a) && (a.constructor === Object);
}


function validateItems (items, parent){
	// Check that items contains a type definition
	if(items) {
		if(items.properties && isEmpty(items.properties)){
			//ignore - 
		}
		else if(!items.type){
			if(items.$ref){
				// Create a reference string to look up by ignoring the flat swagger file name
				var refString = items.$ref.substring(items.$ref.indexOf('#')+1);
				//console.log(refString);
				var lookUp = getReference(refString);
				if(lookUp && lookUp.type) {
					//console.log("OKAY");
				}
				else {
					if(items.$ref.split("/").length>globalDerefCheckLen) {
						//Ignore references to a primary object like #definitions/customer
						fns.logWarn("No type definition in items for attribute " + parent + " or its reference at " + items.$ref);
					}
				}
			}
			else {
				fns.logWarn("No type definition for attribute " + parent);
			}
		}
	}
}

function traverseJsonAllOf(propertiesSchema, name) {
	// Create a JSON file which contains the result of a traverse schema for allOf
	// 		allOf - { allOf : [ { property : value },  ...] }
	if(propertiesSchema && propertiesSchema.allOf){
		traverseJson(propertiesSchema.allOf, name);

	}
	else {
		fns.logError(name + " schema has no allOf.");
	}
}

function traverseJsonObject(propertiesSchema, name) {
	// Create a JSON file which contains the result of properties
	// 		 { properties : { { property : value },  ...}, ... }
	// Consider adding an option to prevent file creation if only one attribute? See IncidentVBO-WorkfocedAppointmentSlot
	if(propertiesSchema && propertiesSchema.properties){
		//traverseJson(propertiesSchema.properties, name);
		var traverseProperties = propertiesSchema.properties
		Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
				if(traverseProperties[key].allOf){
					// Check for a pattern of a proprty which is just an allOf as this needs a type defn.
					validateItems(traverseProperties[key], key);
				}
				traverseJson(traverseProperties[key], key);
			});
	}
	else {
		fns.logError(name + " schema has no properties.");
	}
}

function traverseJson (inputSchema, parentName){
	// Traverse the json schema for parentName - which may be undefined
	// - Patterns expected: 
	// 1) properties - { properties : { property : value }, ... }
	// 2) items - { items :  { property : value }, ... }
	// 3) allOf - { allOf : [ { property : value },  ...] }
	// 4) { property : value }
	// 5) list of { property : value } - { { property : value }, { property : value }, ...}
	if(inputSchema && (isObject(inputSchema)) && (Object.keys(inputSchema).length == 1)) {
		if(inputSchema.properties) {
			//console.log("CASE 1", parentName);
			var traverseProperties = inputSchema.properties
			Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
				if(traverseProperties[key].allOf){
					// Check for a pattern of a proprty which is just an allOf as this needs a type defn.
					validateItems(traverseProperties[key], key);
				}
				traverseJson(traverseProperties[key], key);
			});
		}
		else if(inputSchema.items) {
			//console.log("CASE 2", parentName);
			var traverseProperties = inputSchema.items
			validateItems(traverseProperties, parentName);
			Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
				traverseJson(traverseProperties[key], parentName);
			});
		}
		else if((inputSchema.allOf) && parentName) {
			//console.log("CASE 3A", inputSchema);
			//validateItems(inputSchema, parentName);
			traverseJsonAllOf(inputSchema, parentName);
		}
		else if((inputSchema.allOf) && !parentName) {
			//console.log("CASE 3B",);
			//validateItems(inputSchema, parentName);
			var traverseProperties = inputSchema.allOf // Returns an array
			traverseProperties.forEach(function(value) { // Traverse each property
				traverseJson(value, parentName);
			});
		}
		else {
			// Assumed to be { property : value } so can ignore
			//console.log("CASE 4", parentName);
		}
	}
	else if(inputSchema  && (isObject(inputSchema)) && (inputSchema.type=="object") && (['category'].indexOf(parentName) == -1)  &&
		    (inputSchema.properties) && (Object.keys(inputSchema).length > 1)) {
		// Create a reference file for this object - ignore category
		//console.log("CASE 5A", parentName, "inputschema", inputSchema);
		var traverseProperties = inputSchema
		validateItems(inputSchema, parentName);
		traverseJsonObject(inputSchema, parentName);
	}
	else if(inputSchema  && (isObject(inputSchema)) && (inputSchema.items) && (inputSchema.items.properties) && !(inputSchema.items.allOf) && 
			(['category'].indexOf(parentName) == -1)) {
		// An array without an allof so create a reference file
		//console.log("CASE 5C", parentName);
		validateItems(inputSchema.items, parentName);
		traverseJsonObject(inputSchema.items, parentName);
	}
	else if(inputSchema  && (isObject(inputSchema)) && (Object.keys(inputSchema).length > 1)) {
		// Traverse each property adding it to the result
		//console.log("CASE 5B", parentName, "inputschema", inputSchema);
		var traverseProperties = inputSchema
		var removeRedunantProperties = false;
		Object.keys(traverseProperties).forEach(function(key) { // Traverse each property
			if(traverseProperties[key].allOf){
					// Check for a pattern of a proprty which is just an allOf as this needs a type defn.
					validateItems(traverseProperties[key], parentName);
			}
			//Set parent name for subsequent calls
			var localParent = parentName;
			if(['allOf', 'items'].indexOf(key) == -1 ) {
				localParent = key;
			}
			if(key=="allOf") { // Extract allOf and add it to its own object so we can pass it with the key
				var allOfProp = {};
				allOfProp["allOf"] = traverseProperties["allOf"];
				traverseJson(allOfProp, localParent);
				// Remove the type from the current result
				removeRedunantProperties = true;
			}
			else if (key=="items") {
				 //items
				validateItems(traverseProperties[key], parentName);
				traverseJson(traverseProperties[key], localParent);
			}
			else
			{ 
				traverseJson(traverseProperties[key], localParent);
			}
		});
	}
	else {
		//fns.logError("Incorrect input schema " + inputSchema) - ignore;
		//console.log("CASE 6", parentName);
		//console.log("CASE 6", inputSchema);
	}
}

function checkDefinitionForSingleRef(defSchema, name){
	// Check the schema and report an error if it is just a $ref
	if(defSchema && (Object.keys(defSchema).length == 1) && defSchema.$ref){
		fns.logError("Definition " + name + " consists of a single $ref which will cause generated code not to execute.");
		fns.logError("Replace $ref with dereferenced entry in the flat swagger file.");
	}
}

function validateDefinitions (definitions){
	// Validate each definition
	for(var defn in definitions) {
		fns.logInfo("Checking " + defn);
		checkDefinitionForSingleRef(definitions[defn], defn)
		traverseJson(definitions[defn]);
	}
}

function validate(jsonSchema){
	// Only validate the definitions
	if(jsonSchema.definitions) {
		validateDefinitions(jsonSchema.definitions);
		fns.logInfo("Warnings may prevent generation of flat swagger code; warnings should be resolved in the swagger implementation files.")
		fns.logHeading("Flat Swagger Validation complete");
	}
	else
		fns.logError("Schema has no definitions.");
}


/**
 * Validate the flat swagger
 * @param {string} flatSwagger 
 */
function validateFlatSwagger (flatSwagger) {
	
	fns.logHeading("Flat Swagger Validation");

	var jsonSchema = swag.openJsonFile(flatSwagger);
	globalSchema = jsonSchema; // Copy of original schema

	globalDerefCheckLen = 4;
	validate(jsonSchema);
}

/**
 * Validate the flat swagger but de-reference it first
 * @param {string} flatSwagger 
 */
function validateFlatSwaggerDeRef (flatSwagger) {
	
	fns.logHeading("Flat Swagger Validation");

	//Dereference the schema - equvalient to the flat schema file
	$RefParser.dereference(flatSwagger)
	  .then(function(schema) {
	    // Success
		globalSchema = schema; // Copy of original schema
		globalDerefCheckLen = 3;
		validate(schema);
	  })
	  .catch(function(err) {
	    console.error(err);
	  });
	
}



function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


module.exports = {

  validateFlatSwagger: validateFlatSwagger,

  validateFlatSwaggerDeRef: validateFlatSwaggerDeRef
};
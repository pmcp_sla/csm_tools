const {exec} = require('child_process');
const execSync = require('child_process').execSync;
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const Ajv = require('ajv');
const utils = require("./utils.js");
var swaggerCodeGen = 'swagger-codegen-cli-2.2.3.jar';
var config = null;

//initialize local context vars
var destinationBasicFile = null;
var destinationFolder = null;
var commonFolder = null;

function setSwaggerCodeGen()
{
    try
    {
        var configPath = path.normalize(process.env.APPDATA + '/npm/node_modules/validationrest/config.json');
        config = require(configPath);
        swaggerCodeGen = config.codegenExec;
    } catch (e)
    {
        console.log(chalk.yellow("Swagger CodeGen not set in configuration, trying to use a local one!"));
    }
}

function doErrorCleanup()
{
    console.log("Cleanup function called");
}

function generateCode(codegenExecuteCmd)
{
    //generate the code
    exec(codegenExecuteCmd, (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            console.error(err);
            return;
        }

        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);

        fs.unlink(destinationBasicFile, function (err) {
            if (err)
            {
                console.log(chalk.red(err));
            }
        });

        fs.unlink(destinationFolder, function (err) {
            if (err)
            {
                console.log(chalk.red(err));
            }
        });
    });

}

function validateFile(codegenValidateCmd, codegenExecuteCmd)
{
    exec(codegenValidateCmd, (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            console.error(err);
            return;
        }

        // the *entire* stdout and stderr (buffered)
        //console.log(`stdout: ${stdout}`);
        console.log("%s", stdout);
        if (!stderr || stderr.length === 0)
        {
            console.log("Validation result: [%s]", chalk.green("Valid"));
            generateCode(codegenExecuteCmd);
        } else
        {
            console.log("Validation result: [%s]", chalk.red("Invalid"));
            console.log("Errors: \n %s", stderr);
        }
    });
}

/*
 * Load the swagger file and pass it through the swagger codegen 
 * in order to generate the project source code 
 * 
 * @param {string} swaggerFile - the swagger file
 * 
 */
function processSwagger(swaggerFile)
{
    //try to load the configuration
    setSwaggerCodeGen();

    var file = path.resolve(process.cwd(), swaggerFile);
    rootFolder = path.dirname(file);

    //if no configuration is present check if the swagger
    if (config === null)
    {
        if (!utils.checkExists(rootFolder + "/" + swaggerCodeGen))
            return;
    }

    //TODO: change the common hardcoded string below and either obtain it dynamically 
    // or get it from configuration
    commonFolder = path.resolve(rootFolder + '/../../../../VBO/Common');
    destinationFolder = path.resolve(rootFolder + '/../../../../../Common');

    console.log("Common folder: [%s]", commonFolder);
    console.log("Destination folder: [%s]", destinationFolder);

    //assuming we are in the VBS folder, we need to create a link to the upper levels to Common
    try {
        fs.symlinkSync(rootFolder + '/../../../../VBO/Common', destinationFolder, 'junction');
    } catch (err) {
        if (err.code === 'EEXIST')
        {
            console.log(chalk.yellow('The common folder already exists: [%s]'), destinationFolder);
        } else
        {
            console.log(chalk.red(err));
            console.error("Aborting..");
            doErrorCleanup();
            return;
        }
    }

    //create a symlink to the BasicComponents file
    try {
        utils.copyFile(destinationFolder + "/V1/Impl/Swagger/BasicComponents.json", rootFolder + "/BasicComponents.json");
        destinationBasicFile = rootFolder + "/BasicComponents.json";
    } catch (e) {
        console.log(chalk.red(e));
        console.error("Aborting...");
        doErrorCleanup();
        return;
    } finally {

    }

    var codegenValidateCmd;
    var codegenExecuteCmd;

    if (config !== null)
    {
        console.log(chalk.yellow('Using the configuration file for swagger codegen'));
        codegenValidateCmd = 'java -jar ' + swaggerCodeGen + ' validate -i "' + swaggerFile + '"';
        codegenExecuteCmd = 'java -jar ' + swaggerCodeGen + ' generate -i "' + swaggerFile + '" -l spring -o Output';
    } else
    {
        console.log(chalk.yellow('Using the local file for the swagger codegen'));
        codegenValidateCmd = 'java -jar "' + rootFolder + path.sep + swaggerCodeGen + '" validate -i "' + swaggerFile + '"';
        codegenExecuteCmd = 'java -jar "' + rootFolder + path.sep + swaggerCodeGen + '" generate -i "' + swaggerFile + '" -l spring -o Output';
    }

    validateFile(codegenValidateCmd, codegenExecuteCmd);
}

module.exports = {

    validate: function (swaggerFile)
    {
        console.log("Swagger file: [%s]", swaggerFile);
        processSwagger(swaggerFile);
    }
};

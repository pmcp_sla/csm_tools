#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var ajvValidator = require("./validateAJV.js");
var swaggerValidator = require("./validateSwagger.js");
var genCodeValidator = require("./generateCode.js");
var packageValidator = require("./validatePackage.js");
var configuration = require("./configuration.js");
var swaggerValidatorOperations = require("./validateSwaggerOperations.js")
var generateCSVFromFlatSwagger = require("./generateCSVFromSwagger.js");
var generateCSVFromSOAPVBO = require("./generateCSVFromSoapVBO.js");
var flatSwaggerValidate = require("./validateFlatSwagger.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('validateAJV' in cmdArgs) //check if we need to perform an AJV validation
{
    ajvValidator.validate(cmdArgs['validateAJV']);
} else if ('validateSwagger' in cmdArgs) //check if we need to perform a swagger validation
{
    swaggerValidator.validate(cmdArgs['validateSwagger']);
} else if ('generateCode' in cmdArgs)
{
    genCodeValidator.validate(cmdArgs['generateCode']);
} else if ('validateAllExamples' in cmdArgs)
{
    ajvValidator.validateAllExamples(cmdArgs['validateAllExamples']);
} else if ('validateExample' in cmdArgs)
{
    ajvValidator.validateExample(cmdArgs['validateExample']);
} else if ('validatePackage' in cmdArgs)
{
    packageValidator.validate(cmdArgs['validatePackage']);
} else if ('configureCodegen' in cmdArgs)
{
    configuration.validate(cmdArgs['configureCodegen']);
} else if ('generateFlatSwagger' in cmdArgs)
{
    swaggerValidator.bundle(cmdArgs['generateFlatSwagger']);
} else if ('validateSwaggerOperations' in cmdArgs)
{
    swaggerValidatorOperations.validate(cmdArgs['validateSwaggerOperations']);
} else if ('generateAllFlatSwaggers' in cmdArgs)
{
    swaggerValidator.bundleAll(cmdArgs['generateAllFlatSwaggers']);
}
else if ('generateFlatSwaggerCSV' in cmdArgs)
{
    generateCSVFromFlatSwagger.generateCSVFromJsonSchema(cmdArgs['generateFlatSwaggerCSV'], cmdArgs['serviceName']);
}
else if ('generateCSVFromSoapVBO' in cmdArgs)
{
    generateCSVFromSOAPVBO.generateCSVFromSOAPSchema(cmdArgs['generateCSVFromSoapVBO']);
}
else if ('flatSwaggerValidate' in cmdArgs)
{
    flatSwaggerValidate.validateFlatSwagger(cmdArgs['flatSwaggerValidate']);
}
else
{
    cmdObj.printHelp();
}

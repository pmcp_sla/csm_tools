/**
 * wsdl module.
 * @module wadlValidate
 */
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const convert = require('xml-js');
const optionsConvert = {object: false, ignoreComment: true, ignoreText: true, arrayNotation: true, trim: true};
const fns = require('./helperFunctions');


/**
 * Reads a wsdl file and returns it as a Json object
 * @param {string} filename - The name of the wadl file.
 * @return {Object} 
 */
function openWsdlFile(filename){
    var json = null;
    var file = path.resolve(process.cwd(), filename);

        try {
            json = JSON.parse(convert.xml2json(fs.readFileSync(file).toString().trim(), optionsConvert));
        } 
        catch(JSONerr) {
                fns.logError('error 102: file not found ' + filename + ' ' + JSONerr.message);
        }

    return json;
}

  



module.exports = {

  openWsdlFile: openWsdlFile
}

const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const optionDefinitions = [
  { name: 'help', alias: 'h', type: module.exports.printHelp },
  { name: 'SERVICE', alias: 's', type: String, multiple: true},
  { name: 'SOAPVersion', alias: 'S', type: String, multiple: true},
  { name: 'RESTVersion', alias: 'R', type: String, multiple: true},
  { name: 'timeout', alias: 't', type: Number }
];

module.exports = {
  parseCommandLine: function()
  {
    try {
        return commandLineArgs(optionDefinitions);
    } catch (e) {
        console.error(e.message);
    } finally {
    }
    return null;
  },

  printHelp: function()
  {
    const sections = [
      {
        header: 'Set SOAP Version',
        content: 'Sets the SOAP version'
      },
      {
        header: 'Synopsis',
        content: '$ app <options>'
      },
      {
        header: 'Command List',
        content: [
          { name: 'help', summary: '-h Display help information ' },
          { name: 'SERVICE', summary: '-s Sets the SOAP version \n'+
                                                        '   Example: generateCSV -v <file'
                                                      }

        ]
      }
    ];

    const usage = commandLineUsage(sections);
    console.log(usage);
  }

};

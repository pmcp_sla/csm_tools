#! /usr/bin/env node

var fs = require('fs');

var cmdObj = require("./parseCommandLine.js");
var cmdArgs = cmdObj.parseCommandLine();
var validate = require("./version.js");

//generic print console help
if ('help' in cmdArgs)
    cmdObj.printHelp();
else if ('SOAPVersion' in cmdArgs)
{
    validate.setVersion(cmdArgs['SERVICE'],cmdArgs['SOAPVersion'],cmdArgs['RESTVersion']);
}
else
{
    cmdObj.printHelp();
}


const chalk = require('chalk');
//Local files
const utils = require('./utils.js');
const path = require('path');
const fns = require('./helperFunctions');
const fs = require('fs');
const util = require('util');
const wsdl = require('./getWsdl.js');



function getServiceName (schemaFileName){    
	//Get the resource Collection Name - assumed to be the filename before the extension
	var serviceName = path.basename(schemaFileName);
	serviceName = serviceName.replace( /\.[^/\\.]+$/, "");
	if(serviceName.toLowerCase().endsWith('vbs')){
		serviceName = serviceName.slice(0, -3);
	}
	return serviceName
}



function getWsdlOp(operation) {
	// Get info for a single operation
	//console.log(operation);
	var result;
	if(operation.attributes.name) {
		//fns.logInfo(operation.attributes.name);
		result = operation.attributes.name;
	}
	return result;
}

function getWsdlOperationInfo (wsdlSchema) {
	// Get the operation name and param names for each operation
	var result = new Map();
	if(wsdlSchema && wsdlSchema.elements && wsdlSchema.elements[0].elements) {
		// Traverse elements looking for operation
		wsdlSchema.elements[0].elements.forEach(function (el) {
			if(el.name == "wsdl:binding") {
				// Look within the binding for the operations
				if(el.elements) {
					//Travse for operations
					el.elements.forEach(function (op) {
						if(op.name == "wsdl:operation") {
							result.set(getWsdlOp(op), []);
						}
					});
				}
			}
		});
	} 
	else {
		fns.logWarn("No WSDL operations found");
	}
	return result;
}

function getFileContents (filename, type) {
	// read a file 
	var result;
	if(fs.existsSync(filename)) {
		switch (type){
			case "wadl" :
				result = wadl.openWadlFile(filename);
				break;
			case "wsdl" :
				result = wsdl.openWsdlFile(filename);
				break;
			default: 
		}
	}
	return result; 
}

function display(iterator, type) {
	let list = Array.from( iterator.keys() );
	fns.logHeading(type + " Operations");
	list.forEach(function(op) {
		fns.logInfo(op);
	});
}


/**
 * Valiate that the operations files are consistent - wsdl, wadl and swagger - wsdl and wadl may not always exist 
 * @param {string} service 
 * @param {number} version 
 */
function setVersion (service, SOAPVersion, RESTVersion) {
	
	fns.logHeading("Service " + service);
	fns.logHeading("SET SOAP Version to " + SOAPVersion);

	# Update the VBM file

	# Update the wsdl file
	var wsdlFilename = filenames[2];
	var wsdlContents = getFileContents(wsdlFilename, "wsdl");

	fns.logHeading("SET REST Version to " + RESTVersion);


}



function isEmpty(obj) {
	// Empty object
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


module.exports = {

  setVersion: setVersion
};

